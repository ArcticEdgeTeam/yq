@php
    $currency = DB::table('settings')->where('variable', 'currency')->first();
    if($currency){
        $currency = $currency->value;
    }else{
        $currency = '';
    }
    
@endphp
<html lang="en">
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <main role="main">
        <div class="row">
          
            <div class="col-12" style="margin-top: 25px; padding-top: 10px; text-align: center">
                <h2>Cashup Report</h2>
                <p>Venue: {{ $venue->name}}</p>
                <p>Date: @if ($start != '' && $end != '') From {{$start}} To {{ $end}} @else All Time Cashups Report @endif</p>
                
            </div>
        </div>
        <br>
            <div class="table-responsive row">
            <table class="table table-bordered table-hover" id="datatable">
                <thead class="thead-default thead-lg">
                    <tr>
                        <th>#</th>
                        <th>Invoice</th>
                        <th>Customer</th>
                        <th>Waiter</th>
                        <th>Date</th>
                        <th>Paid</th>
                        <th>Tip</th>
                        <th>Admin Comm</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>

                @if ($transactions->isEmpty())
                 <tr>
                    <td colspan="9"><p style="text-align: center"> No transactions found!</p></td>  
                </tr>
                @else
                @foreach($transactions as $transaction)
                    @php
                        $order = DB::table('orders')->where('id', $transaction->order_id)->first();
                        $customer = DB::table('users')->where('id', $transaction->customer_id)->first();
                        $waiter = DB::table('users')->where('id', $transaction->waiter_id)->first();
                        if($customer->photo == ''){
                            $customer->photo = 'default.png';
                        }
                        if($waiter->photo == ''){
                            $waiter->photo = 'default.png';
                        }
                        
                        if($transaction->transaction_status != 'Refunded'){
                            $total += $transaction->amount + $transaction->waiter_tip;
                        }
                    @endphp
                    <tr>
                        <td>{{ $counter++ }}</td>
                        <td>#{{ $order->invoice_number }}</td>
                        <td><img class="img-circle" width="40" src="{{ asset('public/uploads/users/' . $customer->photo) }}"> {{ $customer->name }}</td>
                        <td><img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="40" height="40"> {{ $waiter->name }}</td>
                        <td>{{ $transaction->created_at->format('d M Y') }}</td>
                        <td style='position: relative;'>{{ $currency }} {{ number_format($transaction->amount, 2) }} @if($transaction->transaction_status == 'Refunded') <span class="badge badge-danger" style='font-size: 8px; position: absolute; top: 2.5px; left: 10px;'>Refunded</span> @endif</td>
                        <td>{{ $currency }} {{ number_format($transaction->waiter_tip, 2) }}</td>
                        <td>{{ $currency }} {{ number_format($transaction->admin_commission, 2) }}</td>
                        <td>{{ $currency }} {{ number_format($transaction->amount +  $transaction->waiter_tip, 2) }}</td>
                    </tr>
                @endforeach
                @endif
                    
                </tbody>
                
                <tfoot>
                    <tr>
                        <th colspan='9'>
                            <div class='text-right'>
                                <b style='margin-right: 50px;'>Total: {{ $currency }} {{ number_format($total, 2) }}</b>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>
            
        </div>
        
    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>