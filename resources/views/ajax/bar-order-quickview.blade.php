@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
<div class='ibox p-4'>
	<div class="ibox-head">
		<div class="ibox-title">Orders Details<br /><a class='text-danger' href="{{ route('bar.order.edit', $order->id) }}" target='_blank'>#{{ $order->order_number }}</a></div>
		
		<div class="ibox-tools">
			<button class='btn btn-danger btn-sm'>Hail Waiter</button>
		</div>
	</div>
	
	<div class="ibox-body">
		<div class='row'>
			<div class='col-md-6 mb-3'>
				<b>Time:</b> {{ $order->created_at->format('H:i') }}
			</div>
			
			<div class='col-md-6 mb-3'>
				<b>Total Amount:</b> {{ $currency }} {{ number_format($order->total_amount, 2) }}
			</div>
			
			<div class='col-md-6 mb-3'>
				<b>Table:</b> {{ $table->table_id }}
			</div>
			
			<div class='col-md-6 mb-3'>
				<b>Waiter:</b> <a href="#" class='text-danger'>{{ $waiter->name }}</a>
			</div>
			
			<div class='col-md-12 mb-3'>
				<b>Customer:</b> <a href="#" class='text-danger'>{{ $customer->name }}</a>
			</div>
			
			
			<table class='table table-bordered'>
				<thead class='thead-default'>
					<th>#</th>
					<th>Product</th>
					<th>Status</th>
				</thead>
				<tbody>
				@foreach($ordered_products as $ordered_product)
				@php
					
					$product = DB::table('products')->where('id', $ordered_product->product_id)->first();
					if($product->image == ''){
						$product->image = 'default.png';
					}
					$total_amount += $ordered_product->price * $ordered_product->quantity;
					
					$variants = DB::table('ordered_product_variables')->where('ordered_product_id', $ordered_product->id)->where('type', 'variant')->get();
					
					if($variants){
						$total_amount = $total_amount - $ordered_product->price * $ordered_product->quantity;
					}
					
					$extras = DB::table('ordered_product_variables')->where('ordered_product_id', $ordered_product->id)->where('type', 'extra')->get();
				@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td class='products_list'>
							@if($variants->isEmpty())
							<img class="img-circle" src="{{ asset('public/uploads/products/' . $product->image) }}" width="50" height="50"> {{ $product->name }}
								
							<div class='table_box'>
								<table class='table table-bordered'>
									<thead>
										<th>Quantity</th>
										<th>Amount</th>
									</thead>
									<tbody>
									<tr>
										<td>{{ number_format($ordered_product->quantity) }}</td>
										<td>{{ $currency }} {{ number_format($ordered_product->price * $ordered_product->quantity, 2) }}</td>
									</tr>
									</tbody>
								</table>
							</div>
						
							@endif
							
							@if(!$variants->isEmpty())
							<img class="img-circle" src="{{ asset('public/uploads/products/' . $product->image) }}" width="50" height="50"> {{ $product->name }}
								<div class='table_box'>
									<table class='table table-bordered'>
										<thead>
											<th width='160'><span data-toggle='tooltip' title='Variant' style='display: ilnline-block; width: 20px; height: 20px; border-radius: 50%;' class="badge badge-danger">V</span></th>
											<th>Quantity</th>
											<th>Amount</th>
										</thead>
										<tbody>
										@foreach($variants as $variant)
										@php
										$total_amount += $variant->price * $variant->quantity;
										@endphp
										<tr>
											<td>{{ $variant->name }}</td>
											<td>{{ number_format($variant->quantity) }}</td>
											<td>{{ $currency }} {{ number_format($variant->price * $variant->quantity, 2) }}</td>
										</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							@endif
							@if(!$extras->isEmpty())
							<div class='table_box'>
								<table class='table table-bordered'>
									<thead>
										<th width='160'><span data-toggle='tooltip' title='Extra' style='display: ilnline-block; width: 20px; height: 20px; border-radius: 50%;' class="badge badge-danger">E</span></th>
										<th>Quantity</th>
										<th>Amount</th>
									</thead>
									<tbody>
									@foreach($extras as $extra)
									@php
									$total_amount += $extra->price * $extra->quantity;
									@endphp
									<tr>
										<td>{{ $extra->name }}</td>
										<td>{{ number_format($extra->quantity) }}</td>
										<td>{{ $currency }} {{ number_format($extra->price * $extra->quantity, 2) }}</td>
									</tr>
									@endforeach
									</tbody>
								</table>
							</div>
							@endif
						</td>
						
						<td>
							@if($ordered_product->order_status == 0)
							<span class="btn btn-danger btn-sm text-white sweet-6">
								<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Ordered</span> <i class="ti-menu"></i>
							</span>
							@elseif($ordered_product->order_status == 1)
							<span class="btn btn-warning btn-sm text-white sweet-6">
								<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>In Oven</span> <i class="ti-menu"></i>
							</span>
							@elseif($ordered_product->order_status == 2)
							<span class="btn btn-info btn-sm text-white sweet-6">
								<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Final Steps</span> <i class="ti-menu"></i>
							</span>
							@else
							<span class="btn btn-success btn-sm text-white sweet-6">
								<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Completed</span> <i class="ti-menu"></i>
							</span>
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			
			
		</div>
	</div>
</div>

<script>
	$(function(){
		$('.sweet-6').click(function(){
			
			var ordered_product_id = $(this).find('.order_status_span').attr('data-order-id');
			var order_status = $(this).find('.order_status_span').attr('data-order-status');
			var status_from_to;
			var _this = $(this);
			
			if(order_status == '0'){
				status_from_to = 'ordered to in oven';
			}else if(order_status == '1'){
				status_from_to = 'in oven to final steps';
			}else if(order_status == '2'){
				status_from_to = 'final steps to completed';
			}else{
				status_from_to = 'completed to completed';
			}
			
			swal({
				title: "Are you sure?",
				text: "Change status from " + status_from_to + '.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn-warning',
				confirmButtonText: 'Yes',
				closeOnConfirm: false,
			},function(){
				
				if(order_status == '0'){
					$(_this).find('.order_status_span').text('In Oven');
					$(_this).removeClass('btn-danger');
					$(_this).addClass('btn-warning');
					$(_this).find('.order_status_span').attr('data-order-status', '1');
					
				}else if(order_status == '1'){
					$(_this).find('.order_status_span').text('Final Steps');
					$(_this).removeClass('btn-warning');
					$(_this).addClass('btn-info');
					$(_this).find('.order_status_span').attr('data-order-status', '2');
					
				}else if(order_status == '2'){
					$(_this).find('.order_status_span').text('Completed');
					$(_this).removeClass('btn-info');
					$(_this).addClass('btn-success');
					$(_this).find('.order_status_span').attr('data-order-status', '3');
				}
				
				var _status = $(_this).find('.order_status_span').attr('data-order-status');
				swal("Status Changed!", "", "success");
				
				$.ajax({
				   type:'GET',
				   url:"{{ route('bar.order.status.update') }}",
				   data: 'id=' + ordered_product_id + '&status=' + _status,
				   success:function(response){
					 //console.log(response);
				   }
				});
			});
		});
	});
	
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});
	
</script>