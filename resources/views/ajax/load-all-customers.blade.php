<div class="form-group mb-3 multiple_select">
<select required name='users[]' class="multi-select" id="multi_select" multiple="multiple">
	@foreach($customers as $customer)
		<option value="{{ $customer->id }}"> {{ $customer->name }} </option>
	@endforeach
</select>
</div>

<div class="form-group" >
<label id='select_all' class='btn btn-danger btn-sm'>Select All</label>
<label id='deselect_all' class='btn btn-danger btn-sm'>Deselect All</label>
</div>

<script>
	$('#multi_select').multiSelect();
	
	$('#select_all').on('click', function(){
		$('#multi_select').multiSelect('select_all');
	});
	
	$('#deselect_all').on('click', function(){
		$('#multi_select').multiSelect('deselect_all');
	});
</script>