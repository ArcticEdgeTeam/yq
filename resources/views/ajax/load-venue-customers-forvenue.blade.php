<div id='venue_elemets2'>
	<div class="form-group mb-3 multiple_select">
	<select required name='users[]' class="multi-select" id="multi_select" multiple="multiple">
		@foreach($customers as $customer)
			<option value="{{ $customer->id }}"> {{ $customer->name }} </option>
		@endforeach
	</select>
	</div>
	
	 <div class="form-group" >
		<label id='select_all' class='btn btn-danger btn-sm'>Select All</label>
		<label id='deselect_all' class='btn btn-danger btn-sm'>Deselect All</label>
	 </div>
</div>

<script>
	$('#multi_select').multiSelect();
	
	$('#select_all').on('click', function(){
		$('#multi_select').multiSelect('select_all');
	});
	
	$('#deselect_all').on('click', function(){
		$('#multi_select').multiSelect('deselect_all');
	});
	
	function loadVenueCustomersForVenue(){
		$('#venue_modules').html("<img src='{{ asset('public/uploads/loader.gif') }}'>");
		$.ajax({
		   type:'GET',
		   url:'{{ route('ajax.loadvenuecustomersforvenue') }}',
		   success:function(data){
			$('#venue_modules').html(data);
		   }
		});
	}
	
	function loadVenueWaitersForVenue(){
		$('#venue_modules').html("<img src='{{ asset('public/uploads/loader.gif') }}'>");
		$.ajax({
		   type:'GET',
		   url:'{{ route('ajax.loadvenuewaitersforvenue') }}',
		   success:function(data){
			$('#venue_modules').html(data);
		   }
		});
	}
</script>