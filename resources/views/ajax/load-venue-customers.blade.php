<div class="form-group mb-3" id='venue_users_dropdown'>								
<label onclick="loadVenueCustomers({{ $venue_id }})" class="radio radio-inline radio-danger">
	<input type="radio" value='Customers' name="sending_to" checked>
	<span class="input-span"></span>Customers</label>
<label onclick="loadVenueWaiters({{ $venue_id }})" class="radio radio-inline radio-danger">
	<input type="radio" value='Waiters' name="sending_to">
	<span class="input-span"></span>Waiters</label>
</div>

<div id='venue_elemets2'>
	<div class="form-group mb-3 multiple_select">
	<select required name='users[]' class="multi-select" id="multi_select" multiple="multiple">
		@foreach($customers as $customer)
			<option value="{{ $customer->id }}"> {{ $customer->name }} </option>
		@endforeach
	</select>
	</div>
	
	 <div class="form-group" >
		<label id='select_all' class='btn btn-danger btn-sm'>Select All</label>
		<label id='deselect_all' class='btn btn-danger btn-sm'>Deselect All</label>
	 </div>
</div>


<script>
	$('#multi_select').multiSelect();
	
	$('#select_all').on('click', function(){
		$('#multi_select').multiSelect('select_all');
	});
	
	$('#deselect_all').on('click', function(){
		$('#multi_select').multiSelect('deselect_all');
	});
	
	$('#venue_users_dropdown').show();
	$('#venue_elemets2').show();
</script>