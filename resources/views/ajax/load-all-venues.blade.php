<select onchange='loadVenueCustomers(this.value);' name='venue_id' class='selectpicker form-control'>
	<option value=''>Select Venue</option>
	@foreach($venues as $venue)
		<option value="{{ $venue->id }}"> {{ $venue->name }} </option>
	@endforeach
</select>

<script>
	$('.selectpicker').selectpicker('refresh');
</script>