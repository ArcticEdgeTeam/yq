@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.venue')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	
	@if($variants->isEmpty())
		#participantTable{
		display: none;
		font-size: 12px;
	}
	@endif
	
	@if($extras->isEmpty())
		#participantTable2{
		display: none;
		font-size: 12px;
	}
	@endif
	
	#participantTable2 input[type="text"], #participantTable2 input[type="number"],#participantTable input[type="text"], #participantTable input[type="number"]{
		padding: 6px 8px;
	}
	
</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Products</li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>



<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
		<form method='post' action="{{ route('venue.variant.update', $product->id) }}">
		@csrf
		<input type='hidden' name='id' id='variant_id'>
		<!-- Modal Header -->
		<div class="modal-header">
		  <h5 class="modal-title text-muted">Variant  <i style='margin: 0 4px; font-size: 4px; position: relative; bottom: 3px;' class="fa fa-circle" aria-hidden="true"></i> <small>Edit</small></h5>
			<div>
				<span style='font-size: 11px;'><b>Active</b></span><br />
			<label class="ui-switch switch-icon switch-solid-danger switch-large">
				<input name='status' type="checkbox" id='variant_status'>
				<span></span>
			</label>
			</div>
		</div>
	
      <div class="modal-body">
			<div class='row'>
				<div class="form-group mb-4 col-md-12 mt-3">
						<label>Variant Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
							<input id='variant_name' name='name' required type='text' class='form-control'>
						</div>
					</div>
					
				<div class="form-group mb-4 col-md-12">
					<label>Variant Price</label>
					<div class="input-group-icon input-group-icon-left">
						<span class="input-icon input-icon-left">{{ $currency }}</span>
						<input id='variant_price' type="number" step="any" min="0" name='price' required  class='form-control'>
					</div>
				</div>
				
				 <div class="col-sm-12 form-group text-center">
					<button class="btn btn-danger btn-fix btn-animated from-left">
					<span class="visible-content">Update Variant</span>
					<span class="hidden-content">
						<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
					</span>
					</button>
					<a href='javascript:;' class="btn btn-light" data-dismiss="modal">Close</a>
				</div>
			</div>
      </div>
	  </form>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">
		<form method='post' action="{{ route('venue.extra.update', $product->id) }}">
		@csrf
		<input type='hidden' name='id' id='extra_id'>
		<!-- Modal Header -->
		<div class="modal-header">
		  <h5 class="modal-title text-muted">Extra  <i style='margin: 0 4px; font-size: 4px; position: relative; bottom: 3px;' class="fa fa-circle" aria-hidden="true"></i> <small>Edit</small></h5>
			<div>
				<span style='font-size: 11px;'><b>Active</b></span><br />
			<label class="ui-switch switch-icon switch-solid-danger switch-large">
				<input name='status' type="checkbox" id='extra_status'>
				<span></span>
			</label>
			</div>
		</div>
	
      <div class="modal-body">
			<div class='row'>
				<div class="form-group mb-4 col-md-12 mt-3">
						<label>Extra Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
							<input id='extra_name' name='name' required type='text' class='form-control'>
						</div>
					</div>
					
				<div class="form-group mb-4 col-md-12">
					<label>Extra Price</label>
					<div class="input-group-icon input-group-icon-left">
						<span class="input-icon input-icon-left">{{ $currency }}</span>
						<input id='extra_price' type="number" step="any" min="0" name='price' required  class='form-control'>
					</div>
				</div>
				
				 <div class="col-sm-12 form-group text-center">
					<button class="btn btn-danger btn-fix btn-animated from-left">
					<span class="visible-content">Update Extra</span>
					<span class="hidden-content">
						<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
					</span>
					</button>
					<a href='javascript:;' class="btn btn-light" data-dismiss="modal">Close</a>
				</div>
			</div>
      </div>
	  </form>
    </div>
  </div>
</div>

<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<form class='form-danger' method='post' action="{{ route('venue.product.update', $product->id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-body" style='position: relative;'>
					<div class='text-center'>
					<img id='previewfile' src="{{ asset('public/uploads/products/' . $product->image) }}">
					</div>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Change Image' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='image' class='d-none'>
					</div>
					
					<div class="form-group mb-4 col-md-12 mt-3">
						<label>Product Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
							<input value="{{ $product->name }}" name='name' required type='text' class='form-control'>
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Product Price</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left">{{ $currency }}</span>
							<input value="{{ $product->price }}" type="number" step="any" min="0" name='price' required  class='form-control'>
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<div class="mb-2">
							<label for='allergen' class="checkbox checkbox-inline">
								<input @if($product->allergen == 1) checked @endif name='allergen' id='allergen' type="checkbox">
								<span class="input-span"></span>Allergen
							</label>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="col-xl-5 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Product Details</div>
					<div class="ibox-tools">
						<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($product->status == 1) checked @endif >
						<span></span>
					</label>
						
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
					
						<div class="form-group mb-4 col-md-6">
							<label>Product ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input value="{{ $product->product_id }}" name='product_id' required type='text' class='form-control' onblur="getProductID($(this))">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Assign to preparation area</label>
							<select name='bar_id' class="selectpicker form-control">
								@foreach($bars as $bar)
									@php
										$selected = '';
										if($bar->id == $product->bar_id){
											$selected = "selected";
										}
									@endphp
									<option {{ $selected }} value="{{ $bar->id }}">{{ $bar->name }}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Video Link</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-control-play"></i></span>
								<input value="{{ $product->video }}" name='video' type='text' class='form-control'>
							</div>
						</div>
						
						
						<div class="form-group mb-4 col-md-12">
							<label>Categories</label>
							<select name='category' class="form-control select2_demo_1" required>
								@foreach($categories as $category)
									<option @if ($category->id == $product->category_id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
								
							</select>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Menus</label>
							<select name='menus[]' class="form-control select2_demo_1" multiple="">
								@foreach($menus as $menu)
									
									@php
										$selected = '';
										if(DB::table('menu_products')->where('product_id', $product->id)->where('menu_id', $menu->id)->first()){
											$selected = 'selected';
										}
									@endphp
								
									<option {{ $selected }} value="{{ $menu->id }}">{{ $menu->name }}</option>
								@endforeach
								
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Description</label>
							<textarea name='description' class='form-control' required>{{ $product->description }}</textarea>
						</div>
						 
						 <div class='col-md-12 mb-3'>
						@if($product_ingredients)
							<table style='font-size: 12px;' class='table table-bordered' width='100%'>
								<thead>
									<tr>
										<th>Ingredient</th>
										<th>Unit</th>
										<th>Quantity</th>
										<th>Action</th>
									</tr>
								</thead>
								@foreach($product_ingredients as $product_ingredient)
								@php
									$ingredient = DB::table('ingredients')->where('id', $product_ingredient->ingredient_id)->first();
									$unit = DB::table('units')->where('id', $product_ingredient->unit_id)->first();
								@endphp
								
								<tr>
									<td>{{ $ingredient->name }}</td>
									<td>{{ $unit->name }}</td>
									<td>{{ $product_ingredient->quantity }}</td>
									<td><a onclick="return confirm('Are You sure want to delete this record?')" href="{{ route('venue.productingredient.delete', $product_ingredient->id) }}"><i class='fa fa-trash text-danger'></i></a></td>
								</tr>
								@endforeach
							</table>
							
						@endif
						
						<table id="participantTable3" style='width: 100%; border: 0;'>
							<thead class='d-none'>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</thead>
							<tr class="participantRow3">
								<td width='50%'>
									<select class='form-control' name='ingredients[]'>
										<option value=''>Select Ingredient</option>
										@foreach($ingredients as $ingredient)
										<option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
										@endforeach
									</select>
								</td>
								<td width='30%'>
									<select class='form-control' name='units[]'>
										<option value=''>Select Unit</option>
										@foreach($units as $unit)
										<option value="{{ $unit->id }}">{{ $unit->name }}</option>
										@endforeach
									</select>
								</td>
								<td width='20%'>
									<input onkeypress="return isNumber(event)" placeholder='Quantity' style='padding: 9px 6px;' class='form-control' type='text' name='quantities[]'>
								</td>
								<td align='right'><button class="btn btn-danger btn-sm remove3" type="button">-</button></td>
							</tr>
							<tr id="addButtonRow3">
								<td colspan='3'></td>
								<td align='right'><button class="btn btn-sm btn-success add3" type="button">+</button></td>
							</tr>
						</table>
						</div>
						
				   </div>
				    
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Addons</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-plus"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					
					@if(!$variants->isEmpty())
					<table style='font-size: 12px' class='table table-bordered'>
						<thead>
							<tr>
								<th>Variant Name</th>
								<th>Variant Price</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($variants as $variant)
								<tr>
									<td>
									@if($variant->status == 1) <i title='Active' data-toggle='tooltip' class='fa fa-check text-success'></i> @else <i title='Inactive' data-toggle='tooltip' class='fa fa-times text-danger'></i> @endif
									{{ $variant->name }}
									</td>
									<td>{{ $currency }}  {{ $variant->price }}</td>
									<td>
									<a data-toggle="modal" data-target="#myModal" data-variant-id="{{ $variant->id }}" data-variant-name="{{ $variant->name }}" data-variant-price="{{ $variant->price }}" data-variant-status="{{ $variant->status }}" onclick="editVariant(this)" href="javascript:;"><i class='fa fa-edit text-warning'></i></a>
										{{-- <a onclick="return confirm('Are You sure want to delete this record?')" href="{{ route('venue.productvariant.delete', $variant->id) }}"><i class='fa fa-trash text-danger'></i></a> --}}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@endif
					
					@if(!$extras->isEmpty())
					<table style='font-size: 12px' class='table table-bordered'>
						<thead>
							<tr>
								<th>Extra Name</th>
								<th>Extra Price</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($extras as $extra)
								<tr>
									<td>
									@if($extra->status == 1) <i title='Active' data-toggle='tooltip' class='fa fa-check text-success'></i> @else <i title='Inactive' data-toggle='tooltip' class='fa fa-times text-danger'></i> @endif
									{{ $extra->name }}
									</td>
									<td>{{ $currency }} {{ $extra->price }}</td>
									<td>
									<a data-toggle="modal" data-target="#myModal2" data-extra-id="{{ $extra->id }}" data-extra-name="{{ $extra->name }}" data-extra-price="{{ $extra->price }}" data-extra-status="{{ $extra->status }}" onclick="editExtra(this)" href="javascript:;"><i class='fa fa-edit text-warning'></i></a>
										{{-- <a onclick="return confirm('Are You sure want to delete this record?')" href="{{ route('venue.productextra.delete',  $extra->id) }}"><i class='fa fa-trash text-danger'></i></a> --}}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@endif
					
				
				   <div class='row'>
						
						<div class="col-sm-12 form-group mb-2">
							<label>Type</label>
							<div class="form-group">
								<div class="mb-2">
									<label class="radio radio-inline">
										<input value='simple' id='type_simple_radio' type="radio" name="type" checked>
										<span class="input-span"></span>Simple</label>
										
									<label class="radio radio-inline">
										<input value='variant' id='type_variable_radio' type="radio" name="type">
										<span class="input-span"></span>Variable</label>
								</div>
							</div>
							
							<table id="participantTable">
									<thead>
										<tr>
											<th>Variable Name</th>
											<th>Price</th>
											<th></th>
										</tr>
									</thead>
									<tr class="participantRow">
										<td><input name="variable_name[]" type="text" placeholder="Variable name.." class="required-entry">
										  </td>
										  <td><input style='width: 70px;' name="variable_price[]" type="number" step="any" min="0" placeholder="Price" class="required-entry">
										  </td>
										<td><button class="btn btn-danger btn-sm remove" type="button">-</button></td>
									</tr>
									<tr id="addButtonRow">
										<td colspan='2'></td>
										<td align='center'><button class="btn btn-sm btn-success add" type="button">+</button></td>
									</tr>
							</table>
							
						</div>
						
						
						<div class="col-sm-12 form-group mb-4 mt-2">
							<div class="form-group">
								<div class="mb-2">
									<label for='add_extra_checkbox' class="checkbox checkbox-inline">
										<input name='extra' id='add_extra_checkbox' type="checkbox">
										<span class="input-span"></span>Add Extras</label>
								</div>
								
							</div>
							
							<table id="participantTable2">
									<thead>
										<tr>
											<th>Extra Name</th>
											<th>Price</th>
											<th></th>
										</tr>
									</thead>
									<tr class="participantRow2">
										<td><input name="extra_name[]" type="text" placeholder="Extra name.." class="required-entry">
										  </td>
										  <td><input style='width: 70px;' name="extra_price[]" type="number" step="any" min="0" placeholder="Price" class="required-entry">
										  </td>
										<td><button class="btn btn-danger btn-sm remove2" type="button">-</button></td>
									</tr>
									<tr id="addButtonRow2">
										<td colspan='2'></td>
										<td align='center'><button class="btn btn-sm btn-success add2" type="button">+</button></td>
									</tr>
								</table>
							
						</div>
						
						<div class="col-sm-12 form-group mb-4">
							<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Product</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
						</div>
							
						
				   </div>
				    
				</div>
			</div>
		</div>
		
	</div>
	</form>
	
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	
	$(document).ready(function(){
		
		$(".select2_demo_1").select2();
		
		/* Variables */
		var p = $("#participants").val();
		var row = $(".participantRow");

		/* Functions */
		function getP(){
		  p = $("#participants").val();
		}

		function addRow() {
		  row.clone(true, true).appendTo("#participantTable");
		}

		function removeRow(button) {
		  button.closest("tr").remove();
		}
		/* Doc ready */
		$(".add").on('click', function () {
		  getP();
		  if($("#participantTable tr").length < 17) {
			addRow();
			var i = Number(p)+1;
			$("#participants").val(i);
		  }
		  $(this).closest("tr").appendTo("#participantTable");
		  if ($("#participantTable tr").length === 3) {
			$(".remove").hide();
		  } else {
			$(".remove").show();
		  }
		});
		$(".remove").on('click', function () {
		  getP();
		  if($("#participantTable tr").length === 3) {
			//alert("Can't remove row.");
			$(".remove").hide();
		  } else if($("#participantTable tr").length - 1 ==3) {
			$(".remove").hide();
			removeRow($(this));
			var i = Number(p)-1;
			$("#participants").val(i);
		  } else {
			removeRow($(this));
			var i = Number(p)-1;
			$("#participants").val(i);
		  }
		});
		$("#participants").change(function () {
		  var i = 0;
		  p = $("#participants").val();
		  var rowCount = $("#participantTable tr").length - 2;
		  if(p > rowCount) {
			for(i=rowCount; i<p; i+=1){
			  addRow();
			}
			$("#participantTable #addButtonRow").appendTo("#participantTable");
		  } else if(p < rowCount) {
		  }
		});
		
		
		
		
		
		/* Variables */
		var p2 = $("#participants2").val();
		var row2 = $(".participantRow2");

		/* Functions */
		function getP2(){
		  p2 = $("#participants2").val();
		}

		function addRow2() {
		  row2.clone(true, true).appendTo("#participantTable2");
		}

		function removeRow2(button2) {
		  button2.closest("tr").remove();
		}
		/* Doc ready */
		$(".add2").on('click', function () {
		  getP2();
		  if($("#participantTable2 tr").length < 17) {
			addRow2();
			var i2 = Number(p2)+1;
			$("#participants2").val(i2);
		  }
		  $(this).closest("tr").appendTo("#participantTable2");
		  if ($("#participantTable2 tr").length === 3) {
			$(".remove2").hide();
		  } else {
			$(".remove2").show();
		  }
		});
		$(".remove2").on('click', function () {
		  getP2();
		  if($("#participantTable2 tr").length === 3) {
			//alert("Can't remove row.");
			$(".remove2").hide();
		  } else if($("#participantTable2 tr").length - 1 ==3) {
			$(".remove2").hide();
			removeRow2($(this));
			var i2 = Number(p2)-1;
			$("#participants2").val(i2);
		  } else {
			removeRow2($(this));
			var i2 = Number(p2)-1;
			$("#participants2").val(i2);
		  }
		});
		$("#participants2").change(function () {
		  var i2 = 0;
		  p2 = $("#participants2").val();
		  var rowCount2 = $("#participantTable2 tr").length - 2;
		  if(p2 > rowCount2) {
			for(i2=rowCount2; i2<p2; i2+=1){
			  addRow2();
			}
			$("#participantTable2 #addButtonRow2").appendTo("#participantTable2");
		  } else if(p2 < rowCount2) {
		  }
		});
		
		
		
		
		/* Variables */
		var p3 = $("#participants3").val();
		var row3 = $(".participantRow3");
		

		/* Functions */
		function getP3(){
		  p3 = $("#participants3").val();
		}

		function addRow3() {
		  var _row3 = row3.clone(true, true).appendTo("#participantTable3");
		$(_row3).find('input').val('');
		}

		function removeRow3(button3) {
		  button3.closest("tr").remove();
		}
		/* Doc ready */
		$(".add3").on('click', function () {
		  getP3();
		  if($("#participantTable3 tr").length < 17) {
			addRow3();
			var i3 = Number(p3)+1;
			$("#participants3").val(i3);
		  }
		  $(this).closest("tr").appendTo("#participantTable3");
		  if ($("#participantTable3 tr").length === 3) {
			$(".remove3").hide();
		  } else {
			$(".remove3").show();
		  }
		});
		$(".remove3").on('click', function () {
		  getP3();
		  if($("#participantTable3 tr").length === 3) {
			//alert("Can't remove row.");
			$(".remove3").hide();
		  } else if($("#participantTable3 tr").length - 1 ==3) {
			$(".remove3").hide();
			removeRow3($(this));
			var i3 = Number(p3)-1;
			$("#participants3").val(i3);
		  } else {
			removeRow3($(this));
			var i3 = Number(p3)-1;
			$("#participants3").val(i3);
		  }
		});
		$("#participants3").change(function () {
		  var i3 = 0;
		  p3 = $("#participants3").val();
		  var rowCount3 = $("#participantTable3 tr").length - 2;
		  if(p3 > rowCount3) {
			for(i3=rowCount3; i3<p3; i3+=1){
			  addRow3();
			}
			$("#participantTable3 #addButtonRow3").appendTo("#participantTable3");
		  } else if(p3 < rowCount3) {
		  }
		});
		
		
		
		
		$('#add_extra_checkbox').click(function() {
			$("#participantTable2").toggle(this.checked);
		});
		
		@if(!$extras->isEmpty())
			$("#add_extra_checkbox").prop("checked", true);
		@endif
		
		@if(!$variants->isEmpty())
			$("#type_variable_radio").prop("checked", true);
		@endif
		
		
		$('#type_variable_radio').click(function() {
			$("#participantTable").show();
		});
		
		
		$('#type_simple_radio').click(function() {
			$("#participantTable").hide();
		});
		
		
		
	});
	
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
	
	function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 45 || charCode > 57)) {
        return false;
    }
  else if(charCode == 46){
    return true;
  }
  else if(charCode == 47){
    return false;
  }
  else {
     return true;
  }}
  
  function editVariant(_this){
		$('#variant_id').val($(_this).attr('data-variant-id'));
		$('#variant_name').val($(_this).attr('data-variant-name'));
		$('#variant_price').val($(_this).attr('data-variant-price'));
		_status = $(_this).attr('data-variant-status');
		if(_status == '1'){
			$('#variant_status').prop('checked', true);
		}else{
			$('#variant_status').prop('checked', false);
		}
	  }
	  
	  function editExtra(_this){
		$('#extra_id').val($(_this).attr('data-extra-id'));
		$('#extra_name').val($(_this).attr('data-extra-name'));
		$('#extra_price').val($(_this).attr('data-extra-price'));
		_status = $(_this).attr('data-extra-status');
		if(_status == '1'){
			$('#extra_status').prop('checked', true);
		}else{
			$('#extra_status').prop('checked', false);
		}
	  }

	  $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});

	function getProductID(obj) {

	  	var product_id = obj.val();
	  	if (product_id != '') {
	  		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					id: '{{$product->id}}',
					product_id: product_id,
					type: 'edit'
				},
				url: "{{ route('venue.get-product') }}",
				success: function(res){
					if(res['error'] == 'true') {
						alert('Product ID already exists. Please provide unique ID.');
						obj.val('');
					} else {
						console.log('false');
					}
				}
			});

	  	}
	}
</script>
@endsection