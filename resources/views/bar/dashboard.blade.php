<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Strawberry pop | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link rel="icon" href="{{ asset('public/assets/logo.png') }}" type="image/png" sizes="16x16">
    <!-- PLUGINS STYLES-->
	@section('page_plugin_css')
	@show
	{{-- <link href="{{ asset('public/assets/vendors/morris.js/morris.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" /> --}}
    <!-- THEME STYLES-->
    <link href="{{ asset('public/assets/css/main.min2.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
	@section('page_css')
	@show
	<link href="{{ asset('public/assets/vendors/dataTables/datatables.min.css') }}" rel="stylesheet" />
	
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
           
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <!-- <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                    </li> -->
                   
                </ul>
				<div >
					<a href="{{ route('bar.home') }}"><img src="{{asset('public/assets/logo.png') }}" width='40'> <span style='font-size: 18px; margin-left: 4px; position: relative; top: 5px;' class='text-danger'>Strawberry Pop</span></a>
				</div>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar" >
                    @if(Auth::user()->photo == '')
					   <?php
							Auth::user()->photo = 'default.png';
						?>
				   @endif
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <span><b>{{ Auth::user()->name }}</b></span>
                            <img height='36' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                        </a>
                        <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu" >
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-header bg-danger" >
                                <div class="admin-avatar">
                                   <img height='88' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                                </div>
                                <div>
                                    <h5 class="font-strong text-white">{{ Auth::user()->name }}</h5>
									<p class='text-white'>Prep Admin</p>
									
                                    
                                </div>
                            </div>
                            <div class="admin-menu-features">
                              
                                <a class="admin-features-item" href="#">
                                   
                                </a>
								
								<a class="admin-features-item ssss" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ti-shift-right"></i>
                                    <span>Logout</span>
                                </a>
								
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
                            </div>
							
                        </div>
                    </li>
                   
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
		
        
        <!-- END SIDEBAR-->
        <div class="content-wrapper" style="min-height: auto !important">
			
			<div class="row" style="padding-top: 100px;">

				@if (!$prepStaff->isEmpty())
				@foreach($prepStaff as $staff)
				<div class="card col-md-3" style="margin: 15px;">
				  <img class="card-img-top" @if ($staff->image == null) 
				  		  src="{{asset('public/uploads/bars/default.png')}}" 
					  @else 
					  	 src="{{asset('public/uploads/bars/'.$staff->image)}}"  
					  @endif  
				  alt="{{$staff->name}}" style="height: 300px;">
				  
				  <div class="card-body" style="text-align:center;">
				    <h5 class="card-title">{{$staff->name}}</h5>
				    <!-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
				    <a href="{{route('bar.home', ['current_bar_id' => encrypt($staff->id)])}}" class="btn btn-primary" >Switch to {{$staff->name}}</a>
				  </div>
				</div>
				@endforeach
				@endif

			</div>
			
        <!-- <footer class="page-footer">
			<div class="font-13">2019 © All rights reserved by <b class='text-danger'>Strawberry Pop</b>.</div>
			<div>
				Made with <img style="width: 24px;" src="{{ asset('public/assets/heart.gif') }}"> by <a class='text-danger' target='_blank' href='https://www.arcticedgeapps.co.za/'><b>Arctic Edge Apps</b></a>.
			</div> -->
			<!--<div class="to-top"><i class="fa fa-angle-double-up"></i></div>-->
		<!-- </footer> -->
        </div>
    </div>
    
   
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    
    <!-- CORE PLUGINS-->
    <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-idletimer/dist/idle-timer.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- PAGE LEVEL PLUGINS-->
	@section('page_plugin_js')
	@show
	
	
    {{--<script src="{{ asset('public/assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/summernote/dist/summernote.min.js') }}"></script>
	--}}
    <!-- CORE SCRIPTS-->
	<script src="{{ asset('public/assets/vendors/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/app.min.js') }}"></script>
    <!-- PAGE LEVEL SCRIPTS-->
	@section('page_js')
	@show


</body>

</html>