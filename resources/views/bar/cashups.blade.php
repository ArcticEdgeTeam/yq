@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.bar')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
.reset_filter_box{
	margin-top: 26px;
	margin-left: 12px;
	font-size: 12px;
}
</style>
@endsection

@section('page_content')

<div class="page-heading">
	<h1 class="page-title">Cashups</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('bar.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('bar.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Cashups</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>

<div class="page-content fade-in-up">
@include('layouts.bar-venue-nav')
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<form method='get' action="{{ route('bar.cashups') }}" class="cashups-filter mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group col-md-4">
					<label>Waiter</label>
					<select name='waiter' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Waiter</option>
						@foreach($getWaiters as $waiter)
							<option @if(request()->waiter == $waiter->id) selected @endif value="{{ $waiter->id }}">{{ $waiter->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == 'Pending') selected @endif  value="Pending">Pending</option>
							<option @if(request()->status == 'Completed') selected @endif  value="Completed">Completed</option>
							<option @if(request()->status == 'Refunded') selected @endif  value="Refunded">Refunded</option>
						
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Tables</label>
					<select name='table' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Table</option>
						@foreach($getTables as $table)
							<option @if(request()->table == $table->id) selected @endif value="{{ $table->id }}">{{ $table->name }}</option>
						@endforeach
					</select>
				</div>

					<div class="form-group col-md-4">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text" value="{{request()->date_range}}">
						
					</div>
				</div>

				<div class="form-group col-md-4" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
					
				</div>

			</div>
		</form>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Invoice</th>
						<th>Customer</th>
						<th>Waiter</th>
						<th>Table</th>
						<th>Date</th>
						<th>Paid</th>
						<th>Tip</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
				
					@if(!$getTransactions->isEmpty())
					@foreach($getTransactions as $transaction)
					@php
						$transaction->customer_photo = $transaction->customer_photo == '' ? 'default.png' : $transaction->customer_photo;
						$transaction->waiter_photo = $transaction->waiter_photo == '' ? 'default.png' : $transaction->waiter_photo;
						
						if($transaction->transaction_status != 'Refunded') {
							$total += $transaction->amount + $transaction->waiter_tip;
						}
					@endphp
					<tr>
						<td>#{{ $transaction->invoice_number }}</td>
						<td><img class="img-circle" width="40" src="{{ asset('public/uploads/users/' . $transaction->customer_photo) }}"> {{ $transaction->customer_name }}</td>
						<td><img class="img-circle" src="{{ asset('public/uploads/users/' . $transaction->waiter_photo) }}" width="40" height="40"> {{ $transaction->waiter_name }}</td>
						<td>{{$transaction->table_name}}</td>
						<td>{{ $transaction->created_at->format('d M Y') }}</td>
						<td style='position: relative;'>{{ $currency }} {{ number_format($transaction->amount, 2) }} @if($transaction->transaction_status == 'Refunded') <span class="badge badge-danger" style='font-size: 8px; position: absolute; top: 2.5px; left: 10px;'>Refunded</span> @endif</td>
						<td>{{ $currency }} {{ number_format($transaction->waiter_tip, 2) }}</td>
						<td>{{ $currency }} {{ number_format($transaction->amount + $transaction->waiter_tip, 2) }}</td>
					</tr>
				@endforeach
				@else
				<tr>
					<td colspan="9" style="text-align:center;">No record found.</td>
				</tr>
				@endif
					<tr>
						<th colspan='9'>
							<div class='text-right'>
								<b style='margin-right: 50px;'>Total: {{ $currency }} {{ number_format($total,2) }}</b>
							</div>
						</th>
					</tr>	
				</tbody>
			</table>
			
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getTransactions->appends(['name' => Request::get('name'), 'waiter' => Request::get('waiter'), 'status' => Request::get('status'), 'table' => Request::get('table'), 'date_range' => Request::get('date_range')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>
			
		</div>
	</div>
</div>
</div>

@endsection
@section('page_plugin_js')

<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>

@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	autoUpdateInput: false,
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / "
	}
  });

  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});
</script>
@endsection