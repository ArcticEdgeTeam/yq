@extends('layouts.bar')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Tables</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('bar.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Tables</li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>


<div class='page-content fade-in-up'>
	@include('layouts.bar-venue-nav')
	<form class='form-danger' method='post' action="{{ route('bar.table.update', $table->id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>

		<div class="col-lg-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					
					<div class="ibox-title">Table Info</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-view-list'></i></a>
						
					</div>
					
				</div>
				<div class="ibox-body">
				   <div class='row'>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table Name</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
									<input required name='name' class="form-control" type="text" value="{{ $table->name }}">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table ID</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
									<input class="form-control" required name='table_id' value="{{ $table->table_id }}" type="text">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>No of Seats</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
									<input class="form-control" required name='seats' value="{{ $table->seats }}" type="number">
								</div>
							</div>
						</div>
						
						
				   </div>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="col-lg-4 p-lg-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Others</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Status</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($table->status == 1) checked @endif >
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
				
					<div class='col-md-12'>
						<div>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Smooking:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input type="radio" value='yes' name="smoking" checked="">
										<span class="input-span"></span>Yes</label>
										<label class="radio radio-inline radio-danger">
											<input value='no' type="radio" name="smoking" @if($table->smoking == 'no') checked @endif >
										<span class="input-span"></span>No</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					<div class='col-md-12'>
						<div class='mb-3'>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Area:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input value='inside' type="radio" name="area" checked="">
										<span class="input-span"></span>Inside</label>
										<label class="radio radio-inline radio-danger">
											<input value='outside' type="radio" name="area" @if($table->area == 'outside') checked @endif>
										<span class="input-span"></span>Outside</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					
					<div class="form-group mb-4 col-md-12">
						<label>Assign Waiter</label>
						<select name='waiters[]' class="form-control select2_demo_1" multiple="">
							@foreach($waiters as $waiter)
								@php
									$selected = '';
									if(DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $waiter->id)->first()){
										$selected = 'selected';
									}
								@endphp
								<option {{ $selected }} value="{{ $waiter->id }}">{{ $waiter->name }}</option>
							@endforeach
						</select>
					</div>
				
					
			   </div>
			</div>
		</div>
	</div>
		
		
		
	
	
	<div class="col-md-4">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				<div class="ibox-title">Waiters</div>
				
				<div class="ibox-tools">
					<a href='javascript:;'><i class='ti-user'></i></a>
					
				</div>
				
			</div>
			<div class="ibox-body">
				<ul class="media-list media-list-divider mr-2" data-height="470px">
					@foreach($waiters as $waiter)
					@php
						if($waiter->photo == ''){
							$waiter->photo = 'default.png';
						}
						if(!DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $waiter->id)->first()){
							continue;
						}
					@endphp
					
					<li class="media align-items-center">
						<a class="media-img" href="javascript:;">
							<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" alt="image" width="50" height='50'/>
						</a>
						<div class="media-body d-flex align-items-center">
							<div class="flex-1">
								<div class="media-heading">{{ $waiter->name }}</div>
								<small title='4.0' data-toggle='tooltip' class="text-warning">
								<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star text-muted'></i>
								</small>
								</div>
							
						</div>
					</li>
					@endforeach
					
				</ul>
				
				<div class='row'>
					<div class='form-group mt-3 col-lg-12'>
						<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Table</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	</div>
	</form>
	
</div>

@endsection
@section('page_plugin_js')
	<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>	
@endsection

@section('page_js')

@endsection
