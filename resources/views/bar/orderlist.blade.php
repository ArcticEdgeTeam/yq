@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.bar')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
.reset_filter_box{
	margin-top: 26px;
	margin-left: 12px;
	font-size: 12px;
}
</style>
@endsection

@section('page_content')

<div class="page-heading">
	<h1 class="page-title">Orders</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('bar.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('bar.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Orders</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
	@include('layouts.bar-venue-nav')
	<div class='ibox p-4'>
		
		<div class="flexbox mb-4">
			<div class="flexbox">
				<form method='get' action="{{ route('bar.order.list.filter') }}">
				<div class="form-group">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text">
						<button class='btn btn-danger'>Filter</button>
					</div>
				</div>
				</form>
			</div>
			
			
			<div class='flexbox'>
				
				<div class='mr-1 mt-1'>
					<a href="{{ route('bar.home') }}" class='btn btn-danger'>View Orders</a>
				</div>
				
				<div class="input-group-icon input-group-icon-left mr-3 mt-1">
					<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
					<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
				</div>
			</div>
			
		</div>
		<div class='reset_filter_box'>
			@if(isset(request()->date_range))
				<a class='text-danger' href="{{ route('bar.order.list') }}"><span class='ti-reload'></span> Reset Filter </a>
			@endif
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Order No</th>
						<th>Customer</th>
						<th>Waiter</th>
						<th>Status</th>
						<th>Table</th>
						<th>Price</th>
						<th>Date</th>
						<th>Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					@php
						if(!DB::table('ordered_products')->where('order_id', $order->id)->where('bar_id', \Session::get('current_bar_id'))->where('order_status', '!=', 3)->exists()) {
						continue;
						}
						$customer = DB::table('users')->where('id', $order->customer_id)->first();
						if($customer->photo == '') {
							$customer->photo = 'default.png';
						}
						$waiter = DB::table('users')->where('id', $order->waiter_id)->first();
						if($waiter->photo == ''){
							$waiter->photo = 'default.png';
						}
						$table = DB::table('tables')->where('id', $order->table_id)->first();
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td>#{{ $order->order_number }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $customer->photo) }}" width="40" height="40" class="img-circle"> {{ $customer->name }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="40" height="40" class="img-circle"> {{ $waiter->name }}</td>
						
						<td>
						@if($order->order_status == 'Completed')
						<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
						</td>
						
						<td>{{ $table->name }}</td>
						<td>{{ $currency }} {{ number_format($order->total_amount, 2) }}</td>
						<td>{{ $order->created_at->format('d M Y') }}</td>
						<td>{{ $order->created_at->format('H:i') }}</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('bar.order.edit', $order->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>	
			
	</div>
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>  
@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	startDate: "{{ $start }}",
    endDate: "{{ $end }}",
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / ",
		startDate: "{{ $start }}",
		endDate: "{{ $end }}",
	}
  });
});
</script>
@endsection