@extends('layouts.bar')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
	
</style>
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Orders</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('bar.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('bar.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Orders</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>

<div class="page-content fade-in-up">
	@include('layouts.bar-venue-nav')
	<div class='row'>
		<div class='col-md-6'>
	<div class='ibox p-4'>
	
			<div class="ibox-head">
				
				<div class="ibox-title">Active Orders</div>
				<div class="ibox-tools">
					<div class="flexbox">
						<a class='btn btn-danger btn-sm' href="{{ route('bar.order.list') }}">All Orders</a>
					</div>
				</div>
				
			</div>
		
		<div class="flexbox mb-4 mt-4">
			
			<div>
				
			</div>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Customer</th>
						<th>Time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@php
						$bar_id = DB::table('bars')->where('user_id', Auth::user()->id)->first()->id;
					@endphp
					@foreach($orders as $order)
					@php
					if(!DB::table('ordered_products')->where('order_id', $order->id)->where('bar_id', $bar_id)->where('order_status', '!=', 3)->exists()){
						continue;
					}
					$customer = DB::table('users')->where('id', $order->customer_id)->first();
					if($customer->photo == ''){
						$customer->photo = 'default.png';
					}
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $customer->photo) }}" width="40" height="40" class="img-circle"> {{ $customer->name }}</td>
						<td>{{ $order->created_at->format('H:i') }}</td>
						<td>
						@if($order->order_status == 'Completed')
							<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('bar.order.edit', $order->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
							<a class='text-info quickview' data-toggle='tooltip' title='Quick Info' href="{{ route('bar.order.quickview', $order->id) }}"><i style='font-size: 22px;' class='fa fa-info-circle'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
				
	</div>
	</div>
	
	
	<div id='quickview_area' class='col-md-6 pl-0'>
		
	</div>
	
	</div>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(".quickview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('href'),
		   success:function(data){
			$('#quickview_area').html(data);
		   }
		});
	});
</script>
@endsection
