@extends('layouts.bar')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection
@section('page_css')
	<style>
	.order_no_box{
		line-height: 1.1; font-weight: 500; margin-bottom: 5px;
	}
	
	.table_box:hover{
		cursor: pointer;
	}
	
	.empty_table{
		margin-bottom: 20px;
		margin-top: 54px;
	}
	
	.empty_table i{
		font-size: 60px;
		color: #ccc;
	}
	
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list .table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.no_shadow{
		box-shadow: none;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
	</style>
@endsection

@section('page_content')

	<div class="page-content fade-in-up">
		<div class='text-center'>
			<span class="badge badge-danger" onclick="gridView()"><i class="fa fa-th-large"></i> Grid</span>		
			<span class="badge badge-success"  onclick="listView()"><i class="fa fa-bars"></i> List</span>
			
		</div>
	 <br>	
	<div class='row grid-data'>
		@php
			$counter = 0;
		@endphp
		@foreach($orders as $order)
		@php
		if(!DB::table('ordered_products')->where('order_id', $order->id)->where('bar_id', \Session::get('current_bar_id'))->where('order_status', '!=', 3)->exists()) {
					continue;
				}
			
			$counter++;
			$table = DB::table('tables')->where('id', $order->table_id)->first();
			$customer = DB::table('users')->where('id', $order->customer_id)->first();
			if($customer->photo == '') {
				$customer->photo = 'default.png';
			}
			$waiter = DB::table('users')->where('id', $order->waiter_id)->first();
			if($waiter->photo == ''){
				$waiter->photo = 'default.png';
			}
		@endphp
		<div class='col-lg-6'>
			
			<div data-address="{{ route('bar.order.popview', $order->id) }}" class="ibox table_box popview" data-toggle="modal" data-target="#new-event-modal" data-toggle="modal" data-target="#new-event-modal">
				
				<div class="ibox-head">
					<div class="ibox-title">{{ $table->table_id }}</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-layout-grid2-alt'></i></a>
					</div>
				</div>
				
				<div class="ibox-body">
					<div class='order_no_box'>
						Order#: {{ $order->order_number }}
					</div>
					<ul class="media-list media-list-divider mr-2" data-height="580px">
						<li class="media align-items-center">
							<a class="media-img" href="javascript:;">
								<img class="img-circle" src="{{ asset('public/uploads/users/' . $customer->photo) }}" alt="image" width="54" />
							</a>
							<div class="media-body d-flex align-items-center">
								<div class="flex-1">
									<div class="media-heading">{{ $customer->name }}</div><small class="text-muted">Customer</small></div>
								<!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
							</div>
						</li>
						
						<li class="media align-items-center">
							<a class="media-img" href="javascript:;">
								<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" alt="image" width="54" />
							</a>
							<div class="media-body d-flex align-items-center">
								<div class="flex-1">
									<div class="media-heading">{{ $waiter->name }}</div><small class="text-muted">Waiter</small></div>
								<!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
							</div>
						</li>
						
					</ul>
					<div class='text-center'>
						@if($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		@endforeach
		
		<div class='col-lg-3 d-none'>
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">tab-003</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-layout-grid2-alt'></i></a>
					</div>
				</div>
				<div class="ibox-body">
					
					<div class='text-center'>
						<div class='empty_table'>
							<i class='ti-view-grid'></i>
						</div>
						<span class="badge badge-primary">Empty</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class='col-lg-12'>
		@if($counter == 0)
			<div class="alert alert-danger alert-bordered">Your active tables with orders will be shown here!</div>
		@endif
		</div>
		
		
	</div>

	<div class='row list-data' style="display: none;">
		<div class='col-md-6'>
	<div class='ibox p-4'>
	
			<div class="ibox-head">
				
				<div class="ibox-title">Active Orders</div>
				<div class="ibox-tools">
					<div class="flexbox">
						<a class='btn btn-danger btn-sm' href="{{ route('bar.order.list') }}">All Orders</a>
					</div>
				</div>
				
			</div>
		
		<div class="flexbox mb-4 mt-4">
			
			<div>
				
			</div>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Customer</th>
						<th>Time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@php
					@endphp
					@foreach($ordersListing as $orderList)
					@php
					if(!DB::table('ordered_products')->where('order_id', $orderList->id)->where('bar_id', \Session::get('current_bar_id'))->where('order_status', '!=', 3)->exists()){
						continue;
					}
					$customer = DB::table('users')->where('id', $orderList->customer_id)->first();
					if($customer->photo == ''){
						$customer->photo = 'default.png';
					}
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $customer->photo) }}" width="40" height="40" class="img-circle"> {{ $customer->name }}</td>
						<td>{{ $orderList->created_at->format('H:i') }}</td>
						<td>
						@if($orderList->order_status == 'Completed')
							<span class="badge badge-success">Completed</span>
						@elseif($orderList->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($orderList->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($orderList->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('bar.order.edit', $orderList->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
							<a class='text-info quickview' data-toggle='tooltip' title='Quick Info' href="{{ route('bar.order.quickview', $orderList->id) }}"><i style='font-size: 22px;' class='fa fa-info-circle'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
				
	</div>
	</div>
	
	
	<div class='quickview_area' class='col-md-6 pl-0'>
		
	</div>
	
	</div>
	
</div>


<!-- New Event Dialog-->
<div class="modal fade" id="new-event-modal" tabindex="-1" role="dialog">
	<div id='quickview_area' class="modal-dialog modal-lg bg-white" role="document">
		
	</div>
</div>
<!-- End New Event Dialog-->

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(".popview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('data-address'),
		   success:function(data){
			$('#quickview_area').html(data);
		   }
		});
	});
</script>

<script>
	$(".quickview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('href'),
		   success:function(data){
			$('.quickview_area').html(data);
		   }
		});
	});

	function gridView() {

		$('.grid-data').show();
		$('.list-data').hide();

	}

	function listView() {
		$('.grid-data').hide();
		$('.list-data').show();
	}
</script>
@endsection