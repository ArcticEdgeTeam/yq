@extends('layouts.venue')


@section('page_plugin_css')
	<!-- <link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" /> -->
	<link href="{{ asset('public/assets/vendors/multiselect-checbox/styles/multiselect.css') }}" rel="stylesheet" />
	<script src="{{ asset('public/assets/vendors/multiselect-checbox/multiselect.min.js') }}"></script>
	 <link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection
@section('page_css')
	<style>
		#child_cat_list{
			display: none;
		}
	</style>
@endsection
@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Menus</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venues</li>
		<li class="breadcrumb-item">Menus</li>
		<li class="breadcrumb-item">New</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<form class='form-danger form-menu' method='post' action="{{ route('venue.menu.create', $id) }}" enctype='multipart/form-data'>
		@csrf
		<div class="row">
					
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-body text-center">
					<img id='previewfile' src="{{ asset('public/uploads/categories/default.png') }}" class='img-fluid'>
					<div class='row'>
						<div class="col-sm-12">
							<label class='btn btn-link text-danger m-0' for='uploadfile'>Upload Pic</label>
							<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' name='image' class="form-control p-2 d-none" style='height: 37px;' type="file" />
							<div class='text-success'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Menu Info</div>
					<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' value='1' type="checkbox" checked>
						<span></span>
					</label>
					
				</div>
				</div>
				<div class="ibox-body">
					
					<br>
							<div class='row'>
								<div class='col-md-4'>
									<div class="form-group mb-4">
										<div class="input-group-icon input-group-icon-left">
											<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
											<input class="form-control" required name='name' type="text"  placeholder="Menu Name" value="{{old('name')}}">
										</div>
									</div>
								</div>

								<div class="form-group mb-4 col-md-4" style="margin-top: -30px">
									<label>Products</label>
									<select id='testSelect1' class="form-control" name='products[]' multiple>
									 	@foreach($products as $product)
										<option checked value="{{ $product->id }}">{{ $product->name }}</option>
										@endforeach
									</select>
								</div>

								<div class='col-md-4'>
									<div class="form-group mb-4">
										<div class="form-check">
									    	<input type="checkbox" class="form-check-input" id="exampleCheck1" name="default" value="yes">
									    	<label class="form-check-label" for="exampleCheck1">Make Default</label>
								  		</div>
									</div>
								</div>
							</div>


							@if(!$categories->isEmpty())
                           <!-- ordering row -->
                        <div class="content" style="margin-top: 20px;">
                            <h3 class="text-center">Change Categories Order</h3>
                           <div class="row" style="margin-left: 200px;">
                            @foreach($categories as $key => $category)

                            @php
                                $key = $key + 1;
                            @endphp
                    
                                <div class='col-md-4'>
                                    <div class="form-group mb-4">
                                        <div class="input-group-icon input-group-icon-left">
                                            
                                            <input class="form-control" name='cat_name' type="text"  placeholder="Category Name" value="{{$category->name}}" style="padding-left: 10px;" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-md-4'>
                                    <div class="form-group mb-4">
                                        <div class="input-group-icon input-group-icon-left">

                                            <select class="form-control parent_category" name="parent_cat_order[{{$category->id}}]"  style="padding-left: 10px;">
                                                @php
                                                for($i=1; $i <= count($categories); $i++) {
                                                @endphp
                                                <option value="{{$i}}" @if ($key == $i) selected @endif>{{$i}}</option>
                                                @php 
                                                }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                               

                                @if ($category->has_childs == 'yes')

                                    @foreach($category->child as $index => $child)
                                    @php
                                        $index = $index + 1;
                                        $child = explode('/', $child);
                                    @endphp

                                    <div class="col-md-1"></div>
                                    <div class='col-md-3'>
                                        <div class="form-group mb-4">
                                            <div class="input-group-icon input-group-icon-left">
                                                
                                                <input class="form-control" name='child_cat_name' type="text"  placeholder="Category Name" value="{{$child[1]}}" style="padding-left: 10px;" readonly>
                                            </div>
                                        </div>
                                    </div>

                                   <div class='col-md-3'>
                                        <div class="form-group mb-4">
                                            <div class="input-group-icon input-group-icon-left">
                                                <select class="form-control" name="child_cat_order[{{$category->id}}][{{$child[0]}}]" style="padding-left: 10px;">
                                                    @php
                                                    for($j=1; $j <= count($category->child); $j++) {
                                                    @endphp
                                                    <option @if ($index == $j) selected @endif value="{{$j}}">{{$j}}</option>
                                                    @php 
                                                    }
                                                @endphp
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5"></div>

                                    @php
                                        $index = $index++;
                                    @endphp

                                    @endforeach
                                @endif

                                 @php
                                    $key = $key++;
                                @endphp
                            
                            @endforeach
                           </div>
                        </div>   
                        <!-- ordering ends -->
                        @endif

							<div class='row'>
								<div class='col-md-8'>
								</div>
								<div class='col-md-4'>
									<div class='form-group'>
										<button class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Add Menu</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
											</span>
										</button>
								   </div>
							   </div>
						   </div>
				     
				</div>
			</div>
		</div>
		
	</div>
	</form>
	
</div>
@endsection

@section('page_plugin_js')
<!-- <script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script> -->
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>

@endsection
@section('page_js')
<script>

	  $(document).ready(function() {
        $('.form-menu').submit(function(event) {
            event.preventDefault();
            // ajax call
            $.ajax({
                type: 'GET',
                data:{
                    name: $('input[name="name"]').val(),
                    id: '{{\Auth::user()->venue_id}}',
                    type: 'add',
                },
                url: "{{ route('ajax.get-menu-info') }}",
                success: function(res) {

                    if(res['error'] != null) {
                        // null the input value
                        $('input[name="name"]').val('');
                        //show message box
                        swal("", res['error'], "error");
                        $('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
                        return false;

                    } else {
                        event.currentTarget.submit();
                    }
                    
                } //success
            });
        });
    });

	$(document).ready(function(){

		$('#child_radio').click(function() {
			if($(this).is(':checked')) { 
				$('#category_id').prop('required', true);
		   }
		});
		
		$('#parent_radio').click(function() {
		   if($(this).is(':checked')) {
			   $('#category_id').prop('required', false);
		   }
		});

		
	});
</script>

<script>
	document.multiselect('#testSelect1')
		.setCheckBoxClick("checkboxAll", function(target, args) {
			console.log("Checkbox 'Select All' was clicked and got value ", args.checked);
		})
		.setCheckBoxClick("1", function(target, args) {
			console.log("Checkbox for item with value '1' was clicked and got value ", args.checked);
		});

        $("#testSelect1").find("option[value=1]").prop("selected", "selected");
        $("#testSelect1").multiselect('refresh');
</script>

<script>

	// $(".select2_demo_1").select2();

	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection