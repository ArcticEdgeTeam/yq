@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
	
</style>
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Orders</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Orders</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-venue-nav')
	<div class='row'>
	<div class='col-md-6'>
	<div class='ibox p-4'>
	
			<div class="ibox-head">
				
				<div class="ibox-title">Active Orders</div>
				<div class="ibox-tools">
					<div class="flexbox">
						<a class='btn btn-danger btn-sm' href="{{ route('venue.order.list') }}">All Orders</a>
					</div>
				</div>
				
			</div>
		
			<form action="{{route('venue.orders')}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group mb-4 col-md-4">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == 'Ordered') selected @endif  value="Ordered">Ordered</option>
							<option @if(request()->status == 'In Oven') selected @endif  value="In Oven">In Oven</option>
							<option @if(request()->status == 'Final Steps') selected @endif  value="Final Steps">Final Steps</option>

						
					</select>
				</div>

				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Customer</th>
						<th>Time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getOrders->isEmpty())
					@foreach($getOrders as $order)
					@php
						$order->user_photo = $order->user_photo == '' ? 'default.png' : $order->user_photo;
					
					@endphp
					<tr>
						<td><img src="{{ asset('public/uploads/users/' . $order->user_photo) }}" width="40" height="40" class="img-circle"> {{ $order->user_name }}</td>
						<td>{{ $order->created_at->format('H:i') }}</td>
						<td>
						@if($order->order_status == 'Completed')
							<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('venue.order.edit', $order->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
							<a class='text-info quickview' data-toggle='tooltip' title='Quick Info' href="{{ route('venue.order.quickview', $order->id) }}"><i style='font-size: 22px;' class='fa fa-info-circle'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="4" style="text-align:center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getOrders->appends(['name' => Request::get('name'), 'status' => Request::get('status')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>
			
		</div>
				
	</div>
	</div>
	
	
	<div id='quickview_area' class='col-md-6 pl-0'>
		
	</div>
	
	</div>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(".quickview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('href'),
		   success:function(data){
			$('#quickview_area').html(data);
		   }
		});
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection