@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Customers</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.customers') }}">Customers</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
		
		@include('layouts.venue-customer-nav')
		
		<div class='ibox p-4'>
		
		<div class="flexbox mb-4">
			<div class="flexbox">
				
			</div>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Order NO</th>
						<th>Waiter</th>
						<th>Avg Rating</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($reviews as $review)
				@php
				
					$waiter = DB::table('users')->where('id', $review->waiter_id)->first();
					if($waiter->photo == ''){
						$waiter->photo = 'default.png';
					}
					
					$venue = DB::table('venues')->where('id', $review->venue_id)->first();
					if($venue->banner == ''){
						$venue->banner = 'default.png';
					}
					
					$stars = round($review->average_rating);
					$remainig_stars = 5 - $stars;
					
					$order = DB::table('orders')->where('id', $review->order_id)->first();
				@endphp
				<tr>
					<td>{{ $counter++ }}</td>
					<td>
					@if($review->status == 1)
					<i style='font-size: 20px; position: relative; top: 1px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
					@else
					<i style='font-size: 20px; position: relative; top: 1px;' data-toggle='tooltip' title='Inactive' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
					@endif
					#{{ $order->order_number }}
					</td>
					<td><img class='img-circle' width='40' src="{{ asset('public/uploads/users/' . $waiter->photo) }}" > {{ $waiter->name }}</td>
					<td class='text-warning'>
					<span class='d-none'>{{ $review->average_rating }}</span>
					<span data-toggle="tooltip" title="{{ $review->average_rating }}">
					@php
						for($i = 0; $i < $stars; $i++){
							echo "<i class='fa fa-star' aria-hidden='true'></i>";
						}
						
						for($j = 0; $j < $remainig_stars; $j++){
							echo "<i class='fa fa-star text-muted'></i>";
						}
					@endphp
					</span>
					</td>
					<td>
						<span data-html='true' class='text-info' data-toggle='tooltip' title="Date: {{ $review->created_at->format('d M Y') }} <br />Time: {{ $review->created_at->format('H:i:s') }}"><i style='font-size: 22px;' class='fa fa-info'></i></span>
							{{-- <a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.venue.review.edit', [$review->venue_id, $review->id]) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a> --}}
					</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div>
				
				
			
	</div>
		
		
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection 