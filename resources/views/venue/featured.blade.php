@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Featured</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.featured') }}">Featured</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<div class="ibox">

		<div class='col-lg-12'>
			@php
			    $venue_date = \Carbon\Carbon::parse($venue->created_at);
			    $current_date = \Carbon\Carbon::now();

			    $differenceInMonth = $venue_date->diffInMonths($current_date);

			@endphp
			@if ($differenceInMonth < $getFeatured->months) 
			
				<div class="alert alert-success alert-bordered">Your are already featured.</div>
			@else

				<div class="alert alert-danger alert-bordered">Pay now to be featured.</div>
			@endif		
		</div>
	
	</div>

	<div class="form-group col-md-12">
		<button class="btn btn-danger btn-fix btn-animated from-left pay-now-btn">
		<span class="visible-content">Featured</span>
		<span class="hidden-content">
			<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Featured</span>
		</span>
		</button>
	</div>

</div>

	<!-- Modal -->
    <div class="modal fade featured-pay-now" id="featured-pay-now" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form method="POST" action="#">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Get Featured</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="form-group mb-4 col-md-12">
				<label>Charges</label>
				<div class="input-group-icon input-group-icon-left">
					<span class="input-icon input-icon-left">ZAR</span>
					<input value="{{number_format($getFeatured->charges, 2)}}" name="charges" class="form-control" type="text" step="any" required readonly>
				</div>
			</div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Pay Now</button>
          </div>
        </div>
        </form>
      </div>
    </div>
    <!-- Table Checker Modal Ends -->
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.pay-now-btn').click(function() {
			$('.featured-pay-now').modal('show');

		});
	});
</script>
@endsection