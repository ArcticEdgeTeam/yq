@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Support</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Support</li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			
			<div class="form-group">
				<a href="{{ route('venue.support.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
					<span class="visible-content">Create Ticket</span>
					<span class="hidden-content">
						<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
					</span>
				</a>
		   </div>
			
			<div></div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Subject</th>
						<th>Date</th>
						<th>Time</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($supports as $support)
					<tr>
						<td>{{ $counter++ }}</td>
						
						<td>{{ $support->subject }}</td>
						<td>{{ $support->created_at->format('d M Y') }}</td>
						<td>{{ $support->created_at->format('H:i') }}</td>
						<td>
						@if($support->status == 1)
						<span class='d-none'>Read</span><i style='font-size: 22px;' data-toggle='tooltip' title='Read' class="fa fa-check-circle text-success" aria-hidden="true"></i>
						@else
							<span class='d-none'>Unread</span><i style='font-size: 22px;' data-toggle='tooltip' title='Unread' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('venue.support.edit', $support->id ) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection 