@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.name_input{
		display: none;
	}
</style>
@endsection

@section('page_content')	

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Bookings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Bookings</li>
		<li class="breadcrumb-item">New</li>
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>

<div class='page-content fade-in-up'>
@include('layouts.venue-venue-nav')
<form class='form-danger' method='post' action="{{ route('venue.booking.save') }}" enctype='multipart/form-data'>
@csrf
	<div class='row'>
		<div class="col-xl-12">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Booking Info</div>
					
					<div class="ibox-tools">
						
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
						<!-- <div class="form-group mb-4 col-md-4">
							<label>Select Customer</label>
							<select required name='customer_id' class="selectpicker form-control customer_id">
								<option value=''>Select Customer</option>
								<option value='2'>Walk In Customer</option>
								@foreach($customers as $customer)
									
									@php
										if($customer->id == 2){
											continue;
										}
									@endphp
								
									<option value="{{ $customer->id }}">{{ $customer->name }}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-4">
							<div class="name_input">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input min='0' required name='name' type='text' class='form-control' >
							</div>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-4">
						</div> -->

						<div class="form-group mb-4 col-md-4">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input min='0' required name='name' type='text' class='form-control' >
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-4">
							<label>Phone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
								<input min='0' required name='phone' type='text' class='form-control' >
							</div>
						</div>
					   
					   <div class="form-group mb-4 col-md-4">
							<label>Assign Table</label>
							<select required name='table_id' class="selectpicker form-control">
								<option value=''>Select Table</option>
								@foreach($tables as $table)
									<option value="{{ $table->id }}">{{ $table->name }}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-4">
							<label class="font-normal">Date & Time</label>
							<div class="input-group date form_datetime" >
								<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
								<input autocomplete='off' required name='date_time' class="form-control">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-4">
							<label>Seating</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input min='0' required name='seating' type='number' class='form-control' >
							</div>
						</div>
						<div class="form-group mb-4 col-md-4">
							<label>Smoking</label>
							<select required name='smooking' class="selectpicker form-control">
								<option value='Yes'>Yes</option>
								<option value='No'>No</option>
							</select>
						</div>
						<div class="form-group mb-4 col-md-12">
							<label>Area</label>
							<select required name='area' class="selectpicker form-control">
								<option value='Indoor'>Indoor</option>
								<option value='Outdoor'>Outdoor</option>
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Message</label>
							<textarea name='message' rows='3' class='form-control'></textarea>
							
						</div>
						
						<div class="form-group col-md-12 mb-4">
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Add Booking</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
								</span>
							</button>
					   </div>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</form>
	
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(document).ready(function(){	
		$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd hh:ii:ss',
			startDate:'+0d',
		});
	});
	
	$('.customer_id').on('change', function(){
		if($(this).val() == '2'){
			$('.name_input').show();
		}else{
			$('.name_input').hide();
		}
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection