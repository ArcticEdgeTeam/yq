@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
											
@endphp

@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Customers</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.customers') }}">Customers</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
	
		<div class="flexbox mb-4">
			
			<div></div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>User</th>
						<th>Email</th>
						<th>Gender</th>
						<th>Total Spent</th>
						<th>Last Visited</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					@php
						$customer = DB::table('users')->where('id', $order->customer_id)->first();
						$lifetime_spent = DB::table('transactions')->where('venue_id', Auth::user()->venue_id)->where('customer_id', $customer->id)->where('transaction_status', 'Completed')->sum('amount');
						if($customer->photo == ''){
							$customer->photo = 'default.png';
						}
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img class='img-circle' width='40' src="{{ asset('public/uploads/users/' . $customer->photo) }}"> {{ $customer->name }}</td>
						<td>{{ $customer->email }}</td>
						<td>
							@if($customer->gender == 'male')
							<span class='d-none'>Male</span><i data-toggle="tooltip" title='Male' style='font-size: 22px;' class="fa fa-male" aria-hidden="true"></i>
							@else
							<span class='d-none'>Female</span><i data-toggle="tooltip" title='Female' style='font-size: 22px;' class="fa fa-female" aria-hidden="true"></i>
							@endif
						</td>
						<td>{{ $currency }} {{ $lifetime_spent }}</td>
						<td>{{ $order->created_at->format('d M Y') }}</td>
						<td>
						@if($customer->status == 1)
						<span class='d-none'>Active</span><i style='font-size: 22px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
						@else
							<span class='d-none'>Inactive</span><i style='font-size: 22px;' data-toggle='tooltip' title='Inactive' class="fa fa-check-circle text-danger" aria-hidden="true"></i>
						@endif
						</td>
						<td>
							<span style='font-size: 22px;' data-html='true' data-toggle='tooltip' class='fa fa-info text-info' title="Member Since: {{ date('d M Y', strtotime($customer->created_at)) }}"></span>
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('venue.customer.overview', $customer->id) }}"><i style='font-size: 22px;' class='fa fa-edit'></i></a>
						</td>
					</tr>
					@endforeach
				
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection