@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
.reset_filter_box{
	margin-top: 26px;
	margin-left: 12px;
	font-size: 12px;
}
</style>
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Orders</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Orders</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-venue-nav')
	<div class='ibox p-4'>
		
		<div class="ibox-head">
			<div class="ibox-title">All Orders</div>
				<div class="ibox-tools">
					<div class='mr-1 mt-1'>
					<a href="{{ route('venue.orders') }}" class='btn btn-danger'>Active Orders</a>
				</div>
			</div>
		</div>
			
		<form action="{{route('venue.order.list')}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group col-md-4">
					<label>Waiter</label>
					<select name='waiter' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Waiter</option>
						@foreach($getWaiters as $waiter)
							<option @if(request()->waiter == $waiter->id) selected @endif value="{{ $waiter->id }}">{{ $waiter->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == 'Ordered') selected @endif  value="Ordered">Ordered</option>
							<option @if(request()->status == 'In Oven') selected @endif  value="In Oven">In Oven</option>
							<option @if(request()->status == 'Final Steps') selected @endif  value="Final Steps">Final Steps</option>
							<option @if(request()->status == 'Completed') selected @endif  value="Completed">Completed</option>
							<option @if(request()->status == 'Refunded') selected @endif  value="Refunded">Refunded</option>

						
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Tables</label>
					<select name='table' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Table</option>
						@foreach($getTables as $table)
							<option @if(request()->table == $table->id) selected @endif value="{{ $table->id }}">{{ $table->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text" value="{{request()->date_range}}">
						
					</div>
				</div>

				<div class="form-group col-md-4" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Order No</th>
						<th>Customer</th>
						<th>Waiter</th>
						<th>Status</th>
						<th>Table</th>
						<th>Price</th>
						<th>Date</th>
						<th>Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getOrders->isEmpty())
					@foreach($getOrders as $order)
					@php
						$order->customer_photo = $order->customer_photo == '' ? 'default.png' : $order->customer_photo;
						$order->waiter_photo = $order->waiter_photo == '' ? 'default.png' : $order->waiter_photo;
					@endphp
					<tr>
						<td>#{{ $order->order_number }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $order->customer_photo) }}" width="40" height="40" class="img-circle"> {{ $order->customer_name }}</td>
						<td><img src="{{ asset('public/uploads/users/' . $order->waiter_photo) }}" width="40" height="40" class="img-circle"> {{ $order->waiter_name }}</td>
						
						<td>
						@if($order->order_status == 'Completed')
						<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@elseif($order->order_status == 'Refunded')
							<span class="badge badge-danger">Refunded</span>
						@endif
						</td>
						
						<td>{{ $table->name }}</td>
						<td>{{ $currency }} {{ number_format($order->total_amount, 2) }}</td>
						<td>{{ $order->created_at->format('d M Y') }}</td>
						<td>{{ $order->created_at->format('H:i') }}</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('venue.order.edit', $order->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="9" style="text-align:center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getOrders->appends(['name' => Request::get('name'), 'waiter' => Request::get('waiter'), 'status' => Request::get('status'), 'table' => Request::get('table'), 'date_range' => Request::get('date_range')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>

		</div>	
			
	</div>
</div>

@endsection
@section('page_plugin_js')

<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>

@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	autoUpdateInput: false,
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / "
	}
  });

  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

$('.venue-open-toggle').change(function() {
	var status = '';
		if ($(this).is(":checked")) {
			status = 1;
		} else {
			status = 0;
		}
	// ajax call
		$.ajax({
			type: 'GET',
			data:{
				status: status,
				id: '{{$venue->id}}'
			},
			url: "{{ route('venue.venue-status.update') }}",
			success: function(res){
				console.log(res);
			}
		});

});
</script>
@endsection