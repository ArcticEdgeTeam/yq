@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
<style type="text/css">
	
	.category-head {
		padding: 10px;
		color: #fff;
    	background-color: #f75a5f;
    	border-color: #f75a5f;
    	font-weight: 1000;
	}

	.category-head-child {
		padding: 10px;
		margin-top: 5px;
		margin-left: 20px;
		color: #495057;
    	background-color: #e9ecef;
    	border-color: #e9ecef;
    	font-weight: 1000;
    	width: 100%;
	} 

	.category-data {
		margin-bottom: 20px;
	}

	.category-data-child {
		margin-bottom: 20px;
		padding-left: 20px;
	}

	.badge-danger-color {
		color: #495057;
    	background-color: #e9ecef;
    	border-color: #e9ecef;
	}

</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}l"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Menus</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
@include('layouts.venue-management-nav')
	<div class='ibox p-4'>
		<div class="flexbox mb-4">
			<div class="flexbox">
				
				<div class="form-group">
					<a href="{{ route('venue.menu.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Menu</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Sr.No</th>
						<th>Menu Name</th>
						<th>Products</th>
						<th>Default</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($menus as $menu)
					@if($menu->image == '')
						<?php
							$menu->image = 'default.png';
						?>
					@endif
					@php
					$products = DB::table('menu_products')
					->select('menu_products.*', 'products.*')
					->leftjoin('products', 'products.id', '=', 'menu_products.product_id')
					->where('products.status', 1)
					->where('menu_id', $menu->id)
					->count();
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img src="{{ asset('public/uploads/menus/' . $menu->image) }}" class='img-circle' height='50' width='50'> {{ $menu->name }}</td>
						
						<td>{{ number_format($products) }}</td>
						<td>
							@if($menu->default == 1)
								<a  onClick="javascript: return confirm('Do you really wants to remove this menu from Default?');" href='{{route("venue.menus-default", [$menu->id, $menu->default])}}' title="Default" class="btn btn-success" style="width: 90px;">Default</a>
							@else
								<a  onClick="javascript: return confirm('Do you really wants to make this menu as Default?');" href='{{route("venue.menus-default", [$menu->id, $menu->default])}}' title="Make Default" class="btn btn-danger">Make Default</a>
							@endif
						</td>
						<td>
							@if($menu->status == 1)
								<a  onClick="javascript: return confirm('Do you really wants to inactive this Menu?');" href='{{route("venue.menus-status", [$menu->id, $menu->status])}}' title="Active" class="btn btn-success">Active</a>
							@else
								<a  onClick="javascript: return confirm('Do you really wants to active this Menu?');" href='{{route("venue.menus-status", [$menu->id, $menu->status])}}' title="Inactive" class="btn btn-danger">Inactive</a>
							@endif
						</td>
						<td>
							
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('venue.menu.edit', $menu->id) }}"><i style='font-size: 22px;' class='fa fa-edit'></i></a>

							<a class='text-warning' data-toggle='tooltip' title='View Menu' href="javascript:void(0)" onclick="menuProducts('{{$menu->id}}', '{{$menu->name}}');" ><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>

						<!-- Menu modal	-->
				<div class="modal fade bd-example-modal-lg menu-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-lg">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h3 class="modal-title menu-modal-ttle" id="exampleModalLabel"></h3>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>

				      </div>

				      <div class="modal-body">
				      	<p class="badge-section" style="display: none;">
				      		<span class="badge badge-pill badge-danger">Parent Category</span>
				      		<span class="badge badge-pill badge-danger-color">Child Category</span>
				      	</p>
				      	<div class="container ">
				      		<div class="row category-product-data">
				      		</div>

				      	</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-primary"  data-dismiss="modal">Close</button>
				     </div>
				    </div>
				  </div>
				</div>	
				<!-- Modal ends -->
	</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>

<script type="text/javascript">
	function menuProducts(menu_id, name) {
		$('.menu-modal').modal('show');
		$('.menu-modal-ttle').text(name + ' Details');
		var token = $('meta[name="csrf-token"]').attr('content');
		// remove th
		$('.category-product-data'). empty();
		// ajax call
			$.ajax({
				type: 'POST',
				data:{
					_token: token,
					menu_id: menu_id,
				},
				url: "{{ route('venue.menu.products') }}",
				success: function(res) {

					if (res.parentCategory == '') {
						$('.badge-section').hide();
						$('.category-product-data').append('<p class="alert alert-danger" style="width:100%">Sorry! No products found for this menu.</p>');
						return false;
					} else {
						$('.badge-section').show();

						for (var key in res.parentCategory) {

							if(res.parentCategory[key].child == 'no') {
								$('.category-product-data').append('<div class="col-md-12 category-head">'+key+'</div>');
								for (let i=0; i< res.parentCategory[key]['products'].length; i++) {
									$('.category-product-data').append('<div class="col-md-4 category-data">'+res.parentCategory[key]['products'][i]+'</dv>');

								}
							} else {
								$('.category-product-data').append('<div class="col-md-12 category-head">'+key+'</div>');

									for (index in res.childCategory[key]) {
										$('.category-product-data').append('<div class="category-head-child">'+index+'</div>');
										for (let j=0; j< res.childCategory[key][index].length; j++) {
											$('.category-product-data').append('<div class="col-md-4 category-data-child">'+res.childCategory[key][index][j]+'</dv>');
										}
									}
								
							}
						}
					}
					
				} // success ends
			});
	}
</script>
@endsection