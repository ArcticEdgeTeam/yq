@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
.reset_filter_box{
	margin-top: 26px;
	margin-left: 12px;
	font-size: 12px;
}
</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Reviews</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Reviews</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
	</ol>
</div>

<div class="page-content fade-in-up">
	@include('layouts.venue-venue-nav')
	<div class='ibox p-4'>

		<form action="{{route('venue.reviews')}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group col-md-3">
					<label>Order No.</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='order_no' class="form-control" type="text" placeholder="Search by order no..." value="{{request()->order_no}}">
						
					</div>
				</div>

				<div class="form-group col-md-3">
					<label>Waiter</label>
					<select name='waiter' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Waiter</option>
						@foreach($getWaiters as $waiter)
							<option @if(request()->waiter == $waiter->id) selected @endif value="{{ $waiter->id }}">{{ $waiter->name }}</option>
						@endforeach
					</select>
				</div>


				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
			
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Order NO</th>
						<th>Customer</th>
						<th>Waiter</th>
						<th>Avg Rating</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getReviews->isEmpty())
					@foreach($getReviews as $review)
					@php
						$review->customer_photo = $review->customer_photo == '' ? 'default.png' : $review->customer_photo;
						$review->waiter_photo = $review->waiter_photo == '' ? 'default.png' : $review->waiter_photo;
						
						$stars = round($review->average_rating);
						$rating = round($review->average_rating, 2);
						$remainig_stars = 5 - $stars;
						
					@endphp
					<tr>
						<td>
						@if($review->status == 1)
						<i style='font-size: 20px; position: relative; top: 1px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
						@else
						<i style='font-size: 20px; position: relative; top: 1px;' data-toggle='tooltip' title='Inactive' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
						@endif
						{{ $review->order_number }}

						</td>
						<td><img class='img-circle' width='40' src="{{ asset('public/uploads/users/' . $review->customer_photo) }}") }}' > {{ $review->customer_name }}</td>
						<td><img class='img-circle' width='40' src="{{ asset('public/uploads/users/' . $review->waiter_photo) }}" > {{ $review->waiter_name }}</td>
						
						<td class='text-warning'>
						<span class='d-none'>{{ $rating }}</span>
						<span data-toggle="tooltip" title="{{ $rating }}">
						@php
							for($i = 0; $i < $stars; $i++){
								echo "<i class='fa fa-star' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
						@endphp
						</span>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5" style="text-align:center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getReviews->appends(['name' => Request::get('name'), 'waiter' => Request::get('waiter'), 'order_no' => Request::get('order_no')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>
		</div>
			
	</div>
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>

 <script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
@endsection

@section('page_js')
<script>

$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection