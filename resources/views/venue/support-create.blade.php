@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Support</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.supports') }}">Support</a></li>
		<li class="breadcrumb-item">New Ticket</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	<form class='form-danger' action="{{ route('venue.support.save') }}" method='post'>
	@csrf
	<div class="row">
	
		<div class="col-xl-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Admin</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				<?php
					if(Auth::user()->photo == ''){
						Auth::user()->photo = 'default.png';
					}
				?>
				
				<div class="ibox-body text-center">
				   <img src="{{ asset('public/uploads/users/' . Auth::user()->photo ) }}" width='120' height='120' class='img-circle'>
				   <br />
				   <h5 class='mt-3 mb-2 text-danger'>{{ Auth::user()->name }}</h5>
				   <div>{{ Auth::user()->email }}</div>
				   <div>{{ Auth::user()->phone }}</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Ticket Details</div>
					<div class="ibox-tools">
						<div class="ibox-tools">
							<a class="font-18" href="javascript:;"><i class="ti-support"></i></a>
						</div>
					</div>
				</div>
				<div class="ibox-body">
				  
				   
				   <div class='form-group mt-4'>
					
					 <div class="input-group-icon input-group-icon-left">
						<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
						<input name='subject' class="form-control" type="text" required placeholder="Subject..">
					</div>
					
				   </div>
				   
				   <div class='form-group mt-4'>
					<textarea name='comment' rows='4' required class='form-control' placeholder='Comment..'></textarea>
				   </div>
				    <div class='form-group'>
					<button href='support-list.php' class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Create Ticket</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
						</span>
					</button>
				   </div>
				</div>
			</div>
		</div>		
		
		
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection    