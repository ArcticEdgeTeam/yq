@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />

@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Staff</li>
		<li class="breadcrumb-item">New</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<form id='reg_form' class='form-danger' method='post' action="{{ route('venue.staff.save') }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		
		<div class='col-lg-3'>
			<div class="ibox">
				<div class="ibox-body text-center" style='position: relative;'>
					<img id='previewfile' width='150' height='150'  src="{{ asset('public/uploads/users/default.png') }}" class='img-circle'>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Upload Picture' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='photo' class='d-none'>
					</div>
					
				</div>
			</div>
		</div>
		
		
		
	<div class="col-lg-6 p-lg-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">User Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" checked>
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
				
					<div class='col-md-12'>
						<div class='mb-3'>
							<span class='mr-4'><b>Role:</b> </span>
							
							<label id='manager_role' class="radio radio-inline radio-danger">
								<input value='bar' type="radio" name="role">
							<span class="input-span"></span>Prep Staff</label>
							<label id='waiter_role' class="radio radio-inline radio-danger">
								<input value='waiter' type="radio" name="role" checked>
							<span class="input-span"></span>Waiter</label>
						</div>
						
					</div>
				
					
					<!-- <div class='col-md-6'>
						<div class="form-group" id="date_1">
							<label class="font-normal">Date of Birth</label>
							<div class="input-group date">
								<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
								<input autocomplete='off' class="form-control" name='dob' type="text">
							</div>
						</div>
					</div> -->
					
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Staff ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input required name='user_id' class="form-control" type="text">
							</div>
						</div>
					</div>
					
					<!-- <div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input id="autocomplete" onFocus="geolocate()" required name='address' class="form-control" type="text">
							</div>
						</div>
					</div> -->
					
					<!-- 
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Gender</label>
							<br />
							<label class="radio radio-inline radio-danger">
							<input value='male' type="radio" name="gender" checked>
							<span class="input-span"></span>Male</label>
							<label class="radio radio-inline radio-danger">
							<input value='female' type="radio" name="gender" >
							<span class="input-span"></span>Female</label>
						</div>
					</div> -->

					
					<!-- <div id='assign_tables' class="form-group mb-4 col-md-12 d-none">
						<label>Assign Tables</label>
						<select name='tables[]' class="form-control select2_demo_1" multiple="">
							@foreach($tables as $table)
								<option value="{{ $table->id }}">{{ $table->name }}</option>
							@endforeach
						</select>
					</div> -->
					
					<div class="form-group mb-4 col-md-12">
						<label>About User</label>
						<textarea rows='4' name='about' class='form-control'></textarea>
					</div>

					<div class='col-md-12 prep-area' style="display: none;">
						<div class="form-group mb-4">
							<label>Prep Area</label>
							<div class="input-group-icon">
								<select name='bar_id[]' class="selectpicker form-control" multiple>
									@foreach($bars as $bar)
										<option value="{{ $bar->id }}">{{ $bar->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					
			   </div>
			</div>
		</div>
	</div>
	
	
	<div class="col-lg-3">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Login Info</div>
				<div class="ibox-tools">
					<a href='javascript:;'><i class='ti-user'></i></a>
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input required name='name' class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input id='email' name='email' required class="form-control" type="email">
							</div>
							<div id='ajax_msg' style='font-size: 11px;'></div>
						</div>
					</div>
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='password' value="" required class="form-control password" type="password">
							</div>
						</div>
					</div>

					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='confirm_password' value="" required class="form-control confirm_password" type="password">
							</div>
							<span class="text-danger confirm-password-message" style="display: none;">Password not matching!</span>
						</div>
					</div>

					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Telephone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-phone"></i></span>
								<input name='phone' class="form-control" type="text">
							</div>
						</div>
					</div>
					
					
					
					<div class='form-group mt-3 col-lg-12'>
						<button class="btn btn-danger btn-fix btn-animated from-left submit-form">
							<span class="visible-content">Add User</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
							</span>
						</button>
					</div>
			   </div>
			</div>
		</div>
	</div>
		
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>

	$('input[name="role"]').change(function(){
		if($(this).val() == 'bar') {
			$('.prep-area').show();
			// apply required attribute
			$('select[name="bar_id"]').attr('required', true);

		} else {
			$('.prep-area').hide();
			$('select[name="bar_id"]').attr('required', false);
		}
	});

	$('input[name="user_id"]').on('blur', function(){
		var user_id = $(this).val();
		if (user_id != '') {
			$.ajax({
				type: 'get',
				data:{
					user_id: user_id,
					venue_id: '{{\Auth::user()->venue_id}}'
				},
				url: "{{ route('ajax.get-staff-information') }}",
				success: function(msg){
					if(msg['error'] != null) {
						swal("", msg['error'], "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						$('input[name="user_id"]').val('');
					}
				}
					
			});
		}

	});

  // Bootstrap datepicker
    $('#date_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
		format: 'dd-mm-yyyy',
    });
	
</script>
<script>
	$(document).ready(function(){
		$('#manager_role').on('click', function(){
			$('#assign_tables').hide();
		});
		
		$('#waiter_role').on('click', function(){
			$('#assign_tables').show();
		});
		
		$('#email').blur(function(){
			var user_email = $(this).val();
			$.ajax({
				type: 'get',
				url: "{{ route('venue.check.email') }}/" + user_email,
				success: function(msg){
					if(msg != 'valid_email'){
						$('#ajax_msg').html("<span class='text-danger email-valid'>Email Already Exist</span>");
						 //  $("#reg_form").submit(function(e){
							// 	 e.preventDefault();
							// });
							swal("", 'Email already exists, try a new email.', "error");
							$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
					}else{
						// $("#reg_form").submit(function(e){
						// 		e.currentTarget.submit();
						// 	});
						$('#ajax_msg').html("<span class='text-success email-valid'>Valid Email</span>");
					}
				}
			});
			
		});

		$('#reg_form').submit(function(e){
			e.preventDefault();
			var count = 0;
			 	var _password = $('.password').val();
				var _password_confirm = $('.confirm_password').val();
				if(_password != '') {

					if (_password.length < 8) {

						swal("", "Password must be 8 chanracters long.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}

					if(_password != _password_confirm) {
						swal("", "Password Mismatch", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}
					
				}

			 	if (count == 0 && $('.email-valid').text() == 'Valid Email') {
			 		e.currentTarget.submit();
			 	}
		});

		
	});
</script>

<script>
	function PreviewprofileImage(){
       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            } 
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>

<script>
var placeSearch, autocomplete;
function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
  document.getElementById('autocomplete'), {types: ['geocode']});
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection 
