@extends('layouts.venue')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Tables</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Tables</li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>


<div class='page-content fade-in-up'>
	@include('layouts.venue-management-nav')
	<form class='form-danger form-table' method='post' action="{{ route('venue.table.update', $table->id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>

		<div class="col-lg-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					
					<div class="ibox-title">Table Info</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-view-list'></i></a>
						
					</div>
					
				</div>
				<div class="ibox-body">
				   <div class='row'>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table Name</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
									<input required name='name' class="form-control" type="text" value="{{ $table->name }}">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table ID</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
									<input class="form-control" required name='table_id' value="{{ $table->table_id }}" type="text">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>No of Seats</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
									<input class="form-control" required name='seats' value="{{ $table->seats }}" type="number">
								</div>
							</div>
						</div>
						
						
				   </div>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="col-lg-4 p-lg-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Others</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($table->status == 1) checked @endif >
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
				
					<div class='col-md-12'>
						<div>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Smoking:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input type="radio" value='yes' name="smoking" checked="">
										<span class="input-span"></span>Yes</label>
										<label class="radio radio-inline radio-danger">
											<input value='no' type="radio" name="smoking" @if($table->smoking == 'no') checked @endif >
										<span class="input-span"></span>No</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					<div class='col-md-12'>
						<div class='mb-3'>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Area:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input value='inside' type="radio" name="area" checked="">
										<span class="input-span"></span>Inside</label>
										<label class="radio radio-inline radio-danger">
											<input value='outside' type="radio" name="area" @if($table->area == 'outside') checked @endif>
										<span class="input-span"></span>Outside</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Assign Waiter</label>
						<select name='waiters[]' class="form-control selectpicker" multiple>
							@foreach($waiters as $waiter)
								@php
									$selected = '';
									if(DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $waiter->id)->first()){
										$selected = 'selected';
									}
								@endphp
								<option {{ $selected }} value="{{ $waiter->id }}">{{ $waiter->name }}</option>
							@endforeach
						</select>
					</div>


					<div class="form-group mb-4 col-md-12">
						<label>Table Occupied</label>
						<select required name='table_occupied' class="form-control selectpicker">
								<option @if($table->occupied == 0) selected @endif value="0">Not Occupied</option>
								<option @if($table->occupied == 1) selected @endif value="1">Occupied</option>
						</select>
					</div>
				
					
			   </div>
			</div>
		</div>
	</div>
		
		
		
	
	
	<div class="col-md-4">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				<div class="ibox-title">Waiter</div>
				
				<div class="ibox-tools">
					<a href='javascript:;'><i class='ti-user'></i></a>
					
				</div>
				
			</div>
			<div class="ibox-body">
				
				<ul class="media-list media-list-divider mr-2" data-height="470px">
					@foreach($waiters as $waiter)
					@php
						if($waiter->photo == ''){
							$waiter->photo = 'default.png';
						}
						if(!DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $waiter->id)->first()){
							continue;
						}
					@endphp
					
					@php
					$review = DB::table('reviews')
					->select(DB::raw('sum(service_rating) as service_rating, count(id) as total_records'))
					->where('waiter_id', $waiter->id)->first();
					
					if($review->total_records > 0){
						$stars = round($review->service_rating / $review->total_records);
						$rating = round($review->service_rating / $review->total_records, 2);
						$remainig_stars = 5 - $stars;
					}else{
						$stars = 0;
						$rating = 0;
						$remainig_stars = 5 - $stars;
					}
					@endphp
						
					<li class="media align-items-center">
						<a class="media-img" href="javascript:;">
							<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" alt="image" width="50" height='50'/>
						</a>
						<div class="media-body d-flex align-items-center">
							<div class="flex-1">
							<div class="media-heading">{{ $waiter->name }}</div>
							<small title="{{ $rating }}" data-toggle='tooltip' class="text-warning">
							@php
							for($i = 0; $i < $stars; $i++){
								echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
							@endphp
							</small>
							</div>
							<a target='_blank' class='text-warning' href="{{ route('venue.staff.edit', $waiter->id) }}" style='font-size: 22px;'><i class='fa fa-eye'></i></a>
						</div>
					</li>
					@endforeach
				</ul>
				
				<div class='row'>
					<div class='form-group mt-3 col-lg-12'>
						<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Table</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	</div>
	</form>
	
</div>

@endsection
@section('page_plugin_js')
	<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script type="text/javascript">

	 $(document).ready(function() {
        $('.form-table').submit(function(event) {
            event.preventDefault();
            // ajax call
            $.ajax({
               	type: 'GET',
				data:{
					table_id: $('input[name="table_id"]').val(),
					venue_id: '{{\Auth::user()->venue_id}}',
					id: '{{$table->id}}',
					type: 'edit'
				},
				url: "{{ route('ajax.get-table-id') }}",
                success: function(res) {

                    if(res['error'] != null) {
                        // null the input value
                        $('input[name="table_id"]').val('');
                        //show message box
                        swal("", res['error'], "error");
                        $('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
                        return false;

                    } else {
                        event.currentTarget.submit();
                    }
                    
                } //success
            });
        });
    });

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection
