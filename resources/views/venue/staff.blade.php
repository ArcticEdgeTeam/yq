@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Staff</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	
	<div class='ibox p-4'>
		
		<div class="ibox-head">
			<div class="ibox-title"></div>
				<div class="ibox-tools">
					<div class='mr-1 mt-1'>
						<a href="{{ route('venue.staff.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Add Staff</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
							</span>
						</a>
				</div>
			</div>
		</div>
		
		<form action="{{route('venue.staff')}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group col-md-3">
					<label>Role</label>
					<select name='role' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Role</option>
						
							<option @if(request()->role == 'waiter') selected @endif  value="waiter">Waiter</option>
							<option @if(request()->role == 'bar') selected @endif  value="bar">Prep Staff</option>
						
					</select>
				</div>

				<div class="form-group col-md-3">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == '1') selected @endif  value="1">Active</option>
							<option @if(request()->status == '0') selected @endif  value="0">Inactive</option>

					</select>
				</div>

				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		
		
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Role</th>
						<th>Rating</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getStaff->isEmpty())
					@foreach($getStaff as $user)
					@php
						if($user->photo == ''){
							$user->photo = 'default.png';
						}
					@endphp
					<tr>
						<td>{{ $user->user_id }}</td>
						<td><img class='img-circle' src="{{ asset('public/uploads/users/' . $user->photo) }}" width='40' height='40'> {{ $user->name }}</td>
						
						<td>
							@if($user->role == 'waiter')
								<span class="badge badge-danger">Waiter</span>
								@else
								<span class="badge badge-success">Prep Staff</span>
							@endif
							
						</td>
						
						<td class='text-warning'>
						@if($user->role == 'waiter')
						@php
						$review = DB::table('reviews')
						->select(DB::raw('sum(service_rating) as service_rating, count(id) as total_records'))
						->where('waiter_id', $user->id)->first();
						
						if($review->total_records > 0){
							$stars = round($review->service_rating / $review->total_records);
							$rating = round($review->service_rating / $review->total_records, 2);
							$remainig_stars = 5 - $stars;
						}else{
							$stars = 0;
							$rating = 0;
							$remainig_stars = 5 - $stars;
						}
						@endphp
						
						<span class='d-none'>{{ $rating }}</span><span title="{{ $rating }}" data-toggle='tooltip'>
							@php
							for($i = 0; $i < $stars; $i++){
								echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
							@endphp
						</span>
						
						@endif
						</td>
						<td>
							@if($user->status)
								<span class='d-none'>Active</span><i class='fa fa-check-circle text-success' style='font-size: 22px;' title='Active' data-toggle='tooltip'>
							@else
								<span class='d-none'>Inactive</span><i class='fa fa-times-circle text-danger' style='font-size: 22px;' title='Inactive' data-toggle='tooltip'>
							@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('venue.staff.edit', $user->id) }}"><i style='font-size: 22px;' class='fa fa-edit'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="6" style="text-align:center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getStaff->appends(['name' => Request::get('name'), 'role' => Request::get('role'), 'status' => Request::get('status')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>

		</div>
				
	</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection 