@extends('layouts.venue')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection
@section('page_css')
	<style>
	.order_no_box{
		line-height: 1.1; font-weight: 500; margin-bottom: 5px;
	}
	
	.table_box:hover{
		cursor: pointer;
	}
	
	.empty_table{
		margin-bottom: 20px;
		margin-top: 54px;
	}
	
	.empty_table i{
		font-size: 60px;
		color: #ccc;
	}
	
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list .table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.no_shadow{
		box-shadow: none;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
	
	</style>
@endsection

@section('page_content')
	
<div class="page-content fade-in-up">
	
	<div class='row mb-4'>
		<div class='col-lg-12 text-center'>
			
			<a href="{{ route('venue.overview') }}" class="btn btn-danger btn-fix btn-animated from-left">
				<span class="visible-content"><i class="ti-stats-up pr-0 pl-2"></i> <span class='ml-1'>Stats View</span></span>
				<span class="hidden-content">
					<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> View</span>
				</span>
			</a>
		</div>
	</div>
	
	<div class='row'>
		@foreach($orders as $order)
		@php
			$table = DB::table('tables')->where('id', $order->table_id)->first();
			$customer = DB::table('users')->where('id', $order->customer_id)->first();
			if($customer->photo == ''){
				$customer->photo = 'default.png';
			}
			$waiter = DB::table('users')->where('id', $order->waiter_id)->first();
			if($waiter->photo == ''){
				$waiter->photo = 'default.png';
			}
		@endphp
		<div class='col-lg-3'>
			
			<div data-address="{{ route('venue.order.popview', $order->id) }}" class="ibox table_box popview" data-toggle="modal" data-target="#new-event-modal" data-toggle="modal" data-target="#new-event-modal">
				
				<div class="ibox-head">
					<div class="ibox-title">{{ $table->table_id }}</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-layout-grid2-alt'></i></a>
					</div>
				</div>
				
				<div class="ibox-body">
					<div class='order_no_box'>
						Order#: {{ $order->order_number }}
					</div>
					<ul class="media-list media-list-divider mr-2" data-height="580px">
						<li class="media align-items-center">
							<a class="media-img" href="javascript:;">
								<img class="img-circle" src="{{ asset('public/uploads/users/' . $customer->photo) }}" alt="image" width="54" />
							</a>
							<div class="media-body d-flex align-items-center">
								<div class="flex-1">
									<div class="media-heading">{{ $customer->name }}</div><small class="text-muted">Customer</small></div>
								<!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
							</div>
						</li>
						
						<li class="media align-items-center">
							<a class="media-img" href="javascript:;">
								<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" alt="image" width="54" />
							</a>
							<div class="media-body d-flex align-items-center">
								<div class="flex-1">
									<div class="media-heading">{{ $waiter->name }}</div><small class="text-muted">Waiter</small></div>
								<!--<button class="btn btn-sm btn-outline-secondary btn-rounded">Follow</button>-->
							</div>
						</li>
						
					</ul>
					<div class='text-center'>
						@if($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		@endforeach
		
		<div class='col-lg-3 d-none'>
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">tab-003</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-layout-grid2-alt'></i></a>
					</div>
				</div>
				<div class="ibox-body">
					
					<div class='text-center'>
						<div class='empty_table'>
							<i class='ti-view-grid'></i>
						</div>
						<span class="badge badge-primary">Empty</span>
					</div>
				</div>
			</div>
		</div>
		
		<div class='col-lg-12'>
		@if($orders->isEmpty())
			<div class="alert alert-danger alert-bordered">Your active tables with orders will be shown here!</div>
		@endif
		</div>
		
	</div>
	
	
</div>


<!-- New Event Dialog-->
<div class="modal fade" id="new-event-modal" tabindex="-1" role="dialog">
	<div id='quickview_area' class="modal-dialog modal-lg bg-white" role="document">
		
	</div>
</div>
<!-- End New Event Dialog-->

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(".popview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('data-address'),
		   success:function(data){
			$('#quickview_area').html(data);
		   }
		});
	});
</script>
@endsection