@extends('layouts.venue')

@section('page_plugin_css')
<link href="./assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Settings</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<form class='form-danger' method='post' action="{{ route('venue.settings.update') }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		
		<!-- <div class="col-md-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Admin</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
						
						
						<div class="form-group mb-4 col-md-12">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input value="{{ $user->name }}" required class="form-control" type="text" name='name'>
							</div>
						</div>
						<div class="form-group mb-4 col-md-12">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input value="{{ $user->email }}" readonly name='email' class="form-control" type="text">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input class="form-control password" value="" name='password' type="password">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='confirm_password' value="" class="form-control confirm_password" type="password">
							</div>
							<span class="text-danger confirm-password-message" style="display: none;">Password not matching!</span>
						</div>
					

						<div class="form-group mb-4 col-md-12">
							<label>Telephone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
								<input value="{{ $user->phone }}" class="form-control" type="text" name='phone'>
							</div>
						</div>
						
						  
					  
					</div>
				</div>
			</div>
		</div> -->

		@if(\Auth::user()->login_type == 'Mighty Super Venue Admin')
		<div class="col-md-6 pl-20">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Payment Details</div>
					
					<div class="ibox-tools">
						
					</div>
					
					
				</div>
				<div class="ibox-body">
				   
						<div class="form-group mb-5">
							<div>
								<span class='mr-4'><b>Payfast</b></span>
								<!--<label class="radio radio-inline radio-danger">
									<input value='payfast' type="radio" name="prefered_way" checked>
									<span class="input-span"></span>Payfast</label>
								<label class="radio radio-inline radio-danger">
									<input @if($venue->prefered_way == 'bank') checked @endif value='bank' type="radio" name="prefered_way">
									<span class="input-span"></span>By Bank</label>-->
							</div>
						</div>
						
						<div class='row'>
							
							
						<div class="form-group mb-4 col-md-6">
								<label>Payfast Merchant ID</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><img width='16' src="{{ asset('public/assets/payfast-icon.png') }}"></span>
									<input value="{{ $venue->payfast_merchant_id }}" name='payfast_merchant_id' class="form-control" type="text">
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-6">
								<label>Payfast Merchant Key</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><img width='16' src="{{ asset('public/assets/payfast-icon.png') }}"></span>
									<input value="{{ $venue->payfast_merchant_key }}" name='payfast_merchant_key' class="form-control" type="text">
								</div>
							</div>
							
						<!-- 	<div class="form-group mb-4 col-md-6">
								<label>Bank Name</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
									<input value="{{ $venue->bank_name }}" class="form-control" type="text" name='bank_name'>
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-6">
								<label>Bank Account Type</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-key"></i></span>
									<input value="{{ $venue->bank_account_type }}" class="form-control" type="text" name='bank_account_type'>
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-12">
								<label>IBAN</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-world"></i></span>
									<input value="{{ $venue->iban }}" class="form-control" type="text" name='iban'>
								</div>
							</div>
							 -->
							
							
						</div>
   
				</div>
			</div>
		</div>
		
		
		
		<div class="col-md-6 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Other Settings</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-settings"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>

						<div class='col-md-6'>
							<span style="font-size: 11px;"><b>Active</b></span>
						</div>
						
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input type="checkbox" name='status' @if($venue->status == 1) checked @endif >
								<span></span>
							</label>
						</div>
					
						<div class='col-md-6'>
							<span style='font-size: 11px;'><b>Bookings</b></span>
						</div>
						
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input @if($venue->booking == 1) checked @endif name='booking' type="checkbox">
								<span></span>
							</label>
						</div>
						
						
						<div class='col-md-6'>
						<span style="font-size: 11px;"><b>Auto Accept App Booking</b></span>
						</div>
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input type="checkbox" name='booking_auto_accept' @if($venue->booking_auto_accept == 1) checked @endif >
								<span></span>
							</label>
						</div>

						<div class='col-md-12'>
						<span style="font-size: 11px;"><b>Average Booking</b></span>
						</div>
						<div class='col-md-12 text-right clockpicker' data-autoclose="true">
							<input style='padding-left: 6px; padding-right: 6px;' autocomplete='off' class='form-control' type='text' name='average_booking' value="{{ $venue->average_booking }}" required>
						</div>

						<div class='col-md-12 mb-4 mt-4'>
							<span style='font-size: 16px; font-weight: 500;'> Social Links</span>
							<div style='border-bottom: 1px solid #eee;'></div>
						</div>

						
						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-facebook"></i></span>
								<input name='fb_link' class="form-control" value="{{$venue->fb_link}}" type="text" placeholder="https://www.facebook.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
						
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-instagram"></i></span>
								<input name='insta_link' class="form-control"  value="{{$venue->insta_link}}" type="text" placeholder="https://www.instagram.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-twitter"></i></span>
								<input name='twitter_link' class="form-control"  value="{{$venue->twitter_link}}" type="text" placeholder="https://www.twitter.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-link"></i></span>
								<input name='web_link' class="form-control"  value="{{$venue->web_link}}" type="text" placeholder="https://www.xyz.com">
							</div>
						</div>
						
						
						<div class="form-group col-md-6">
							<br>
							<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Venue</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
							</div>
						
					</div>
				</div>
			</div>
		</div>
		@else

		<div class="col-md-6 pl-20">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Other Settings</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-settings"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
					
						<div class='col-md-6'>
							<span style='font-size: 11px;'><b>Bookings</b></span>
						</div>
						
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input @if($venue->booking == 1) checked @endif name='booking' type="checkbox">
								<span></span>
							</label>
						</div>
						
						
						<div class='col-md-6'>
							<span style="font-size: 11px;"><b>Active</b></span>
						</div>
						
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input type="checkbox" name='status' @if($venue->status == 1) checked @endif >
								<span></span>
							</label>
						</div>
						
						
						<div class='col-md-6'>
						<span style="font-size: 11px;"><b>Auto Accept App Booking</b></span>
						</div>
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input type="checkbox" name='booking_auto_accept' @if($venue->booking_auto_accept == 1) checked @endif >
								<span></span>
							</label>
						</div>

						<div class='col-md-12'>
						<span style="font-size: 11px;"><b>Average Booking</b></span>
						</div>
						<div class='col-md-12 text-right clockpicker' data-autoclose="true">
							<input style='padding-left: 6px; padding-right: 6px;' autocomplete='off' class='form-control' type='text' name='average_booking' value="{{ $venue->average_booking }}" required>
						</div>
						
						
						<div class="form-group col-md-12">
							<br>
							<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Venue</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
							</div>
						
					</div>
				</div>
			</div>
		</div>

		@endif
		
	</div>
	</form>
	
	
</div>

@endsection

@section('page_plugin_js')
<script src="./assets/vendors/select2/dist/js/select2.full.min.js"></script>
<script src="./assets/js/scripts/form-plugins.js"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(){

		if ($('input[name="booking"]').prop('checked') == true) {
				$('input[name="booking_auto_accept"]').attr('disabled', false);
				//$('input[name="booking_auto_accept"]').attr('checked', true);
				$('input[name="average_booking"]').attr('disabled', false);
			} else {
				$('input[name="booking_auto_accept"]').attr('disabled', true);
				//$('input[name="booking_auto_accept"]').attr('checked', false);
				$('input[name="average_booking"]').attr('disabled', true);
			}


		$('input[name="booking"]').change(function(){
			if ($(this).prop('checked') == true) {
				$('input[name="booking_auto_accept"]').attr('disabled', false);
				$('input[name="booking_auto_accept"]').attr('checked', true);
				$('input[name="average_booking"]').attr('disabled', false);
			} else {
				$('input[name="booking_auto_accept"]').attr('disabled', true);
				$('input[name="booking_auto_accept"]').attr('checked', false);
				$('input[name="average_booking"]').attr('disabled', true);
			}
		});

			$('.form-danger').submit(function(e) {

			 	// if ($('.password').val() != $('.confirm_password').val()) {
			 	// 	e.preventDefault();
			 	// 	$('.confirm-password-message').css('display', 'block');
			 	// } else {
			 	// 	$('.confirm-password-message').css('display', 'none');
			 	// 	$('.form-danger').submit();
			 	// }

			 		// check URL

			 	e.preventDefault();
				var count = 0;

			 	if ($('input[name="fb_link"]').val() != '') {

		    		var FBurl = /^(http|https)\:\/\/(www.|)facebook.com\/.*/i;
		    		if(!$('input[name="fb_link"]').val().match(FBurl)) {

				      	swal("", "Facebook URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
				    } 
		    	}

		    	if ($('input[name="insta_link"]').val() != '') {

		    		var Instaurl = /^(http|https)\:\/\/(www.|)instagram.com\/.*/i;
		    		if(!$('input[name="insta_link"]').val().match(Instaurl)) {

				      	swal("", "Instagram URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
				    }

		    	}

		    	if ($('input[name="twitter_link"]').val() != '') {

		    		var TWurl = /^(http|https)\:\/\/(www.|)twitter.com\/.*/i;
		    		if(!$('input[name="twitter_link"]').val().match(TWurl)) {

				      	swal("", "Twitter URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
			      	} 
		    	}

		    	if ($('input[name="web_link"]').val() != '') {

		    		var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;

		    		  if(!pattern.test($('input[name="web_link"]').val())){

					        swal("", "Website URL is incorrect.", "error");
							$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
							return false;
					    } 
		    	}

		    	if (count == 0) {
			 		e.currentTarget.submit();

			 		if ($(this).data('submitted') === true) {
			            // Form is already submitted
			            console.log('Form is already submitted, waiting response.');
			            // Stop form from submitting again
			           $('.reg_btn').attr('disabled', true);
			        } else {
			            // Set the data-submitted attribute to true for record
			            $(this).data('submitted', true);
			        }
			        
			 	}
			});
	});

	$('.clockpicker').clockpicker();

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection

@section('page_js')
@endsection
