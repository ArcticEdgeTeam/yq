@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	#child_cat_list{
		display: none;
	}
</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Categories</li>
		<li class="breadcrumb-item">New</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<form class='form-danger form-category' method='post' action="{{ route('venue.category.create') }}" enctype='multipart/form-data'>
		@csrf
		<div class="row">
					
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-body text-center">
					<img id='previewfile' src="{{ asset('public/uploads/categories/default.png') }}" class='img-fluid'>
					<div class='row'>
						<div class="col-sm-12">
							<label class='btn btn-link text-danger m-0' for='uploadfile'>Upload Pic</label>
							<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' name='image' class="form-control p-2 d-none" style='height: 37px;' type="file" />
							<div class='text-success'>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Category Info</div>
					<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' value='1' type="checkbox" checked>
						<span></span>
					</label>
					
				</div>
				</div>
				<div class="ibox-body">
					
					
							<div class='row'>
								<div class='col-md-6'>
									<div class="form-group mb-4">
										<div class="input-group-icon input-group-icon-left">
											<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
											<input class="form-control" required name='name' type="text"  placeholder="Category Name">
										</div>
									</div>
								</div>
								<div class='col-md-6'>
									
									<div class="form-group mb-5">
                                        <div>
                                            <label onclick="document.getElementById('child_cat_list').style.display='none';" class="radio radio-inline radio-danger">
                                                <input id='parent_radio' type="radio" value='0' name="has_parent" checked>
                                                <span class="input-span"></span>Parent</label>
                                           
										   <label onclick="document.getElementById('child_cat_list').style.display='block';" class="radio radio-inline radio-danger">
                                                <input id='child_radio' type="radio" value='1' name="has_parent">
                                                <span class="input-span"></span>Child</label>
                                        </div>
                                    </div>

                                      <div class="form-group mb-5" id='child_cat_list'>
										<select id='category_id' name='category_id' class='selectpicker form-control'>
											@foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->name }}</option>
											@endforeach
										</select>
									</div>
									
									<!-- <div class="form-group mb-5" id='child_cat_list'>
										<select id='category_id' name='category_id' class='selectpicker form-control'>
											@foreach($categories as $category)
												<option value="{{ $category->id }}">{{ $category->name }}</option>
												@foreach ($category->childrenCategories as $childCategory)
													@include('child_category', ['child_category' => $childCategory])
												@endforeach
											@endforeach
										</select>
									</div> -->
									
								</div>
							</div>
							
							<div class='row'>
								<div class='col-md-6'>
								</div>
								<div class='col-md-6'>
									<div class='form-group'>
										<button class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Add Category</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
											</span>
										</button>
								   </div>
							   </div>
						   </div>
				     
				</div>
			</div>
		</div>
		
	</div>
	</form>
</div>
@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>

		$(document).ready(function() {
		$('.form-category').submit(function(event) {
			event.preventDefault();
			// ajax call
			$.ajax({
				type: 'GET',
				data:{
					name: $('input[name="name"]').val(),
					id: '{{\Auth::user()->venue_id}}',
					type: 'add',
				},
				url: "{{ route('ajax.get-category-info') }}",
				success: function(res) {

					if(res['error'] != null) {
						// null the input value
						$('input[name="name"]').val('');
						//show message box
						swal("", res['error'], "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;

				    } else {
						event.currentTarget.submit();
					}
					
				} //success
			});
		});
	});

	$(document).ready(function(){
		
		$('#child_radio').click(function() {
			if($(this).is(':checked')) { 
				$('#category_id').prop('required', true);
		   }
		});
		
		$('#parent_radio').click(function() {
		   if($(this).is(':checked')) {
			   $('#category_id').prop('required', false);
		   }
		});

		
	});
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection