@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/core/main.css')}}" rel='stylesheet' />
<link href="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/daygrid/main.css')}}" rel='stylesheet' />
<link href="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/timegrid/main.css')}}" rel='stylesheet' />
<link href="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages-premium/timeline/main.css')}}" rel='stylesheet' />
<link href="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages-premium/resource-timeline/main.css')}}" rel='stylesheet' />

<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />

@endsection

@section('page_css')
<style>

   body {
    overflow: hidden;
  }

  #calendar {
    max-width: 1000px;
    margin: 0px auto;
  }

  html {
  	overflow-y:visible
  }

  #ui-datepicker-div { z-index: 2000; }

</style>
@endsection

@section('page_content')

@php
if (request()->search_date != '') {
	$date = request()->search_date;
} else {
	$date = date("Y-m-d");
}
@endphp


<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Bookings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Booking View</li>
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>

<div class='page-content fade-in-up'>
@include('layouts.venue-venue-nav')

	<div class="row">
		<div class="col-md-4"> 
			<span class="badge" style="background-color: green;">Completed</span>
			<span class="badge" style="background-color: blue;">Booked</span>
			<span class="badge" style="background-color: orange;">Pending</span>
			<!-- <span class="badge" style="background-color: red;">Cancelled</span> -->
				
		</div>

		<div class="col-md-4">
			<a href="{{ route('venue.bookings') }}" class="btn btn-danger"> Go Back to List View</a>
				
		</div>

		<div class="col-md-4" style="margin-top: -20px;">
			<form method='get' action="{{ route('venue.table-booking-view', $venue->id) }}">
			   <div class="form-group" id="date_1" >
					<label class="font-normal"></label>
					<div class="input-group">
						<span class="input-group-addon bg-white search_date"><i class="fa fa-calendar"></i></span>
						<input name='search_date' class="form-control search_date" type="text" value="{{$date}}">
						<button class='btn btn-danger'><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>

		</div>

	</div>
	<!-- row ends -->
	<div id='calendar'></div>

	<!-- Update Booking Modal -->
	<div class="modal fade update-bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Update Booking</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	     <form class='form-danger update-booking-form' method='post' action="#" enctype='multipart/form-data'>
	      <div class="modal-body">
	      
			@csrf
				<div class='row'>
				<div class="col-xl-12">
					<div class="ibox ">
						<div class="ibox-head">
							<div class="ibox-title update-booking-info">Booking Info</div>
							
							<div class="ibox-tools">
								
							</div>
						</div>
						<p class="alert alert-warning update-error-msg" style="display: none;"></p>
						<div class="ibox-body">
							<div class='row'>
								
								<!-- 
								<div class="form-group mb-4 col-md-6">
									<label>Name</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
										<input min='0' required name='name' type='text' class='form-control' disabled="disabled">
									</div>
								</div>
								
								
								<div class="form-group mb-4 col-md-6">
									<label>Phone</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
										<input min='0' required name='phone' type='text' class='form-control' disabled="disabled">
									</div>
								</div>
							    -->
							   <div class="form-group mb-4 col-md-12">
									<label>Assign Table</label>
									<select required name='table_id[]' class="form-control" id="update-selectpicker" multiple style="height: 150px;">
										<option value=''>Select Table</option>
										@foreach($bookingTables as $tab)
											<option value="{{ $tab->id }}">{{ $tab->name }}</option>
										@endforeach
									</select>
								</div>
								
								<div class="form-group mb-4 col-md-12">
									<label class="font-normal">Date & Time</label>
									<div class="input-group date form_datetime" >
										<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
										<input autocomplete='off' required name='date_time' class="form-control">
									</div>
								</div>
								
								<div class="form-group mb-4 col-md-12">
									<label>Seating</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
										<input min='0' required name='seating' type='number' class='form-control seating'>
									</div>
								</div>

								<!-- <div class="form-group mb-4 col-md-6">
									<label>Smoking</label>
									<select required name='smooking' class="selectpicker form-control" id="update-smooking">
										<option value='yes'>Yes</option>
										<option value='no'>No</option>
									</select>
								</div>

								<div class="form-group mb-4 col-md-12">
									<label>Area</label>
									<select required name='area' class="form-control area" id="update-area">
										<option value='inside'>Inside</option>
										<option value='outside'>Outside</option>
									</select>
								</div> -->
								
								<div class="form-group mb-4 col-md-12">
									<label>Message</label>
									<textarea name='message' rows='3' class='form-control'> </textarea>
									
								</div>

								<div class="form-group mb-4 col-md-12">
									<label>Status</label>
									<select name="booking_status" class="form-control" required>
										
										<option value="booked">Booked</option>
										<option value="pending">Pending</option>
										<option value="cancelled">Cancelled</option>
									</select>
								</div>
								<!-- <input type="hidden" name="booking_id" class="booking_id" value=""> -->
								<input type="hidden" name="booking_page" value="booking_calendar">
							</div>
						</div>
					</div>
				</div>

				</div>
			

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Update Booking</button>
	      </div>
	    </form>
	    </div>
	  </div>
	</div>
	<!-- Modal Ends -->

</div>

@endsection
@section('page_plugin_js')
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/core/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/interaction/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/daygrid/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages/timegrid/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages-premium/timeline/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages-premium/resource-common/main.js')}}"></script>
<script src="{{asset('public/assets/vendors/fullcalendar-scheduler-4.4.0/packages-premium/resource-timeline/main.js')}}"></script>

<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
@endsection

@section('page_js')
<script>

  document.addEventListener('DOMContentLoaded', function() {
  	var tables = `<?= $tables ?>`;
  	var bookings = `<?= $bookings ?>`;
  	tables = JSON.parse(tables);
  	bookings = JSON.parse(bookings);

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {

    	  height: 300,
    	  contentHeight: 300,
    	  schedulerLicenseKey: '0669690818-fcs-1589096117',
      
	      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'resourceTimeline' ],
	      now: '{{date("Y-m-d")}}',
	      editable: true, // enable draggable events
	      aspectRatio: 1.8,
	      scrollTime: '{{date("H:i")}}', // undo default 6am scrollTime
	      // header: {
		     //    left: 'today prev,next',
		     //    center: 'title',
		     //    right: 'resourceTimelineDay,dayGridMonth'
		     //  },
	     
	      defaultView: 'resourceTimelineDay',
	      views: {
	        resourceTimelineThreeDays: {
	          type: 'resourceTimeline',
	          duration: { days: 3},
	          buttonText: '3 days'
	        }
	      },
	   
	       resourceAreaWidth: '150px',
	      resourceLabelText: 'Tables',
	      resources: tables,
	      events: bookings,
	       eventRender: function (info) {
				$(info.el).tooltip({ title: info.event.extendedProps.description});
			},

			 resourceRender: function (renderInfo) {
			 	
	            $(renderInfo.el).tooltip({ title: renderInfo.resource.extendedProps.occupied});
	            //renderInfo.el.style.backgroundColor = 'blue';
	            renderInfo.el.style.color = renderInfo.resource.extendedProps.color;

	            $(renderInfo.el).find(".fc-cell-text").prepend("<i class='fa "+renderInfo.resource.extendedProps.icon+"'> </i>");

	        },

	        eventClick: function(info) {
			// var route = "{{route('admin.venue.booking.edit', [$venue->id, ":id"])}}";
			//    route = route.replace(':id',info.event.id);
			//    window.location.href = route;
			// ajax call

				$('.update-bd-example-modal-lg').modal('show');
				// hide error message
				$('.update-error-msg').css('display', 'none');

				
				var form_route = "{{ route('venue.booking.update', [":id"]) }}";
				form_route = form_route.replace(':id',info.event.id);
				$('.update-booking-form').attr('action', form_route);
				
				// booking id
				$('.booking_id').val(info.event.id);

				$.ajax({
					type: 'GET',
					data:{
						id: '{{\Auth::user()->venue_id}}',
						booking_id: info.event.id
					},
					url: "{{ route('ajax.get-booking-information') }}",
					success: function(res){
						$('.update-booking-form input[name="name"]').val(res.booking.name);
						$('.update-booking-form input[name="phone"]').val(res.booking.phone);
						// $(".update-booking-form .selectpicker option[value='"+res.table_id+"']").prop('selected' , true);
						//$('#update-selectpicker option[value='+res.table_id+']').attr('selected','selected');
						$("#update-selectpicker").val(res.bookingTables).change();

						$('.update-booking-form input[name="date_time"]').val(res.booking.date_time);
						$('.update-booking-form input[name="seating"]').val(res.booking.seating);
						$('.update-booking-form textarea[name="message"]').val(res.booking.message);

						// $("#update-smooking > option").each(function() {
							
						// 	if($(this).val() == res.smooking) {
						// 		$(this).attr("selected","selected").change();
						// 	}

						// });

						// $("#update-area > option").each(function() {
							
						// 	if($(this).val() == res.area) {	
						// 		$(this)[0].attr("selected","selected");
						// 		$("#update-area").find('option[value="<required-value>"]').attr('selected','selected')
						// 	}

						// });

						// $(".update-booking-form .area > option").each(function() {
						// 	//$('.update-booking-form .area').empty();
						// 	if($(this).val() == res.area) {	
						// 		$(this).attr('selected','selected');
								
						// 	}

						// });

						if(res.booking.booking_status == 'completed') {
							$("select[name='booking_status']").prepend('<option value="completed" selected>Completed</option>');
							$("select[name='booking_status']").prop('disabled', true);
						} else if (res.booking.booking_status == 'cancelled') {
							$("select[name='booking_status']").val('cancelled');
							$("select[name='booking_status']").prop('disabled', true);

						} else {
							$("select[name='booking_status']").prop('disabled', false);
							$("select[name='booking_status']").val(res.booking.booking_status);
						}
						
					}
				});

		  	},
    });

    calendar.gotoDate( '{{$date}}' );
    calendar.render();
  });



</script>
<script>

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});

	$(".update-booking-form .form_datetime").datetimepicker({
			format: 'yyyy-mm-dd hh:ii',
			startDate:'+0d',
			autoclose: true,
			// onClose: function() {
	  //       	$(this).trigger('change');
	  //   	}
	});

	$('.search_date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
		format: 'yyyy-mm-dd',
		orientation: 'bottom'
    });
</script>
@endsection