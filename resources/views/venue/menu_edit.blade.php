@extends('layouts.venue')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
	<!-- <link href="{{ asset('public/assets/vendors/multiselect-checbox/styles/multiselect.css') }}" rel="stylesheet" />
	<script src="{{ asset('public/assets/vendors/multiselect-checbox/multiselect.min.js') }}"></script> -->
	<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')

@endsection
@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Menus</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venues</li>
		<li class="breadcrumb-item">Menus</li>
		<li class="breadcrumb-item">Edit</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
@include('layouts.venue-management-nav')
	<form class='form-danger form-menu' method='post' action="{{ route('venue.menu.update', $menu->id) }}" enctype='multipart/form-data'>
	@csrf
	@if($menu->image == '')
		<?php
			$menu->image = 'default.png';
		?>
	@endif
		<div class="row">
					
		<div class='col-md-3'>
			<div class="ibox ibox-fullheight">
				<div class="ibox-body text-center">
					<img id='previewfile' src="{{ asset('public/uploads/menus/' . $menu->image) }}">
					<div class='row'>
						<div class="col-sm-12 mt-2">
							<label class='btn btn-link text-danger m-0' for='uploadfile'>Change Pic</label>
							<input name='image' class="form-control p-2 d-none" style='height: 37px;' type="file" accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' />
							<div>
							@if($menu->status == 1)
								<span class="badge badge-success">Active</span>
							@else
								<span class="badge badge-danger">Inactive</span>
							@endif
							
						</div>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Menu Info</div>
					<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($menu->status == 1) checked @endif>
						<span></span>
					</label>
					
				</div>
				</div>
				<div class="ibox-body">
					
							<div class='row'>
								<div class='col-md-12'>
									<div class="form-group mb-4">
										<div class="input-group-icon input-group-icon-left">
											<label>Menu Name</label>
											<span class="input-icon input-icon-left"><i class="ti-tag" style="padding-top: 25px;"></i></span>
											<input class="form-control" name='name' required type="text" value="{{ $menu->name }}" placeholder="menu Name">
										</div>
									</div>
								</div>


								<div class="form-group mb-4 col-md-12">
									<label>Products</label>
									<select id='select2_demo_1' class="form-control select2_demo_1" name='products[]' multiple>
									 	@foreach($products as $product)
										 @php
											$selected = '';
											if(DB::table('menu_products')->where('product_id', $product->id)->where('menu_id', $menu->id)->first()){
												$selected = 'selected';
											}
										@endphp
										<option {{$selected}} value="{{ $product->id }}" checked>{{ $product->name }}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group mb-4 col-md-4" style="margin-left: 20px;">
									<div class="form-group mb-5">
                                       <div class="form-check">
									    	<input type="checkbox" class="form-check-input" id="exampleCheck1" name="default" value="1"  @if($menu->default == 1) checked @endif>
									    	<label class="form-check-label" for="exampleCheck1">Make Default</label>
								  		</div>
                                    </div>
								</div>
							</div>

								@if(!$categories->isEmpty())
							  <!-- ordering row -->
                        <div class="content" style="margin-top: 20px;">
                            <h3 class="text-center">Change Categories Order</h3>
                           <div class="row" style="margin-left: 200px;">
                            @foreach($categories as $key => $category)

                            @php
                                $key = $key + 1;
                            @endphp
                    
                                <div class='col-md-4'>
                                    <div class="form-group mb-4">
                                        <div class="input-group-icon input-group-icon-left">
                                            
                                            <input class="form-control" name='cat_name' type="text"  placeholder="Category Name" value="{{$category->name}}" style="padding-left: 10px;" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class='col-md-4'>
                                    <div class="form-group mb-4">
                                        <div class="input-group-icon input-group-icon-left">

                                            <select class="form-control parent_category" name="parent_cat_order[{{$category->id}}]"  style="padding-left: 10px;">
                                                @php
                                                for($i=1; $i <= count($categories); $i++) {
                                                @endphp
                                                <option value="{{$i}}" @if ($category->order == $i) selected @endif>{{$i}}</option>
                                                @php 
                                                }
                                                @endphp
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                               

		                            @if ($category->has_childs == 'yes')

		                                @foreach($category->child as $index => $child)
		                                @php
		                                    $index = $index + 1;
		                                    $child = explode('/', $child);
		                                @endphp

		                                <div class="col-md-1"></div>
		                                <div class='col-md-3'>
		                                    <div class="form-group mb-4">
		                                        <div class="input-group-icon input-group-icon-left">
		                                            
		                                            <input class="form-control" name='child_cat_name' type="text"  placeholder="Category Name" value="{{$child[1]}}" style="padding-left: 10px;" readonly>
		                                        </div>
		                                    </div>
		                                </div>

		                               <div class='col-md-3'>
		                                    <div class="form-group mb-4">
		                                        <div class="input-group-icon input-group-icon-left">
		                                            <select class="form-control" name="child_cat_order[{{$category->id}}][{{$child[0]}}]" style="padding-left: 10px;">
		                                                @php
		                                                for($j=1; $j <= count($category->child); $j++) {
		                                                @endphp
		                                                <option @if ($child[2] == $j) selected @endif value="{{$j}}">{{$j}}</option>
		                                                @php 
		                                                }
		                                            @endphp
		                                            </select>
		                                        </div>
		                                    </div>
		                                </div>

		                                <div class="col-md-5"></div>

		                                @php
		                                    $index = $index++;
		                                @endphp

		                                @endforeach
		                            @endif

		                             @php
		                                $key = $key++;
		                            @endphp
		                        
		                        @endforeach
		                       </div>
		                    </div>   
		                    <!-- ordering ends -->
							@endif
							
							<div class='row'>
								<div class='col-md-10'>
								</div>
								<div class='col-md-2'>
									<div class='form-group'>
										<button class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Update Menu</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
											</span>
										</button>
								   </div>
							   </div>
						   </div>
				     
				</div>
			</div>
		</div>
		
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
 <script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
 <script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script> 
@endsection

@section('page_js')

<script>
		$(".select2_demo_1").select2({
	    	closeOnSelect: false
		});
</script>

<script>

	 $(document).ready(function() {
        $('.form-menu').submit(function(event) {
            event.preventDefault();
            // ajax call
            $.ajax({
                type: 'GET',
                data:{
                    name: $('input[name="name"]').val(),
                    menu_id: '{{$menu->id}}',
                    id: '{{\Auth::user()->venue_id}}',
                    type: 'edit',
                },
                url: "{{ route('ajax.get-menu-info') }}",
                success: function(res) {

                    if(res['error'] != null) {
                        // null the input value
                        $('input[name="name"]').val('');
                        //show message box
                        swal("", res['error'], "error");
                        $('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
                        return false;

                    } else {
                        event.currentTarget.submit();
                    }
                    
                } //success
            });
        });
    });

	$(document).ready(function(){
		
		$('#child_radio').click(function() {
			if($(this).is(':checked')) { 
				$('#category_id').prop('required', true);
		   }
		});
		
		$('#parent_radio').click(function() {
		   if($(this).is(':checked')) {
			   $('#category_id').prop('required', false);
		   }
		});

		
	});
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
    
    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection