@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Categories</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
@include('layouts.venue-management-nav')
	<div class='ibox p-4'>
		<div class="flexbox mb-4">
			<div class="flexbox">
				
				<div class="form-group">
					<a href="{{ route('venue.category.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Category</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Sr.No</th>
						<th>Category Name</th>
						<th>Parent</th>
						<th>Products</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($categories as $category)
					@if($category->image == '')
						<?php
							$category->image = 'default.png';
						?>
					@endif
					@php
					$products = DB::table('category_products')->where('category_id', $category->id)->count();
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img src="{{ asset('public/uploads/categories/' . $category->image) }}" class='img-circle' height='50' width='50'> {{ $category->name }}</td>
						<td>@if($category->category_id == null) <span class="badge badge-danger">SELF</span> @else 
							<?php
								
								$cat = DB::table('categories')->where('id', $category->category_id)->first();
								if($cat->image == ''){
									$cat->image = 'default.png';
								}
								
							?>
							<img src="{{ asset('public/uploads/categories/' . $cat->image) }}" class='img-circle' height='50' width='50'>
							<?php
								echo $cat->name;
							?>
							
							@endif</td>
						<td>{{ number_format($products) }}</td>
						<td>
							@if($category->status == 1)
								<span class='d-none'>Active</span><i style='font-size: 22px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
							@else
								<span class='d-none'>Inactive</span><i style='font-size: 22px;' data-toggle='tooltip' title='Inactive' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
							@endif
						</td>
						<td>
							<span data-html='true' class='text-info' data-toggle='tooltip' title="Created at: {{ $category->created_at->format('d M Y') }}<br />Updated at: {{ $category->updated_at->format('d M Y') }}</b>"><i style='font-size: 22px;' class='fa fa-info'></i></span>
							
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('venue.category.edit', $category->id) }}"><i style='font-size: 22px;' class='fa fa-edit'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
				
				
			
	</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection