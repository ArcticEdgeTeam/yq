@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Ingredients & Units</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>

<div class="page-content fade-in-up">
@include('layouts.venue-management-nav')
<div class='row'>
<div class='col-md-6'>
<div class="form-group text-right">
	<a href="{{ route('venue.ingredient.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
		<span class="visible-content">Add Ingredient</span>
		<span class="hidden-content">
			<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
		</span>
	</a>
</div>
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				<h4>Ingredients</h4>
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Code</th>
						<th>Name</th>
						
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($ingredients as $ingredient)
					<tr>
						<td>{{ $ingredient->code }}</td>
						
						<td>
						@if($ingredient->status == 1)
							<span class='d-none'>Active</span><a class='text-success' data-toggle='tooltip' title='Active' href='#'><i style='font-size: 22px;' class='fa fa-check-circle'></i></a>
						@else
							<span class='d-none'>Inactive</span><a class='text-danger' data-toggle='tooltip' title='Inactive' href='#'><i style='font-size: 22px;' class='fa fa-times-circle'></i></a>
						@endif
						{{ $ingredient->name }}
						</td>
						
						<td>
							<span data-html='true' class='text-info' data-toggle='tooltip' title="Created at: {{ $ingredient->created_at->format('d M Y') }}<br />Updated at: {{ $ingredient->updated_at->format('d M Y') }}"><i style='font-size: 22px;' class='fa fa-info'></i></span>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('venue.ingredient.edit', $ingredient->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>



<div class='col-md-6 pl-0'>
<div class="form-group text-right">
	<a href="{{ route('venue.unit.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
		<span class="visible-content">Add Unit</span>
		<span class="hidden-content">
			<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
		</span>
	</a>
</div>
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
			<h4>Units</h4>
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search2" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable2">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Symbol</th>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($units as $unit)
					<tr>

						<td>{{ $unit->symbol }}</td>
			
						<td>
						@if($unit->status == 1)
							<span class='d-none'>Active</span><a class='text-success' data-toggle='tooltip' title='Active' href='#'><i style='font-size: 22px;' class='fa fa-check-circle'></i></a>
						@else
							<span class='d-none'>Inactive</span><a class='text-danger' data-toggle='tooltip' title='Inactive' href='#'><i style='font-size: 22px;' class='fa fa-times-circle'></i></a>
						@endif
						{{ $unit->name }}
						</td>
						
						
						<td>
							<span data-html='true' class='text-info' data-toggle='tooltip' title="Created at: {{ $unit->created_at->format('d M Y') }}<br />Updated at: {{ $unit->updated_at->format('d M Y') }}"><i style='font-size: 22px;' class='fa fa-info'></i></span>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('venue.unit.edit', $unit->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>


</div>
</div>
@endsection

@section('page_plugin_js')

@endsection

@section('page_js')
<script type="text/javascript">
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection