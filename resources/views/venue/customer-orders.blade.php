@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Customers</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.customers') }}">Customers</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
		@include('layouts.venue-customer-nav')
		
		<div class='ibox p-4'>
		
		<div class="flexbox mb-4">
			<div class="flexbox">
				
				<!-- <div class="form-group">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input class="form-control" id="daterange_1" type="text">
						<button class='btn btn-danger'>Filter</button>
					</div>
				</div>
				 -->
			</div>
		
			<div class='text-left'>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Order No</th>
						
						<th>Waiter</th>
						<th>Status</th>
						<th>Table</th>
						<th>Price</th>
						<th>Date</th>
						<th>Time</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				@foreach($orders as $order)
				@php
					$venue = DB::table('venues')->where('id', $order->venue_id)->first();
					if($venue->banner == ''){
						$venue->banner = 'default.png';
					}
					$waiter = DB::table('users')->where('id', $order->waiter_id)->first();
					if($waiter->photo == ''){
						$waiter->photo = 'default.png';
					}
					$table = DB::table('tables')->where('id', $order->table_id)->first();
				@endphp
				<tr>
					<td>{{ $counter++ }}</td>
					<td>#{{ $order->order_number }}</td>
					<td><img src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="40" height="40" class="img-circle"> {{ $waiter->name }}</td>
					
					<td>
					@if($order->order_status == 'Completed')
					<span class="badge badge-success">Completed</span>
					@elseif($order->order_status == 'Ordered')
						<span class="badge badge-danger">Ordered</span>
					@elseif($order->order_status == 'In Oven')
						<span class="badge badge-warning">In Oven</span>
					@elseif($order->order_status == 'Final Steps')
						<span class="badge badge-info">Final Steps</span>
					@elseif($order->order_status == 'Refunded')
						<span class="badge badge-danger">Refunded</span>
					@endif
					</td>
					
					<td>{{ $table->name }}</td>
					<td>{{ $currency }} {{ number_format($order->total_amount, 2) }}</td>
					<td>{{ $order->created_at->format('d M Y') }}</td>
					<td>{{ $order->created_at->format('H:i') }}</td>
					<td>
						<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('venue.order.edit', $order->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
				
				
			
	</div>
		
		
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
@endsection

@section('page_js')
@endsection