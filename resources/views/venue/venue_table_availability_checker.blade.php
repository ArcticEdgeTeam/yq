@extends('layouts.venue')

@section('page_plugin_css')

@endsection

@section('page_css')
<style>
	
</style>
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Bookings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.table-availability-checker') }}">Availability Checker</a></li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
@include('layouts.venue-venue-nav')
	<div class='ibox p-4'>
		<div class="flexbox mb-4">
			<div class="flexbox">
				
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Check</th>
						<!-- <th>Sr.No</th> -->
						<th>Table Name</th>
						<th>Table ID</th>
						<th>Seats</th>
						<th>Smooking</th>
						<th>Area</th>
					</tr>
				</thead>
				<tbody>
					@php
						$counter = 1;
					@endphp
					@foreach($getTables as $table)
						@if($table->bookingSlot == '')
							
							<tr>
								<td>
									<input type="checkbox" name="check-table[]" value="{{$table->id}}" id="{{$table->seats}}" class="form-control check-table" onchange="getSelectedTables(this)">
								</td>
								<!-- <td>
									{{$counter}}
								</td> -->
								<td>
									{{$table->name}}
								</td>
								<td>
									{{$table->table_id}}
								</td>
								<td>
									<span class="badge badge-pill badge-secondary">{{$table->seats}}</span>
								</td>
								<td>
									@if ($table->smoking == 'yes')
										<span class="badge badge-pill badge-success">{{$table->smoking}}</span>
									@else
										<span class="badge badge-pill badge-danger">{{$table->smoking}}</span>
									@endif
									
								</td>
								<td>
									@if ($table->area == 'inside')
										<span class="badge badge-pill badge-success">{{$table->area}}</span>
									@else
										<span class="badge badge-pill badge-danger">{{$table->area}}</span>
									@endif
									
								</td>
							</tr>
							@php
								$counter ++;
							@endphp
						@endif
					@endforeach
					
				</tbody>
			</table>
		</div>
				
		<div class="ibox-tools">
		<button class="btn btn-danger confirm-booking">Add Booking</button>
		</div>		
			
	</div>

</div>

<!-- ADD Booking Modal -->
	<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  		<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Add New Booking</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	     <form class='form-danger add-booking-form' method='post' action="{{ route('venue.booking.save') }}" enctype='multipart/form-data'>
	      <div class="modal-body">
	      
			@csrf
				<div class='row'>
				<div class="col-xl-12">
					<div class="ibox ">
						<div class="ibox-head">
							<div class="ibox-title">Booking Info</div>
							
							<div class="ibox-tools">
								
							</div>
						</div>
						<p class="alert alert-warning error-msg" style="display: none;"></p>
						<div class="ibox-body">
							<div class='row'>
								

								<div class="form-group mb-4 col-md-6">
									<label>Name</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
										<input min='0' required name='name' type='text' class='form-control' >
									</div>
								</div>
								
								
								<div class="form-group mb-4 col-md-6">
									<label>Phone</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
										<input min='0' required name='phone' type='text' class='form-control' >
									</div>
								</div>
							   
							   <div class="form-group mb-4 col-md-6">
									<label>Assign Table</label>
									<select required name='table' class="selectpicker form-control" multiple disabled>
										<option value=''>Select Table</option>
										@foreach($bookingTables as $tab)
											<option value="{{ $tab->id }}">{{ $tab->name }}</option>
										@endforeach
									</select>
									<input type="hidden" name="table_id" value="">
								</div>
								
								<div class="form-group mb-4 col-md-6">
									<label class="font-normal">Date & Time</label>
									<div class="input-group date form_datetime" >
										<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
										<input autocomplete='off' required name='date_time' class="form-control" value="{{request()->table_date_time}}" readonly>
									</div>
								</div>
								
								<div class="form-group mb-4 col-md-12">
									<label>Seating</label>
									<div class="input-group-icon input-group-icon-left">
										<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
										<input min='0' required name='seating' type='number' class='form-control seating'>
									</div>
								</div>

								<!-- <div class="form-group mb-4 col-md-6">
									<label>Smoking</label>
									<select required name='smooking' class="selectpicker form-control smooking">
										<option value='yes'>Yes</option>
										<option value='no'>No</option>
									</select>
								</div>

								<div class="form-group mb-4 col-md-12">
									<label>Area</label>
									<select required name='area' class="selectpicker form-control area">
										<option value='inside'>Inside</option>
										<option value='outside'>Outside</option>
									</select>
								</div> -->
								
								<div class="form-group mb-4 col-md-12">
									<label>Message</label>
									<textarea name='message' rows='3' class='form-control'> </textarea>
									
								</div>
								<input type="hidden" name="booking_page" value="booking_calendar">
								<input type="hidden" name="availability_check_page" value="availability_check_page">
							</div>
						</div>
					</div>
				</div>

				</div>
			

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Add Booking</button>
	      </div>
	    </form>
	    </div>
	  </div>
	</div>
	<!-- Modal Ends -->

@endsection
@section('page_plugin_js')

@endsection

@section('page_js')
<script>
  var selectedTables = [];
	var seats = 0;

	function getSelectedTables(data) {
	
		if (data.checked == true) {
			selectedTables.push(data.value);
			seats = parseFloat(seats) + Number(data.id);
		} else {
			// remove value from array
			var index = selectedTables.indexOf(data.value);
			if (index !== -1) selectedTables.splice(index, 1);
			//selectedTables.pop(data.value);
			seats = parseFloat(seats) - Number(data.id);
		}
	}

	$('.confirm-booking').click(function() {
		if (selectedTables == '') {
			alert("Please select table first to proceed for booking.");
		} else {
			// hide error message
			$('.error-msg').css('display', 'none');
			// populate the selected tables
			$('select[name="table"]').val(selectedTables).change();
			// assign value
			$('input[name="table_id"]').val(JSON.stringify(selectedTables));
			// set the seats value
			$('.seating').val(seats);

			$('.bd-example-modal-lg').modal('show');
		}
	});

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection