@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Ingredients & Units</li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
@include('layouts.venue-management-nav')
<form class='form-danger form-unit' method='post' action="{{ route('venue.unit.update', $unit->id) }}" enctype='multipart/form-data'>
@csrf
<div class="row">
	
	<div class="col-xl-12">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Unit Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($unit->status == 1) checked @endif >
						<span></span>
					</label>
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					<div class='col-md-6'>
						<label>Unit</label>
						<div class="form-group mb-4">
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
								<input required name='name' class="form-control" type="text" value="{{ $unit->name }}" placeholder="Unit Name">
							</div>
						</div>
					</div>
					<div class='col-md-2'>
						 <div class="form-group mb-4">
							<label>Symbol</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
								<input class="form-control " value="{{ $unit->symbol }}" required name='symbol' type="text" placeholder="Unit Symbol">
							</div>
						</div>
					</div>
					
					<div class='col-md-2'>
						<div class='form-group'>
							<label>&nbsp;</label>
							<br />
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Unit</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
			   </div>
			</div>
		</div>
	</div>
	
</div>
</form>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script type="text/javascript">

	$(document).ready(function() {
		$('.form-unit').submit(function(event) {
			event.preventDefault();
			// ajax call
			$.ajax({
				type: 'GET',
				data:{
					name: $('input[name="name"]').val(),
					symbol: $('input[name="symbol"]').val(),
					unit_id: '{{$unit->id}}',
					id: '{{\Auth::user()->venue_id}}',
					type: 'edit',
				},
				url: "{{ route('ajax.get-unit-info') }}",
				success: function(res) {

					if(res['error_name'] != null) {
						// null the input value
						$('input[name="name"]').val('');
						//show message box
						swal("", res['error_name'], "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;

					} else if (res['error_symbol'] != null) {

						// null the input value
						$('input[name="symbol"]').val('');
						//show message box
						swal("", res['error_symbol'], "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;

					} else {
						event.currentTarget.submit();
					}
					
				} //success
			});
		});
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection