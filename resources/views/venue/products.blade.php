@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.venue')

@section('page_plugin_css')
	
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('venue.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Products</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	<div class='ibox p-4'>
		
		<div class="ibox-head">
			<div class="ibox-title"></div>
				<div class="ibox-tools">
					<div class='mr-1 mt-1'>
					<a href="{{ route('venue.product.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Product</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
					</div>
			</div>
		</div>

		<form action="{{route('venue.products')}}" method="GET" class="mt-3 mb-3">

			<div class="row">

				<div class="form-group col-md-4">
					<label>ID</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='product_id' class="form-control" type="text" placeholder="Search by ID..." value="{{request()->product_id}}">
						
					</div>
				</div>
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group col-md-4">
					<label>Prep Area</label>
					<select name='prep_id' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Prep Area</option>
						@foreach($getPrepAreas as $prep)
							<option @if(request()->prep_id == $prep->id) selected @endif value="{{ $prep->id }}">{{ $prep->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Category</label>
					<select name='category_id' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Category</option>
						@foreach($getCategories as $category)
							<option @if(request()->category_id == $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-4">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == '1') selected @endif  value="1">Active</option>
							<option @if(request()->status == '0') selected @endif  value="0">Inactive</option>
						
					</select>
				</div>

				

				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>

		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Prep Area</th>
						<th>Category</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getProducts->isEmpty())
					@foreach($getProducts as $product)
					@php
						
						$product->image = $product->image == '' ? 'default.png' : $product->image;
						$product->bar_image = $product->bar_image == '' ? 'default.png' : $product->bar_image;
						
					@endphp
					<tr>
						<td>{{ $product->product_id }}</td>
						<td><img class='img-circle' src="{{ asset('public/uploads/products/' . $product->image) }}" width='50' height='50'> {{ $product->name }}</td>
						<td><img class='img-circle' src="{{ asset('public/uploads/bars/' . $product->bar_image) }}" width='50' height='50'> {{ $product->bar_name }}</td>
						<td>{{ $product->category_name }}</td>
						<td>{{ $currency }} {{ number_format($product->price,2) }}</td>
						<td>
							@if($product->status == 1)
								<span class='d-none'>Active</span><i class='fa fa-check-circle text-success' data-toggle='tooltip' title='Active' style='font-size: 22px;'></i>
							@else
								<span class='d-none'>Inctive</span><i class='fa fa-times-circle text-danger' data-toggle='tooltip' title='Inctive' style='font-size: 22px;'></i>
							@endif
							
							@if($product->product_error == 1)
							<span data-html='true' class='text-danger' style='font-size: 22px;' data-toggle="tooltip" title="Please assign valid category to the product!"><i class='fa fa-exclamation-triangle'></i></span>
							@endif
						</td>
						<td>
							<span data-html='true' class='text-info' style='font-size: 22px;' data-toggle="tooltip" title="Created at: {{ $product->created_at->format('d M Y') }}<br />Updated at: {{ $product->updated_at->format('d M Y') }}"><i class='fa fa-info'></i></span>
							<a class='text-warning' style='font-size: 22px;' data-toggle="tooltip" title='Edit' href="{{ route('venue.product.edit', [$product->id]) }}"><i class='fa fa-edit'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="7" style="text-align:center;">No record found.</td>
					</tr>
					@endif
					
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getProducts->appends(['name' => Request::get('name'), 'product_id' => Request::get('product_id'), 'status' => Request::get('status'), 'prep_id' => Request::get('prep_id'), 'category_id' => Request::get('category_id')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>

		</div>	
			
	</div>
</div>

@endsection
@section('page_plugin_js')
	
@endsection

@section('page_js')
<script type="text/javascript">
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});
	});
</script>
@endsection