@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />

<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Trail Logs</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.trail_logs') }}">Trail Logs</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<form action="{{route('venue.trail_logs')}}" method="GET">

			<div class="row">
			
				<div class="form-group col-md-5">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text" value="{{request()->date_range}}">
						
					</div>
				</div>

				<div class="form-group mb-4 col-md-5">
					<label>Users</label>
					<select name='user' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select User</option>
						@foreach($users as $user)
							<option @if(request()->user == $user->id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>User</th>
						<th>Role</th>
						<th>Event</th>
						<th>Note</th>
						<th>Venue</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getLogs->isEmpty())
					@foreach($getLogs as $log)
					<tr>
						<td>{{$log->user_name}}</td>
						<td>{{$log->user_role}}</td>
						<td>
							@if($log->event_type == 'Venue Closed' || $log->event_type == 'Venue Opened' || $log->event_type == 'Venue Information Updated')
								<a href="{{route('venue.information')}}"> {{$log->event_type}} </a>
							
							@elseif($log->event_type == 'Staff Added')
								<a href="{{route('venue.staff')}}"> {{$log->event_type}} </a>
							
							@elseif($log->event_type == 'Staff Updated')
								<a href="{{route('venue.staff.edit', $log->event_id)}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Venue Settings Updated')
								<a href="{{route('venue.settings')}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Product Added')
								<a href="{{route('venue.products')}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Product Updated')
								<a href="{{route('venue.product.edit', $log->event_id)}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Category Added')
								<a href="{{route('venue.categories')}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Category Updated')
								<a href="{{route('venue.category.edit', $log->event_id)}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Menu Added')
								<a href="{{route('venue.menus')}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Menu Updated')
								<a href="{{route('venue.menu.edit', $log->event_id)}}"> {{$log->event_type}} </a>

							@elseif($log->event_type == 'Push Notification Added')
								<a href="{{route('venue.push-notification.edit', $log->event_id)}}"> {{$log->event_type}} </a>

							@else
								{{$log->event_type}}
							@endif
						</td>
						<td>
							@if($log->event_type == 'Venue Closed' || $log->event_type == 'Venue Opened' || $log->event_type == 'Venue Information Updated')
								<a href="{{route('venue.information')}}"> {{$log->event_message}} </a>

							@elseif($log->event_type == 'Staff Added')
								<a href="{{route('venue.staff')}}"> {{$log->event_message}} </a>
							
							@elseif($log->event_type == 'Staff Updated')
								<a href="{{route('venue.staff.edit', $log->event_id)}}"> {{$log->event_message}} </a>

							@elseif($log->event_type == 'Venue Settings Updated')
								<a href="{{route('venue.settings')}}"> {{$log->event_message}} </a>

							@elseif($log->event_type == 'Product Added')
								<a href="{{route('venue.products')}}"> {{$log->event_message}} </a>

							@elseif($log->event_type == 'Product Updated')
								<a href="{{route('venue.product.edit', $log->event_id)}}"> {{$log->event_message}} </a>

							@elseif($log->event_type == 'Category Added')
								<a href="{{route('venue.categories')}}">  {{$log->event_message}} </a>

							@elseif($log->event_type == 'Category Updated')
								<a href="{{route('venue.category.edit', $log->event_id)}}">  {{$log->event_message}}  </a>

							@elseif($log->event_type == 'Menu Added')
								<a href="{{route('venue.menus')}}"> {{$log->event_message}}  </a>

							@elseif($log->event_type == 'Menu Updated')
								<a href="{{route('venue.menu.edit', $log->event_id)}}"> {{$log->event_message}} </a>


							@elseif($log->event_type == 'Push Notification Added')
								<a href="{{route('venue.push-notification.edit', $log->event_id)}}"> {{$log->event_message}} </a>

							@else
								{{ $log->event_message }}
							@endif

						</td>
						<td>{{$log->venue_name}}</td>
						<td>{{$log->created_at}}</td>
					</tr>

					@endforeach
					@else
					<tr>
						<td colspan="6" style="text-align: center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	              {{ $getLogs->appends(['date_range' => Request::get('date_range'), 'user' => Request::get('user')])->links('pagination::default') }}
	        	</ul>
			</div>

		</div>
	</div>
</div>
</div>
@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	autoUpdateInput: false,
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / "
	}
  });

  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

</script>
@endsection