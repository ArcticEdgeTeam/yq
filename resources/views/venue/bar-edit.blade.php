@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item">Prep Area</li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>


<div class="page-content fade-in-up">
	@include('layouts.venue-management-nav')
	
	<form class='form-danger form-bar' method='post' action="{{ route('venue.bar.update', $bar->id) }}" enctype='multipart/form-data'>
	@csrf
	<div class="row">
		
	<div class='col-md-3'>
		<div class="ibox ibox-fullheight">
			<div class="ibox-body text-center" style='position: relative;'>
				<img id='previewfile' width='150' height='150'  src="{{ asset('public/uploads/bars/' . $bar->image) }}" class='img-circle'>
				<div style='position: absolute; right: 20px; top: 20px;'>
					<label data-toggle='tooltip' title='Change Logo' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
					<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='image' class='d-none'>
				</div>
				
				<div class='row'>
					<div class="col-sm-12 mt-4">
						<h5><span class='text-danger'>{{ $bar->name }}</span></h5>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xl-9 pl-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Prep Area Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($bar->status == 1) checked @endif >
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Prep Area Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
								<input required name='name' value="{{ $bar->name }}" class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Prep Area ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input class="form-control" type="text" required name='bar_id' value="{{ $bar->bar_id }}">
							</div>
						</div>
					</div>
			   </div>
			   
			   <div class='row'>
					<!-- <div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Assign Manager</label>
							<div class="input-group-icon">
								<select name='user_id' required class="selectpicker form-control">
									@foreach($managers as $manager)
										@php
										if(DB::table('bars')->where('user_id', $manager->id)->count() > 0 && $manager->id != $bar->user_id){
											continue;
										}
										@endphp
										
										<option @if($manager->id == $bar->user_id) selected @endif value="{{ $manager->id }}">{{ $manager->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div> -->
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<label>&nbsp;</label>
							<br />
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Prep Area</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
			   </div>
			</div>
		</div>
	</div>
	
</div>
	</form>
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')

<script>

	 $(document).ready(function() {
        $('.form-bar').submit(function(event) {
            event.preventDefault();
            // ajax call
            $.ajax({
                type: 'GET',
				data:{
					bar_id: $('input[name="bar_id"]').val(),
					venue_id: '{{\Auth::user()->venue_id}}',
					id: '{{$bar->id}}',
					type: 'edit'
				},
				url: "{{ route('ajax.get-prep-id') }}",
                success: function(res) {

                    if(res['error'] != null) {
                        // null the input value
                        $('input[name="bar_id"]').val('');
                        //show message box
                        swal("", res['error'], "error");
                        $('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
                        return false;

                    } else {
                        event.currentTarget.submit();
                    }
                    
                } //success
            });
        });
    });


	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection 