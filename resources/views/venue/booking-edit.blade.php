@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.venue')

@section('page_plugin_css')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.sweet-alert h2{
		margin-top: 30px;
	}
</style>
@endsection

@section('page_content')	
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Bookings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item">Venue</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.bookings') }}">Bookings</li></a>
		<li class="breadcrumb-item">Edit</li>
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>

<div class='page-content fade-in-up'>
@include('layouts.venue-venue-nav')
<form class='form-danger' method='post' action="{{ route('venue.booking.update', $booking->id) }}" enctype='multipart/form-data'>
@csrf
	<div class='row'>
		<div class="col-xl-4">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Customer Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div class="ibox-body text-center">
				   @if ($customer == null)
				    	<img src="{{ asset('public/uploads/users/default.png') }}" width='120' height='120' class='img-circle'>
				   		<br />
				   		<h5 class='mt-3 text-danger'>{{ $booking->name }}</h5>
				   @else
					   <img src="{{ asset('public/uploads/users/'. $customer->photo) }}" width='120' height='120' class='img-circle'>
					   <br />
					  
					   <h5 class='mt-3 text-danger'>{{ $customer->name }}</h5>

					   <div class='text-success mt-2'>
					   <i data-toggle='tooltip' title='Lifetime Spent' class="fa fa-money" aria-hidden="true"></i> {{ $currency }} {{ number_format(DB::table('transactions')->where('customer_id', $customer->id)->where('transaction_status', 'Completed')->sum('amount'), 2) }}
					   </div>
					   
					   <div class='mt-2'>
						{{ $customer->email }}
					   </div>
					   
					   <div class='mt-2'>
						{{ $customer->phone }}
					   </div>
						
					   <div class='mt-2'>
					   <a target='_blank' href="{{ route('venue.customer.edit', $customer->id) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
					   </div>
				   @endif
				</div>
			</div>
		</div>
		
		<div class="col-xl-8 pl-0">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Booking Info</div>
					
					<!-- <div class="ibox-tools">
						@if($booking->booking_status == 'Pending')
						<a style="font-size: 14px;" onclick="return false;" href="{{ route('admin.venue.booking.accept', [$venue->id, $booking->id]) }}" class="text-white btn btn-sm btn-danger btn-fix btn-animated from-left sweet-7">
							<span class="visible-content">Accept</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Accept</span>
							</span>
						</a>
						<a style="font-size: 14px;" onclick="return false;" href="{{ route('admin.venue.booking.cancel', [$venue->id, $booking->id]) }}" class="text-white btn btn-sm btn-danger btn-fix btn-animated from-left sweet-6">
							<span class="visible-content">Reject</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Reject</span>
							</span>
						</a>
					@endif
					</div> -->
				</div>
				<div class="ibox-body">
					<div class='row'>
					   <div class="form-group mb-4 col-md-12">
							<label>Assign Table</label>
							<select name='table_id[]' class="selectpicker form-control" @if($booking->booking_status == 'completed') disabled @endif required multiple>
								<option value=''>Select Table</option>
								@foreach($tables as $table)
									@php
										if (in_array($table->id, $bookingTables)) {
											$selected = "selected";
										} else {
											$selected = "";
										}
									@endphp
									<option {{ $selected }}  value="{{ $table->id }}">{{ $table->name }}</option>
								@endforeach
							</select>
						</div>
						
						<!-- <div class="form-group mb-4 col-md-6">
							<label>Status</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-settings"></i></span>
								<input type='text' value="{{ $booking->booking_status }}" class='form-control' readonly>
							</div>
						</div> -->

						<!-- <div class="form-group mb-4 col-md-6">
							<label>Date</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-calendar"></i></span>
								<input type='text' value="{{ date('d M Y', strtotime($booking->date_time)) }}" class='form-control' @if($booking->booking_status == 'completed') disabled @endif>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Time</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-time"></i></span>
								<input type='text' value="{{ date('H:i:s', strtotime($booking->date_time)) }}" class='form-control' @if($booking->booking_status == 'completed') disabled @endif>
							</div>
						</div> -->

						<div class="form-group mb-4 col-md-12">
							<label class="font-normal">Date & Time</label>
							<div class="input-group date form_datetime" >
								<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
								<input autocomplete='off' required name='date_time' class="form-control" value="{{ $booking->date_time }}" @if($booking->booking_status == 'completed') disabled @endif>
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Seating</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input min='0' name='seating' type='number' value="{{ $booking->seating }}" class='form-control seating' @if($booking->booking_status == 'completed') disabled @endif required>
							</div>
						</div>

						<!-- <div class="form-group mb-4 col-md-6">
							<label>Smoking</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-cloud"></i></span>
								<input type='text' name='smooking' value="{{ $booking->smooking }}" class='form-control' @if($booking->booking_status == 'completed') disabled @endif required>
							</div>
						</div> -->
						<!-- <div class="form-group mb-4 col-md-12">
							<label>Area</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-map-alt"></i></span>
								<input type='text' value="{{ $booking->area }}" class='form-control' @if($booking->booking_status == 'completed') disabled @endif required>
							</div>
						</div> -->

						<!-- <div class="form-group mb-4 col-md-6">
							<label>Smoking</label>
							<select name='smooking' class="selectpicker form-control smooking" @if($booking->booking_status == 'completed') disabled @endif required>
								<option @if ($booking->smooking == 'yes') selected @endif value='yes'>Yes</option>
								<option @if ($booking->smooking == 'no') selected @endif  value='no'>No</option>
							</select>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Area</label>
							<select name='area' class="selectpicker form-control area" @if($booking->booking_status == 'completed') disabled @endif required>
								<option @if ($booking->area == 'inside') selected @endif value='inside'>Inside</option>
								<option @if ($booking->area == 'outside') selected @endif  value='outside'>Outside</option>
							</select>
						</div> -->
						
						<div class="form-group mb-4 col-md-12">
							<label>Message</label>
							<textarea name='message' rows='3' class='form-control' @if($booking->booking_status == 'completed') disabled @endif>{{ $booking->message }}</textarea>
							
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Status</label>
							<select name="booking_status" class="form-control" @if($booking->booking_status == 'completed' || $booking->booking_status == 'cancelled') disabled @endif required>
								@if($booking->booking_status == 'completed')
								<option selected value="completed">Completed</option>
								@endif
								<option @if($booking->booking_status == 'booked') selected @endif value="booked">Booked</option>
								<option @if($booking->booking_status == 'pending') selected @endif value="pending">Pending</option>
								<option @if($booking->booking_status == 'cancelled') selected @endif value="cancelled">Cancelled</option>
							</select>
						</div>
						
						
						
						<div class="form-group col-md-12 mb-4">
							<button class="btn btn-danger btn-fix btn-animated from-left" @if($booking->booking_status == 'completed') disabled @endif>
								<span class="visible-content">Update Booking</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
					   </div>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</form>
	
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
@endsection

@section('page_js')
<script>

	$(".form_datetime").datetimepicker({
			format: 'yyyy-mm-dd hh:ii',
			startDate:'+0d',
			autoclose: true,
			// onClose: function() {
	  //       	$(this).trigger('change');
	  //   	}
	});
	
	 $('.sweet-6').click(function(){
        swal({
            title: "Are you sure?",
            text: "Cancel this booking",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn-warning',
            confirmButtonText: 'Yes',
			cancelButtonText: 'No',
            closeOnConfirm: false,
        },function(isConfirm){
          if(isConfirm){
            window.location = $('.sweet-6').attr('href');
          }
        });
    });
	
	$('.sweet-7').click(function(){
        swal({
            title: "Are you sure?",
            text: "Accept this booking",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn-warning',
            confirmButtonText: 'Yes',
			cancelButtonText: 'No',
            closeOnConfirm: false,
        },function(isConfirm){
          if(isConfirm){
            window.location = $('.sweet-7').attr('href');
          }
        });
    });

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$venue->id}}'
				},
				url: "{{ route('venue.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection