@extends('layouts.venue')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Push Notifications</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('venue.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('venue.push-notifications') }}">Push Notifications</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	
	<div class="row">
		
		<div class="col-xl-12">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Notification Details</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-bell'></i></a>
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
						@if($push_notification->push_notification_status == 'Scheduled')
						<div class="form-group col-md-12 text-right">
							<a onclick="return confirm('Are you sure want to cancel this notification?')" href="{{ route('venue.push-notification.cancel', $push_notification->id) }}" class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Cancel Notification</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Cancel</span>
							</span>
							</a>
						</div>
						@endif
						<div class="form-group col-md-12">
							<label style='display: inline-block; width: 60px;'>Status</label>
							@if($push_notification->push_notification_status == 'Scheduled')
								<span class='label label-warning'>{{ $push_notification->push_notification_status }}</span>
							@elseif($push_notification->push_notification_status == 'Cancelled')
								<span class='label label-danger'>{{ $push_notification->push_notification_status }}</span>
							@else
								<span class='label label-success'>{{ $push_notification->push_notification_status }}</span>
							@endif
						</div>
						
						
						<div class="form-group col-md-12">
							<label style='display: inline-block; width: 60px;'>Sent to</label>
							<span class='label label-warning'>{{ $push_notification->sending_to }}</span>
						</div>
						@if($push_notification->push_notification_status == 'Scheduled')
						<div class="form-group mb-4 col-md-12">
							<label>Sending at</label>
							<div class="input-group date form_datetime">
								<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
								<input readonly id='date_time' value="{{ $push_notification->sending_date_time }}" required name='sending_date_time' class="form-control">
							</div>
						</div>
						@endif
						<div class="form-group mb-4 col-md-12">
							<label>Subject</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
								<input readonly required name='subject' type='text' class='form-control' value="{{ $push_notification->subject }}">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Body</label>
							<div class="form-group">
								<textarea readonly rows='3' name='body' class='form-control' required>{{ $push_notification->body }}</textarea>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<a href="{{ route('venue.push-notifications') }}" class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Go Back</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-angle-double-left pr-0 pl-2"></i> Back</span>
							</span>
							</a>
						</div>
						
				   </div>
				    
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection