@extends('layouts.venue')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">
	<h1 class="page-title">Reports</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item">Home</li>
		<li class="breadcrumb-item">Reports</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<form method='post' action="{{ route('venue.report.export') }}">
	@csrf
	<div class='row'>
		<div class='col-lg-3 col-md-2'></div>
		<div class='col-lg-6 col-md-8'>
			<div class='ibox p-4'>
				<div class='ibox-body'>
					
					<div class="form-group">
						<label>Specify Date Range</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input required name='date_range' class="form-control" id="daterange_1" type="text">
						</div>
					</div>
					
					<div class="form-group mt-4">
						<label class="radio radio-inline radio-danger">
							<input value="earnings" type="radio" name="report" checked="">
							<span class="input-span"></span>Earnings Report</label>
						<label class="radio radio-inline radio-danger">
							<input value="sales" type="radio" name="report">
							<span class="input-span"></span>Sales Report</label>
					</div>
					
					<div class="form-group">
						<label>Venue Name</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-building"></i></span>
							<input required name='venue_name' value="{{ $venue->name }}" class="form-control" type="text" disabled>
						</div>
					</div>
						
					
					<div class="form-group mt-4">
						<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Download</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Download</span>
							</span>
						</button>
				   </div>
					
					
				</div>
			</div>
		</div>
		<div class='col-lg-3 col-md-2'></div>
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
@endsection

@section('page_js')
@section('page_js')

<script>
$(document).ready(function(){
	
	function download_csv(csv, filename) {
			var csvFile;
			var downloadLink;

			// CSV FILE
			csvFile = new Blob([csv], {type: "text/csv"});

			// Download link
			downloadLink = document.createElement("a");

			// File name
			downloadLink.download = filename;

			// We have to create a link to the file
			downloadLink.href = window.URL.createObjectURL(csvFile);

			// Make sure that the link is not displayed
			downloadLink.style.display = "none";

			// Add the link to your DOM
			document.body.appendChild(downloadLink);

			// Lanzamos
			downloadLink.click();
		}

		function export_table_to_csv(html, filename) {
			var csv = [];
			var rows = document.querySelectorAll("table tr");
			
			for (var i = 0; i < rows.length; i++) {
				var row = [], cols = rows[i].querySelectorAll("td, th");
				
				for (var j = 0; j < cols.length; j++) 
					row.push(cols[j].innerText);
				
				csv.push(row.join(","));		
			}

			// Download CSV
			download_csv(csv.join("\n"), filename);
		}

		document.querySelector(".btn-dangerr").addEventListener("click", function () {
			var html = document.querySelector("table").outerHTML;
			export_table_to_csv(html, "table.csv");
		});

});
</script>
@endsection
