@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-content fade-in-up">
	<form class='form-danger' method='post' action="{{ route('order.save') }}">
	@csrf
	<div class="row">
	<div class="col-xl-12">
		<div class="ibox ibox-fullheight">
			<div class="ibox-body">
			   <div class='row'>
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<label>Order#</label>
							<input class='form-control' readonly type='number' value="{{ $order_number }}" name='order_number' />
						</div>
					</div>
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<label>Payment Type</label>
							<input class='form-control' readonly type='text' value="Visa" name='payment_type' />
						</div>
					</div>
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<label>Venue</label>
							<input type='hidden' name='venue_id' value="{{ $venue->id }}">
							<input class='form-control' readonly type='text' value="{{ $venue->name }}" name='venue_name' />
						</div>
					</div>
					
					@php
						$products = DB::table('products')->where('status', 1)->where('venue_id', $venue->id)->get();
					@endphp
					<div class='col-md-12'>
					<div class='mb-4'>Select Products</div>
					@foreach($products as $product)
						<div class="form-group col-md-12">
							<div class="mb-2">
								<label for="product-{{ $product->id }}" class="checkbox checkbox-inline">
									<input onclick="updateAmount(this);" data-product-price="{{ $product->price }}" value="{{ $product->id }}" name='products[]' id="product-{{ $product->id }}" type="checkbox">
									<span class="input-span"></span>{{ $product->name }} | <span class='text-danger'>{{ $currency }} {{ $product->price }}</span>
								</label>
							</div>
						</div>
					@endforeach
					</div>
					<div class='col-md-6 mt-3'>
						 <div class="form-group mb-4">
							<label>Customer</label>
							<input type='hidden' name='customer_id' value="{{ $customer->id }}">
							<input class='form-control' readonly type='text' value="{{ $customer->name }}" name='customer_name' />
						</div>
					</div>
					
					<div class='col-md-6 mt-3'>
						 <div class="form-group mb-4">
							<label>Table</label>
							<select class='form-control' name='table_id'>
								@foreach($tables as $table)
								<option value="{{ $table->id }}">{{ $table->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class='col-md-6 d-none'>
						 <div class="form-group mb-4">
							<label>Order Status</label>
							<select class='form-control' name='order_status'>
								<option>Completed</option>
							</select>
						</div>
					</div>
					
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<label>Total Amount</label> <span class='text-danger'>{{ $currency }}</span>
							<input readonly class='form-control' id='total_amount' name='total_amount' type='number' step='any' min='0' required value='0'>
						</div>
					</div>
					
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<label>Waiter Tip</label>
							<input class='form-control' name='waiter_tip' type='number' step='any' min='0' required value='0'>
						</div>
					</div>
					
					<div class='col-md-12'>
						<div class="form-group">
							<strong>Reserved Fund:</strong> <span class='text-danger'>{{ $currency }} {{ $customer_reserved_fund }}</span>
						</div>
					</div>
					
					<div class="form-group col-md-12">
						<div class="mb-2">
							<label class="checkbox checkbox-inline">
								<input onclick="reservedPayment(this);" value="{{ $customer_reserved_fund }}" name='reserved_payment' type="checkbox">
								<span class="input-span"></span> Pay from reserve fund
							</label>
						</div>
					</div>
					
					<div class='col-md-12'>
						<div class="form-group">
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Create Order</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
								</span>
							</button>
					   </div>
					</div>
			   </div>
			</div>
		</div>
	</div>
	
</div>
</form>
</div>


@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
	<script>
		function updateAmount(_this){
			_total_amount = parseInt($('#total_amount').val());
			
			if($(_this).prop('checked') == true){
				$('#total_amount').val(_total_amount+parseInt($(_this).attr('data-product-price')));
			}else{
				$('#total_amount').val(_total_amount-parseInt($(_this).attr('data-product-price')));
			}
			
		}
		
		function reservedPayment(_this){
			_total_amount = parseInt($('#total_amount').val());
			
			if(_total_amount > parseInt($(_this).val())){
				alert('Amount limit exceeded from reserve funds');
				$(_this).prop('checked', false);
			}
			
		}
	</script>
@endsection