<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Strawberry Pop | login</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link rel="icon" href="{{ asset('public/assets/logo.png') }}" type="image/png" sizes="16x16">
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <link href="{{ asset('public/assets/css/main.min.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <style>
        body {
            background-repeat: no-repeat;
            background-size: cover;
            background-image: url("{{ asset('public/assets/img/blog/17.jpg') }}");
        }

        .cover {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: rgba(117, 54, 230, .1);
        }

        .login-content {
            max-width: 400px;
            margin: 50px auto 10px auto;
			background: rgba(0,0,0,0.8);
			color: #fff;
        }
		
		.form-control-line{
			padding: .65rem 1.25rem;
		}


        .auth-head-icon {
            position: relative;
            height: 60px;
            width: 60px;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            font-size: 30px;
            background-color: #fff;
            color: #5c6bc0;
            box-shadow: 0 5px 20px #d6dee4;
            border-radius: 50%;
            transform: translateY(-50%);
            z-index: 2;
        }
		
		
    </style>
</head>

<body>
    <div class="cover"></div>
    <div class="ibox login-content">
       
       <form class="ibox-body" id="login-form" method="POST" action="{{ route('password.email') }}">
			@csrf
			<div class='text-center'>
				<img width='100' src="{{ asset('public/assets/logo.png') }}" />
			</div>
            <h4 class="font-strong text-center mb-5 mt-5">FORGOT PASSWORD</h4>
			<p class="mb-4 text-center">Enter your email address below and we'll send you password reset instructions.</p>
			@if ($errors->any())
				<div class="text-danger text-center">
					
					@foreach ($errors->all() as $error)
							<p class='m-0 mb-3'>{{ $error }}</p>
					@endforeach
				</div>
			@endif
			
			@if (session('status'))
				<div class="text-success text-center">
					<p class='m-0 mb-3'>{{ session('status') }}</p>
				</div>
			@endif
			
			
            <div class="form-group mb-5">
                <input class="form-control form-control-line" type="text" name="email" placeholder="Email">
            </div>
           
            <div class="text-center mb-4">
                <button class="btn btn-danger btn-rounded btn-block">SEND RESET LINK</button>
            </div>
			
			<div class="text-center mb-4">
				<a href="{{ route('login') }}" class="btn btn-danger btn-rounded btn-block">GO BACK TO LOGIN</a>
			</div>
        </form>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- CORE PLUGINS-->
    <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-idletimer/dist/idle-timer.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <script src="{{ asset('public/assets/js/app.min.js') }}"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script>
        $(function() {
            $('#login-form').validate({
                errorClass: "help-block",
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    }
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
            });
        });
    </script>
</body>

</html>

{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}