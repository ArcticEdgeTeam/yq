@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Reserved Payments</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.reservedpayments') }}">Reserved Payments</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<!--<div class="flexbox">
				<a href="waiter-create.php" class="btn btn-success btn-fix">
					<span class="btn-icon">Add new</span>
				</a>
			</div>-->
			<div></div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Customer</th>
						<th>Order No</th>
						<th>Venue</th>
						<th>Order Amount</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($reservedpayments as $reservedpayment)
					@php
					$customer = DB::table('users')->where('id', $reservedpayment->customer_id)->first();
					$venue = DB::table('venues')->where('id', $reservedpayment->venue_id)->first();
					$order = DB::table('orders')->where('id', $reservedpayment->order_id)->first();
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td>{{ $customer->name }}</td>
						<td><a class='text-danger' href="{{ route('admin.venue.order.edit', [$venue->id, $order->id]) }}" target='_blank'>#{{ $order->order_number }}</a></td>
						<td>{{ $venue->name }}</td>
						<td>{{ $currency }} {{ $reservedpayment->amount }}</td>
						<td>
							@if($reservedpayment->reserved_payment == 2)
							<span class='label label-success'> Paid </span>
							@else
							<span class='label label-danger'> Pending </span>
							@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.reservedpayment.view', $reservedpayment->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection