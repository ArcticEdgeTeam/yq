@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Push Notifications</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.push-notifications') }}">Push Notifications</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				<div class="form-group">
					<a href="{{ route('admin.push-notification.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Create Push Notification</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
						</span>
					</a>
			   </div>
				
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Sent by</th>
						<th>Sent to</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($push_notifications as $push_notification)

					<tr>
						<td>{{ $counter++ }}</td>
						<td>{{ $push_notification->subject }}</td>
						<td><span class="badge badge-success">
							{{ ucfirst(DB::table('users')->where('id', $push_notification->created_by)->first()->role) }}
						</span></td>
						<td><span class="badge badge-warning">{{ ucfirst($push_notification->sending_to) }}</span></td>
						<td>
							@if($push_notification->push_notification_status == 'Sent')
								<span class='d-none'>Sent</span><i style='font-size: 22px;' data-toggle='tooltip' title='Sent' class="fa fa-check-circle text-success" aria-hidden="true"></i>
							@elseif($push_notification->push_notification_status == 'Cancelled')
								<span class='d-none'>Cancelled</span><i style='font-size: 22px;' data-toggle='tooltip' title='Cancelled' class="fa fa-ban text-danger" aria-hidden="true">
							@else
								<span class='d-none'>Scheduled</span><i style='font-size: 22px;' data-toggle='tooltip' title='Scheduled' class="fa fa-calendar-check-o text-warning" aria-hidden="true">
							@endif
						</td>
						<td>
							<span class='text-info' data-html='true' data-toggle='tooltip' title='Created at: {{ $push_notification->created_at->format('d M Y H:i:s') }}<br />Sending at: {{ date('d M Y H:i:s', strtotime($push_notification->sending_date_time)) }}'><i style='font-size: 22px;' class='fa fa-info'></i></span>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.push-notification.edit', $push_notification->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection