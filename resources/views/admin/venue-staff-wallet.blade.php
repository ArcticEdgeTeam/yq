@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
<style>
	.static-widget{
		height: 76px;
		line-height: 22px;
		width: auto !important;
		min-width: 120px;
	}
	
	.static-widget h4{
		height: 26px;
	}
</style>
@endsection

@section('page_content')


<!-- The Modal -->
<div class="modal" id="payment_form_modal">
<div class="modal-dialog">
  <div class="modal-content">
	<!-- Modal body -->
	<div class="modal-body">
		<form class='form-danger' method='post' action="{{ route('admin.venue.staff.payment.create', [$venue->id, $user->id]) }}">
		@csrf
		  <div class='form-group'>
			<label>Amount to Pay</label>
			<input name='amount' required min='1' type='number' max="{{ $total_tip_amount - $total_tip_paid }}" class='form-control'>
		  </div>
		  <div class='form-group'>
		  <input type="submit" class="btn btn-success" value='Pay Money'>
		   <span  class="btn btn-danger" data-dismiss="modal">Cancel</span>
		  </div>
	  </form>
	</div>
  </div>
</div>
</div>

<div class="static-widget bg-danger text-white" style='position: fixed; z-index: 30; right: 0; top: 84px;'>
	<h4 class='m-0'>
	{{ $currency }}  {{ $total_tip_amount - $total_tip_paid }}
	</h4>
	<small>Wallet Balance</small>
</div>

<div class="static-widget2" style='position: fixed; z-index: 30; right: 0; top: 160px;'>
	<button data-toggle="modal" data-target="#payment_form_modal" class='btn btn-success btn-block'>Pay Money</button>
</div>



<div class="page-heading">
	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.staff', $id) }}">Staff</a></li>
		<li class="breadcrumb-item">Wallet</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<div class='row'>
		<div class='col-md-4'>
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Waiter Info</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				<div class="ibox-body text-center">
					<img width='150' height='150'  src="{{ asset('public/uploads/users/' . $user->photo) }}" class='img-circle'>
					<div class='row'>
						<div class="col-sm-12 mt-3">
							@if($user->role == 'waiter')	
							@php
							$review = DB::table('reviews')
							->select(DB::raw('sum(service_rating) as service_rating, count(id) as total_records'))
							->where('waiter_id', $user->id)->first();
							
							if($review->total_records > 0){
								$stars = round($review->service_rating / $review->total_records);
								$rating = round($review->service_rating / $review->total_records, 2);
								$remainig_stars = 5 - $stars;
							}else{
								$stars = 0;
								$rating = 0;
								$remainig_stars = 5 - $stars;
							}
							@endphp
						
							<span class='d-none'>{{ $rating }}</span><span title="{{ $rating }}" data-toggle='tooltip'>
								@php
								for($i = 0; $i < $stars; $i++){
									echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
								}
								
								for($j = 0; $j < $remainig_stars; $j++){
									echo "<i class='fa fa-star text-muted'></i>";
								}
								@endphp
							</span>
						
							@endif
							<h5 class="mt-2 text-danger">{{ $user->name }}</h5>
							<div class="text-success">
								<a target="_blank" href="{{ route('admin.venue.staff.edit', [$venue->id, $user->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Profile</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class='col-md-8 pl-0'>
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Transactions</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-credit-card"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='flexbox'>
						<div></div>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
							<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
						</div>
					</div>
					<div class="table-responsive row">
						<table class="table table-bordered table-hover" id="datatable">
							<thead class="thead-default thead-lg">
								<tr>
									<th>#</th>
									<th>Amount</th>
									<th>Date/Time</th>
								</tr>
							</thead>
							<tbody>
								@foreach($waiter_payments as $waiter_payment)
									<tr>
										<td>{{  $transaction_counter++ }}</td>
										<td>{{ $currency }} {{  number_format($waiter_payment->amount,2) }}</td>
										<td>{{  $waiter_payment->created_at->format('d M Y') }} at {{  $waiter_payment->created_at->format('H:i:s') }}</td>
										
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
			
			
			
			
			<div class="ibox">
				<div class="ibox-head">
					<div class="ibox-title">Orders</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-shopping-cart"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='flexbox'>
						<div></div>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
							<input class="form-control form-control-solid" id="key-search2" type="text" placeholder="Search ...">
						</div>
					</div>
					<div class="table-responsive row">
						<table class="table table-bordered table-hover" id="datatable2">
							<thead class="thead-default thead-lg">
								<tr>
									<th>#</th>
									<th>Order Number</th>
									<th>Date/Time</th>
									<th>Tip</th>
									<th>Status</th>
									<th>View</th>
								</tr>
							</thead>
							<tbody>
								@foreach($orders as $order)
									<tr>
										<td>{{  $counter++ }}</td>
										<td>{{  $order->order_number }}</td>
										<td>{{  $order->created_at->format('d M Y') }} at {{  $order->created_at->format('H:i:s') }}</td>
										<td>{{ $currency }} {{  number_format($order->waiter_tip,2) }}</td>
										<td>
										@if($order->order_status == 'Completed')
										<span class="badge badge-success">Completed</span>
										@elseif($order->order_status == 'Ordered')
											<span class="badge badge-danger">Ordered</span>
										@elseif($order->order_status == 'In Oven')
											<span class="badge badge-warning">In Oven</span>
										@elseif($order->order_status == 'Final Steps')
											<span class="badge badge-info">Final Steps</span>
										@elseif($order->order_status == 'Refunded')
											<span class="badge badge-danger">Refunded</span>
										@endif
										</td>
										<td>
											<a target='_blank' href="{{ route('admin.venue.order.edit', [$venue->id, $order->id]) }}" style='font-size: 22px;' class='text-warning'><i class='fa fa-eye'></i></a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					
				</div>
			</div>
			
			
			
		</div>
		
	</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script>
$(document).ready(function(){
	$('.static-widget2').css('width', $('.static-widget').css('width'));
	$('form').on('submit', function(e){
		$(this).find("input[type='submit']").prop('disabled', true);
	});
});
</script>
@endsection 
