@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.admin')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
</style>
@endsection

@section('page_content')


<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.reviews', $id) }}">Orders</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>

<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<div class='row'>
		<div class='col-md-7'>
			<div class='ibox'>
				<div class='ibox ibox-body'>
				<h5 class="font-strong mb-4">Products List</h5>
				<table class='table table-bordered'>
					<thead class='thead-default'>
						<th>#</th>
						<th>Product</th>
						<th width='90'>Status</th>
					</thead>
					<tbody>
					@foreach($ordered_products as $ordered_product)
					@php
						
						$product = DB::table('products')->where('id', $ordered_product->product_id)->first();
						if($product->image == ''){
							$product->image = 'default.png';
						}
						$total_amount += $ordered_product->price * $ordered_product->quantity;
						
						$variants = DB::table('ordered_product_variables')->where('ordered_product_id', $ordered_product->id)->where('type', 'variant')->get();
						
						if($variants){
							$total_amount = $total_amount - $ordered_product->price * $ordered_product->quantity;
						}
						
						$extras = DB::table('ordered_product_variables')->where('ordered_product_id', $ordered_product->id)->where('type', 'extra')->get();
					@endphp
						<tr>
							<td>{{ $counter++ }}</td>
							<td class='products_list'>
								@if($variants->isEmpty())
								<img class="img-circle" src="{{ asset('public/uploads/products/' . $product->image) }}" width="50" height="50"> {{ $product->name }}
									
								<div class='table_box'>
									<table class='table table-bordered'>
										<thead>
											<th>Quantity</th>
											<th>Unit Price</th>
											<th>Amount</th>
										</thead>
										<tbody>
										<tr>
											<td>{{ number_format($ordered_product->quantity) }}</td>
											<td>{{ $currency }} {{ number_format($ordered_product->price, 2) }}</td>
											<td>{{ $currency }} {{ number_format($ordered_product->price * $ordered_product->quantity, 2) }}</td>
										</tr>
										</tbody>
									</table>
								</div>
							
								@endif
								
								@if(!$variants->isEmpty())
								<img class="img-circle" src="{{ asset('public/uploads/products/' . $product->image) }}" width="50" height="50"> {{ $product->name }}
									<div class='table_box'>
										<table class='table table-bordered'>
											<thead>
												<th width='160'><span data-toggle='tooltip' title='Variant' style='display: ilnline-block; width: 20px; height: 20px; border-radius: 50%;' class="badge badge-danger">V</span></th>
												<th>Quantity</th>
												<th>Unit Price</th>
												<th>Amount</th>
											</thead>
											<tbody>
											@foreach($variants as $variant)
											@php
											$total_amount += $variant->price * $variant->quantity;
											@endphp
											<tr>
												<td>{{ $variant->name }}</td>
												<td>{{ number_format($variant->quantity) }}</td>
												<td>{{ $currency }} {{ number_format($variant->price, 2) }}</td>
												<td>{{ $currency }} {{ number_format($variant->price * $variant->quantity, 2) }}</td>
											</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								@endif
								@if(!$extras->isEmpty())
								<div class='table_box'>
									<table class='table table-bordered'>
										<thead>
											<th width='160'><span data-toggle='tooltip' title='Extra' style='display: ilnline-block; width: 20px; height: 20px; border-radius: 50%;' class="badge badge-danger">E</span></th>
											<th>Quantity</th>
											<th>Unit Price</th>
											<th>Amount</th>
										</thead>
										<tbody>
										@foreach($extras as $extra)
										@php
										$total_amount += $extra->price * $extra->quantity;
										@endphp
										<tr>
											<td>{{ $extra->name }}</td>
											<td>{{ number_format($extra->quantity) }}</td>
											<td>{{ $currency }} {{ number_format($extra->price, 2) }}</td>
											<td>{{ $currency }} {{ number_format($extra->price * $extra->quantity, 2) }}</td>
										</tr>
										@endforeach
										</tbody>
									</table>
								</div>
								@endif
							</td>
							
							<td>
								@if($ordered_product->order_status == 0)
								<span class="btn btn-danger btn-sm text-white sweet-6">
									<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Ordered</span> <i class="ti-menu"></i>
								</span>
								@elseif($ordered_product->order_status == 1)
								<span class="btn btn-warning btn-sm text-white sweet-6">
									<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>In Oven</span> <i class="ti-menu"></i>
								</span>
								@elseif($ordered_product->order_status == 2)
								<span class="btn btn-info btn-sm text-white sweet-6">
									<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Final Steps</span> <i class="ti-menu"></i>
								</span>
								@else
								<span class="btn btn-success btn-sm text-white sweet-6">
									<span data-order-id="{{ $ordered_product->id }}" data-order-status="{{ $ordered_product->order_status }}" class='order_status_span'>Completed</span> <i class="ti-menu"></i>
								</span>
								@endif
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				
				@if($order->order_status == 'Completed' || $order->order_status == 'Refunded')
				<div class="d-flex justify-content-end">
					<div class="text-right" style="width:300px;">
						<div class="row mb-2">
							<div class="col-6">Subtotal</div>
							<div class="col-6">{{ $currency }} {{  number_format($order->total_amount, 2) }}</div>
						</div>
						<div class="row mb-2">
							<div class="col-6">Admin Comission:</div>
							<div class="col-6">{{ $currency }} {{  number_format($order->admin_commission, 2) }}</div>
						</div>
						<div class="row mb-2">
							<div class="col-6">Waiter Tip:</div>
							<div class="col-6">{{ $currency }} {{  number_format($order->waiter_tip, 2) }}</div>
						</div>
						<div class="row font-strong font-20">
							<div class="col-6">Total Price:</div>
							<div class="col-6">
								<div class="h3 font-strong">{{ $currency }} {{  number_format($order->total_amount - $order->admin_commission, 2) }}</div>
							</div>
						</div>
					</div>
				</div>
				@endif
				</div>
			</div>
			
			<div class='ibox'>
				<div class='ibox ibox-body'>
				<h5 class="font-strong mb-4">Ingredients</h5>
				<table class='table'>
					<thead>
						<tr>
							<th>Ingredient</th>
							<th>Unit</th>
							<th>Quantity</th>
						</tr>
					</thead>
					<tbody>
						@foreach($ordered_products as $ordered_product)
						@php
							$product_ingredients = DB::table('product_ingredients')->where('product_id', $ordered_product->product_id)->get();
							if(!$product_ingredients){
								continue;
							}
						@endphp
							@foreach($product_ingredients as $product_ingredient)
							@php
								$ingredient = DB::table('ingredients')->where('id', $product_ingredient->ingredient_id)->first();
								$unit = DB::table('units')->where('id', $product_ingredient->unit_id)->first();
							@endphp
							<tr>
								<td>{{ $ingredient->name }}</td>
								<td>{{ $unit->name }}</td>
								<td>{{ number_format($product_ingredient->quantity) }}</td>
							</tr>
							@endforeach
						@endforeach
					</tbody>
				</table>
				</div>
			</div>
		</div>
		
		<div class="col-xl-5 pl-0">
			<div class="ibox">
				<div class="ibox-body">
					<h5 class="font-strong mb-1">Order Info</h5>
					<div class='mb-4'>
						<h6><a style='font-size: 15px;' class="text-danger" href="javascript:;">#{{ $order->order_number }}</a></h6>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Total Price</div>
						<div class="col-8 h3 font-strong text-pink mb-0">{{ $currency }} {{ number_format($order->total_amount, 2) }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Date</div>
						<div class="col-8">{{ $order->created_at->format('d M Y') }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Time</div>
						<div class="col-8">{{ $order->created_at->format('H:i') }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Status</div>
						<div class="col-8">
						@if($order->order_status == 'Completed')
						<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@elseif($order->order_status == 'Refunded')
							<span class="badge badge-danger">Refunded</span>
						@endif
						</div>
					</div>
					<div class="row align-items-center">
						<div class="col-4 text-light">Payment</div>
						<div class="col-8">
						{{ $order->payment_type }}
						{{-- <img src="{{ asset('public/assets/img/logos/payment/visa.png') }}" alt="image" width="55" /> --}}
						</div>
					</div>
				</div>
			</div>
			<div class="ibox">
				<div class="ibox-body">
					<h5 class="font-strong mb-4">Buyer Info</h5>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Customer</div>
						<div class="col-8">{{ $customer->name }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Address</div>
						<div class="col-8">{{ $customer->address }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Email</div>
						<div class="col-8">{{ $customer->email }}</div>
					</div>
					<div class="row align-items-center">
						<div class="col-4 text-light">Phone</div>
						<div class="col-8">{{ $customer->phone }}</div>
					</div>
					
					<div class="row align-items-center">
						<div class="col-12 mt-4">
							<a target='_blank' href="{{ route('admin.customer.overview', $customer->id) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Profile</a>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-body">
					<h5 class="font-strong mb-4">Waiter Info</h5>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Waiter</div>
						<div class="col-8">{{ $waiter->name }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Table#</div>
						<div class="col-8">{{ $table->table_id }}</div>
					</div>
					<div class="row align-items-center mb-3">
						<div class="col-4 text-light">Tip</div>
						<div class="col-8">{{ $currency }} {{ number_format($order->waiter_tip, 2) }}</div>
					</div>
					
					<div class="row align-items-center">
						<div class="col-12 mt-2">
							<a target='_blank' href="{{ route('admin.venue.staff.edit', [$id, $waiter->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Profile</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>

	</div>
	
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(function(){
		$('.sweet-6').click(function(){
			
			var ordered_product_id = $(this).find('.order_status_span').attr('data-order-id');
			var order_status = $(this).find('.order_status_span').attr('data-order-status');
			var status_from_to;
			var _this = $(this);
			
			if(order_status == '0'){
				status_from_to = 'ordered to in oven';
			}else if(order_status == '1'){
				status_from_to = 'in oven to final steps';
			}else if(order_status == '2'){
				status_from_to = 'final steps to completed';
			}else{
				status_from_to = 'completed to completed';
			}
			
			swal({
				title: "Are you sure?",
				text: "Change status from " + status_from_to + '.',
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn-warning',
				confirmButtonText: 'Yes',
				closeOnConfirm: false,
			},function(){
				
				if(order_status == '0'){
					$(_this).find('.order_status_span').text('In Oven');
					$(_this).removeClass('btn-danger');
					$(_this).addClass('btn-warning');
					$(_this).find('.order_status_span').attr('data-order-status', '1');
					
				}else if(order_status == '1'){
					$(_this).find('.order_status_span').text('Final Steps');
					$(_this).removeClass('btn-warning');
					$(_this).addClass('btn-info');
					$(_this).find('.order_status_span').attr('data-order-status', '2');
					
				}else if(order_status == '2'){
					$(_this).find('.order_status_span').text('Completed');
					$(_this).removeClass('btn-info');
					$(_this).addClass('btn-success');
					$(_this).find('.order_status_span').attr('data-order-status', '3');
				}
				
				var _status = $(_this).find('.order_status_span').attr('data-order-status');
				swal("Status Changed!", "", "success");
				
				$.ajax({
				   type:'GET',
				   url:"{{ route('admin.order.status.update') }}",
				   data: 'id=' + ordered_product_id + '&status=' + _status,
				   success:function(response){
					 //console.log(response);
				   }
				});
			});
		});
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection
