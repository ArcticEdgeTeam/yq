@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Refunds</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.refunds') }}">Refunds</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Customer</th>
						<th>Order Number</th>
						<th>Order Total</th>
						<th>Status</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>
					@foreach($refunds as $refund)
					@php
						$order = DB::table('orders')->where('id', $refund->order_id)->first();
						$customer = DB::table('users')->where('id', $refund->customer_id)->first();
						if($customer->photo == ''){
							$customer->photo = 'default.png';
						}
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img  width='50' height='50' src="{{ asset('public/uploads/users/' . $customer->photo) }}" class="img-circle">{{ $customer->name }}</td>
						<td>{{ $order->order_number }}</td>
						<td>{{ $currency }} {{ $order->total_amount }}</td>
						<td>
						@if($refund->refund_status == 'Pending')
						<span class="badge badge-warning">Refund Claimed</span>
						@elseif($refund->refund_status == 'Refunded')
						<span class="badge badge-success">Refunded</span>
						@elseif($refund->refund_status == 'Cancelled')
						<span class="badge badge-danger">Cancelled</span>
						@endif
						</td>
						<td><a href="{{ route('admin.refund.edit', $refund->id) }}"><i class='fa fa-eye text-warning' style='font-size: 22px;'></i></a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection