@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}									
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Customers</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.customers') }}">Customers</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
		@include('layouts.admin-customer-nav')
		<div class='row'>
					<div class='col-md-4'>
						<div class="ibox">
							<div class="ibox-body">
								<h5 class="font-strong mb-4">General Info</h5>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">First Name</div>
									<div>{{ $customer->first_name }}</div>
								</div>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">Surname</div>
									<div>{{ $customer->last_name }}</div>
								</div>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">Date of Birth</div>
									<div>{{ Carbon\Carbon::parse($customer->dob)->format('d M Y') }}</div>
								</div>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">Gender</div>
									<div>{{ ucfirst($customer->gender) }}</div>
								</div>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">Email</div>
									<div>{{ $customer->email }}</div>
								</div>
								<div class="row align-items-center mb-3">
									<div class="col-4 text-light">Phone</div>
									<div>{{ $customer->phone }}</div>
								</div>
								
							</div>
						</div>
					</div>
					
					
					<div class='col-md-8 pl-0'>
						<div class='ibox p-4'>
							<h5 class='font-strong mb-4'>Orders Based on Venues</h5>
							<div class="flexbox">
								<div>
								</div>
								<div class="input-group-icon input-group-icon-left mr-3">
									<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
									<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
								</div>
							</div>
							
							<div class="table-responsive row">
								<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
									<thead class="thead-default thead-lg">
										<tr>
											<th>#</th>
											<th>Company ID</th>
											<th>Venue</th>
											<th>Orders</th>
											<th>Amount</th>
											<th>Last Visited</th>
										</tr>
									</thead>
									<tbody>
									@foreach($orders as $order)
									@php
										$venue = DB::table('venues')->where('id', $order->venue_id)->first();
										if($venue->banner == ''){
											$venue->banner = 'default.png';
										}
									@endphp
									<tr>
										<td>{{ $counter++ }}</td>
										<td>{{ $venue->company_id }}</td>
										<td><img src="{{ asset('public/uploads/banners/' . $venue->banner) }}" width='50' height='50' class='img-circle'> {{ $venue->name }}</td>
										<td>{{ number_format($order->total_orders) }}</td>
										<td>{{ $currency }} {{ number_format($order->total_amount, 2) }}</td>
										<td>
											{{ Carbon\Carbon::parse($order->created_at)->format('d M Y H:i') }}
										</td>
									</tr>
									@endforeach
									</tbody>
								</table>
							</div>
							
						
						</div>
						
						
						
					
					</div>
					
					
				</div>
		
            
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection 