@extends('layouts.admin')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Tables</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.tables', $id) }}">Tables</a></li>
		<li class="breadcrumb-item">New</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>


<div class='page-content fade-in-up'>

            
		@include('layouts.admin-venue-nav')
	
	<form class='form-danger form-table' method='post' action="{{ route('admin.venue.table.save', $id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>

		<div class="col-lg-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					
					<div class="ibox-title">Table Info</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-view-list'></i></a>
						
					</div>
					
				</div>
				<div class="ibox-body">
				   <div class='row'>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table Name</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
									<input name='name' required class="form-control" type="text">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>Table ID</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
									<input class="form-control" name='table_id' required  type="text">
								</div>
							</div>
						</div>
						
						<div class='col-md-12'>
							<div class="form-group mb-4">
								<label>No of Seats</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
									<input class="form-control" name='seats' required  type="number">
								</div>
							</div>
						</div>
						
						
				   </div>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="col-lg-8 pl-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Others</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" checked>
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
				
					<div class='col-md-12'>
						<div>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Smoking:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input value='yes' type="radio" name="smoking" checked="">
										<span class="input-span"></span>Yes</label>
										<label class="radio radio-inline radio-danger">
											<input value='no' type="radio" name="smoking" >
										<span class="input-span"></span>No</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					<div class='col-md-12'>
						<div class='mb-3'>
							
							<table class='table no-border'>
								<tr>
									<td class='p-0' style='width: 100px;'>
										<span><b>Area:</b> </span>
									</td>
									<td>
										<label class="radio radio-inline radio-danger">
											<input value='inside' type="radio" name="area" checked="">
										<span class="input-span"></span>Inside</label>
										<label class="radio radio-inline radio-danger">
											<input value='outside' type="radio" name="area">
										<span class="input-span"></span>Outside</label>
									</td>
								</tr>
							</table>
							
							
						</div>
						
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Assign Waiter</label>
						<select name='waiters[]' class="form-control selectpicker" multiple>
							@foreach($waiters as $waiter)
								<option value="{{ $waiter->id }}">{{ $waiter->name }}</option>
							@endforeach
						</select>
					</div>
					
					<div class='form-group mt-3 col-lg-12'>
						<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Add Table</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
							</span>
						</button>
					</div>
			   </div>
			</div>
		</div>
	</div>
		
		
	</div>
	</form>
	
</div>


@endsection
@section('page_plugin_js')
	<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
	<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>	
	<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script type="text/javascript">

	  $(document).ready(function() {
        $('.form-table').submit(function(event) {
            event.preventDefault();
            // ajax call
            $.ajax({
                type: 'GET',
                data:{
				table_id: $('input[name="table_id"]').val(),
				id: '{{$id}}',
				type: 'add'
			},
			url: "{{ route('ajax.get-table-id') }}",
                success: function(res) {

                    if(res['error'] != null) {
                        // null the input value
                        $('input[name="table_id"]').val('');
                        //show message box
                        swal("", res['error'], "error");
                        $('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
                        return false;

                    } else {
                        event.currentTarget.submit();
                    }
                    
                } //success
            });
        });
    });

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>

@endsection
