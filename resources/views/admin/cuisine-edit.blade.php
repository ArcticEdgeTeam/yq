@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Cuisines</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.cuisines') }}">Cuisines</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>
<div class="page-content fade-in-up">
<form class='form-danger' method='post' action="{{ route('admin.cuisine.update', $cuisine->id) }}" enctype='multipart/form-data'>
	@csrf
	@if($cuisine->image == '')
		<?php
			$cuisine->image = 'default.png';
		?>
	@endif
<div class="row">
	<div class='col-md-3'>
		<div class="ibox ibox-fullheight">
			<div class="ibox-body text-center" style='position: relative;'>
				<img id='previewfile' width='150' height='150' src="{{ asset('public/uploads/cuisines/' . $cuisine->image) }}" class='img-circle'>
				<div style='position: absolute; right: 20px; top: 20px;'>
					<label data-toggle='tooltip' title='Change Icon' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
					<input onchange="PreviewprofileImage();" id='uploadfile' accept="image/*" type='file' name='image' class='d-none'>
				</div>
				
				<div class='row'>
					<div class="col-sm-12 mt-2">
						<h5 class='mt-2 text-danger'>{{ $cuisine->name }}</h5>
						
						<div>
							@if($cuisine->status == 1)
								<span class="badge badge-success">Active</span>
							@else
								<span class="badge badge-danger">Inactive</span>
							@endif
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-xl-9">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Cuisine Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($cuisine->status == 1) checked @endif >
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
								<input required name='name' class="form-control" type="text" value="{{ $cuisine->name }}" placeholder="Dietary Name">
							</div>
						</div>
					</div>
					<div class='col-md-6'>
						
						<div class='form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Cuisine</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
			   </div>
			</div>
		</div>
	</div>
	
</div>
</form>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>
@endsection