@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')		
<div class="page-heading">
	<h1 class="page-title">Support</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.supports') }}">Support</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	<form action='#' method='post' enctype='multipart/form-data'>
	<div class="row">
	
	
		<div class="col-xl-3">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">User Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div class="ibox-body text-center">
				   <img src="{{ asset('public/uploads/users/' . $user->photo) }}" width='120' height='120' class='img-circle'>
				   <br />
				   <h5 class='mt-3'>{{ $user->name }}</h5>
						<div class='mb-2'>{{ $user->email }}</div>
						@if($user->role == 'customer')
							<span class="badge badge-danger">Customer</span>
						 <div class='mt-3'>
						   <a target='_blank' href="{{ route('admin.customer.overview', $user->id) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
						 </div>
						@else
							<span class="badge badge-warning">Venue</span>
						<div class='mt-3'>
						   <a target='_blank' href="{{ route('admin.venue.settings', $user->venue_id) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
						 </div>
						@endif
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Information</div>
					<div class="ibox-tools">
						<div class="ibox-tools">
							<a class="font-18" href="javascript:;"><i class="ti-support"></i></a>
						</div>
					</div>
				</div>
				<div class="ibox-body">
				   <div class='row '>
						<div class='col-md-3'><b>Date:</b> {{ $support->created_at->format('d M Y') }}</div>
						<div class='col-md-9'><b>Time:</b> {{ $support->created_at->format('H:i') }}</div>
				   </div>
				   
				   
				   <div class='form-group mt-4'>
					 <div class="input-group-icon input-group-icon-left">
						<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
						<input readonly class="form-control" type="text" value="{{ $support->subject }}">
					</div>
					
				   </div>
				   
				   <div class='form-group mt-4'>
					<textarea readonly rows='4' class='form-control'>{{ $support->comment }}</textarea>
				   </div>
				    <div class='form-group'>
					<a href="{{ route('admin.supports') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Go Back</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-angle-double-left pr-0 pl-2"></i> Go Back</span>
						</span>
					</a>
				   </div>
				</div>
			</div>
		</div>		
		
		
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection