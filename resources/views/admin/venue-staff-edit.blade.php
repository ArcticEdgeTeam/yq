@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.staff', $id) }}">Staff</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<form class='form-danger' method='post' action="{{ route('admin.venue.staff.update', [$id, $user->id]) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		@php
			if($user->photo == ''){
				$user->photo = 'default.png';
			}
		@endphp
		<div class='col-lg-3'>
			<div class="ibox">
				<div class="ibox-body text-center" style='position: relative;'>
					<img id='previewfile' width='150' height='150'  src="{{ asset('public/uploads/users/' . $user->photo) }}" class='img-circle'>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Upload Picture' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='photo' class='d-none'>
					</div>
					
					<div class='row'>
						<div class="col-sm-12 mt-3">
							@if($user->role == 'waiter')
								
							@php
							$review = DB::table('reviews')
							->select(DB::raw('sum(service_rating) as service_rating, count(id) as total_records'))
							->where('waiter_id', $user->id)->first();
							
							if($review->total_records > 0){
								$stars = round($review->service_rating / $review->total_records);
								$rating = round($review->service_rating / $review->total_records, 2);
								$remainig_stars = 5 - $stars;
							}else{
								$stars = 0;
								$rating = 0;
								$remainig_stars = 5 - $stars;
							}
							@endphp
						
							<span class='d-none'>{{ $rating }}</span><span title="{{ $rating }}" data-toggle='tooltip'>
								@php
								for($i = 0; $i < $stars; $i++){
									echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
								}
								
								for($j = 0; $j < $remainig_stars; $j++){
									echo "<i class='fa fa-star text-muted'></i>";
								}
								@endphp
							</span>
						
							@endif
							<h5 class="mt-2 text-danger">{{ $user->name }}</h5>
							<div class="text-success">
								<span class="badge badge-danger">
								@if($user->role == 'waiter')
									Waiter
								@else
									Prep Staff
								@endif
								</span>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		
		
	<div class="col-lg-6 p-lg-0">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">User Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($user->status == 1) checked @endif >
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
				
					<div class='col-md-12'>
						<div class='mb-3'>
							<span class='mr-4'><b>Role:</b> </span>
							@if($user->role == 'waiter')
								Waiter
							@else
								Prep Staff
							@endif
						</div>
						
					</div>
				
					<!-- <div class='col-md-6'>
						<div class="form-group" id="date_1">
							<label class="font-normal">Date of Birth</label>
							<div class="input-group date">
								<span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
								<input class="form-control" name='dob' type="text" value="{{ $user->dob }}">
							</div>
						</div>
					</div> -->
					
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Staff ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input value="{{ $user->user_id }}" required name='user_id' class="form-control" type="text">
							</div>
						</div>
					</div>
					
					<!-- <div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input id="autocomplete" onFocus="geolocate()" value="{{ $user->address }}" required name='address' class="form-control" type="text">
							</div>
						</div>
					</div>
					
					
					<div class='col-md-6'>
						<div class="form-group mb-4">
							<label>Gender</label>
							<br />
							<label class="radio radio-inline radio-danger">
							<input value='male' type="radio" name="gender" checked>
							<span class="input-span"></span>Male</label>
							<label class="radio radio-inline radio-danger">
							<input value='female' type="radio" name="gender" @if($user->gender == 'female') checked @endif >
							<span class="input-span"></span>Female</label>
						</div>
					</div> -->
					
					@if($user->role == 'waiter')
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Tables Assigned</label>
							@php $loop = 1; @endphp
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-view-list"></i></span>
								<input readonly value="<?php foreach($tables as $table){ if(!DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $user->id)->first()){ continue; } if($loop == 1){ $loop++; }else{ echo ', '; } echo $table->name; }?>" class="form-control" type="text">
							</div>
						</div>
					</div>
					@endif
					
					<!-- <div id='assign_tables' class="form-group mb-4 col-md-12 d-none ">
						<label>Assign Tables</label>
						<select name='tables[]' class="form-control select2_demo_1" multiple="">
							@foreach($tables as $table)
								@php
									$selected = '';
									if(DB::table('table_waiters')->where('table_id', $table->id)->where('user_id', $user->id)->first()){
										$selected = 'selected';
									}
								@endphp
									
								<option {{ $selected }} value="{{ $table->id }}">{{ $table->name }}</option>
							@endforeach
						</select>
					</div> -->
					
					<div class="form-group mb-4 col-md-12">
						<label>About User</label>
						<textarea rows='4' name='about' class='form-control'>{{ $user->about }}</textarea>
					</div>

					@if($user->role == 'bar')
					<div class='col-md-12 prep-area'>
						<div class="form-group mb-4">
							<label>Prep Area</label>
							<div class="input-group-icon">
								<select name='bar_id[]' class="selectpicker form-control" multiple>
									@foreach($bars as $bar)
									@php
									 	$selectBar = '';
									@endphp
									@foreach($getUserBars as $userBars)

									@php
									 	if ($bar->id == $userBars->bar_id) {
									 		$selectBar = 'selected';
										} 
									@endphp

									@endforeach

										<option {{$selectBar}} value="{{ $bar->id }}">{{ $bar->name }}</option>

									
									@endforeach
								</select>
							</div>
						</div>
					</div>
					@endif
					
			   </div>
			</div>
		</div>
	</div>
	
	
	<div class="col-lg-3">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Login Info</div>
				<div class="ibox-tools">
					<a href='javascript:;'><i class='ti-user'></i></a>
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input value="{{ $user->name }}" required name='name' class="form-control" type="text">
							</div>
						</div>
					</div>
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input value="{{ $user->email }}" readonly required class="form-control" type="email">
							</div>
						</div>
					</div>
					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='password' value="" class="form-control password" type="password">
							</div>
						</div>
					</div>

					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='confirm_password' value="" class="form-control confirm_password" type="password">
							</div>
							<span class="text-danger confirm-password-message" style="display: none;">Password not matching!</span>
						</div>
					</div>


					<div class='col-md-12'>
						<div class="form-group mb-4">
							<label>Telephone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-phone"></i></span>
								<input value="{{ $user->phone }}" name='phone' class="form-control" type="text">
							</div>
						</div>
					</div>
					
					<div class='form-group mt-3 col-lg-12'>
						<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update User</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
						</button>
					</div>
			   </div>
			</div>
		</div>
	</div>
		
	</div>
	</form>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')

<script>

	$('input[name="user_id"]').on('blur', function() {
		var user_id = $(this).val();
		if (user_id != '') {
			$.ajax({
					type: 'get',
					data:{
						user_id: user_id,
						venue_id: '{{$id}}',
						id: '{{$user->id}}'
					},
					url: "{{ route('ajax.get-edit-staff-information') }}",
					success: function(msg){
						if(msg['error'] != null) {
							swal("", msg['error'], "error");
							$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
							$('input[name="user_id"]').val('');
						}
					}
						
				});
		}

	});

  // Bootstrap datepicker
    $('#date_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
		format: 'dd-mm-yyyy',
    });
	
</script>
<script>
	$(document).ready(function(){
		$('#manager_role').on('click', function(){
			$('#assign_tables').hide();
		});
		
		$('#waiter_role').on('click', function(){
			$('#assign_tables').show();
		});

		
		$('.form-danger').submit(function(e) {
			
			 	var _password = $('.password').val();
				var _password_confirm = $('.confirm_password').val();
				if(_password != '') {

					if (_password.length < 8) {

						swal("", "Password must be 8 chanracters long.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}

					if(_password != _password_confirm) {
						swal("", "Password Mismatch", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}
					
				}
		});
	});
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>

<script>
var placeSearch, autocomplete;
function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
  document.getElementById('autocomplete'), {types: ['geocode']});
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection 
