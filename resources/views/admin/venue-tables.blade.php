@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Tables</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.tables', $id) }}">Tables</a></li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
            
	@include('layouts.admin-venue-nav')	

<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
			
				
				<div class="form-group">
					<a href="{{ route('admin.venue.table.create', $id) }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Table</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
				
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Table ID</th>
						<th>Table Name</th>
						<th>Seats</th>
						<th>Smoking</th>
						<th>Area</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($tables as $table)
					<tr>
						<td>{{ $counter++ }}</td>
						<td> 
							@if($table->status == 1)
								<i class='fa fa-check-circle text-success' data-toggle='tooltip' title='Active' style='font-size: 22px;'></i>
							@else
								<i class='fa fa-times-circle text-danger' data-toggle='tooltip' title='Inactive' style='font-size: 22px;'></i>
							@endif
							{{ $table->table_id }}
						</td>
						<td>{{ $table->name }}</td>
						<td>{{ number_format($table->seats) }}</td>
						<td>
							@if($table->smoking == 'yes')
							<span class='d-none'>Yes</span><i class='fa fa-check-circle text-success' data-toggle='tooltip' title='Yes' style='font-size: 22px;'></i>
							@else
								<span class='d-none'>No</span><i class='fa fa-times-circle text-danger' data-toggle='tooltip' title='No' style='font-size: 22px;'></i>
							@endif
						</td>
						<td>
						@if($table->area == 'inside')
							<span class="badge badge-success">Inside</span>
						@else
							<span class="badge badge-danger">Outside</span>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.venue.table.edit', [$id, $table->id]) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection 