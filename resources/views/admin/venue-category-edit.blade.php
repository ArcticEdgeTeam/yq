@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@if($category->category_id == null)
	<style>
		#child_cat_list{
			
			display: none;
			
		}
	</style>
@endif
@endsection
@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Categories</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.categories', $id) }}">Categories</a></li>
		<li class="breadcrumb-item">Edit</li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
@include('layouts.admin-venue-nav')
	<form class='form-danger category-submit' method='post' action="{{ route('admin.venue.category.update', [$id, $category->id]) }}" enctype='multipart/form-data'>
	@csrf
	@if($category->image == '')
		<?php
			$category->image = 'default.png';
		?>
	@endif
		<div class="row">
					
		<div class='col-md-3'>
			<div class="ibox ibox-fullheight">
				<div class="ibox-body text-center">
					<img id='previewfile' src="{{ asset('public/uploads/categories/' . $category->image) }}">
					<div class='row'>
						<div class="col-sm-12 mt-2">
							<label class='btn btn-link text-danger m-0' for='uploadfile'>Change Pic</label>
							<input name='image' class="form-control p-2 d-none" style='height: 37px;' type="file" accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' />
							<div>
							@if($category->status == 1)
								<span class="badge badge-success">Active</span>
							@else
								<span class="badge badge-danger">Inactive</span>
							@endif
							
						</div>
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Category Info</div>
					<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" @if($category->status == 1) checked @endif>
						<span></span>
					</label>
					
				</div>
				</div>
				<div class="ibox-body">
					
							<div class='row'>
								<div class='col-md-6'>
									<div class="form-group mb-4">
										<div class="input-group-icon input-group-icon-left">
											<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
											<input class="form-control" name='name' required type="text" value="{{ $category->name }}" placeholder="Category Name">
										</div>
									</div>
								</div>
								<div class='col-md-6'>
									
									<div class="form-group mb-5">
                                        <div>
                                            <label onclick="document.getElementById('child_cat_list').style.display='none';" class="radio radio-inline radio-danger">
                                                <input id='parent_radio' type="radio" value='0' name="has_parent" checked>
                                                <span class="input-span"></span>Parent</label>
                                            <label onclick="document.getElementById('child_cat_list').style.display='block';" class="radio radio-inline radio-danger">
                                                <input id='child_radio' type="radio" value='1' name="has_parent" @if($category->category_id != 0) checked @endif>
                                                <span class="input-span"></span>Child</label>
                                        </div>
                                    </div>
									
									<div class="form-group mb-5" id='child_cat_list'>
										<select id='category_id' name='category_id' class="selectpicker form-control">
											@foreach($categories as $cat)
												<option @if($category->category_id == $cat->id) selected @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
												<!-- @foreach ($cat->childrenCategories as $childCategory)
													@php
														if($edited_id == $childCategory->id){
															continue;
														}
													@endphp
													@include('child_category', ['child_category' => $childCategory])
												@endforeach -->
											@endforeach
										</select>
									</div>
									
								</div>
							</div>
							
							<div class='row'>
								<div class='col-md-6'>
								</div>
								<div class='col-md-6'>
									<div class='form-group'>
										<button class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Update Category</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
											</span>
										</button>
								   </div>
							   </div>
						   </div>
				     
				</div>
			</div>
		</div>
		
	</div>
	<input type="hidden" name="child_count" value="{{$getChildCount}}">
	</form>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	$(document).ready(function(){
		
		$('#child_radio').click(function() {
			if($(this).is(':checked')) { 
				$('#category_id').prop('required', true);
		   }
		});
		
		$('#parent_radio').click(function() {
		   if($(this).is(':checked')) {
			   $('#category_id').prop('required', false);
		   }
		});

		
	});
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
    
    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});

	$('.category-submit').submit(function(event) {
		event.preventDefault();
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					name: $('input[name="name"]').val(),
					category_id: '{{$category->id}}',
					id: '{{$id}}',
					type: 'edit',
				},
				url: "{{ route('ajax.get-category-info') }}",
				success: function(res) {

					if(res['error'] != null) {
						// null the input value
						$('input[name="name"]').val('');
						//show message box
						swal("", res['error'], "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;

				    } 

				    if ($('#child_radio').prop('checked') == true) {
							if ($('input[name="child_count"]').val() > 0) {
								
								swal("", "Changing this parent to child category will make '"+$('input[name="child_count"]').val()+"' other categories to unassigned.", "error");
								$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
								return false;
							}
						}

					event.currentTarget.submit();
				} //success
		});

	});
</script>
@endsection