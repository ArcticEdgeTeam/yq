@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
											
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Manage Admins</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.manage.admins') }}">Manage Admins</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				<div class="form-group">
					<a href="{{ route('admin.create.new-admin') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Create New Admin</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create Admin</span>
						</span>
					</a>
			   </div>
				
			</div>
			<div></div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>User</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Role</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($getUsers as $key => $user)
					<tr>
						<td>{{$key + 1}}</td>
						<td> {{$user->name}}</td>
						<td> {{$user->email}}</td>
						<td> {{$user->phone}}</td>
						<td> {{$user->role}}</td>
						<td>
							@if($user->status == 1)
								<span class='d-none'>Active</span><i style='font-size: 22px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
							@else
								<span class='d-none'>Inactive</span><i style='font-size: 22px;' data-toggle='tooltip' title='Inactive' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
							@endif</td>
						<td>
							<a class='text-info' data-toggle='tooltip' title='{{$user->created_at}}' href="javascript::void()"><i style='font-size: 22px;' class='fa fa-info'></i></a>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.edit.admin', $user->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection