@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
	\Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SUNDAY);
	\Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::SATURDAY);
										
@endphp


@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
<style>
	.veneu_performance .ibox{
		width: 32%;
	}
	
	.veneu_performance2 .ibox{
		width: 23.79%;
		height: 350px;
	}
</style>
@endsection
@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')
	
	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item">Overview</li>
	</ol>
	
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
	
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<div class='row'>
	
	
		<div class="col-md-3">
			
			 <div class="card bg-success mb-3">
				<div class="card-body">
					<h2 class="text-white">{{ number_format($current_orders)}} <i class="ti-shopping-cart float-right"></i></h2>
					<div>Current Orders</div>
					<div class="text-white mt-1"><i class="ti-stats-up mr-1"></i><small>First Order: @if($first_order) {{ $first_order->created_at->format('H:i:s') }} @endif</small></div>
				</div>
				<div class="progress mb-2 widget-dark-progress">
					<div class="progress-bar" role="progressbar" style="width:100%; height:5px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
			
			<div class="card bg-danger">
				<div class="card-body">
					<h2 class="text-white">{{ number_format($current_orders) }} <i class="ti-reload float-right"></i></h2>
					<div>Pending Orders</div>
					<div class="text-white mt-1"><i class="ti-stats-up mr-1"></i><small>Last Order: @if($last_order) {{ $last_order->created_at->format('H:i:s') }} @endif</small></div>
				</div>
				<div class="progress mb-2 widget-dark-progress">
					<div class="progress-bar" role="progressbar" style="width:100%; height:5px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
			
		</div>
		
		<div class='col-md-9 pl-0'>
			
			<div class="ibox">
				<div class="ibox-body">
					<div class="flexbox mb-5">
						<div class="flexbox">
							<span class="mr-4 text-danger" style="width:60px;height:60px;background-color:#e7e9f6;display:inline-flex;align-items:center;justify-content:center;font-size:35px;"><i class="ti-pulse"></i></span>
							<div>
								<h5 class="font-strong">QUICK STATS</h5>
								<div class="text-light">Performance of Venue</div>
							</div>
						</div>
						<ul class="nav nav-pills nav-pills-danger nav-pills-rounded nav-pills-air">
							<li class="nav-item ml-1">
								<a class="nav-link active" href="#tab_1" data-toggle="tab">Today</a>
							</li>
							<li class="nav-item ml-1">
								<a class="nav-link" href="#tab_2" data-toggle="tab">Lifetime</a>
							</li>
							
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="tab_1">
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($orders_today) }}</h2>
										<div>ORDERS TODAY</div><i class="ti-shopping-cart widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($sales_today ,2)}}</h2>
										<div>SALES TODAY</div><i class="ti-stats-up widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($earnings_today,2) }}</h2>
										<div>EARNINGS TODAY</div><i class="ti-money widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
							
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($customers_today) }}</h2>
										<div>CUSTOMERS TODAY</div><i class="ti-user widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($bookings_today) }}</h2>
										<div>BOOKINGS TODAY</div><i class="ti-calendar widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($views_today) }}</h2>
										<div>VIEWS TODAY</div><i class="ti-eye widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="tab-pane fade" id="tab_2">
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($orders_lifetime) }}</h2>
										<div>ORDERS LIFETIME</div><i class="ti-shopping-cart widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($sales_lifetime,2) }}</h2>
										<div>SALES LIFETIME</div><i class="ti-stats-up widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($earnings_lifetime,2) }}</h2>
										<div>EARNINGS LIFETIME</div><i class="ti-money widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
							
							
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($customers_lifetime) }}</h2>
										<div>CUSTOMERS LIFETIME</div><i class="ti-user widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($bookings_lifetime) }}</h2>
										<div>BOOKINGS LIFETIME</div><i class="ti-calendar widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($views_lifetime) }}</h2>
										<div>VIEWS LIFETIME</div><i class="ti-eye widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	
	<div class='row'>
		<div class='col-md-12'>
		
		<div class="ibox">
			<div class="ibox-body">
				<div class="flexbox mb-5">
					<div class="flexbox">
						<span class="mr-4 text-danger" style="width:60px;height:60px;background-color:#e7e9f6;display:inline-flex;align-items:center;justify-content:center;font-size:35px;"><i class="ti-announcement"></i></span>
						<div>
							<h5 class="font-strong">PERFORMANCE</h5>
							<div class="text-light">Top Rated Bar, Waiter Product & Table</div>
						</div>
					</div>
					<ul class="nav nav-pills nav-pills-rounded nav-pills-air nav-pills-danger">
						<li class="nav-item ml-1">
							<a class="nav-link active" href="#tab_3" data-toggle="tab">Day</a>
						</li>
						<li class="nav-item ml-1">
							<a class="nav-link" href="#tab_4" data-toggle="tab">Week</a>
						</li>
						
						<li class="nav-item ml-1">
							<a class="nav-link" href="#tab_5" data-toggle="tab">Month</a>
						</li>
						
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade show active" id="tab_3">
						<div class="flexbox veneu_performance2">
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Prep Area of the Day</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-glass'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
								   @if($bar_of_the_day)
									@php
									$bar = DB::table('bars')->where('id', $bar_of_the_day->bar_id)->first();
									if($bar->image == ''){
										$bar->image = 'default.png';
									}
									@endphp
									<div>
										<img class="img-circle" src="{{ asset('public/uploads/bars/' . $bar->image) }}" width="100" height="100">
									</div>
									
									<h5 class='text-danger mt-3'>{{ $bar->name }}</h5>
									<div class='mb-2'>
									{{ $currency }} {{ number_format($bar_of_the_day->sales, 2) }}
									</div>
									<a target='_blank' href="{{ route('admin.venue.bar.edit', [$bar->venue_id, $bar->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Bar</a>
									@endif
								</div>
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Waiter of the Day</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-user'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
									 @if($waiter_of_the_day)
										@php
										$waiter = DB::table('users')->where('id', $waiter_of_the_day->waiter_id)->first();
										if($waiter->photo == ''){
											$waiter->photo = 'default.png';
										}
										
										$stars = round($waiter_of_the_day->average_rating / $waiter_of_the_day->total_records);
										$remainig_stars = 5 - $stars;
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $waiter->name }}</h5>
										<div>
											<span title="{{ $waiter_of_the_day->average_rating / $waiter_of_the_day->total_records }}" data-toggle='tooltip'>
												@php
													for($i = 0; $i < $stars; $i++){
														echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
													}
													
													for($j = 0; $j < $remainig_stars; $j++){
														echo "<i class='fa fa-star text-muted'></i>";
													}
												@endphp
											</span>
										</div>
										<div class='mb-2'>
											
										@php
											$waiter_of_the_day_tips = DB::table('orders')
											->select(DB::raw('sum(waiter_tip) as waiter_tips'))
											->where('venue_id', $id)
											->whereDate('created_at', \Carbon\Carbon::today())
											->groupBy('waiter_id')
											->orderBy('waiter_tips', 'desc')
											->first();
										@endphp
										@if($waiter_of_the_day_tips)
											Tip: {{ $currency }} {{ number_format($waiter_of_the_day_tips->waiter_tips, 2) }}
										@endif
										</div>
										<a target='_blank' href="{{ route('admin.venue.staff.edit', [$waiter->venue_id, $waiter->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Waiter</a>
									@endif
								</div>
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Most Selling Product</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-package'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
									@if($product_of_the_day)
										@php
											$product = DB::table('products')->where('id', $product_of_the_day->product_id)->first();
											if($product->image == ''){
												$product->image = 'default.png';
											}
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/products/'.  $product->image) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $product->name }}</h5>
										<div class='mb-2'>
										{{ number_format($product_of_the_day->quantity) }} Orders
										</div>
										<a target='_blank' href="{{ route('admin.venue.product.edit', [$product->venue_id, $product->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Product</a>
									@endif
								</div>
							</div>
							
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									<div class="ibox-title">Hot Table</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-view-grid'></i></a>
									</div>
								</div>
								<div class="ibox-body">
									@if($table_of_the_day)
									@php
										$table = DB::table('tables')->where('id', $table_of_the_day->table_id)->first();
										$table_of_the_day_cost = DB::table('orders')
										->select(DB::raw('sum(total_amount) as table_cost'))
										->where('venue_id', $id)
										->whereDate('created_at', \Carbon\Carbon::today())
										->groupBy('table_id')
										->orderBy('table_cost', 'desc')
										->first();
										
										$bookings = DB::table('bookings')->where('table_id', $table->id)
										->whereDate('created_at', \Carbon\Carbon::today())->count();
									@endphp
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Table ID
										</div>
										<div class='col-md-5'>
										{{ $table->table_id }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Orders
										</div>
										<div class='col-md-5'>
										{{ number_format($table_of_the_day->total_orders) }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Cost
										</div>
										<div class='col-md-5'>
										{{ $currency }} {{ number_format($table_of_the_day_cost->table_cost, 2) }}
										</div>
								   </div>
								   
								   <div class='row'>
										<div class='col-md-7'>
											Bookings
										</div>
										<div class='col-md-5'>
										{{ $bookings }}
										</div>
								   </div>
								   @endif
								</div>
							</div>
							
							
						</div>
					</div>
					
					
					<div class="tab-pane fade" id="tab_4">
						<div class="flexbox veneu_performance2">
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Bar of the Week</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-glass'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
								   @if($bar_of_the_week)
									@php
									$bar = DB::table('bars')->where('id', $bar_of_the_week->bar_id)->first();
									if($bar->image == ''){
										$bar->image = 'default.png';
									}
									@endphp
									<div>
										<img class="img-circle" src="{{ asset('public/uploads/bars/' . $bar->image) }}" width="100" height="100">
									</div>
									
									<h5 class='text-danger mt-3'>{{ $bar->name }}</h5>
									<div class='mb-2'>
									{{ $currency }} {{ number_format($bar_of_the_week->sales, 2) }}
									</div>
									<a target='_blank' href="{{ route('admin.venue.bar.edit', [$bar->venue_id, $bar->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Bar</a>
									@endif
								</div>
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Waiter of the Week</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-user'></i></a>
										
									</div>
								</div>
								
								<div class="ibox-body text-center">
									 @if($waiter_of_the_week)
										@php
										$waiter = DB::table('users')->where('id', $waiter_of_the_week->waiter_id)->first();
										if($waiter->photo == ''){
											$waiter->photo = 'default.png';
										}
										$stars = round($waiter_of_the_week->average_rating / $waiter_of_the_week->total_records);
										$remainig_stars = 5 - $stars;
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $waiter->name }}</h5>
										<div>
											<span title="{{ $waiter_of_the_week->average_rating / $waiter_of_the_week->total_records }}" data-toggle='tooltip'>
												@php
													for($i = 0; $i < $stars; $i++){
														echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
													}
													
													for($j = 0; $j < $remainig_stars; $j++){
														echo "<i class='fa fa-star text-muted'></i>";
													}
												@endphp
											</span>
										</div>
										<div class='mb-2'>
											
										@php
											$waiter_of_the_week_tips = DB::table('orders')
											->select(DB::raw('sum(waiter_tip) as waiter_tips'))
											->where('venue_id', $id)
											->whereBetween('created_at', [\Carbon\Carbon::now()->startOfWeek(), \Carbon\Carbon::now()->endOfWeek()])
											->groupBy('waiter_id')
											->orderBy('waiter_tips', 'desc')
											->first();
										@endphp
										@if($waiter_of_the_week_tips)
											Tip: {{ $currency }} {{ number_format($waiter_of_the_week_tips->waiter_tips, 2) }}
										@endif
										</div>
										<a target='_blank' href="{{ route('admin.venue.staff.edit', [$waiter->venue_id, $waiter->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Waiter</a>
									@endif
								</div>
								
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Most Selling Product</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-package'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
									@if($product_of_the_week)
										@php
											$product = DB::table('products')->where('id', $product_of_the_week->product_id)->first();
											if($product->image == ''){
												$product->image = 'default.png';
											}
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/products/'.  $product->image) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $product->name }}</h5>
										<div class='mb-2'>
										{{ number_format($product_of_the_week->quantity) }} Orders
										</div>
										<a target='_blank' href="{{ route('admin.venue.product.edit', [$product->venue_id, $product->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Product</a>
									@endif
								</div>
							</div>
							
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									<div class="ibox-title">Hot Table</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-view-grid'></i></a>
									</div>
								</div>
								<div class="ibox-body">
									@if($table_of_the_week)
									@php
										$table = DB::table('tables')->where('id', $table_of_the_week->table_id)->first();
										$table_of_the_day_cost = DB::table('orders')
										->select(DB::raw('sum(total_amount) as table_cost'))
										->where('venue_id', $id)
										->whereBetween('created_at', [\Carbon\Carbon::now()->startOfWeek(), \Carbon\Carbon::now()->endOfWeek()])
										->groupBy('table_id')
										->orderBy('table_cost', 'desc')
										->first();
										
										$bookings = DB::table('bookings')->where('table_id', $table->id)
										->whereBetween('created_at', [\Carbon\Carbon::now()->startOfWeek(), \Carbon\Carbon::now()->endOfWeek()])->count();
									@endphp
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Table ID
										</div>
										<div class='col-md-5'>
										{{ $table->table_id }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Orders
										</div>
										<div class='col-md-5'>
										{{ number_format($table_of_the_week->total_orders) }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Cost
										</div>
										<div class='col-md-5'>
										{{ $currency }} {{ number_format($table_of_the_day_cost->table_cost, 2) }}
										</div>
								   </div>
								   
								   <div class='row'>
										<div class='col-md-7'>
											Bookings
										</div>
										<div class='col-md-5'>
										{{ number_format($bookings) }}
										</div>
								   </div>
								   @endif
								</div>
							</div>
							
						</div>
					</div>
					
					
					
					<div class="tab-pane fade" id="tab_5">
						
						<div class="flexbox veneu_performance2">
							
							
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Bar of the Month</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-glass'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
								   @if($bar_of_the_month)
									@php
									$bar = DB::table('bars')->where('id', $bar_of_the_month->bar_id)->first();
									if($bar->image == ''){
										$bar->image = 'default.png';
									}
									@endphp
									<div>
										<img class="img-circle" src="{{ asset('public/uploads/bars/' . $bar->image) }}" width="100" height="100">
									</div>
									
									<h5 class='text-danger mt-3'>{{ $bar->name }}</h5>
									<div class='mb-2'>
									{{ $currency }} {{ number_format($bar_of_the_month->sales, 2) }}
									</div>
									<a target='_blank' href="{{ route('admin.venue.bar.edit', [$bar->venue_id, $bar->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Bar</a>
									@endif
								</div>
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Waiter of the Month</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='fa fa-user'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
									 @if($waiter_of_the_month)
										@php
										$waiter = DB::table('users')->where('id', $waiter_of_the_month->waiter_id)->first();
										if($waiter->photo == ''){
											$waiter->photo = 'default.png';
										}
										$stars = round($waiter_of_the_month->average_rating / $waiter_of_the_month->total_records);
										$remainig_stars = 5 - $stars;
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $waiter->name }}</h5>
										<div>
											<span title="{{ $waiter_of_the_month->average_rating / $waiter_of_the_month->total_records }}" data-toggle='tooltip'>
												@php
													for($i = 0; $i < $stars; $i++){
														echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
													}
													
													for($j = 0; $j < $remainig_stars; $j++){
														echo "<i class='fa fa-star text-muted'></i>";
													}
												@endphp
											</span>
										</div>
										<div class='mb-2'>
											
										@php
											$waiter_of_the_month_tips = DB::table('orders')
											->select(DB::raw('sum(waiter_tip) as waiter_tips'))
											->where('venue_id', $id)
											->whereBetween('created_at', [\Carbon\Carbon::today()->startOfMonth(), \Carbon\Carbon::today()->endOfMonth()])
											->groupBy('waiter_id')
											->orderBy('waiter_tips', 'desc')
											->first();
										@endphp
										@if($waiter_of_the_month_tips)
											Tip: {{ $currency }} {{ number_format($waiter_of_the_month_tips->waiter_tips, 2) }}
										@endif
										</div>
										<a target='_blank' href="{{ route('admin.venue.staff.edit', [$waiter->venue_id, $waiter->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Waiter</a>
									@endif
								</div>
							</div>
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Most Selling Product</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-package'></i></a>
										
									</div>
								</div>
								<div class="ibox-body text-center">
									@if($product_of_the_month)
										@php
											$product = DB::table('products')->where('id', $product_of_the_month->product_id)->first();
											if($product->image == ''){
												$product->image = 'default.png';
											}
										@endphp
										<div>
											<img class="img-circle" src="{{ asset('public/uploads/products/'.  $product->image) }}" width="100" height="100">
										</div>
										
										<h5 class='text-danger mt-3'>{{ $product->name }}</h5>
										<div class='mb-2'>
										{{ number_format($product_of_the_month->quantity) }} Orders
										</div>
										<a target='_blank' href="{{ route('admin.venue.product.edit', [$product->venue_id, $product->id]) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Product</a>
									@endif
								</div>
							</div>
							
							
							<div class="ibox ibox-fullheight">
								<div class="ibox-head">
									
									<div class="ibox-title">Hot Table</div>
									<div class="ibox-tools">
										<a href='javascript:;'><i class='ti-view-grid'></i></a>
										
									</div>
								</div>
								<div class="ibox-body">
									@if($table_of_the_month)
									@php
										$table = DB::table('tables')->where('id', $table_of_the_month->table_id)->first();
										$table_of_the_day_cost = DB::table('orders')
										->select(DB::raw('sum(total_amount) as table_cost'))
										->where('venue_id', $id)
										->whereBetween('created_at', [\Carbon\Carbon::today()->startOfMonth(), \Carbon\Carbon::today()->endOfMonth()])
										->groupBy('table_id')
										->orderBy('table_cost', 'desc')
										->first();
										
										$bookings = DB::table('bookings')->where('table_id', $table->id)
										->whereBetween('created_at', [\Carbon\Carbon::today()->startOfMonth(), \Carbon\Carbon::today()->endOfMonth()])->count();
									@endphp
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Table ID
										</div>
										<div class='col-md-5'>
										{{ $table->table_id }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Orders
										</div>
										<div class='col-md-5'>
										{{ number_format($table_of_the_month->total_orders) }}
										</div>
								   </div>
								   <div class='row mb-3'>
										<div class='col-md-7'>
											Total Cost
										</div>
										<div class='col-md-5'>
										{{ $currency }} {{ number_format($table_of_the_day_cost->table_cost, 2) }}
										</div>
								   </div>
								   
								   <div class='row'>
										<div class='col-md-7'>
											Bookings
										</div>
										<div class='col-md-5'>
										{{ number_format($bookings) }}
										</div>
								   </div>
								   @endif
								</div>
							</div>
							
							
							
						</div>
						
					</div>
					
					
					
				</div>
			</div>
		</div>
	</div>
		
		
	</div>
	
	
	<div class='row'>
		
		<div class="col-xl-4">
			<div class="ibox " style="min-height:494px;">
				<div class="ibox-head">
					<div class="ibox-title">Average Ratings</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-star"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class="flexbox mb-3">
						<span class="flexbox">
							<span class="btn-icon-only btn-circle bg-primary-50 text-primary mr-3"><i class="ti-star"></i></span>
							<span>Current Day</span>
						</span>
						@if($rating_of_the_day && $rating_of_the_day->average_rating != 0)
						@php
							$stars = round($rating_of_the_day->average_rating / $rating_of_the_day->total_records);
							$remainig_stars = 5 - $stars;
						@endphp
						<span title="{{ $rating_of_the_day->average_rating / $rating_of_the_day->total_records }}" data-toggle='tooltip' class="h3 mb-0 text-warning ml-2" style='font-size: 16px;'>
							@php
								for($i = 0; $i < $stars; $i++){
									echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
								}
								
								for($j = 0; $j < $remainig_stars; $j++){
									echo "<i class='fa fa-star text-muted'></i>";
								}
							@endphp
						</span>
						@endif
					</div>
					<div class="flexbox mb-3">
						<span class="flexbox">
							<span class="btn-icon-only btn-circle bg-success-50 text-success mr-3"><i class="ti-star"></i></span>
							<span>Current Week</span>
						</span>
						@if($rating_of_the_week && $rating_of_the_week->average_rating != 0)
						@php
							$stars = round($rating_of_the_week->average_rating / $rating_of_the_week->total_records);
							$remainig_stars = 5 - $stars;
						@endphp
						<span title="{{ $rating_of_the_week->average_rating / $rating_of_the_week->total_records }}" data-toggle='tooltip' class="h3 mb-0 text-warning ml-2" style='font-size: 16px;'>
							@php
								for($i = 0; $i < $stars; $i++){
									echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
								}
								
								for($j = 0; $j < $remainig_stars; $j++){
									echo "<i class='fa fa-star text-muted'></i>";
								}
							@endphp
						</span>
						@endif
					</div>
					<div class="flexbox mb-3">
						<span class="flexbox">
							<span class="btn-icon-only btn-circle bg-pink-50 text-pink mr-3"><i class="ti-star"></i></span>
							<span>Current Month</span>
						</span>
						@if($rating_of_the_month && $rating_of_the_month->average_rating != 0)
						@php
							$stars = round($rating_of_the_month->average_rating / $rating_of_the_month->total_records);
							$remainig_stars = 5 - $stars;
						@endphp
						<span title="{{ $rating_of_the_month->average_rating / $rating_of_the_month->total_records }}" data-toggle='tooltip' class="h3 mb-0 text-warning ml-2" style='font-size: 16px;'>
							@php
								for($i = 0; $i < $stars; $i++){
									echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
								}
								
								for($j = 0; $j < $remainig_stars; $j++){
									echo "<i class='fa fa-star text-muted'></i>";
								}
							@endphp
						</span>
						@endif
					</div>
					<div class="abs-bottom">
						<canvas id="subscriptions_chart" style="height:220px;"></canvas>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div class="col-lg-8">
			<div class='row'>
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Prep Area</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="fa fa-glass"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_bars == 0) 0 @else {{ round($active_bars * 100 / $total_bars ) }} @endif" data-bar-color="#18C5A9" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_bars == 0) 0 @else {{ round($active_bars * 100 / $total_bars ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_bars) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_bars) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Tables</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-view-grid"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_tables == 0) 0 @else {{ round($active_tables * 100 / $total_tables ) }} @endif" data-bar-color="#5c6bc0" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_tables == 0) 0 @else {{ round($active_tables * 100 / $total_tables ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_tables) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_tables) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Products</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-package"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_products == 0) 0 @else {{ round($active_products * 100 / $total_products ) }} @endif" data-bar-color="#f75b60" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_products == 0) 0 @else {{ round($active_products * 100 / $total_products ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_products) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_products) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Staff</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="{{ round($active_staff * 100 / $total_satff ) }}" data-bar-color="#f39c12" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">{{ round($active_staff * 100 / $total_satff ) }}<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_satff) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_staff) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
			</div>
        </div>
		
		
		
		
	</div>
	
</div>
@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	
	// SUBSCRIPTIONS Chart

    var ctx = document.getElementById("subscriptions_chart").getContext("2d");
    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
    gradientFill.addColorStop(0, "#00dcaf");
    gradientFill.addColorStop(1, "#7536e6");
    var chartData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
            {
                label: "Rating",
                backgroundColor: gradientFill,
                hoverBackgroundColor: gradientFill,
                //data: [30, 25, 50, 30, 50, 35, 60],
                data: [{{ $jan_rating }}, {{ $feb_rating }}, {{ $mar_rating }}, {{ $apr_rating }}, {{ $may_rating }}, {{ $jun_rating }}, {{ $jul_rating }}, {{ $aug_rating }}, {{ $sep_rating }}, {{ $oct_rating }},{{ $nov_rating }},{{ $dec_rating }}],
                pointBorderWidth: 1,
                pointRadius: 0,
                pointHitRadius: 30,
                pointHoverBackgroundColor: gradientFill,
                pointHoverBorderColor: '#ffe8f0',
                pointHoverBorderWidth: 5,
                pointHoverRadius: 6
            }
        ],
    };
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        showScale: false,
        scales: {
            xAxes: [{
                display: false,
                gridLines: false,
            }],
            yAxes: [{
                display: false,
                gridLines: false,
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                top: 10,
                bottom: 0
            }
        },
        legend: {display: false}
    };
    new Chart(ctx, {type: 'line', data: chartData, options: chartOptions});
		
		 $('.easypie').each(function(){
        $(this).easyPieChart({
          trackColor: $(this).attr('data-trackColor') || '#f2f2f2',
          scaleColor: false,
        });
    });

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});	 
	
</script>
@endsection