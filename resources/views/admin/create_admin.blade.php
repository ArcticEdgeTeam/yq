@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')


<div class="page-heading">
	<h1 class="page-title">Settings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item">Create Admin</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	<form id='profile_form' class='form-danger' method='post' action="{{ route('admin.create.new-admin') }}" enctype='multipart/form-data'>
	
	@csrf
	<div class="row">
		
		<div class='col-md-4'>
			<div class="ibox">
				<div class="ibox-body text-center" style='position: relative;height: 450px;'>
					<div class='text-center'>
					<img id='previewfile' class='img-circle' width='300' height='300' src="{{ asset('public/uploads/users/default.png') }}">
					</div>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Change Avatar' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input onchange="PreviewprofileImage();" id='uploadfile' type='file' name='photo' class='d-none' accept="image/*">
					</div>
					<h5 class='text-danger mt-3'> </h4>
					<span class='text-success'>Admin</span>
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-8 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Contact Details</div>
					<div class="ibox-tools">
						<!-- <a href='javascript:;'><i class='ti-user'></i></a> -->
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input checked name='status' type="checkbox">
								<span></span>
							</label>
						</div>
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
				   
						<div class="form-group mb-4 col-md-6">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input required name='name' type='text' class='form-control' value="{{old('name')}}">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6 pl-0">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input name='email' type='text' class='form-control' value="{{old('email')}}" required>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Phone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-phone"></i></span>
								<input name='phone' value="{{old('phone')}}" type='text' class='form-control' required>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6 pl-0">
							<label>Address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input id="autocomplete" onFocus="geolocate()" name='address' value="{{old('address')}}" type='text' class='form-control' required>
							</div>
						</div>
						
						
						
						<div class="form-group mb-4 col-md-6">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input id='password' name='password' type='password' class='form-control' required>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='password_confirm' type='password' id='password_confirm' class='form-control' required>
							</div>
						</div>

						<div class="col-md-8"></div>

					 	<div class="form-group mb-4 col-md-4">
							<button value="abc" name="action" class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Create New Admin</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create Admin</span>
							</span>
							</button>
						</div>
						
				   </div>


				    
				</div>
			</div>
		</div>	
		
	</div>
	</form>
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>

<script>
var placeSearch, autocomplete;
function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
  document.getElementById('autocomplete'), {types: ['geocode']});
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}



$(document).ready(function(){
	
	$('#profile_form').on('submit', function(e){
		
		var _password = $('#password').val();
		var _password_confirm = $('#password_confirm').val();
		if(_password != '') {

			if (_password.length < 8) {

				swal("", "Password must be 8 chanracters long.", "error");
				$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
				return false;
			}
			
			if(_password != _password_confirm){
				e.preventDefault();
				swal("", "Password Mismatch", "error");
				$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
			}
			
		}
		
	});
	
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection