@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')


<!-- The Modal -->
<div class="modal" id="approve_form_modal">
<div class="modal-dialog">
  <div class="modal-content">
	<!-- Modal body -->
	<div class="modal-body text-center">
		<form class='form-danger' method='post' action="{{ route('admin.refund.approve', $refund->id) }}">
		@csrf
		  <h4 class="modal-title mb-2">Are You Sure <br /> <small class='text-muted'>Refund amount of this order?</small></h4>
		  <div class='form-group'>
		  <textarea name='reply' rows='3' placeholder='Reply' class='form-control'></textarea>
		  </div>
		  <div class='form-group'>
		  <input type="submit" class="btn btn-success" value='Yes'>
		  <span  class="btn btn-danger" data-dismiss="modal">No</span>
		  </div>
	  </form>
	</div>
  </div>
</div>
</div>


<!-- The Modal -->
<div class="modal" id="cancel_form_modal">
<div class="modal-dialog">
  <div class="modal-content">
	<!-- Modal body -->
	<div class="modal-body text-center">
		<form class='form-danger' method='post' action="{{ route('admin.refund.cancel', $refund->id) }}">
		@csrf
		  <h4 class="modal-title mb-2">Are You Sure <br /> <small class='text-muted'>Cancel this request?</small></h4>
		  <div class='form-group'>
		  <textarea name='reply' rows='3' placeholder='Reply' class='form-control'></textarea>
		  </div>
		  <div class='form-group'>
		  <input type="submit" class="btn btn-success" value='Yes'>
		  <span  class="btn btn-danger" data-dismiss="modal">No</span>
		  </div>
	  </form>
	</div>
  </div>
</div>
</div>





<div class="page-content fade-in-up">

	<div class='row'>
		<div class="col-xl-3">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Customer Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div class="ibox-body text-center">
				   <img src="{{ asset('public/uploads/users/' . $customer->photo) }}" width='120' height='120' class='img-circle'>
				   <br />
				   <h5 class='mt-3 text-danger'>{{ $customer->name }}</h5>
				   <div class='text-success mt-2'>
				   <i data-toggle='tooltip' title='Lifetime Spent' class="fa fa-money" aria-hidden="true"></i> {{ $currency }} {{ DB::table('transactions')->where('customer_id', $customer->id)->sum('amount') }}
				   </div>
				   
				   <div class='mt-2'>
					{{ $customer->email }}
				   </div>
				   
				   <div class='mt-2'>
					{{ $customer->phone }}
				   </div>
					
				   <div class='mt-2'>
				   <a target='_blank' href="{{ route('admin.customer.edit', $customer->id) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
				   </div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-6 pl-0">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Refund Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-loop"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
					 
						
						<div class="form-group mb-4 col-md-12">
							<b>Reason</b>
							<p>{{ $refund->reason }}</p>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<b>Reply</b>
							<p>{{ $refund->reply }}</p>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<b>Status</b>
							<p>
							@if($refund->refund_status == 'Pending')
							<span class="badge badge-warning">Refund Claimed</span>
							@elseif($refund->refund_status == 'Refunded')
							<span class="badge badge-success">Refunded</span>
							@elseif($refund->refund_status == 'Cancelled')
							<span class="badge badge-danger">Cancelled</span>
							@endif
							</p>
						</div>
						
						<div class="form-group col-md-12 mb-4">
							
							@if($refund->refund_status == 'Pending')
								<div class='row'>
									
									<div class='col-sm-8'>
										<button  data-toggle="modal" data-target="#approve_form_modal" class='btn btn-success'>Refund</button>
										<button data-toggle="modal" data-target="#cancel_form_modal" class='btn btn-danger'>Cancel</button>
									</div>
									
									<div class='col-sm-4 text-right'>
										<a href="{{ route('admin.refunds') }}" class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Go Back</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Back</span>
											</span>
										</a>
									</div>
								</div>
							@else
								<div class='row'>
									<div class='col-sm-12'>
										<a href="{{ route('admin.refunds') }}" class="btn btn-danger btn-fix btn-animated from-left">
											<span class="visible-content">Go Back</span>
											<span class="hidden-content">
												<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Back</span>
											</span>
										</a>
									</div>
								</div>
							@endif
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-3 pl-0">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Order Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div style='line-height: 30px;' class="ibox-body">
				  <div class='row'>
					<div class='col-md-6'>
						Order#
					</div>
					<div class='col-md-6'>
						<a class='text-danger' target='_blank' href="{{ route('admin.venue.order.edit', [$order->venue_id, $order->id]) }}">{{ $order->order_number }}</a>
					</div>
				  </div>
				  
				  <div class='row'>
					<div class='col-md-6'>
						Order Total
					</div>
					<div class='col-md-6'>
					{{ $currency }} {{ $order->total_amount }}
					</div>
				  </div>
				  
				  <div class='row'>
					<div class='col-md-6'>
						Waiter
					</div>
					<div class='col-md-6'>
						<a class='text-danger' target='_blank' href="{{ route('admin.venue.staff.edit', [$order->venue_id, $waiter->id]) }}">{{ $waiter->name }}</a>
					</div>
				  </div>
				  
				  <div class='row'>
					<div class='col-md-6'>
						Waiter Tip
					</div>
					<div class='col-md-6'>
					{{ $currency }} {{ $order->waiter_tip }}
					</div>
				  </div>
				  
				  <div class='row'>
					<div class='col-md-6'>
						Order Date
					</div>
					<div class='col-md-6'>
					{{ date('d M Y', strtotime($order->created_at)) }}
					</div>
				  </div>
				  
				   <div class='row'>
					<div class='col-md-6'>
						Order Time
					</div>
					<div class='col-md-6'>
					{{ date('H:i', strtotime($order->created_at)) }}
					</div>
				  </div>
				  
				</div>
			</div>
		</div>
		
	</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection