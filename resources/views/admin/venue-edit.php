	
<div class="page-heading">
	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item">KFC</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<div class="ibox p-4">             
		
			<ul class="nav nav-tabs tabs-line tabs-line-pink">
				<li class="nav-item">
					<a class="nav-link" href="overview.php" >Overview</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="categories.php" >Categories</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="products.php" >Products</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="reports.php" >Analytics | Reports</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="orders.php" >Orders</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="waiters.php" >Waiters</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="bars.php" >Bars</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="information.php" >Information</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="settings.php" >Settings</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="ratings.php" >Reviews</a>
				</li>
			</ul>
			
	</div>
	
	<div class='ibox p-4'>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="tab-5-1">
			</div>
			<div class="tab-pane fade" id="tab-5-2">2
			</div>
			<div class="tab-pane fade" id="tab-5-3">3
			</div>
			<div class="tab-pane fade" id="tab-5-4">4
			</div>
			<div class="tab-pane fade" id="tab-5-5">
				
				<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Order Number</th>
						<th>Date Time</th>
						<th>Waiter</th>
						<th>Status</th>
						<th>Cost</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						
						<td>7-3</td>
						<td>2 Aug 2019 9:03</td>
						<td>Jane Doe</td>
						<td>Paid</td>
						<td>Rs 33</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href='order-edit.php'><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					<tr>
						
						<td>11-2</td>
						<td>2 Aug 2019 9:03</td>
						<td>Jhon Doe</td>
						<td>Pending</td>
						<td>Rs 45</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href='order-edit.php'><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
				
				
			</div>
			<div class="tab-pane fade" id="tab-5-6">6
			</div>
			<div class="tab-pane fade" id="tab-5-7">7
			</div>
			<div class="tab-pane fade" id="tab-5-8">8
			</div>
			<div class="tab-pane fade" id="tab-5-9">9
			</div>
			<div class="tab-pane fade" id="tab-5-10">10
			</div>
		</div>
	</div>
</div>
 