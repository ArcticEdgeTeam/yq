@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
	\Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SUNDAY);
	\Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::SATURDAY);
										
@endphp

@extends('layouts.admin')
@section('page_css')
	<style>
	.veneu_performance .ibox{
		width: 32%;
	}
	
	.veneu_performance2 .ibox{
		width: 23.79%;
		height: 350px;
	}
	
	.static-widget{
		height: 76px;
		line-height: 22px;
		width: auto !important;
	}
	
	.static-widget h4{
		height: 26px;
	}
	
	</style>
@endsection

@section('page_content')

@if($venue_id !=0)	
<div class="static-widget bg-danger text-white" style='position: fixed; z-index: 30; right: 0; top: 84px;'>
	<h4 class='m-0'>
	{{ $currency }} {{ number_format($earnings_this_month,2) }}
	</h4>
	<small>This Month</small>
</div>
@endif

<div class="page-content fade-in-up">
	<form method='post' action="{{ route('admin.home.filter') }}">
	@csrf
	<div class='row mb-4'>
		<div class='col-lg-12 text-center'>
			<div class="form-group mt-3">
				<select name="venue_id" required class="selectpicker bg-white">
					<option value=''>Select Venue</option>
					@foreach($venues as $venue)
						<option @if($venue->id == $venue_id) selected @endif value="{{ $venue->id }}">{{ $venue->name }}</option>
					@endforeach
				</select>
				<input type='submit' name='action' value='Filter' class='btn btn-danger'/>
			</div>
		</div>
	</div>
	</form>
	
	@if($venue_id !=0)
	<div class='row'>
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-head">
					
					<div class="ibox-title">Venue Admin</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-user'></i></a>
						
					</div>
				</div>
				<div class="ibox-body text-center">
					@php
						$venue_admin = DB::table('users')->where('role', 'venue')->where('venue_id', $venue_id)->first();
						if($venue_admin->photo == ''){
							$venue_admin->photo = 'default.png';
						}
					@endphp
						<div>
							<img class="img-circle" src="{{ asset('public/uploads/users/' . $venue_admin->photo) }}" width="100" height="100">
						</div>
						
						<h5 class='text-danger mt-3'>{{ $venue_admin->name }}</h5>
						
						<div class='mb-1'>
							{{ $venue_admin->phone }}
						</div>
						<div class='mb-2'>
							{{ $venue_admin->email }}
						</div>
						
						<a href="{{ route('admin.venue.setting', $venue_admin->venue_id) }}" class="btn btn-outline-danger btn-fix btn-thick btn-air btn-sm">View Venue</a>
				</div>
			</div>
		</div>
		
		<div class='col-md-9 pl-0'>
			
			<div class="ibox">
				<div class="ibox-body">
					<div class="flexbox mb-5">
						<div class="flexbox">
							<span class="mr-4 text-danger" style="width:60px;height:60px;background-color:#e7e9f6;display:inline-flex;align-items:center;justify-content:center;font-size:35px;"><i class="ti-pulse"></i></span>
							<div>
								<h5 class="font-strong">QUICK STATS</h5>
								<div class="text-light">Performance of Venue</div>
							</div>
						</div>
						<ul class="nav nav-pills nav-pills-danger nav-pills-rounded nav-pills-air">
							<li class="nav-item ml-1">
								<a class="nav-link active" href="#tab_1" data-toggle="tab">Today</a>
							</li>
							<li class="nav-item ml-1">
								<a class="nav-link" href="#tab_2" data-toggle="tab">Lifetime</a>
							</li>
							
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="tab_1">
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($orders_today)}}</h2>
										<div>ORDERS TODAY</div><i class="ti-shopping-cart widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($sales_today,2) }}</h2>
										<div>SALES TODAY</div><i class="ti-stats-up widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($earnings_today, 2) }}</h2>
										<div>EARNINGS TODAY</div><i class="ti-money widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
							
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($customers_today) }}</h2>
										<div>CUSTOMERS TODAY</div><i class="ti-user widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($bookings_today) }}</h2>
										<div>BOOKINGS TODAY</div><i class="ti-calendar widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($views_today) }}</h2>
										<div>VIEWS TODAY</div><i class="ti-eye widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
						</div>
						<div class="tab-pane fade" id="tab_2">
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($orders_lifetime) }}</h2>
										<div>ORDERS LIFETIME</div><i class="ti-shopping-cart widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($sales_lifetime,2) }}</h2>
										<div>SALES LIFETIME</div><i class="ti-stats-up widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ $currency }} {{ number_format($earnings_lifetime,2) }}</h2>
										<div>EARNINGS LIFETIME</div><i class="ti-money widget-stat-icon text-danger"></i>
										
									</div>
								</div>
								
								
							</div>
							
							
							<div class="flexbox veneu_performance">
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($customers_lifetime) }}</h2>
										<div>CUSTOMERS LIFETIME</div><i class="ti-user widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($bookings_lifetime) }}</h2>
										<div>BOOKINGS LIFETIME</div><i class="ti-calendar widget-stat-icon text-danger"></i>
									</div>
								</div>
								
								<div class="ibox">
									<div class="ibox-body">
										<h2 class="mb-1">{{ number_format($views_lifetime) }}</h2>
										<div>VIEWS LIFETIME</div><i class="ti-eye widget-stat-icon text-danger"></i>
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	
	
	<div class='row'>
		
		<div class="col-lg-4">
			<div class="ibox" style="min-height:494px;">
				<div class="ibox-head">
					<div>
					<div class="ibox-title">Earnings</div></div>
					<div class="ibox-tools">
						<a class="dropdown-toggle" data-toggle="dropdown"><i class="ti-server"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class="h1 mt-4">{{ $currency }} {{ number_format($earnings_lifetime,2) }}<i class="ti-stats-up float-right text-success" style="font-size:40px;"></i></div>
					<div class="text-lighter">Total Earnings Lifetime</div>
					<div class="abs-bottom">
						<canvas id="revenue_chart" style="height:220px;"></canvas>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-8">
			<div class='row'>
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Prep Area</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="fa fa-glass"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_bars == 0) 0 @else {{ round($active_bars * 100 / $total_bars ) }} @endif" data-bar-color="#18C5A9" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_bars == 0) 0 @else {{ round($active_bars * 100 / $total_bars ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_bars) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_bars) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Tables</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-view-grid"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_tables == 0) 0 @else {{ round($active_tables * 100 / $total_tables ) }} @endif" data-bar-color="#5c6bc0" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_tables == 0) 0 @else {{ round($active_tables * 100 / $total_tables ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_tables) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_tables) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Products</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-package"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="@if($total_products == 0) 0 @else {{ round($active_products * 100 / $total_products ) }} @endif" data-bar-color="#f75b60" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">@if($total_products == 0) 0 @else {{ round($active_products * 100 / $total_products ) }} @endif<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_products) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_products) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
				<div class='col-lg-6 pl-0'>
					
					<div class="ibox">
						<div class="ibox-head">
							<div class="ibox-title">Staff</div>
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
							</div>
						</div>
						<div class="ibox-body">
							<div class="row align-items-center">
								<div class="col-md-6">
									<div class="easypie centered" data-percent="{{ round($active_staff * 100 / $total_satff ) }}" data-bar-color="#f39c12" data-size="80" data-line-width="5">
										<span class="easypie-data h4 font-strong">{{ round($active_staff * 100 / $total_satff ) }}<sup>%</sup></span>
									</div>
								</div>
								<div class="col-md-6">
									
									<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($total_satff) }}</div>
									<div class="text-muted">Total</div>
								</div>
								<hr />
								<div class="text-center">
									<div class="mb-1 text-success h4">{{ number_format($active_staff) }}</div>
									<div class="text-muted">Active</div>
								</div>
									
									
								</div>
							</div>
							
						</div>
					</div>
					
					
				</div>
				
				
			</div>
        </div>
		
	</div>
	@endif
	
</div>
@endsection

@section('page_plugin_js')
	<script src="{{ asset('public/assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
@endsection

@section('page_js')
@if($venue_id !=0)
	<script>
	
	 // Revenue Chart

    var ctx = document.getElementById("revenue_chart").getContext("2d");
    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
    gradientFill.addColorStop(0, "#f39c12");
    gradientFill.addColorStop(1, "#e91e63");
    var chartData = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
            {
                label: "ZAR",
                backgroundColor: gradientFill,
                hoverBackgroundColor: gradientFill,
                //data: [30, 25, 50, 30, 50, 35, 60],
                data: [{{ $jan_earnings }}, {{ $feb_earnings }}, {{ $mar_earnings }}, {{ $apr_earnings }}, {{ $may_earnings }}, {{ $jun_earnings }}, {{ $jul_earnings }}, {{ $aug_earnings }}, {{ $sep_earnings }}, {{ $oct_earnings }}, {{ $nov_earnings }}, {{ $dec_earnings }}],
                pointBorderWidth: 1,
                pointRadius: 0,
                pointHitRadius: 30,
                pointHoverBackgroundColor: gradientFill,
                pointHoverBorderColor: '#ffe8f0',
                pointHoverBorderWidth: 5,
                pointHoverRadius: 6
            }
        ],
    };
	
	
	
    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        showScale: false,

        elements: {
            line: {
                tension: 0
            },
        },
        scales: {
            xAxes: [{
                display: false,
                gridLines: false,
                scaleLabel: {
                    display: true,
                }
            }],
            yAxes: [{
                display: false,
                gridLines: false,
                scaleLabel: {
                    display: true,
                },
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        layout: {
            padding: {
                left: 0,
                right: 0,
                top: 10,
                bottom: 0
            }
        },
        legend: {display: false}
    };
    new Chart(ctx, {type: 'line', data: chartData, options: chartOptions});
	
	$('.easypie').each(function(){
        $(this).easyPieChart({
          trackColor: $(this).attr('data-trackColor') || '#f2f2f2',
          scaleColor: false,
        });
    });
	
</script>
@endif
@endsection