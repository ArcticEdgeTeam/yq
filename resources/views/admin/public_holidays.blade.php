@extends('layouts.admin')

@section('page_plugin_css')

<link href="{{asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" />

@endsection

@section('page_css')
	
<style>
 /*Age Rage*/

.container {
  margin-top: 40px;
}

.slider-labels {
  margin-top: 10px;
}

.datepicker-inline {
    width: 100%;
}
.datepicker table {
    width: 100%;
}


	
</style>
@endsection

@section('page_content')

<div class="page-heading">
	<h1 class="page-title">Public Holidays</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.holidays') }}">Public Holidays</a></li>
		<li class="breadcrumb-item">New</li>
	</ol>
</div>
<div class="page-content fade-in-up">
						

	<div class='row'>
		
			<div class="col-lg-12 pl-20">
				<div class="ibox ibox-fullheight">
					<div class="ibox-head">
						<div class="ibox-title">Holidays</div>
						<div class="ibox-tools">
							<div class="ibox-tools">
								<a class="font-18" href="javascript:;"><i class="ti-time"></i></a>
							</div>
						</div>
					</div>

				<form class='form-danger' method='post' action="{{ route('admin.holidays') }}" enctype='multipart/form-data'>
						@csrf
					<div class="ibox-body">
					   <input type="hidden" name="holiday_id" value="{{$holiday->id}}">
					   <input type="hidden" name="date" value="" id="booking_date">
					   <div class="form-group mb-4">
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
								<input required name='holiday_title' value="{{$holiday->day}}" class="form-control" type="text" placeholder="Holiday Title" readonly>
							</div>
						</div>
						
					
						<!-- <div class="form-group" id="date_1">
                            <label class="font-normal"></label>
                            <div class="input-group date">
                                <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>

                                <input class="form-control" name="date" type="text"\
                                @if ($holiday->date != '') 
                                @php 
                                	$date = json_decode($holiday->date); 
                                @endphp
                                 value="{{ $date }}"
                                 @else {{date('Y-m-d')}} 
                                 @endif" 

                                 required>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label></label>
                            <div id="date_6"  data-date-format="mm/dd/yyyy"> </div>
                        </div>
							
							
						<br>
						<div class='form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Holidays</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
								</span>
							</button>
					   </div>	
	
					</div>
				</form>
				</div>
			</div>
		</div>
	
</div>



@endsection

@section('page_plugin_js')

<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	// initialize date picker
		$("#date_6").datepicker({
			multidate: true,
			altField: '#booking_date'

		});

		var booking_date = '{{json_decode($holiday->date)}}';
		booking_date = booking_date.split(',');
		var formattedDate = [];
		for (var j = 0; j < booking_date.length; j++) {
			var date = new Date(booking_date[j]);
			formattedDate.push(date.toLocaleDateString());
		}

		//set values in date picker
		$("#date_6").datepicker('setDate', formattedDate);

	// save values to the hidden field
	$('.from-left').click(function(){
		 var value = $('#date_6').datepicker('getFormattedDate');
		 $('#booking_date').val(value);
		 if(value == '')  {
		 	event.preventDefault();
		 	alert('Please select public holidays!');
		 }
	});
	 

</script>
@endsection

 