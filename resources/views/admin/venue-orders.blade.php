@extends('layouts.admin')
@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />

<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.products_list th, .products_list td{
		padding: 0;
		text-align: left;
	}
	
	.products_list table{
		border: 0 !important;
		font-size: 11px;
	}
	
	.products_list thead th{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.products_list tbody td{
		border-right: 0 !important;
		border-left: 0 !important;
	}
	
	.table_box{
		padding-left: 54px;
		position: relative;
		bottom: 10px;
	}
	.order_status a{
		padding: 2px 8px 2px 8px !important;
		font-size: 12px;
	}
	
	.sweet-alert h2{
		margin-top: 30px;
	}
	
</style>
@endsection

@section('page_content')

<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.orders', $id) }}">Orders</a></li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<div class='row'>
	<div class='col-md-6'>
	<div class='ibox p-4'>
	
			<div class="ibox-head">
				<div class="ibox-title">Active Orders</div>
				<div class="ibox-tools">
					<div class="flexbox">
						<a class='btn btn-danger btn-sm' href="{{ route('admin.venue.order.list', $id) }}">All Orders</a>
					</div>
				</div>
				
			</div>
		
		<!-- <div class="flexbox mb-4 mt-4">
			
			<div>
				
			</div>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div> -->
		
		<form action="{{route('admin.venue.orders', $id)}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-4">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group mb-4 col-md-4">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == 'Ordered') selected @endif  value="Ordered">Ordered</option>
							<option @if(request()->status == 'In Oven') selected @endif  value="In Oven">In Oven</option>
							<option @if(request()->status == 'Final Steps') selected @endif  value="Final Steps">Final Steps</option>

						
					</select>
				</div>

				<div class="form-group col-md-2" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Customer</th>
						<th>Time</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getOrders->isEmpty())
					@foreach($getOrders as $order)
					@php
						$order->user_photo = $order->user_photo == '' ? 'default.png' : $order->user_photo;
					
					@endphp 
					<tr>
						
						<td><img src="{{ asset('public/uploads/users/' . $order->user_photo) }}" width="40" height="40" class="img-circle"> {{ $order->user_name }}</td>
						<td>{{ $order->created_at->format('H:i') }}</td>
						<td>
						@if($order->order_status == 'Completed')
						<span class="badge badge-success">Completed</span>
						@elseif($order->order_status == 'Ordered')
							<span class="badge badge-danger">Ordered</span>
						@elseif($order->order_status == 'In Oven')
							<span class="badge badge-warning">In Oven</span>
						@elseif($order->order_status == 'Final Steps')
							<span class="badge badge-info">Final Steps</span>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.venue.order.edit', [$id, $order->id]) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
							<a class='text-info quickview' data-toggle='tooltip' title='Quick Info' href="{{ route('admin.venue.order.quickview', [$id, $order->id]) }}"><i style='font-size: 22px;' class='fa fa-info-circle'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="4" style="text-align:center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getOrders->appends(['name' => Request::get('name'), 'status' => Request::get('status')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>
		</div>
			
	</div>
	</div>
	
	
	<div id='quickview_area' class='col-md-6 pl-0'>	
		
	</div> 
	
	</div>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('page_js')
<script>

	$(function() {
	  $('#daterange_1').daterangepicker({
		autoUpdateInput: false,
	    locale: {
			format: 'DD-MM-YYYY',
			separator: " / "
		}
	  });

	  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
	      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
	  });

	  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
	      $(this).val('');
	  });

	});

	$(".quickview").click(function(e){
		e.preventDefault();
		$.ajax({
		   type:'GET',
		   url:$(this).attr('href'),
		   success:function(data){
			$('#quickview_area').html(data);
		   }
		});
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection