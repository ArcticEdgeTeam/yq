@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.admin')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	#participantTable, #participantTable2, #participantTable4, #participantTable5, #variable-ingredient{
		display: none;
		font-size: 12px;
	}
	
	#participantTable2 input[type="text"], #participantTable2 input[type="number"], #participantTable input[type="text"], #participantTable input[type="number"], #participantTable3 input[type="number"], #participantTable3 select, #participantTable5 input[type="text"],{
		padding: 6px 8px;
		font-size: 12px;
	}

	.side_required {
		padding: 6px 8px;
		font-size: 1rem;
	    line-height: 1.25;
	    color: #495057;
	    background-color: #fff;
	    background-image: none;
	    background-clip: padding-box;
	    border: 1px solid rgba(0,0,0,.15);
		height: 30px !important;
	}

	.select2_demo_11 {
		width: 180px !important;
	}

	.select2_demo_12 {
		width: 180px !important;
	}

	/*.image-required-text {
		color: red;
	}*/
	
</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.products', $id) }}">Products</a></li>
		<li class="breadcrumb-item">New</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<form id="product-form" class='form-danger' method='post' action="{{ route('admin.venue.product.save', $id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-body" style='position: relative;'>
					<div class='text-center'>
					<img id='previewfile' src="{{ asset('public/uploads/products/default.png') }}">
					<br>
					<!-- <span class='text-center image-required-text'>Image is required*</span> -->
					</div>
					
					<br><br>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Upload Image' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='image' class='d-none'>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Product Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
							<input name='name' required type='text' class='form-control'>
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Product Price</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left">{{ $currency }}</span>
							<input type="text" name='price' required  class='form-control price' onkeyup="numberSeperator(this)">
						</div>
					</div>

					<div class="row">
						<div class="form-group mb-4 col-md-6">
							<div class="mb-2">
								<label for='allergen' class="checkbox checkbox-inline">
									<input name='allergen' id='allergen' type="checkbox">
									<span class="input-span"></span>Allergen
								</label>
							</div>
						</div>

						<div class="form-group mb-4 col-md-6">
							<div class="mb-2">
								<label for='side' class="checkbox checkbox-inline">
									<input name='side_product' id='side' type="checkbox" value="1">
									<span class="input-span"></span>Sides
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-5 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Product Details</div>
					<div class="ibox-tools">
						<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input name='status' type="checkbox" checked>
						<span></span>
					</label>
						
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
				   
						<div class="form-group mb-4 col-md-6">
							<label>Product ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input name='product_id' required type='text' value="" class='form-control' onblur="getProductID($(this))">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Assign to Prep Area</label>
							<select name='bar_id' class="selectpicker form-control" required>
								@foreach($bars as $bar)
									<option value="{{ $bar->id }}">{{ $bar->name }}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Video Link</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-control-play"></i></span>
								<input name='video' type='text' class='form-control'>
							</div>
						</div>
						
						
						<div class="form-group mb-4 col-md-12">
							<label>Categories</label>
							<select name='category' class="form-control select2_demo_1" required>
								<option value="">--Select Category--</option>
								@foreach($categories as $category)
									@if ($category->has_childs == 'yes')
									<option value="{{ $category->id }}" disabled>{{ $category->name }}</option>
										@foreach($category->child as $child)
										@php
											$child = explode('/', $child);
										@endphp
										<option value="{{ $child[0] }}">- {{ $child[1]}}</option>
										@endforeach
									@else
									<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endif
								@endforeach
							</select>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Menus</label>
							<select name='menus[]' class="form-control select2_demo_1" multiple="">
								@foreach($menus as $menu)
									<option value="{{ $menu->id }}">{{ $menu->name }}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Description</label>
							<textarea name='description' class='form-control' required></textarea>
						</div>
						
						
						 
				   </div>
				    
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Addons</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-plus"></i></a>
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
						
						<div class="col-sm-12 form-group mb-2">
							<label>Type</label>
							<div class="form-group">
								<div class="mb-2">
									<label class="radio radio-inline">
										<input value='simple' id='type_simple_radio' type="radio" name="type" checked>
										<span class="input-span"></span>Simple</label>
										
									<label class="radio radio-inline variable-section">
										<input value='variant' id='type_variable_radio' type="radio" name="type">
										<span class="input-span"></span>Variable</label>
								</div>
							</div>
							
							<table id="participantTable">
									<thead>
										<tr>
											<th>Variable Name</th>
											<th>Price</th>
											<th></th>
										</tr>
									</thead>
									<tr class="participantRow" id="variable-row-1" data-variable-row='1'>
										<td><input name="variable_name[]" type="text" placeholder="Variable name.." class="form-control required-entry variation_required">
										  </td>
										  <td><input style='width: 130px;' name="variable_price[]" type="text" placeholder="" class=" form-control required-entry variation_required" onkeyup="numberSeperator(this)">
										  </td>
										
									</tr>
									<tr id="addButtonRow">
										<td colspan='2'></td>
										<td align='center'><button class="btn btn-sm btn-success add" type="button">+</button></td>
									</tr>
							</table>
							
						</div>
						
						<div class="col-sm-12 form-group mb-4 mt-2">
							<div class="form-group">
								<div class="mb-2">
									<label for='child_side_product' class="checkbox checkbox-inline">
										<input name='child_side_product' id='child_side_product' type="checkbox" value="1">
										<span class="input-span"></span>Add Sides</label>
								</div>
								
							</div>
							
							<table id="participantTable4">
								<thead>
									<tr>
										<th>Side Title <span style="margin-left: 130px;">No# Allowed</span></th>
										<th></th>
									</tr>
								</thead>
								<tbody id="table-body-parent">
								<tr class="participantRow4" id="table-row-1" data-parent-row="1">
									<td>
										<input name="side_name[1]" type="text" placeholder="Side Title.." class="required-entry side_required" style="width: 200px;">

										<input style='width: 70px;' name="no_allowed[1]" type="number" step="any" min="1" placeholder="" class="required-entry side_required" style="width: 70px;">
										
										<table id="childTableId-1">
											<tr class="childTableRow-1" data-child-row="1">
												<td>

													<select name='child_side_name[1][]' class="form-control select2_demo_11 side_required">
														<option value="">Select Product</option>
														@foreach($sideProducts as $product)
															<option value="{{ $product->id }}">{{ $product->name }}</option>
														@endforeach
													</select>
													<!-- <input name="child_side_name[1][]" type="text" placeholder="Add Side.." class="required-entry"> -->
												 </td>
												<td>
													
													<button class="btn btn-sm btn-success" type="button" onclick="addChildRow('1')">+</button>
												</td>
											</tr>
										</table>

									</td>
									  
									<td>
										
									</td>
								</tr>
								</tbody>

								<tr id="addButtonRow4">
									<td colspan='2'></td>
									<td align='center'><button class="btn btn-sm btn-success" onclick="addRow4()" type="button">+</button></td>
								</tr>
							</table>
							
						</div>

						<div class="col-sm-12 form-group mb-2">
							<div class="form-group">
								<div class="mb-2">
									<label for='add_preferences_checkbox' class="checkbox checkbox-inline">
										<input name='preferences' id='add_preferences_checkbox' type="checkbox">
										<span class="input-span"></span>Add Preferences</label>
								</div>
								
							</div>
							
							<table id="participantTable5">
								<thead>
									<tr>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody id="preference-table-body">
										<tr class="participantRow5-1">
											<td>
												<input name="preference_name[]" type="text" placeholder="name.." class="form-control required-entry preference_required">
											 </td>
											 
											<td>
												<button class="btn btn-danger btn-sm remove" type="button" onclick="removeRowPreference('1')">-</button>
											</td>
										</tr>
								</tbody>
										<tr id="addButtonRow4">
											<td colspan='2'></td>
											<td align='center'><button class="btn btn-sm btn-success" onclick="addRowPreference()" type="button">+</button></td>
										</tr>
							</table>
							
						</div>

						<div class="col-sm-12 form-group mb-4 mt-2">
							<div class="form-group">
								<div class="mb-2">
									<label for='add_extra_checkbox' class="checkbox checkbox-inline">
										<input name='extra' id='add_extra_checkbox' type="checkbox">
										<span class="input-span"></span>Add Extras</label>
								</div>
								
							</div>
							
							<table id="participantTable2">
								<thead>
									<tr>
										<th>Extra Name</th>
										<th>Discount %</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="ExtraTable2-Body">
								<tr class="participantRow2-1">
									<td>
										<select name='extra_name[]' class="form-control select2_demo_12 extra_required">
											<option value="">Select Product</option>
											@foreach($Products as $prod)
												<option value="{{ $prod->id }}">{{ $prod->name }}</option>
											@endforeach
										</select>
										
									  </td>
									  <td><input style='width: 100px;' name="extra_price[]" type="number" step="any" min="0" max="100" placeholder="%" class="form-control required-entry extra_required">
									  </td>
									<td><button class="btn btn-danger btn-sm remove2" type="button" onclick="removeRowExtra('1')">-</button></td>
								</tr>
								</tbody>
								<tr id="addButtonRow2">
									<td colspan='2'></td>
									<td align='center'><button class="btn btn-sm btn-success add2" type="button" onclick="addRowExtra()">+</button></td>
								</tr>
							</table>
							
						</div>		
						
				   </div>  
				</div>
			</div>
		</div>
	</div>

	<!-- Ingredient Section -->
	<div class="col-xl-12">
		<div class="ibox ibox-fullheight">
			<div class="ibox-body">
				<div class='row'>
					<div class='col-md-12 mb-3'>
						<b>Ingredients</b>
						<hr>
							<table id="participantTable3" style='width: 100%; border: 0;'>
								<thead>
									<tr>
										<th>Inggredient</th>
										<th>Unit</th>
										<th>Quantity</th>
										<th></th>
									</tr>
								</thead>
								<tr class="participantRow3">
									<td width='50%'>
										<select class='form-control normal_ingredient_required' name='ingredients[]'>
											<option value=''>Select Ingredient</option>
											@foreach($ingredients as $ingredient)
											<option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
											@endforeach
										</select>
									</td>
									<td width='30%'>
										<select class='form-control normal_ingredient_required' name='units[]'>
											<option value=''>Select Unit</option>
											@foreach($units as $unit)
											<option value="{{ $unit->id }}">{{ $unit->name }} ({{$unit->symbol}})</option>
											@endforeach
										</select>
									</td>
									<td width='20%'>

										<input name="quantities[]" type="text" step="any" min="0" value="0" placeholder='Quantity' class="form-control required-entry normal_ingredient_required" onkeyup="numberSeperator(this)">
									</td>
									<td align='right'><button class="btn btn-danger btn-sm remove3" type="button">-</button></td>
								</tr>
								<tr id="addButtonRow3">
									<td colspan='3'></td>
									<td align='right'><button class="btn btn-sm btn-success add3" type="button">+</button></td>
								</tr>
							</table>

<!-- //////////////////////////////////////////////// variable Ingredient //////////////////////////////////// -->
			<div class="row">
				<table id="variable-ingredient" style='width: 100%; border: 0;'>
					<thead class="ingredient-head">
						<th>Ingredients</th>
						<th>Units</th>
						<th class="td-title-1"></th>
					</thead>
					<tbody id="variable-ingredient-body">
						<tr id="variable-ingredient-1" data-ingredient-row="1" class="ingredient-row">
							<td>
								
								<select class='form-control variable_ingredient_required' name='var_ingredients[]'>
									<option value=''>Select Ingredient</option>
									@foreach($ingredients as $ingredient)
									<option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
									@endforeach
								</select>

							</td>
							<td>									
								
								<select class='form-control variable_ingredient_required' name='var_units[]'>
									<option value=''>Select Unit</option>
									@foreach($units as $unit)
									<option value="{{ $unit->id }}">{{ $unit->name }} ({{$unit->symbol}})</option>
									@endforeach
								</select>

							</td>	
							<td class="td-data-1">
								<input name="variable_qty_1[]" type="text" step="any" min="0" value="0" placeholder="" class=" form-control required-entry variable_ingredient_required" onkeyup="numberSeperator(this)">
							</td>
						</tr>
					</tbody>
					<tr id="addButtonRow3">
						<td colspan='3'></td>
						<td align='right'><button class="btn btn-sm btn-success add-ingredient" type="button">+</button></td>
					</tr>
				</table>
			</div>

	<!-- ///////////////////////////////////////////////////////////// Variable Ingredient ends ///////////////////////////// -->
					</div>
					<!-- col-12-ends -->
					<div class="col-md-10"></div>
					<div class="col-md-2 form-group mb-4">
						<button class="btn btn-danger btn-fix btn-animated from-left add-product">
						<span class="visible-content">Add Product</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
						</button>
					</div>
				</div>
				<!-- Row ends -->
			</div>
			<!-- ibox body ends -->
		</div>
			<!-- ibox full ends -->
	</div>
<!-- Ingredient Section ends -->
</form>
	<input type="hidden" id="last-added" value="1">
	<input type="hidden" id="extra-last-added" value="1">
	<input type="hidden" id="preference-last-added" value="1">
	<input type="hidden" id="variable-last-added" value="1">
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>


// thousand seperator
function numberSeperator(object) {
	var val = object.value;
  	val = val.replace(/[^0-9\.]/g,'');
  
	if(val != "") {
	    valArr = val.split('.');
	    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
	    val = valArr.join('.');
	 }
  
  	object.value = val;
}

///////////////////////////////// sides /////////////////////////////////////////
$(".select2_demo_11").select2();

$('#participantTable4').on('change', '.select2_demo_11', function() {
	var sidesValue = $(this);
	$('.select2_demo_11').not(this).each(function(){
		if (sidesValue.val() === $(this).val()) {
			sidesValue.select2('val', 'All');
			alert('This Product is already selected.');
			return false;
		}	
	});

});


function removeParentRow(rowId) {
	$("#table-row-"+rowId).remove();
} 

function removeChildRow(rowId) {
	$("#childTableRow-"+rowId).remove();
}

function addChildRow(id) {
	var unique_row = Math.floor(1000 + Math.random() * 9000);
	var child_row=`<tr id="childTableRow-`+unique_row+`">
						<td>
							<select name='child_side_name[`+id+`][]' class="form-control select2_demo_11 side_required" required>
								<option value="">Select Product</option>
								@foreach($sideProducts as $product)
								<option value="{{ $product->id }}">{{ $product->name }}</option>
								@endforeach
							</select>
						 </td>
						<td>
							<button class="btn btn-danger btn-sm" type="button" onclick="removeChildRow('`+unique_row+`')">-</button>
							<button class="btn btn-sm btn-success" type="button" onclick="addChildRow('`+id+`')">+</button>
						</td>
					</tr>`;
	$("#childTableId-"+id).append(child_row);

	$('.select2_demo_11').select2();
}

function addRow4() {
	
var row_length=parseInt($('#last-added').val()) +1;
$('#last-added').val(row_length);
var parent_row=`<tr class="participantRow4" id="table-row-`+row_length+`" data-parent-row="`+row_length+`">
					<td>
						<input name="side_name[`+row_length+`]" type="text" placeholder="Side Title.." class="required-entry side_required" required style="width: 200px;">

						<input style='width: 70px;' name="no_allowed[`+row_length+`]" type="number" step="any" min="1" placeholder="" class="required-entry side_required" required style="width: 70px;">
						
						<table id="childTableId-`+row_length+`">
							<tr class="childTableRow-`+row_length+`" data-child-row="`+row_length+`">
								<td>
									<select name='child_side_name[`+row_length+`][]' class="form-control select2_demo_11 side_required" required>
										<option value="">Select Product</option>
										@foreach($sideProducts as $product)
											<option value="{{ $product->id }}">{{ $product->name }}</option>
										@endforeach
									</select>
								<td>
									<button class="btn btn-sm btn-success" type="button" onclick="addChildRow('`+row_length+`')">+</button>
								</td>
							</tr>
						</table>

					</td>

					<td>
					<button class="btn btn-danger btn-sm" onclick="removeParentRow('`+row_length+`')" type="button" style="margin-bottom: 40px;">-</button><br>	
					</td>
				</tr>`;	
	var parent_row = $("#table-body-parent").append(parent_row);

	$('.select2_demo_11').select2();
	
}
/////////////////////////// sides ends //////////////////////////////////////////////////////////


////////////////////////// extras ////////////////////////////////////////////////////////////////

// extra select
$(".select2_demo_12").select2();

$('#participantTable2').on('change', '.select2_demo_12', function() {
	var extraValue = $(this);
	$('.select2_demo_12').not(this).each(function(){
		if (extraValue.val() === $(this).val()) {
			extraValue.select2('val', 'All');
			alert('This Product is already selected.');
			return false;
		}	
	});

});

function addRowExtra() {
	
var row_length= parseInt($('#extra-last-added').val()) +1;
$('#extra-last-added').val(row_length);
var parent_row=`<tr class="participantRow2-`+row_length+`">
									<td>
										<select name='extra_name[]' class="form-control select2_demo_12 extra_required" required>
											<option value="">Select Product</option>
											@foreach($Products as $prod)
												<option value="{{ $prod->id }}">{{ $prod->name }}</option>
											@endforeach
										</select>
										
									  </td>
									  <td><input style='width: 100px;' name="extra_price[]" type="number" step="any" min="0" max="100" placeholder="%" class="form-control required-entry extra_required" required>
									  </td>
									<td><button class="btn btn-danger btn-sm remove2" type="button" onclick="removeRowExtra(`+row_length+`)">-</button></td>
								</tr>`;	
	var parent_row = $("#ExtraTable2-Body").append(parent_row);

	$('.select2_demo_12').select2();
	
}

function removeRowExtra(rowId) {
	$(".participantRow2-"+rowId).remove();
}

///////////////////////// extras end //////////////////////////////////////////////

///////////////////////////////////// Preferences ////////////////////////////////////////////

function addRowPreference() {
	
var row_length=parseInt($('#preference-last-added').val()) +1;
$('#preference-last-added').val(row_length);
var parent_row=`<tr class="participantRow5-`+row_length+`">
					<td><input name="preference_name[]" type="text" placeholder="name.." class="form-control required-entry preference_required" required>
					  </td>
					 
					<td><button class="btn btn-danger btn-sm remove" type="button" onclick="removeRowPreference(`+row_length+`)">-</button></td>
				</tr>`;	
	var parent_row = $("#preference-table-body").append(parent_row);
	
}

function removeRowPreference(rowId) {
	$(".participantRow5-"+rowId).remove();
}

////////////////////////////////////////////////// Preferences Ends ////////////////////////////////////////////////////

/////////////////////////// ingredient starts ////////////////////////////////////////////////////


function removeVariableRow(id) {
	// remove variable row
	$('#variable-row-'+id+'').remove();
	// remove variable ingedrient thead
	$('.td-title-'+id+'').remove();
	// remove variable ingedrient row
	$('.td-data-'+id+'').remove();
}

function removeIngredientRow(id) {
	// remove variable row
	$('#variable-ingredient-'+id+'').remove();

}

$('.participantRow td').find('input[name="variable_name[]"]').keyup(function(){
	var row_id = $(this).closest('tr').attr('data-variable-row');
	var variable_name = $(this).val();
	// required all fields
	$('.variation_required').prop('required', true);
	// $('.variable_ingredient_required').prop('required', true);
	// set table head to variable name
	$('.td-title-'+row_id+'').text(variable_name);
	// change input field name of td to variable name
	$('.td-data-'+row_id+'').each(function() {
		// this will change the input field name to variable name
		$(this).find('input[type="text"]').attr('name', variable_name+'[]');
	});

});

// check for duplicate values
$('.participantRow td').find('input[name="variable_name[]"]').blur(function(){
	var row_id = $(this).closest('tr').attr('data-variable-row');
	var variable_name = $(this);
	$('.participantRow td').find('input[type="text"]').not(this).each(function(){
		if($(this).val() != '' && $(this).val() == variable_name.val()) {
			alert('Variable name already exists!');
			$('.td-title-'+row_id+'').text('');
			$('.td-data-'+row_id+'').each(function() {
			// this will change the input field name to variable name
			$(this).find('input[type="text"]').attr('name', '');

		});
			variable_name.val('');
		}
	});
});

/////////////////////////// integredient ends ////////////////////////////////////////////////////


//////////////////////////////////////////////////////
$(document).ready(function(){
	$(".select2_demo_1").select2();

		
		/* Variables */
		var p = $("#participants").val();
		var row = $(".participantRow");

		/* Functions */
		function getP(){
		  p = $("#participants").val();
		}

		function addRow() {

		// get last added row
		  var row_length = parseInt($('#variable-last-added').val()) +1;
		  $('#variable-last-added').val(row_length);
		  var _row = row.clone(true, true).append('<td><button class="btn btn-danger btn-sm" type="button" onclick="removeVariableRow('+row_length+')">-</button></td>');
		  
		  // set attribute and id to unique row
		  var set_attribute = _row.attr('data-variable-row', row_length);
		  var id = _row.attr('id', 'variable-row-'+row_length+'');


		  // add title to table head
		  $('#variable-ingredient > thead > tr').append('<th class="td-title-'+row_length+'"></th>');
		  var count = 0;
		  // add dynamic colums to the ingredient table row
		  $('.ingredient-row').each(function() {
		  	if (count == 0) {
		  		$(this).append('<td class="td-data-'+row_length+'">\
								<input name="variable_qty_'+row_length+'[]" type="text" step="any" min="0" value="0" placeholder=""\
								class="form-control required-entry variable_ingredient_required" onkeyup="numberSeperator(this)">\
								</td>');
		  	} else {
		  		$(this).find("td:last").before('<td class="td-data-'+row_length+'">\
								<input name="variable_qty_'+row_length+'[]" type="text" step="any" min="0" value="0" placeholder=""\
								class="form-control required-entry variable_ingredient_required" onkeyup="numberSeperator(this)">\
								</td>');
		  	}
		  	count++;
		  	
		  });

		  $(_row).appendTo("#participantTable");
		  $(_row).find('input').val('');
		}

		$(".add-ingredient").on('click', function () {
			// generate uniquue number
			var unique_row = Math.floor(1000 + Math.random() * 9000);

			var row = $('#variable-ingredient-1').clone(true, true).append('<td><button class="btn btn-danger btn-sm" type="button" onclick="removeIngredientRow('+unique_row+')">-</button></td>');

				// set attribute to unique row
		  	var set_attribute = row.attr('data-ingredient-row', unique_row);
		  	var id = row.attr('id', 'variable-ingredient-'+unique_row+'');

		  	row.appendTo("#variable-ingredient-body");
		  	row.find('input').val(0);
		});

		// function removeRow(button) {
		//   var current_row = button.closest("tr");
		//   var get_attribute = current_row.attr('data-variable-row');
		//   // remove variable ingedrient thead
		//   $('.td-title-'+get_attribute +'').remove();
		//   // remove variable ingedrient row
		//   $('.td-data-'+get_attribute +'').remove();
		//   //remove row
		//   current_row.remove();
		// }
		/* Doc ready */
		$(".add").on('click', function () {
		  getP();
		  if($("#participantTable tr").length < 17) {
			addRow();
			var i = Number(p)+1;
			$("#participants").val(i);
		  }
		  $(this).closest("tr").appendTo("#participantTable");
		  if ($("#participantTable tr").length === 3) {
			$(".remove").hide();
		  } else {
			$(".remove").show();
		  }
		});

		// $(".remove").on('click', function () {
		//   getP();
		//   if($("#participantTable tr").length === 3) {
		// 	//alert("Can't remove row.");
		// 	$(".remove").hide();
		//   } else if($("#participantTable tr").length - 1 ==3) {
		// 	$(".remove").hide();
		// 	removeRow($(this));
		// 	var i = Number(p)-1;
		// 	$("#participants").val(i);
		//   } else {
		// 	removeRow($(this));
		// 	var i = Number(p)-1;
		// 	$("#participants").val(i);
		//   }
		// });
		$("#participants").change(function () {
		  var i = 0;
		  p = $("#participants").val();
		  var rowCount = $("#participantTable tr").length - 2;
		  if(p > rowCount) {
			for(i=rowCount; i<p; i+=1){
			  addRow();
			}
			$("#participantTable #addButtonRow").appendTo("#participantTable");
		  } else if(p < rowCount) {
		  }
		});
		
		
		/* Variables */
		var p3 = $("#participants3").val();
		var row3 = $(".participantRow3");
		

		/* Functions */
		function getP3(){
		  p3 = $("#participants3").val();
		}

		function addRow3() {
		  var _row3 = row3.clone(true, true).appendTo("#participantTable3");
		  $(_row3).find('input').val(0);
		}

		function removeRow3(button3) {
		  button3.closest("tr").remove();
		}
		/* Doc ready */
		$(".add3").on('click', function () {
		  getP3();
		  if($("#participantTable3 tr").length < 17) {
			addRow3();
			var i3 = Number(p3)+1;
			$("#participants3").val(i3);
		  }
		  $(this).closest("tr").appendTo("#participantTable3");
		  if ($("#participantTable3 tr").length === 3) {
			$(".remove3").hide();
		  } else {
			$(".remove3").show();
		  }
		});
		$(".remove3").on('click', function () {
		  getP3();
		  if($("#participantTable3 tr").length === 3) {
			//alert("Can't remove row.");
			$(".remove3").hide();
		  } else if($("#participantTable3 tr").length - 1 ==3) {
			$(".remove3").hide();
			removeRow3($(this));
			var i3 = Number(p3)-1;
			$("#participants3").val(i3);
		  } else {
			removeRow3($(this));
			var i3 = Number(p3)-1;
			$("#participants3").val(i3);
		  }
		});
		$("#participants3").change(function () {
		  var i3 = 0;
		  p3 = $("#participants3").val();
		  var rowCount3 = $("#participantTable3 tr").length - 2;
		  if(p3 > rowCount3) {
			for(i3=rowCount3; i3<p3; i3+=1){
			  addRow3();
			}
			$("#participantTable3 #addButtonRow3").appendTo("#participantTable3");
		  } else if(p3 < rowCount3) {
		  }
		});
		
		
		$('#add_extra_checkbox').click(function() {
			if($(this).prop("checked") == true) {
				$('.extra_required').prop('required', true);
			} else {
				$('.extra_required').removeAttr('required');
			}
			$("#participantTable2").toggle(this.checked);
		});

		$('#type_simple_radio').click(function() {
			$('.variation_required').removeAttr('required');
			$("#participantTable").hide();
			// hide and show simple/ variable ingredient section
			$("#participantTable3").show();
			$('#variable-ingredient').hide();
			// apply and required class for both inputs
			// $('.variable_ingredient_required').prop('required', false);
			// $('.normal_ingredient_required').prop('required', true);
			//required price
			if($('#side').prop('checked') == false) {
				$('.price').prop('required', true);
			}
		});	
		
		$('#type_variable_radio').click(function() {
			$('.variation_required').prop('required', true);
			$("#participantTable").show();
			// hide and show simple/ variable ingredient section
			$("#participantTable3").hide();
			$('#variable-ingredient').show();
			// apply and required class for both inputs
			// $('.variable_ingredient_required').prop('required', true);
			// $('.normal_ingredient_required').prop('required', false);
			$('.price').prop('required', false);
		});
		
		
		$('#child_side_product').click(function() {
			if($(this).prop("checked") == true) {
				$('.side_required').prop('required', true);
			} else {
				$('.side_required').removeAttr('required');
			}

			$("#participantTable4").toggle(this.checked);
		});

		$('#side').change(function() {
			if($(this).prop("checked") == true) {
				$('.price').removeAttr('required');
				// check simple radion and show simple ingredient table
				$('#type_simple_radio').prop('checked', true);
				$('#participantTable3').show();
				// hide variable section and variable table
				$('.variable-section').hide();
				$("#participantTable").hide();
				// hide variable ingredient table
				$("#variable-ingredient").hide();
				//remove required from varibale inputs
				$('.variation_required').prop('required', false);
				$('.variable_ingredient_required').prop('required', false);
			} else {
				// check if variable radion button is is not checked
				if($('#type_variable_radio').prop('checked') == false) {
					$('.price').prop('required', true);
				} else {
					$('.variation_required').prop('required', true);
					$('.variable_ingredient_required').prop('required', true);
				}
				$('.variable-section').show();
			}
			
		});


		$('#add_preferences_checkbox').click(function() {
			if($(this).prop("checked") == true) {
				$('.preference_required').prop('required', true);
			} else {
				$('.preference_required').removeAttr('required');
			}

			$("#participantTable5").toggle(this.checked);
		});

		
		
	});

	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
	
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#product-form').submit(function(event){
			if($('#child_side_product').is(':checked')) {
				var error_count = 0;
				$('.participantRow4').each(function() {
					// get parent row id
					var parent_id = $(this).attr('data-parent-row');
					// get row number allowed value
					var allowed_input = $(this).find("td:eq(0) input[type='number']");
					// count child table rows
					var child_rows = $('#childTableId-'+parent_id+' tr').length;
					if (allowed_input.val() != "") {
						if (child_rows <= allowed_input.val()) {
							error_count++;
						}
					}	
				});

				if(error_count > 0) {
					event.preventDefault();
					alert('Sides must be greater then No# Allowed!');
					return false;
				} else {
					event.currentTarget.submit();
				}
			}
		});
	});
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            	// $('.add-product').removeAttr('disabled');
            	// $('.image-required-text').hide();
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
	
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 45 || charCode > 57)) {
			return false;
		}
	  else if(charCode == 46){
		return true;
	  }
	  else if(charCode == 47){
		return false;
	  }
	  else {
		 return true;
	  }}

	  function getProductID(obj) {

	  	var product_id = obj.val();
	  	if (product_id != '') {
	  		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					id: '{{$id}}',
					product_id: product_id,
					type: 'add'
				},
				url: "{{ route('admin.get-product') }}",
				success: function(res){
					if(res['error'] == 'true') {
						swal("", "Product ID already exists. Please provide unique ID.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						obj.val('');
					} else {
						console.log('false');
					}
				}
			});

	  	}
	  }
</script>
@endsection