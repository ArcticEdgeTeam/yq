<?php include('includes/header.php'); ?>	
<div class="page-heading">
	<h1 class="page-title">Support</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.supports') }}">Support</a></li>
		<li class="breadcrumb-item">New Ticket</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	<form action='#' method='post' enctype='multipart/form-data'>
	<div class="row">
	
	
		<div class="col-xl-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Admin</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div class="ibox-body text-center">
				   <img src='assets/img/users/u3.jpg' width='120' height='120' class='img-circle'>
				   <br />
				   <h5 class='mt-3 mb-2 text-danger'>Jane Doe</h5>
				   <div>jhondoe122@gmail.com</div>
				   <div>+27-812-5558-82</div>
				</div>
			</div>
		</div>
		
		<div class="col-xl-9 pl-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Ticket Details</div>
					<div class="ibox-tools">
						<div class="ibox-tools">
							<a class="font-18" href="javascript:;"><i class="ti-support"></i></a>
						</div>
					</div>
				</div>
				<div class="ibox-body">
				  
				   
				   <div class='form-group mt-4'>
					
					 <div class="input-group-icon input-group-icon-left">
						<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
						<input  class="form-control" type="text" required placeholder="Subject..">
					</div>
					
				   </div>
				   
				   <div class='form-group mt-4'>
					<textarea rows='4' required class='form-control' placeholder='Comment..'></textarea>
				   </div>
				    <div class='form-group'>
					<button href='support-list.php' class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Create Ticket</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Create</span>
						</span>
					</button>
				   </div>
				</div>
			</div>
		</div>		
		
		
	</div>
	</form>
</div>

<?php include('includes/footer.php'); ?>   