@extends('layouts.admin')

@section('page_plugin_css')
	<link href="{{ asset('public/assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">CMS</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item">CMS</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<div class="ibox p-4">             
		
			<ul class="nav nav-tabs tabs-line tabs-line-2x nav-justified tabs-line-pink">
				<li class="nav-item">
					<a class="nav-link active" href="#tab-5-1" data-toggle="tab"><i class="ti-layout-media-left-alt mr-2"></i>About Us</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#tab-5-2" data-toggle="tab"><i class="ti-layout-media-left-alt mr-2"></i>Help</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#tab-5-3" data-toggle="tab"><i class="ti-layout-media-left-alt mr-2"></i>Terms & Conditions</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#tab-5-4" data-toggle="tab"><i class="ti-layout-media-left-alt mr-2"></i>Privacy Policy</a>
				</li>
			</ul>
			
	</div>
	
	<div class='ibox p-4'>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="tab-5-1">
				<form id='form1' class='form-danger' method='post' action="{{ route('admin.cms.update', 1) }}">
					@csrf
					<div class='row'>
						
						<div class='col-md-12'>
							
							<div class="alert alert-pink alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>
                                    <button class="close" data-dismiss="alert" aria-label="Close"></button>Please only use bold, italic options in text editor.</div>
							
						</div>
						
						
						<div class="col-md-12 form-group mb-4">
							<label>Video Link</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-control-play"></i></span>
								<input placeholder='Video Link' type='text' value="{{ $page1->video }}" name='video' class='form-control'>
							</div>
						</div>
						
						<div class='col-md-12 form-group'>
							<label>Text</label>
						
							<textarea required id='textarea1' class="summernote" data-plugin="summernote" data-air-mode="true" name='text'>{!! $page1->text !!}</textarea>
							
							
						</div>
						<div class='col-md-12 form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update About</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
							
						</div>
					</div>
				</form>
			</div>
			<div class="tab-pane fade" id="tab-5-2">
				<form id='form2' class='form-danger' method='post' action="{{ route('admin.cms.update', 2) }}">
					@csrf
					<div class='row'>
						<div class='col-md-12'>
							
							<div class="alert alert-pink alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>
                                    <button class="close" data-dismiss="alert" aria-label="Close"></button>Please only use bold, italic options in text editor.</div>
							
						</div>


						<div class="col-md-12 form-group mb-4">
							<label>Video Link</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-control-play"></i></span>
								<input placeholder='Video Link' type='text' value="{{ $page2->video }}" name='video' class='form-control'>
							</div>
						</div>
						

						<div class='col-md-12 form-group'>
							<label>Text</label>
							<textarea required id='textarea2' class="summernote" data-plugin="summernote" data-air-mode="true" name='text'>{!! $page2->text !!}</textarea>
						</div>
						<div class='col-md-12 form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Help</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="tab-pane fade" id="tab-5-3">
				<form id='form3' class='form-danger' method='post' action="{{ route('admin.cms.update', 3) }}">
					@csrf
					<div class='row'>
						<div class='col-md-12'>
							
							<div class="alert alert-pink alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>
                                    <button class="close" data-dismiss="alert" aria-label="Close"></button>Please only use bold, italic options in text editor.</div>
							
						</div>
						<div class='col-md-12 form-group'>
							<label>Text</label>
							<textarea required id='textarea3' class="summernote" data-plugin="summernote" data-air-mode="true" name='text'>{!! $page3->text !!}</textarea>
						</div>
						<div class='col-md-12 form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Terms</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
				</form>
			</div>

			<div class="tab-pane fade" id="tab-5-4">
				<form id='form4' class='form-danger' method='post' action="{{ route('admin.cms.update', 4) }}">
					@csrf
					<div class='row'>
						<div class='col-md-12'>
							
							<div class="alert alert-pink alert-dismissable fade show alert-outline has-icon"><i class="la la-info-circle alert-icon"></i>
                                    <button class="close" data-dismiss="alert" aria-label="Close"></button>Please only use bold, italic options in text editor.</div>
							
						</div>
						<div class='col-md-12 form-group'>
							<label>Text</label>
							<textarea required id='textarea4' class="summernote" data-plugin="summernote" data-air-mode="true" name='text'>{!! $page4->text !!}</textarea>
						</div>
						<div class='col-md-12 form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Privacy</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('page_plugin_js')
	<script src="{{ asset('public/assets/vendors/summernote/dist/summernote.min.js') }}"></script>
@endsection

@section('page_js')
<script>
	
	$(document).ready(function() {
		
	  $('.summernote').summernote({
		  height: "200px",
		  toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
	  });
	});
	
	
    
</script>
@endsection