@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.bars', $id) }}">Prep Area</a></li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	
	<div class='ibox p-4'>
		
		<div class="flexbox mb-4">
			<div class="flexbox">
			
				<div class='form-group'>
					<a href="{{ route('admin.venue.bar.create', $id) }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Prep Area</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
				
			</div>
			
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
				
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>ID</th>
						<th>Name</th>
						<th>Prep Staff</th>
						<th>Products</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bars as $bar)
					@php
						if($bar->image == ''){
							$bar->image = 'default.png';
						}
						
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td>{{ $bar->bar_id }}</td>
						<td><img class='img-circle' src="{{ asset('public/uploads/bars/' . $bar->image) }}" width='50' height='50'> {{ $bar->name }}</td>
						<td> {{$bar->prep_staff}} </td>
						<td>
						{{ number_format(DB::table('products')->where('status', 1)->where('bar_id', $bar->id)->count()) }}
						</td>
						<td>
						@if($bar->status == 1)
						<span class='d-none'>Active</span><i title='Active' data-toggle='tooltip' class='fa fa-check-circle text-success' style='font-size: 22px;'></i>
						@else
							<span class='d-none'>Inactive</span><i title='Inactive' data-toggle='tooltip' class='fa fa-times-circle text-danger' style='font-size: 22px;'></i>
						@endif
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='Edit' href="{{ route('admin.venue.bar.edit', [$id, $bar->id]) }}"><i style='font-size: 22px;' class='fa fa-edit'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
				
				
			
	</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script type="text/javascript">
	$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection