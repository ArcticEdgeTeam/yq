@extends('layouts.admin')

@section('page_plugin_css')

<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')

<div class="page-heading">

		@include('layouts.venue-status-toggle')
	
	<h1 class="page-title">Bookings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.bookings', $venue->id) }}">Bookings</a></li>
		
		<div class='vene_name_box text-danger'>
		{{ $venue->name }}
		</div>
		
	</ol>
</div>
<div class="page-content fade-in-up">
@include('layouts.admin-venue-nav')
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		
		<form action="{{route('admin.venue.bookings', $id)}}" method="GET" class="mt-3 mb-3">

			<div class="row">
			
				<div class="form-group col-md-3">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text" value="{{request()->date_range}}">
						
					</div>
				</div>

			
				<div class="form-group col-md-3">
					<label>Name</label>
					<div class="input-group">
						<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> -->
						<input name='name' class="form-control" type="text" placeholder="Search by name..." value="{{request()->name}}">
						
					</div>
				</div>

				<div class="form-group mb-4 col-md-3">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == 'booked') selected @endif value="booked">Booked</option>
							<option @if(request()->status == 'pending') selected @endif value="pending">Pending</option>
							<option @if(request()->status == 'completed') selected @endif value="completed">Completed</option>
							<option @if(request()->status == 'cancelled') selected @endif value="cancelled">Cancelled</option>
						
					</select>
				</div>

				<div class="form-group col-md-3" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
					<a href="{{ route('admin.table-booking-view',  $venue->id) }}" class=" btn btn-success" style="margin-left: 5px;" title="Calendar View"><i class="fa fa-tasks"></i></a>
				</div>
				

			</div>
		</form>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>ID</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Status</th>
						<th>Table</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getBookings->isEmpty())
					@foreach($getBookings as $booking)
					@php
						$customer = DB::table('users')->where('id', $booking->customer_id)->first();
						if ($customer != null) {
							if($customer->photo == ''){
								$customerPhoto = 'default.png';
							} else {
								$customerPhoto = $customer->photo;
							}
						} else {
								$customerPhoto = 'default.png';
						}
							
					@endphp
						<tr>
							<td>{{ $booking->booking_number }}</td>
							<td><img src="{{ asset('public/uploads/users/' . $customerPhoto) }}" width='40' height='40' class='img-circle'> <span data-toggle="tooltip" title="{{ $booking->name }}">@if ($customer != null) {{ $customer->name }} @else {{ $booking->name }} @endif</span></td>
							
							<td>{{ date('d M Y', strtotime($booking->date_time)) }}</td>
							<td>{{ date('H:i:s', strtotime($booking->date_time)) }}</td>
							<td>
							
							@if($booking->booking_status == 'booked')
								<span class='d-none'>Booked</span><i style='font-size: 22px;' data-toggle='tooltip' title='Booked' class="fa fa-calendar-check-o text-info" aria-hidden="true"></i>
							@elseif($booking->booking_status == 'completed')
								<span class='d-none'>Completed</span><i style='font-size: 22px;' data-toggle='tooltip' title='Completed' class="fa fa-check-circle text-success" aria-hidden="true"></i>

							@elseif($booking->booking_status == 'pending')
								<span class='d-none'>Pending</span><i style='font-size: 22px;' data-toggle='tooltip' title='Pending' class="fa fa-circle-o-notch text-warning" aria-hidden="true"></i>	
							@else
								<span class='d-none'>Cancelled</span><i style='font-size: 22px;' data-toggle='tooltip' title='Cancelled' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
							@endif
							
							</td>
							
							<td>
							{{ $booking->booking_table }}
							</td>
							<td>
								<a class='text-warning' title='View' style='font-size: 22px;' data-toggle='tooltip' href="{{ route('admin.venue.booking.edit', [$venue->id, $booking->id]) }}"><i class='fa fa-eye'></i></a>
							</td>
						</tr>
					@endforeach
					@else
					<tr>
						<td colspan="7" style="text-align: center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getBookings->appends(['date_range' => Request::get('date_range'), 'name' => Request::get('name'), 'status' => Request::get('status')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>

		</div>
	</div>
</div>
</div>

@endsection
@section('page_plugin_js')

<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

@endsection

@section('page_js')
<script>
 
$(function() {
  $('#daterange_1').daterangepicker({
	autoUpdateInput: false,
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / "
	}
  });

  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

    $('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
@endsection