@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Customers</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{ route('admin.home') }}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.customers') }}">Customers</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
		@include('layouts.admin-customer-nav')
		<form class='form-danger' method='post' action="{{ route('admin.customer.update', $customer->id) }}" enctype='multipart/form-data'>
		@csrf
		
		<div class='row'>
			
			<div class="col-xl-8">
				<div class="ibox ibox-fullheight">
					<div class="ibox-head">
						<div class="ibox-title">General Info</div>
						<div class="ibox-tools">
							<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
						</div>
					</div>
					<div class="ibox-body">
						<div class='row'>
							<div class="form-group mb-4 col-md-6">
								<label>First Name</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
									<input readonly required name='first_name' value="{{ $customer->first_name }}" class="form-control" type="text">
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-6">
								<label>Surname</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
									<input readonly required name='last_name' value="{{ $customer->last_name }}" class="form-control" type="text">
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-6">
								<label class="font-normal">Date of Birth</label>
								<div class="input-group date">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input readonly class="form-control" name='dob' type="text" value="{{ $customer->dob }}">
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-6">
								<label>Phone</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
									<input readonly name='phone' value="{{ $customer->phone }}" class="form-control" type='text'>
								</div>
							</div>
							
							<div class='form-group mb-4 col-md-12'>
								<label class='mr-2'>Gender :</label>
								@if($customer->gender == 'male')
									<i data-toggle="tooltip" title="" style="font-size: 22px;" class="fa fa-male" aria-hidden="true" data-original-title="Male"></i>
								@else
									<i data-toggle="tooltip" title="" style="font-size: 22px;" class="fa fa-female" aria-hidden="true" data-original-title="Female"></i>
								@endif
							</div>
							
							<div class="form-group mb-4 col-md-6 d-none">
								<label class='mr-4'>Gender</label>
								<br />
								<label class="radio radio-inline radio-danger">
									<input value='male' type="radio" name="gender" checked>
									<span class="input-span"></span>Male</label>
								<label class="radio radio-inline radio-danger">
									<input value='female' type="radio" name="gender" @if($customer->gender == 'female') checked @endif >
									<span class="input-span"></span>Female</label>
							</div>
							
						
							<div class="mb-4 col-md-12 d-none">
								<label data-toggle='tooltip' title='Upload Avatar' class='btn btn-circle btn-danger btn-sm' for='avatar'><i class='fa fa-upload mb-2'></i></label>
								<input id='avatar' accept="image/*" name='photo' class='d-none' type='file' name='avatar'>
								
							</div>
						  
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="col-xl-4 pl-0">
				<div class="ibox ibox-fullheight">
					<div class="ibox-head">
						<div class="ibox-title">Login Info</div>
						<div class="ibox-tools">
							<span style='font-size: 11px;'><b>Active</b></span>
						<br />
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
							<input name='status' type="checkbox" @if($customer->status == 1) checked @endif>
							<span></span>
							</label>
						</div>
					</div>
					<div class="ibox-body">
						<div class='row'>
							<div class="form-group mb-4 col-md-12">
								<label>Email</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
									<input readonly name='email' value="{{ $customer->email }}" class="form-control" type="email">
								</div>
							</div>
							
							<div class="form-group mb-4 col-md-12 d-none">
								<label>Password</label>
								<div class="input-group-icon input-group-icon-left">
									<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
									<input name='password' class="form-control" type="password">
								</div>
							</div>
							
							
							<div class="form-group col-md-12 mb-4">
								<button class="btn btn-danger btn-fix btn-animated from-left">
									<span class="visible-content">Update Customer</span>
									<span class="hidden-content">
										<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
									</span>
								</button>
						   </div>
						  
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
		</form>
            
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('page_js')
<script>
  // Bootstrap datepicker
    $('#date_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
		format: 'dd-mm-yyyy',
    });
</script>
@endsection