@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/multiselect/css/multi-select.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.reset_filter_box{
		margin-top: 26px;
		margin-left: 12px;
		font-size: 12px;
	}
</style>
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Transactions</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="{{route('admin.home')}}"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item">Home</li>
		<li class="breadcrumb-item">Transactions</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				<form method='get' action="{{ route('admin.transaction.filter') }}">
				<div class="form-group">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text">
						<button class='btn btn-danger'>Filter</button>
					</div>
				</div>
				</form>
				
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class='reset_filter_box'>
			@if(isset(request()->date_range))
				<a class='text-danger' href="{{ route('admin.transactions') }}"><span class='ti-reload'></span> Reset Filter </a>
			@endif
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Transaction ID</th>
						<th>Date</th>
						<th>Time</th>
						<th>Customer</th>
						<th>Venue</th>
						<th>Amount</th>
						<th>Waiter Tip</th>
						<th>Admin Comm</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				@foreach($transactions as $transaction)
				@php
				$customer = DB::table('users')->where('id', $transaction->customer_id)->first();
				$venue = DB::table('venues')->where('id', $transaction->venue_id)->first();
				if($venue->banner == ''){
					$venue->banner = 'default.png';
				}
				
				if($customer->photo == ''){
					$customer->photo = 'default.png';
				}
				@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td>{{ $transaction->transaction_id }}</td>
						<td>{{ $transaction->created_at->format('d M Y') }}</td>
						<td>{{ $transaction->created_at->format('H:i:s') }}</td>
						<td><img class="img-circle" width="40" src="{{ asset('public/uploads/users/' . $customer->photo) }}"> {{ $customer->name }}</td>
						<td><img src="{{ asset('public/uploads/banners/' . $venue->banner) }}" width="50" height="50" class="img-circle"> {{ $venue->name }}</td>
						<td style='position: relative;'>{{ $currency }} {{ number_format($transaction->amount,2) }} @if($transaction->transaction_status == 'Refunded') <span class="badge badge-danger" style='font-size: 8px; position: absolute; top: 6px; left: 10px;'>Refunded</span> @endif </td>
						<td>{{ $currency }} {{ number_format($transaction->waiter_tip,2) }}</td>
						<td>{{ $currency }} {{ number_format($transaction->admin_commission,2) }}</td>
						<td>{{ $currency }} {{ number_format($transaction->amount + $transaction->waiter_tip,2) }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			
			
			
			
		</div>
	</div>
</div>
</div>

@endsection
@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/jquery-minicolors/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('public/assets/js/scripts/form-plugins.js') }}"></script>
@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	startDate: "{{ $start }}",
    endDate: "{{ $end }}",
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / ",
		startDate: "{{ $start }}",
		endDate: "{{ $end }}",
	}
  });
});
</script>
@endsection