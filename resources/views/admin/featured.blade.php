@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style type="text/css">

fieldset, label { margin: 0; padding: 0; }

h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  }

</style>

@endsection

@section('page_content')


<div class="page-heading">
	
	<h1 class="page-title">Featured</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.featured') }}">Featured</a></li>
	</ol>
	
</div>
<div class="page-content fade-in-up">

	<form class='form-danger' method='post' action="{{route('admin.featured-save')}}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>

		
		<div class="col-xl-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">New Signup Duration</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-time"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
							
						<div class="form-group mb-4 col-md-12">
							<label>Months</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-time"></i></span>

								<input  value="{{$getFeatured->months}}" class="form-control" type="number" name='months' min="1" required>
							</div>
						</div> 
					  
					</div>
				</div>
			</div>
		</div>
	
		<div class="col-xl-4 p-10">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Credibility Criteria</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-star"></i></a>
					</div>
					
				</div>
				<div class="ibox-body">
					<div class="row">

						<div class="form-group mb-4 col-md-12">
						<label>Star Rating</label><br>
							<fieldset class="rating">
							    <input type="radio" id="star5" name="rating" value="5" @if($getFeatured->rating == '5') checked @endif/><label class = "full" for="star5" title="Awesome - 5 stars"></label>
							    <input type="radio" id="star4half" name="rating" value="4 plus" @if($getFeatured->rating == '4 plus') checked @endif /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
							    <input type="radio" id="star4" name="rating" value="4" @if($getFeatured->rating == '4') checked @endif /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
							    <input type="radio" id="star3half" name="rating" value="3 plus" @if($getFeatured->rating == '3 plus') checked @endif/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
							    <input type="radio" id="star3" name="rating" value="3" @if($getFeatured->rating == '3') checked @endif/><label class = "full" for="star3" title="Meh - 3 stars"></label>
							    <input type="radio" id="star2half" name="rating" value="2 plus" @if($getFeatured->rating == '2 plus') checked @endif/><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
							    <input type="radio" id="star2" name="rating" value="2" @if($getFeatured->rating == '2') checked @endif/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
							    <input type="radio" id="star1half" name="rating" value="1 plus" @if($getFeatured->rating == '1 plus') checked @endif/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
							    <input type="radio" id="star1" name="rating" value="1" @if($getFeatured->rating == '1') checked @endif/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
							    <input type="radio" id="starhalf" name="rating" value="0 plus" @if($getFeatured->rating == '0 plus') checked @endif/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
							</fieldset>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Number of Reviews</label>
							<div class="input-group-icon">
								<input value="{{number_format($getFeatured->reviews)}}" name="reviews" class="form-control" type="text" step="any" onkeyup="numberSeperator(this)" required>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Monthly Charges</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-pie-chart"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>

						<div class="form-group mb-4 col-md-12">
							<label>Charges</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left">ZAR</span>
								<input value="{{number_format($getFeatured->charges, 2)}}" name="charges" class="form-control" type="text" step="any" onkeyup="numberSeperator(this)" required>
							</div>
						</div>
					
					</div>
					
					
					<br>

					<div class='row'>
						<div class="form-group col-md-12">
							<button class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
						</div>
						
					</div>
				</div>
			</div>
		</div>


	
		
	</div>
	</form>
	
</div>

@endsection

@section('page_plugin_js')
<script src="./assets/vendors/select2/dist/js/select2.full.min.js"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>

@endsection

@section('page_js')
<script type="text/javascript">
	
	// thousand seperator
	function numberSeperator(object) {
		var val = object.value;
	  	val = val.replace(/[^0-9\.]/g,'');
	  
		if(val != "") {
		    valArr = val.split('.');
		    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
		    val = valArr.join('.');
		 }
	  
	  	object.value = val;
	}

	$('.form-danger').submit(function(e){
		 	e.preventDefault();
			var count = 0;

			 	if ($('input[name="months"]').val() == '') {
			 		swal("", "Months field is required.", "error");
					$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
					return false;
			 	}

			 	if (!$('input[name="rating"]').is(':checked')) {
			 		swal("", "Rattings field is required.", "error");
					$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
					return false;
			 	}

			 	if ($('input[name="reviews"]').val() == '') {
			 		swal("", "Reviews field is required.", "error");
					$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
					return false;
			 	}

			 	if ($('input[name="charges"]').val() == '') {
			 		swal("", "Charges field is required.", "error");
					$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
					return false;
			 	}


		    	if (count == 0) {
			 		e.currentTarget.submit();

			 		if ($(this).data('submitted') === true) {
			            // Form is already submitted
			            console.log('Form is already submitted, waiting response.');
			            // Stop form from submitting again
			           $('.reg_btn').attr('disabled', true);
			        } else {
			            // Set the data-submitted attribute to true for record
			            $(this).data('submitted', true);
			        }
			        
			 	}
		});

	
</script>
@endsection
