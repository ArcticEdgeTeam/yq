@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')


<div class="page-heading">
	<h1 class="page-title">Settings</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item">Settings</li>
	</ol>
</div>


<div class='page-content fade-in-up'>
	<form id='profile_form' class='form-danger' method='post' action="{{ route('admin.setting.update', $user->id) }}" enctype='multipart/form-data'>
	@if($user->photo == '')
		<?php
			$user->photo = 'default.png';
		?>
	@endif
	@csrf
	<div class="row">
		
		<div class='col-md-3'>
			<div class="ibox">
				<div class="ibox-body text-center" style='position: relative; height: 400px'>
					<div class='text-center'>
					<img id='previewfile' class='img-circle' width='300' height='300' src="{{ asset('public/uploads/users/' . $user->photo) }}">
					</div>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Change Avatar' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input onchange="PreviewprofileImage();" id='uploadfile' type='file' name='photo' class='d-none' accept="image/*">
					</div>
					<h5 class='text-danger mt-3'>{{ $user->name }}</h4>
					<span class='text-success'>Admin</span>
				</div>
			</div>
		</div>
		
		
		<div class="col-xl-6 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Contact Details</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-user'></i></a>
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
				   
						<div class="form-group mb-4 col-md-6">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input required name='name' type='text' class='form-control' value="{{ $user->name }}">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6 pl-0">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input name='email' type='text' readonly class='form-control' value="{{ $user->email }}">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Phone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-phone"></i></span>
								<input name='phone' value="{{ $user->phone }}" type='text' class='form-control'>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6 pl-0">
							<label>Address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input id="autocomplete" onFocus="geolocate()" name='address' value="{{ $user->address }}" type='text' class='form-control'>
							</div>
						</div>

						
						<div class="form-group mb-4 col-md-12">
							<label>Currency</label>
							<select class='form-control' name='currency'>
								<option value='ZAR'>ZAR</option>
							</select>
						</div>
						
					<!-- 	<div class="form-group mb-4 col-md-6 pl-0">
							<label>Percentage</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-percent" style='font-size: 12px;'></i></span>
								<input name='percentage' value="{{ $user->percentage }}" type='text' class='form-control'>
							</div>
						</div> -->
						
						
						
						<div class="form-group mb-4 col-md-6 d-none">
							<label>Username</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input name='username' type='text' class='form-control' value="{{ $user->username }}">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input id='password' name='password' type='password' class='form-control'>
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='password_confirm' type='password' id='password_confirm' class='form-control'>
							</div>
						</div>


						@if(\Auth::user()->login_type != 'Mighty Super Admin')
						<div class="col-md-8"></div>

						<div class="form-group mb-4 col-md-4">
							<button value='abc' name='action' class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Admin</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
						</div>
						@endif
						
				   </div>
				    
				</div>
			</div>

		</div>
		
		@if(\Auth::user()->login_type == 'Mighty Super Admin')
		<div class="col-xl-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Payment Details</div>
					<div class="ibox-tools">
						<a href='javascript:;'><i class='ti-money'></i></a>
					</div>
				</div>
				<div class="ibox-body">
				   
				   <div class='row'>
				   
						
						<div class="form-group mb-4 col-md-12">
							<label>Payfast Merchant ID</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><img width='16' src="{{ asset('public/assets/payfast-icon.png') }}"></span>
								<input value="{{ $user->payfast_merchant_id }}" name='payfast_merchant_id' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Payfast Merchant Key</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><img width='16' src="{{ asset('public/assets/payfast-icon.png') }}"></span>
								<input value="{{ $user->payfast_merchant_key }}" name='payfast_merchant_key' class="form-control" type="text">
							</div>
						</div>
						
				   </div>
				   <!-- row ends -->

				   <div class='row'>
						<div class='col-md-12 mb-4'>
							Commission Settings
							<div style='border-bottom: 1px solid #eee;'></div>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Admin Commission Limit</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left">{{ $currency }}</span>
								<input name='commission' value="{{ number_format($user->commission,2) }}" type='text' class='form-control' onkeyup="numberSeperator(this)">
							</div>
						</div>


						<div class="form-group mb-4 col-md-12">
							<label>Default Percentage</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="fa fa-percent"></i></span>
								<input name='percentage' value="{{$user->percentage}}" type='number' min='-100' max="100" class='form-control' required>
							</div>
						</div>


						<div class="form-group mb-4 col-md-12">
							<button value='abc' name='action' class="btn btn-danger btn-fix btn-animated from-left">
							<span class="visible-content">Update Admin</span>
							<span class="hidden-content">
								<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
							</span>
							</button>
						</div>

					</div>
					<!-- row ends here -->
				    
				</div>

			</div>
		</div>
		@endif
		
	</div>
	</form>
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')
<script>

	// thousand seperator
	function numberSeperator(object) {
		var val = object.value;
	  	val = val.replace(/[^0-9\.]/g,'');
	  
		if(val != "") {
		    valArr = val.split('.');
		    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
		    val = valArr.join('.');
		 }
	  
	  	object.value = val;
	}

	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>

<script>
var placeSearch, autocomplete;
function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
  document.getElementById('autocomplete'), {types: ['geocode']});
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}



$(document).ready(function(){
	
	$('#profile_form').on('submit', function(e){
		
		var _password = $('#password').val();
		var _password_confirm = $('#password_confirm').val();
		if(_password != '') {

			if (_password.length < 8) {

				swal("", "Password must be 8 chanracters long.", "error");
				$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
				return false;
			}

			if(_password != _password_confirm) {
				e.preventDefault();
				swal("", "Password Mismatch", "error");
				$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
			}
			
		}
		
	});
	
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection