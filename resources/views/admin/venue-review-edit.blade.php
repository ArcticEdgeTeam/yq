@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.admin')
@section('page_plugin_css')
@endsection

@section('page_css')
<style>
	.fa-star{
		color: #f39c12;
		display: inline-block;
		margin-bottom: 10px;
		margin-left: 4px;
	}
</style>
@endsection

@section('page_content')

<div class="page-heading">
	<h1 class="page-title">Venue</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venue.reviews', $id) }}">Reviews</a></li>
		<li class="breadcrumb-item">Edit</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>
</div>


<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	<form method='post' action="{{ route('admin.venue.review.update', [$id, $review->id]) }}">
	@csrf
	<div class='row'>
		<div class="col-xl-3">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Customer Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				
				
				<div class="ibox-body text-center">
				   <img src="{{ asset('public/uploads/users/' . $customer->photo) }}" width='120' height='120' class='img-circle'>
				   <br />
				   <h5 class='mt-3'>{{ $customer->name }}</h5>
				   <i data-toggle='tooltip' title='Lifetime Spent' class="fa fa-money" aria-hidden="true"></i> {{ $currency }}  {{ number_format($lifetime_spent) }}
				   <div class='mt-2'>
				   <a target='_blank' href="{{ route('admin.customer.overview', $customer->id) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
				   </div>
				</div>
			</div>
		</div>
	
		<div class="col-xl-6 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Ratings Info</div>
					<div class="ibox-tools">
						<span style='font-size: 11px;'><b>Status</b></span>
						<br />
						<label class="ui-switch switch-icon switch-solid-danger switch-large">
							<input name='status' type="checkbox" @if($review->status == 1) checked @endif>
							<span></span>
						</label>
						
					</div>
				</div>
				<div class="ibox-body">
				   <div class='row text-center'>
						<div class='col-md-6'><b>Date:</b> {{ $review->created_at->format('d M Y') }}</div>
						<div class='col-md-6'><b>Time:</b> {{ $review->created_at->format('H:i') }}</div>
				   </div>
				   
				   <div class='row text-center mt-2'>
						<div class='col-md-6'><b>Order#:</b> <a href="{{ route('admin.venue.order.edit', [$venue->id, $order->id]) }}" class='text-danger btn-link' target='_blank'>{{ $order->order_number }}</a></div>
						<div class='col-md-6'><b>Order Total:</b> {{ $currency }} {{ $order->total_amount }}</div>
				   </div>
				   
				   <div class='text-center mt-4'>
						@php
						$food_rating = $review->food_rating;
						$food_stars = round($food_rating);
						$food_remainig_stars = 5 - $food_stars;
						
						$service_rating = $review->service_rating;
						$service_stars = round($service_rating);
						$service_remainig_stars = 5 - $service_stars;
						
						$value_rating = $review->value_rating;
						$value_stars = round($value_rating);
						$value_remainig_stars = 5 - $value_stars;
						
						$venue_rating = $review->venue_rating;
						$venue_stars = round($venue_rating);
						$venue_remainig_stars = 5 - $venue_stars;
						@endphp
						<span style='width: 100px; display: inline-block;'>Food</span> <span title="{{ $food_rating }}" data-toggle='tooltip'>
						@php
							for($i = 0; $i < $food_stars; $i++){
							echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $food_remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
						@endphp
						</span>
						<br />
						
						<span style='width: 100px; display: inline-block;'>Service</span> <span title="{{ $service_rating }}" data-toggle='tooltip'>
						@php
							for($i = 0; $i < $service_stars; $i++){
							echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $service_remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
						@endphp
						</span>
						<br />
						
						<span style='width: 100px; display: inline-block;'>Value</span> <span title="{{ $value_rating }}" data-toggle='tooltip'>
						@php
							for($i = 0; $i < $value_stars; $i++){
							echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $value_remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
						@endphp
						</span>
						<br />
						<span style='width: 100px; display: inline-block;'>Venue</span> <span title="{{ $venue_rating }}" data-toggle='tooltip'>
						@php
							for($i = 0; $i < $venue_stars; $i++){
							echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
							}
							
							for($j = 0; $j < $venue_remainig_stars; $j++){
								echo "<i class='fa fa-star text-muted'></i>";
							}
						@endphp
						</span>
						<br />
					
				   </div>
				   <div class='form-group mt-4'>
					<textarea rows='4' class='form-control' name='comment'>{{ $review->comment }}</textarea>
				   </div>
				    <div class='form-group'>
					<button class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Update Review</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
						</span>
					</button>
				   </div>
				</div>
			</div>
		</div>
	
		<div class="col-xl-3">
			<div class="ibox ">
				<div class="ibox-head">
					<div class="ibox-title">Waiter Info</div>
					@php
						DB::table('bars')->where('id', $waiter->bar_id);
					@endphp
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				<div class="ibox-body text-center">
				   <img src="{{ asset('public/uploads/users/' . $waiter->photo) }}" width='120' height='120' class='img-circle'>
				   <h5 class='mt-3'>{{ $waiter->name }}</h5>
					
					@php
						$waiter_rating = DB::table('reviews')
						->select(DB::raw('sum(service_rating) as service_rating, count(id) as total_records'))
						->where('waiter_id', $waiter->id)->first();
						
						if($waiter_rating->total_records > 0){
							$stars = round($waiter_rating->service_rating / $waiter_rating->total_records);
							$rating = round($waiter_rating->service_rating / $waiter_rating->total_records, 2);
							$remainig_stars = 5 - $stars;
						}else{
							$stars = 0;
							$rating = 0;
							$remainig_stars = 5 - $stars;
						}
						@endphp
					
					<span class='d-none'>{{ $rating }}</span><span title="{{ $rating }}" data-toggle='tooltip'>
						@php
						for($i = 0; $i < $stars; $i++){
							echo "<i class='fa fa-star text-warning' aria-hidden='true'></i>";
						}
						
						for($j = 0; $j < $remainig_stars; $j++){
							echo "<i class='fa fa-star text-muted'></i>";
						}
						@endphp
					</span>
				  
				   <div class='mt-2'>
				   <a target='_blank' href="{{ route('admin.venue.staff.edit', [$id, $waiter->id]) }}" class='btn btn-outline-danger btn-fix btn-thick btn-air btn-sm'>View Profile</a>
				   </div>
				</div>
			</div>
		</div>
	</div>
	</form>
	
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection