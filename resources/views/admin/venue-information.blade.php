@extends('layouts.admin')

@section('page_plugin_css')

<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.contact_info_row, .venue_details_row{
		font-size: 12px;
	}
	
	.venue_info_card .bootstrap-select{
		width: 80px !important;
		
	}
	.venue_info_card .bootstrap-select .btn{
		padding-left: 6px !important;
		padding-right: 2px !important;
	}
</style>
@endsection

@section('page_content')
<div class="page-heading">

	@include('layouts.venue-status-toggle')

	<h1 class="page-title">Venue</h1>

	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item">Information</li>
	</ol>
	<div class='vene_name_box text-danger'>
		{{ $venue->name }}
	</div>

</div>
<div class="page-content fade-in-up">
	@include('layouts.admin-venue-nav')
	
	<form class='form-danger' method='post' action="{{ route('admin.venue.update', $venue->id) }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		
		<div class="col-xl-4 venue_info_card">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Info</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-location-pin"></i></a>
					</div>
				</div>
				
				<div class="ibox-body text-center pl-0 pr-0 pt-0" style='position: relative;'>
					<div class='text-center mt-2'>
					<img id='previewfile' width='200' height='200' src="{{ asset('public/uploads/banners/' . $venue->banner) }}">
					</div>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Upload Banner' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input onchange="PreviewprofileImage();" id='uploadfile' type='file' name='banner' class='d-none' accept="image/*">
					</div>
				   
				    <div class='text-bold mt-3'>Trading Hours</div>
				   
				   <!-- @php
					$trading_hours = DB::table('trading_hours')->where('venue_id', $venue->id)->orderBy('id', 'asc')->get();
				   @endphp
				   @if($trading_hours)
				   <table style='font-size: 12px;' class='table'>
					<thead>
						<tr>
							<th>Label</th>
							<th>From</th>
							<th>To</th>
							<th>State</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($trading_hours as $trading_hour)
							<tr>
								<td>{{ $trading_hour->booking_id }}</td>
								<td>{{ $trading_hour->from }}</td>
								<td>{{ $trading_hour->to }}</td>
								<td>@if($trading_hour->isclosed) Closed @else Open @endif</td>
								<td><a onclick="return confirm('Are You sure want to delete this record?')" href="{{ route('admin.venue.tradinghour.delete', [$venue->id, $trading_hour->id]) }}"><i class='fa fa-trash text-danger'></i></a></td>
							</tr>
						@endforeach
					</tbody>
				   </table>
				   @endif
				    -->
				   
				   <table id="participantTable3" style='width: 100%; border: 0;'>
						<thead class=''>
							<tr>
								<td>Label</td>
								<td>From</td>
								<td>To</td>
								<td>State</td>
								<td></td>
							</tr>
						</thead>
						<tbody class='participantRow3_area'>
						@if (!$getDayTiming->isEmpty())
						@foreach($getDayTiming as $day)
						<tr class="participantRow3">
							<td width='30%'>
								<input class='form-control' type='text' name='days[{{$day->booking_id}}]' value="{{$day->day}}" readonly>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' value="{{$day->from}}" name='from[{{$day->booking_id}}]'>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' value="{{$day->to}}" name='to[{{$day->booking_id}}]'>
							</td>
							<td width='25%'>
								<select style='font-size: 12px;' name='isclosed[{{$day->booking_id}}]' class="form-control">
									<option @if($day->isclosed == 0) selected @endif value='0'>Open</option>
									<option @if($day->isclosed == 1) selected @endif value='1'>Close</option>
								</select>
							</td>
							
						</tr>
						@endforeach
						@else
						@foreach($getDays as $day)
						<tr class="participantRow3">
							<td width='30%'>
								<input class='form-control' type='text' name='days[{{$day->id}}]' value="{{$day->day}}" readonly>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' value="" name='from[{{$day->id}}]'>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' value="" name='to[{{$day->id}}]'>
							</td>
							<td width='25%'>
								<select style='font-size: 12px;' name='isclosed[{{$day->id}}]' class="form-control">
									<option value='0'>Open</option>
									<option value='1'>Close</option>
								</select>
							</td>
							
						</tr>
						@endforeach

						@endif
						</tbody>
						
					</table>
					
					<div class='col-md-12 form-group mt-3 text-left'>
						<label>Notice</label>
						<textarea name='notice' rows='4' class='form-control'>{{ $venue->notice }}</textarea>
					</div>
				   
				   
				</div>
			</div>
		</div>
		
		<div class="col-xl-5 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Details</div>
					
				</div>
				<div class="ibox-body">
				   
				   <div class='row venue_details_row'>
				   
				   <div class="form-group mb-4 col-md-6">
						<label>Trading Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
							<input value="{{ $venue->trading_name }}" required name='trading_name' class="form-control" type="text">
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Address</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
							<input id="autocomplete" onFocus="geolocate()" value="{{ $venue->address }}" name='address' class="form-control" type="text">
							<input type="hidden" name="latitude" id="latitude" value="{{$venue->latitude}}">
							<input type="hidden" name="longitude" id="longitude" value="{{$venue->longitude}}">
							<input type="hidden" name="location_id" id="location_id" value="{{$venue->location_id}}">
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Tagline</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
							<input value="{{ $venue->tagline }}" name='tagline' class="form-control" type="text">
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Average Cost Per Person</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left">ZAR</span>
							<input value="{{ number_format($venue->avg_cost,2) }}" name='avg_cost' class="form-control" type="text" step='any' onkeyup="numberSeperator(this)">
						</div>
					</div>
					
					
					<div class="form-group mb-4 col-md-6">
						<label>Atmosphere </label>
						<select class='form-control selectpicker' name='atmosphere_id'>
							@foreach($atmospheres as $atmosphere)
								<option @if($venue->atmosphere_id == $atmosphere->id) selected @endif value="{{ $atmosphere->id }}">{{ $atmosphere->name }}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Cuisine </label>
						<select class='form-control selectpicker' name='cuisine_id'>
							@foreach($cuisines as $cuisine)
								<option @if($venue->cuisine_id == $cuisine->id) selected @endif value="{{ $cuisine->id }}">{{ $cuisine->name }}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Dietaries</label>
						<select name='dietaries[]' class="form-control select2_demo_1" multiple="">
							@foreach($dietaries as $dietary)
								@php
									$selected = '';
									if(DB::table('venue_dietaries')->where('dietary_id', $dietary->id)->where('venue_id', $venue->id)->first()){
										$selected = 'selected';
									}
								@endphp
								<option {{ $selected }} value="{{ $dietary->id }}">{{ $dietary->name }}</option>
							@endforeach
						</select>
					</div>
					
					
					<div class="form-group mb-4 col-md-12">
						<label>Facilities</label>
						<select name='facilities[]' class="form-control select2_demo_1" multiple="">
							@foreach($facilities as $facility)
								@php
									$selected = '';
									if(DB::table('facility_venues')->where('facility_id', $facility->id)->where('venue_id', $venue->id)->first()){
										$selected = 'selected';
									}
								@endphp
								<option {{ $selected }} value="{{ $facility->id }}">{{ $facility->name }}</option>
							@endforeach
						</select>
					</div>
					
					
					<div class="form-group mb-4 col-md-6 d-none">
							<label>VAT Number</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input value="{{ $venue->vat }}" name='vat' class="form-control" type="text">
							</div>
						</div>
						<div class="form-group mb-4 col-md-6 d-none">
							<label>VAT Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input value="{{ $venue->vat_name }}" name='vat_name' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Company Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input required value="{{ $venue->name }}" name='name' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-6">
							<label>Company Registration Number</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input value="{{ $venue->registration_number }}" name='registration_number' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Postal address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input value="{{ $venue->postal_address }}" name='postal_address' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>About Venue</label>
							<textarea name='about' class='form-control' rows='4'>{{ $venue->about }}</textarea>
						</div>
   
				   </div>
				    
				</div>
			</div>
		</div>
	
		<div class="col-xl-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Contact Info</div>
					
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-mobile"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row contact_info_row'>
						<div class="form-group mb-4 col-md-12">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input value="{{ $venue->contact_email }}" name='contact_email' class="form-control" type="text">
							</div>
						</div>
						<div class="form-group mb-4 col-md-12">
							<label>Phone Number</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
								<input value="{{ $venue->contact_number }}" name='contact_number' class="form-control" type="text">
							</div>
						</div>
						<!--
						<div class="form-group mb-4 col-md-12">
							<label>Manager Name</label>
							<select class="selectpicker form-control">
								<option>Manager 1</option>
								<option>Manager 2</option>
								<option>Manager 3</option>
								<option>Manager 4</option>
							</select>
						</div>
						-->
						
						<div class="form-group mb-4 col-lg-5 pr-0">
							Company ID:
						</div>
						<div class="form-group mb-4 col-lg-7 p-0">
							<b class='text-danger' style='font-size: 13px; position: relative; bottom: 0px;'>{{ $venue->company_id }}</b>
						</div>
						
						<div class="form-group mb-4 col-md-12">
						<div class='form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Update Venue</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Update</span>
								</span>
							</button>
						</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
	</form>
	
	
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
@endsection

@section('page_js')
<script>

	// thousand seperator
	function numberSeperator(object) {
		var val = object.value;
	  	val = val.replace(/[^0-9\.]/g,'');
	  
		if(val != "") {
		    valArr = val.split('.');
		    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
		    val = valArr.join('.');
		 }
	  
	  	object.value = val;
	}


	$(document).ready(function(){
		$(".select2_demo_1").select2();
		$(".select2_demo_2").select2({
			placeholder: "Select a state",
			allowClear: true
		});
		
		$('.participantRow3_area tr').first().find('.remove3').hide();
	});
	
	
	
	/* Variables */
	var p3 = $("#participants3").val();
	var row3 = $(".participantRow3");
	
	/* Functions */
	function getP3(){
	  p3 = $("#participants3").val();
	}

	function addRow3() {
	  var _row3 = row3.clone().appendTo(".participantRow3_area");
		$(_row3).find('input').val('');
		$('.clockpicker').clockpicker();
		
		$('.participantRow3_area tr').find('.remove3').show();
		$('.participantRow3_area tr').first().find('.remove3').hide();
		
	}

	function removeRow3(button3) {
		if($('.participantRow3_area tr').length > 1){
			button3.closest("tr").remove();
		}
		
		$('.participantRow3_area tr').find('.remove3').show();
		$('.participantRow3_area tr').first().find('.remove3').hide();
	}
	
	
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>


<script>
 function initAutocomplete() {
    var input = document.getElementById('autocomplete');
    // var options = {
    //   types: ['(regions)'],
    //   componentRestrictions: {country: "IN"}
    // };
    var options = {}

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      var placeId = place.place_id;
      // to set city name, using the locality param
      var componentForm = {
        locality: 'short_name',
      };
      $("#latitude").val(lat);
      $("#longitude").val(lng);
      $("#location_id").val(placeId);
    });
  }

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

$(document).ready(function(){
	$('.clockpicker').clockpicker();
});

$('.venue-open-toggle').change(function() {
		var status = '';
			if ($(this).is(":checked")) {
				status = 1;
			} else {
				status = 0;
			}
		// ajax call
			$.ajax({
				type: 'GET',
				data:{
					status: status,
					id: '{{$id}}'
				},
				url: "{{ route('admin.venue-status.update') }}",
				success: function(res){
					console.log(res);
				}
			});

	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection