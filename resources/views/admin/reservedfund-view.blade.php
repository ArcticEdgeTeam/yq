@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
@endphp

@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Reserved Funds</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.reservedfunds') }}">Reserved Funds</a></li>
		<li class="breadcrumb-item">View</li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<!--<div class="flexbox">
				<a href="waiter-create.php" class="btn btn-success btn-fix">
					<span class="btn-icon">Add new</span>
				</a>
			</div>-->
			<div></div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-hover" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Customer</th>
						<th>Date & Time</th>
						<th>Transaction ID</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					@foreach($reservedfunds as $reservedfund)
					@php
					$customer = DB::table('users')->where('id', $reservedfund->customer_id)->first();
					$total += $reservedfund->amount;
					@endphp
					<tr>
						<td>{{ $counter++ }}</td>
						<td>{{ $customer->name }}</td>
						<td>{{ \Carbon\Carbon::parse($reservedfund->created_at)->format('d M Y') }} at {{ \Carbon\Carbon::parse($reservedfund->created_at)->format('H:i:s') }}</td>
						<td>{{ $reservedfund->transaction_id }}</td>
						<td>{{ $currency }} {{ $reservedfund->amount }}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th colspan='5' class='text-right'>
							Total: {{ $currency }} {{ $total }}
						</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection