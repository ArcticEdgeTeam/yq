@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Cuisines</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.cuisines') }}">Cuisines</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				
				<div class="form-group">
					<a href="{{ route('admin.cuisine.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Cuisine</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
			</div>
			<div class="input-group-icon input-group-icon-left mr-3">
				<span class="input-icon input-icon-right font-16"><i class="ti-search"></i></span>
				<input class="form-control form-control-solid" id="key-search" type="text" placeholder="Search ...">
			</div>
		</div>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5" id="datatable">
				<thead class="thead-default thead-lg">
					<tr>
						<th>#</th>
						<th>Icon</th>
						<th>Name</th>
						<th>Venues</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($cuisines as $cuisine)
					@if($cuisine->image == '')
						<?php
							$cuisine->image = 'default.png';
						?>
					@endif
					<tr>
						<td>{{ $counter++ }}</td>
						<td><img src="{{ asset('public/uploads/cuisines/' . $cuisine->image) }}" width='50' height='50' class='img-circle'></td>
						<td>{{ $cuisine->name }}</td>
						<td>{{ number_format(DB::table('venues')->where('cuisine_id', $cuisine->id)->count()) }}</td>
						<td>
						@if($cuisine->status == 1)
							<span class='d-none'>Active</span><a class='text-success' data-toggle='tooltip' title='Active' href='venue-edit.php'><i style='font-size: 22px;' class='fa fa-check-circle'></i></a>
						@else
							<span class='d-none'>Inactive</span><a class='text-danger' data-toggle='tooltip' title='Inactive' href='venue-edit.php'><i style='font-size: 22px;' class='fa fa-times-circle'></i></a>
						@endif
						</td>
						<td>
							<span data-html='true' class='text-info' data-toggle='tooltip' title="Created at: {{ $cuisine->created_at->format('d M Y') }}<br />Updated at: {{ $cuisine->updated_at->format('d M Y') }}"><i style='font-size: 22px;' class='fa fa-info'></i></span>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.cuisine.edit', $cuisine->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
@endsection