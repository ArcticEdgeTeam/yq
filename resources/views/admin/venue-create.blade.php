@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}
	
@endphp
@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
<style>
	.contact_info_row, .venue_details_row{
		font-size: 12px;
	}
</style>
@endsection

@section('page_content')
<div class="page-heading">
	<h1 class="page-title">Venues</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
		<li class="breadcrumb-item">New</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<form id="reg_form" class='form-danger' method='post' action="{{ route('admin.venue.create') }}" enctype='multipart/form-data'>
	@csrf
	<div class='row'>
		
		<div class="col-xl-4">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Info</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-location-pin"></i></a>
					</div>
				</div>
				
				<div class="ibox-body text-center pl-0 pr-0 pt-0" style='position: relative;'>
					<div class='text-center mt-2'>
					<img id='previewfile' width='200' height='200' src="{{ asset('public/uploads/banners/default.png') }}">
					</div>
					<div style='position: absolute; right: 20px; top: 20px;'>
						<label data-toggle='tooltip' title='Upload Banner' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
						<input onchange="PreviewprofileImage();" id='uploadfile' type='file' name='banner' class='d-none' accept="image/*">
					</div>
					
				   <div class='text-bold mt-3'>Trading Hours</div>
				  
				   <table id="participantTable3" style='width: 100%; border: 0;'>
						<thead class=''>
							<tr>
								<td>Days</td>
								<td>From</td>
								<td>To</td>
								<td>State</td>
								<td></td>
							</tr>
						</thead>
						<tbody class='participantRow3_area'>
						@foreach($getDays as $day)
						<tr class="participantRow3">
							<td width='30%'>
								<input class='form-control' type='text' name='days[{{$day->id}}]' value="{{$day->day}}" readonly>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' name='from[{{$day->id}}]'>
							</td>
							<td width='22%' class="clockpicker" data-autoclose="true">
								<input autocomplete='off' class='form-control' type='text' name='to[{{$day->id}}]'>
							</td>
							<td width='25%'>
								<select style='font-size: 12px;' name='isclosed[{{$day->id}}]' class="form-control">
									<option value='0'>Open</option>
									<option value='1'>Close</option>
								</select>
							</td>
							
						</tr>
						@endforeach
						</tbody>
						
					</table>
					
					
					<div class='col-md-12 form-group mt-3 text-left'>
						<label>Notice</label>
						<textarea name='notice' rows='4' class='form-control'></textarea>
					</div>
				   
				</div>
			</div>
		</div>
	
		<div class="col-xl-5 p-0">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Details</div>
					
				</div>
				<div class="ibox-body">
				   
				   <div class='row venue_details_row'>
				   
				   <div class="form-group mb-4 col-md-6">
						<label>Trading Name</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
							<input required name='trading_name' class="form-control" type="text" >
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Address</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
							<input id="autocomplete" onFocus="geolocate()" name='address' class="form-control" type="text">
							<input type="hidden" name="latitude" id="latitude" value="">
							<input type="hidden" name="longitude" id="longitude" value="">
							<input type="hidden" name="location_id" id="location_id" value="">
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Tagline</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-pencil"></i></span>
							<input name='tagline' class="form-control" type="text">
						</div>
					</div>
					
					
					<div class="form-group mb-4 col-md-6">
						<label>Company ID</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
							<input id='company_id' required name='company_id' class="form-control" type="text">
						</div>
						<div id='ajax_msg2' style='font-size: 11px;'></div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Email</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
							<input name='contact_email' class="form-control" type="text">
						</div>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Phone</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
							<input name='contact_number' class="form-control" type="text">
						</div>
					</div>
					
					
					<div class="form-group mb-4 col-md-6">
						<label>Atmosphere </label>
						<select class='form-control selectpicker' name='atmosphere_id'>
							@foreach($atmospheres as $atmosphere)
								<option value="{{ $atmosphere->id }}">{{ $atmosphere->name }}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group mb-4 col-md-6">
						<label>Cuisine </label>
						<select class='form-control selectpicker' name='cuisine_id'>
							@foreach($cuisines as $cuisine)
								<option value="{{ $cuisine->id }}">{{ $cuisine->name }}</option>
							@endforeach
						</select>
					</div>
					
					<div class="form-group mb-4 col-md-12">
						<label>Average Cost Per Person</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left">{{ $currency }}</span>
							<input name='avg_cost' class="form-control" type="text" step='any' onkeyup="numberSeperator(this)">
						</div>
					</div>
					
					<!-- <div class="form-group mb-4 col-md-6">
						<label>Admin Commission</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left">{{ $currency }}</span>
							<input required name='admin_commission' class="form-control" type="number" step='any'>
						</div>
					</div> -->
					

					<!-- <div class="form-group mb-4 col-md-6">
						<label>Admin Commission Limit Percentage</label>
						<div class="input-group-icon input-group-icon-left">
							<span class="input-icon input-icon-left"><i class="fa fa-percent"></i></span>
							<input name='admin_commission_percentage' value="" type='number' min='0' max="100" class='form-control'>
						</div>
					</div> -->
					
					
					<div class="form-group mb-4 col-md-12">
						<label>Dietaries</label>
						<select name='dietaries[]' class="form-control select2_demo_1" multiple="">
							@foreach($dietaries as $dietary)
								<option value="{{ $dietary->id }}">{{ $dietary->name }}</option>
							@endforeach
						</select>
					</div>
					
					
					
					<div class="form-group mb-4 col-md-12">
						<label>Facilities</label>
						<select name='facilities[]' class="form-control select2_demo_1" multiple="">
							@foreach($facilities as $facility)
								<option value="{{ $facility->id }}">{{ $facility->name }}</option>
							@endforeach
						</select>
					</div>
					
						
						<div class="form-group mb-4 col-md-6 d-none">
							<label>VAT Number</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input name='vat' class="form-control" type="text">
							</div>
						</div>
						<div class="form-group mb-4 col-md-6 d-none">
							<label>VAT Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input name='vat_name' class="form-control" type="text">
							</div>
						</div>
						<div class="form-group mb-4 col-md-6">
							<label>Company Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input required name='name' class="form-control" type="text">
							</div>
						</div>
						<div class="form-group mb-4 col-md-6">
							<label>Company Registration Number</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-flag"></i></span>
								<input name='registration_number' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Postal address</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-location-pin"></i></span>
								<input name='postal_address' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>About Venue</label>
							<textarea name='about' class='form-control' rows='4'></textarea>
						</div>
						
						
					
   
				   </div>
				    
				</div>
			</div>
		</div>
		
		<div class="col-xl-3">
			<div class="ibox ibox-fullheight">
				<div class="ibox-head">
					<div class="ibox-title">Venue Admin</div>
					<div class="ibox-tools">
						<a class="font-18" href="javascript:;"><i class="ti-user"></i></a>
					</div>
				</div>
				<div class="ibox-body">
					<div class='row'>
						
						<div class="form-group mb-4 col-md-12">
							<label>Name</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-user"></i></span>
								<input required name='user_name' class="form-control" type="text">
							</div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Email</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-email"></i></span>
								<input id="email" required name='email' class="form-control" type="text">
							</div>
							<div id='ajax_msg' style='font-size: 11px;'></div>
						</div>
						
						<div class="form-group mb-4 col-md-12">
							<label>Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input required name='password' value="" class="form-control password" type="password">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							<label>Confirm Password</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
								<input name='confirm_password' value="" required class="form-control confirm_password" type="password" required>
							</div>
							<span class="text-danger confirm-password-message" style="display: none;">Password not matching!</span>
						</div>
					
						
						<div class="form-group mb-4 col-md-12">
							<label>Telephone</label>
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-mobile"></i></span>
								<input name='phone' class="form-control" type="text">
							</div>
						</div>

						<div class='col-md-6'>
							<span style='font-size: 11px;'><b>Bookings</b></span>
						</div>
						
						<div class='col-md-6 text-right'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input checked name='booking' type="checkbox">
								<span></span>
							</label>
						</div>


						<div class='col-md-8 mb-3 mt-3'>
						<span style="font-size: 11px;"><b>Auto Accept App Booking</b></span>
						</div>
						<div class='col-md-4 text-right  mb-3 mt-3'>
							<label class="ui-switch switch-icon switch-solid-danger switch-large">
								<input type="checkbox" name='booking_auto_accept' checked>
								<span></span>
							</label>
						</div>

						<div class='col-md-12'>
						<label>Average Booking</label>
						</div>
						<div class='col-md-12 text-right clockpicker' data-autoclose="true">
							<input style='padding-left: 6px; padding-right: 6px;' autocomplete='off' class='form-control' type='text' name='average_booking' value="" required>
						<br>
						</div>


						<div class='col-md-12 mb-4'>
							<span style='font-size: 16px; font-weight: 500;'> Social links</span>
							<div style='border-bottom: 1px solid #eee;'></div>
						</div>

						
						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-facebook"></i></span>
								<input name='fb_link' class="form-control" type="text" placeholder="https://www.facebook.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
						
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-instagram"></i></span>
								<input name='insta_link' class="form-control" type="text" placeholder="https://www.instagram.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-twitter"></i></span>
								<input name='twitter_link' class="form-control" type="text" placeholder="https://www.twitter.com/xyz">
							</div>
						</div>

						<div class="form-group mb-4 col-md-12">
							
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-link"></i></span>
								<input name='web_link' class="form-control" type="text" placeholder="https://www.xyz.com">
							</div>
						</div>
						
						
						<div class="form-group mb-4 col-md-12">
						<div class='form-group'>
							<button class="btn btn-danger btn-fix btn-animated from-left reg_btn">
								<span class="visible-content">Add Venue</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
								</span>
							</button>
						</div>
						</div>
						  
					  
					</div>
				</div>
			</div>
		</div>
	
		
	</div>
	</form>
	
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('public/assets/vendors/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js') }}"></script>
@endsection

@section('page_js')

<script>

	function numberSeperator(object) {
		var val = object.value;
	  	val = val.replace(/[^0-9\.]/g,'');
	  
		if(val != "") {
		    valArr = val.split('.');
		    valArr[0] = (parseInt(valArr[0],10)).toLocaleString();
		    val = valArr.join('.');
		 }
	  
	  	object.value = val;
	}

</script>

<script>

 function initAutocomplete() {
    var input = document.getElementById('autocomplete');
    // var options = {
    //   types: ['(regions)'],
    //   componentRestrictions: {country: "IN"}
    // };
    var options = {}

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      var placeId = place.place_id;
      // to set city name, using the locality param
      var componentForm = {
        locality: 'short_name',
      };
      $("#latitude").val(lat);
      $("#longitude").val(lng);
      $("#location_id").val(placeId);
    });
  }

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }

}

$(document).ready(function(){
	$('.clockpicker').clockpicker();
});

</script>

<script>
	$(document).ready(function(){

		if ($('input[name="booking"]').prop('checked') == true) {
				$('input[name="booking_auto_accept"]').attr('disabled', false);
				$('input[name="booking_auto_accept"]').attr('checked', true);
				$('input[name="average_booking"]').attr('disabled', false);
			} else {
				$('input[name="booking_auto_accept"]').attr('disabled', true);
				$('input[name="booking_auto_accept"]').attr('checked', false);
				$('input[name="average_booking"]').attr('disabled', true);
			}


		$('input[name="booking"]').change(function(){
			if ($(this).prop('checked') == true) {
				$('input[name="booking_auto_accept"]').attr('disabled', false);
				$('input[name="booking_auto_accept"]').attr('checked', true);
				$('input[name="average_booking"]').attr('disabled', false);
			} else {
				$('input[name="booking_auto_accept"]').attr('disabled', true);
				$('input[name="booking_auto_accept"]').attr('checked', false);
				$('input[name="average_booking"]').attr('disabled', true);
			}
		});
		
		 $(".select2_demo_1").select2();
		$(".select2_demo_2").select2({
			placeholder: "Select a state",
			allowClear: true
		});
		
		$('#email').blur(function() {
			var user_email = $(this).val();
			$.ajax({
				type: 'get',
				url: "{{ route('check.email') }}/" + user_email,
				success: function(msg){
					if(msg != 'valid_email'){
						$('#ajax_msg').html("<span class='text-danger email-valid'>Email Already Exist</span>");
						 //  $("#reg_form").submit(function(e){
							// 	 e.preventDefault();
							// });
					}else{
						// $("#reg_form").submit(function(e){
						// 		e.currentTarget.submit();
						// 	});
						$('#ajax_msg').html("<span class='text-success email-valid'>Valid Email</span>");
					}
				}
			});
			
		});


		$('#company_id').blur(function(){
			var company_id = $(this).val();
			$.ajax({
				type: 'get',
				url: "{{ route('check.company.id') }}/" + company_id,
				success: function(msg){
					if(msg != 'valid_id'){
						$('#ajax_msg2').html("<span class='text-danger company-valid'>Company ID already Registered.</span>");
						 //  $("#reg_form").submit(function(e){
							// 	 e.preventDefault();
							// });
					}else{
						// $("#reg_form").submit(function(e){
						// 		e.currentTarget.submit();
						// 	});
						$('#ajax_msg2').html("<span class='text-success company-valid'>Valid ID</span>");
					}
				}
			});
			
		});

			$('#reg_form').submit(function(e) {
			e.preventDefault();

			 	var _password = $('.password').val();
				var _password_confirm = $('.confirm_password').val();
				if(_password != '') {

					if (_password.length < 8) {
						swal("", "Password must be 8 chanracters long.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}

					if(_password != _password_confirm) {
						swal("", "Password Mismatch", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
					}
					
				}

			 	// check URL

			 	if ($('input[name="fb_link"]').val() != '') {

		    		var FBurl = /^(http|https)\:\/\/(www.|)facebook.com\/.*/i;
		    		if(!$('input[name="fb_link"]').val().match(FBurl)) {

				      	swal("", "Facebook URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
				    } 
		    	}

		    	if ($('input[name="insta_link"]').val() != '') {

		    		var Instaurl = /^(http|https)\:\/\/(www.|)instagram.com\/.*/i;
		    		if(!$('input[name="insta_link"]').val().match(Instaurl)) {

				      	swal("", "Instagram URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
				    }

		    	}

		    	if ($('input[name="twitter_link"]').val() != '') {

		    		var TWurl = /^(http|https)\:\/\/(www.|)twitter.com\/.*/i;
		    		if(!$('input[name="twitter_link"]').val().match(TWurl)) {

				      	swal("", "Twitter URL is incorrect.", "error");
						$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
						return false;
			      	} 
		    	}

		    	if ($('input[name="web_link"]').val() != '') {

		    		var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;

		    		  if(!pattern.test($('input[name="web_link"]').val())){

					        swal("", "Website URL is incorrect.", "error");
							$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
							return false;
					    } 
		    	}



			 	if ($('.email-valid').text() == 'Valid Email' && $('.company-valid').text() == 'Valid ID') {
			 		e.currentTarget.submit();

			 		if ($(this).data('submitted') === true) {
			            // Form is already submitted
			            console.log('Form is already submitted, waiting response.');
			            // Stop form from submitting again
			           $('.reg_btn').attr('disabled', true);
			        } else {
			            // Set the data-submitted attribute to true for record
			            $(this).data('submitted', true);
			        }
			        
			 	} else {
			 	
			 		swal("", "Please verify your Email ID and Company ID.", "error");
					$('.confirm.btn.btn-lg.btn-primary').addClass('btn-danger');
			 	}
		});
		
		
		
		
		$('.participantRow3_area tr').first().find('.remove3').hide();
		
		
	});
	
	
	
	/* Variables */
	var p3 = $("#participants3").val();
	var row3 = $(".participantRow3");
	
	/* Functions */
	function getP3(){
	  p3 = $("#participants3").val();
	}

	function addRow3() {
	  var _row3 = row3.clone().appendTo(".participantRow3_area");
		$(_row3).find('input').val('');
		$('.clockpicker').clockpicker();
		
		$('.participantRow3_area tr').find('.remove3').show();
		$('.participantRow3_area tr').first().find('.remove3').hide();
		
	}

	function removeRow3(button3) {
		if($('.participantRow3_area tr').length > 1){
			button3.closest("tr").remove();
		}
		
		$('.participantRow3_area tr').find('.remove3').show();
		$('.participantRow3_area tr').first().find('.remove3').hide();
	}
	
	
</script>

<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB23jFhSwxGXdVrgd0LNI4amvw418pTgOc&&libraries=places&callback=initAutocomplete" async defer></script>
@endsection