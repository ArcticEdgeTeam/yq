@extends('layouts.admin')

@section('page_plugin_css')
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Facilities</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.facilities') }}">Facilities</a></li>
		<li class="breadcrumb-item">New</li>
	</ol>
</div>
<div class="page-content fade-in-up">
	<form class='form-danger' method='post' action="{{ route('admin.facility.create') }}" enctype='multipart/form-data'>
	@csrf
	
<div class="row">
	<div class='col-md-3'>
		<div class="ibox ibox-fullheight">
			<div class="ibox-body text-center" style='position: relative;'>
				<img id='previewfile' width='150' height='150'  src="{{ asset('public/uploads/facilities/default.png') }}" class='img-circle'>
				<div style='position: absolute; right: 20px; top: 20px;'>
					<label data-toggle='tooltip' title='Upload Icon' class='btn btn-circle btn-danger btn-sm' for='uploadfile'><i class='fa fa-upload mb-2'></i></label>
					<input accept="image/*" onchange="PreviewprofileImage();" id='uploadfile' type='file' name='image' class='d-none'>
				</div>
				
				
			</div>
		</div>
	</div>
	
	<div class="col-xl-9">
		<div class="ibox ibox-fullheight">
			<div class="ibox-head">
				
				<div class="ibox-title">Facility Info</div>
				<div class="ibox-tools">
					<span style='font-size: 11px;'><b>Active</b></span>
					<br />
					<label class="ui-switch switch-icon switch-solid-danger switch-large">
						<input type="checkbox" name='status' checked>
						<span></span>
					</label>
					
				</div>
				
			</div>
			<div class="ibox-body">
			   <div class='row'>
					<div class='col-md-6'>
						 <div class="form-group mb-4">
							<div class="input-group-icon input-group-icon-left">
								<span class="input-icon input-icon-left"><i class="ti-tag"></i></span>
								<input class="form-control " required name='name' type="text" placeholder="Facility Name">
							</div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class="form-group">
							<button class="btn btn-danger btn-fix btn-animated from-left">
								<span class="visible-content">Add Facility</span>
								<span class="hidden-content">
									<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
								</span>
							</button>
					   </div>
					</div>
			   </div>
			</div>
		</div>
	</div>
	
</div>
</form>
</div>
@endsection

@section('page_plugin_js')
@endsection

@section('page_js')
<script>
	function PreviewprofileImage(){

       var oFReader = new FileReader();
       oFReader.readAsDataURL(document.getElementById("uploadfile").files[0]);
        var checkimage = $('#uploadfile').val().split('.').pop();
		checkimage = checkimage.toLowerCase();
        if(checkimage=="png"  || checkimage=="jpg" || checkimage=="jpeg" || checkimage=="gif")
        {
            oFReader.onload = function (oFREvent) {
            document.getElementById("previewfile").src = oFREvent.target.result;
            }
           
        }
        else
        {
            alert("Please upload only PNG and JPEG image.");
            return;
        }
        
    }
</script>
@endsection