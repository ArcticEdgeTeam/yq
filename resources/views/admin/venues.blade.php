@extends('layouts.admin')

@section('page_plugin_css')
<link href="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
<link href="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />

<link href="{{ asset('public/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('page_css')
@endsection

@section('page_content')	
<div class="page-heading">
	<h1 class="page-title">Venues</h1>
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="index.html"><i class="la la-home font-20"></i></a>
		</li>
		<li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
		<li class="breadcrumb-item"><a href="{{ route('admin.venues') }}">Venues</a></li>
	</ol>
</div>
<div class="page-content fade-in-up">
<div class="ibox">
	<div class="ibox-body">
		<!-- <h5 class="font-strong mb-4">DATATABLE</h5> -->
		<div class="flexbox mb-4">
			<div class="flexbox">
				
				<div class="form-group">
					<a href="{{ route('admin.venue.create') }}" class="btn btn-danger btn-fix btn-animated from-left">
						<span class="visible-content">Add Venue</span>
						<span class="hidden-content">
							<span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
						</span>
					</a>
			   </div>
			</div>
			
		</div>
		<form action="{{route('admin.venues')}}" method="GET">

			<div class="row">
			
				<div class="form-group col-md-3">
					<label>Specify Date Range</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						<input name='date_range' class="form-control" id="daterange_1" type="text" value="{{request()->date_range}}">
						
					</div>
				</div>

				<div class="form-group mb-4 col-md-3">
					<label>Users</label>
					<select name='user' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select User</option>
						@foreach($users as $user)
							<option @if(request()->user == $user->id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group mb-4 col-md-3">
					<label>Venues</label>
					<select name='venue' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Venue</option>
						@foreach($venues as $venue)
							<option @if(request()->venue == $venue->id) selected @endif  value="{{ $venue->id }}">{{ $venue->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group mb-4 col-md-2">
					<label>Status</label>
					<select name='status' class="selectpicker form-control" data-dropup-auto="false">
						<option value="">Select Status</option>
						
							<option @if(request()->status == '1') selected @endif  value="1">Active</option>
							<option @if(request()->status == '0') selected @endif  value="0">Inactive</option>
						
					</select>
				</div>

				<div class="form-group col-md-1" style="margin-top: 27px;">
					<button class='btn btn-danger'>Filter</button>
				</div>

			</div>
		</form>
		<div class="table-responsive row">
			<table class="table table-bordered table-head-purple table-border-purple mb-5">
				<thead class="thead-default thead-lg">
					<tr>
						<th>Venue Name</th>
						<th>Created BY</th>
						<th>Created At</th>
						<th>Status</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>
					@if(!$getVenues->isEmpty())
					@foreach($getVenues as $venue)
					<tr>
						<td>{{ $venue->name }}</td>
						<td>
							<!-- <?php
								$venue_admin = DB::table('users')->where('venue_id', $venue->id)->where('role', 'venue')->first();
								if($venue_admin){
									echo $venue_admin->name;
								}else{
									echo 'N/A';
								}
							?> -->
							{{$venue->admin_name == ''? 'N/A' : $venue->admin_name}}
							
						</td>
						<td>{{ $venue->created_at->format('d M Y') }}</td>						
						<td>
							@if($venue->status == 1)
								<span class='d-none'>Active</span><i style='font-size: 22px;' data-toggle='tooltip' title='Active' class="fa fa-check-circle text-success" aria-hidden="true"></i>
							@else
								<span class='d-none'>Inactive</span><i style='font-size: 22px;' data-toggle='tooltip' title='Inactive' class="fa fa-times-circle text-danger" aria-hidden="true"></i>
							@endif
							
							@php
							    $venue_date = \Carbon\Carbon::parse($venue->created_at);
							    $current_date = \Carbon\Carbon::now();

							    $differenceInMonth = $venue_date->diffInMonths($current_date);

							@endphp
							    @if ($differenceInMonth < $getFeatured->months) 
							    	<span class='d-none'>Featured</span><i style='font-size: 22px;' data-toggle='tooltip' title='Featured' class="fa fa-star text-warning" aria-hidden="true"></i>
							       
							    @else 
							    	<span class='d-none'>Not Feautured</span><i style='font-size: 22px;' data-toggle='tooltip' title='Not Feautured' class="fa fa-star" aria-hidden="true"></i>
							    @endif
								
						</td>
						<td>
							<a class='text-warning' data-toggle='tooltip' title='View' href="{{ route('admin.table-booking-view', $venue->id) }}"><i style='font-size: 22px;' class='fa fa-eye'></i></a>
						</td>
					</tr>
					@endforeach
					@else
					<tr>
						<td colspan="5" style="text-align: center;">No record found.</td>
					</tr>
					@endif
				</tbody>
			</table>

			<div class="col-md-6"></div>
			<div class="col-md-6">
				<ul class="pagination justify-content-center mt-4" style="float: right;">
	                <li class="page-item active">
	                    {{ $getVenues->appends(['date_range' => Request::get('date_range'), 'user' => Request::get('user'), 'venue' => Request::get('venue')])->links('pagination::default') }}
	                </li>
	        	</ul>
			</div>
		</div>
	</div>
</div>
</div>

@endsection

@section('page_plugin_js')
<script src="{{ asset('public/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('public/assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script src="{{ asset('public/assets/vendors/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('page_js')
<script>
$(function() {
  $('#daterange_1').daterangepicker({
	autoUpdateInput: false,
    locale: {
		format: 'DD-MM-YYYY',
		separator: " / "
	}
  });

  $('#daterange_1').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('#daterange_1').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });

});

</script>
@endsection