<div id='subnav' class="ibox p-4">   
	<ul class="nav nav-tabs tabs-line tabs-line-pink">
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.booking.create') ? 'active' : '' }} {{ Route::is('venue.bookings') ? 'active' : '' }} {{ Route::is('venue.bookings.filter') ? 'active' : '' }} {{ Route::is('venue.booking.edit') ? 'active' : '' }}" href="{{ route('venue.table-booking-view') }}" >Bookings</a>
		</li>
		
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.orders') ? 'active' : '' }} {{ Route::is('venue.order.list') ? 'active' : '' }} {{ Route::is('venue.order.list.filter') ? 'active' : '' }} {{ Route::is('venue.order.edit') ? 'active' : '' }} " href="{{ route('venue.orders') }}" >Orders</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.cashups') ? 'active' : '' }} {{ Route::is('venue.cashups.filter') ? 'active' : '' }}" href="{{ route('venue.cashups') }}" >Cashups</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.reviews') ? 'active' : '' }} {{ Route::is('venue.reviews.filter') ? 'active' : '' }}" href="{{ route('venue.reviews') }}" >Reviews</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.overview') ? 'active' : '' }}" href="{{ route('venue.overview') }}" >Overview</a>
		</li>
		<!-- <li class="nav-item">
			<a class="nav-link {{ Route::is('venue.table-booking-view') ? 'active' : '' }}" href="{{ route('venue.table-booking-view') }}" >Booking View</a>
		</li> -->
	</ul>
</div>