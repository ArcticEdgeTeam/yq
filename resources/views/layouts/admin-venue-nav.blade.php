<div id='subnav' class="ibox p-4">	
	<ul class="nav nav-tabs tabs-line tabs-line-pink">
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.booking.create') ? 'active' : '' }} {{ Route::is('admin.venue.bookings') ? 'active' : '' }} {{ Route::is('admin.venue.bookings.filter') ? 'active' : '' }} {{ Route::is('admin.venue.booking.edit') ? 'active' : '' }} {{ Route::is('admin.table-booking-view') ? 'active' : '' }}" href="{{ route('admin.table-booking-view', $id) }}" >Bookings</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.orders') ? 'active' : '' }} {{ Route::is('admin.venue.order.list') ? 'active' : '' }} {{ Route::is('admin.venue.order.list.filter') ? 'active' : '' }} {{ Route::is('admin.venue.order.edit') ? 'active' : '' }}" href="{{ route('admin.venue.orders', $id) }}" >Orders</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.cashups') ? 'active' : '' }} {{ Route::is('admin.venue.cashups.filter') ? 'active' : '' }}" href="{{  route('admin.venue.cashups', $id) }}" >Cashups</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.staff') ? 'active' : '' }} {{ Route::is('admin.venue.staff.create') ? 'active' : '' }} {{ Route::is('admin.venue.staff.edit') ? 'active' : '' }} {{ Route::is('admin.venue.staff.wallet') ? 'active' : '' }}" href="{{ route('admin.venue.staff', $id) }}" >Staff</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.information') ? 'active' : '' }}" href="{{ route('admin.venue.information', $id) }}" >Information</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.setting') ? 'active' : '' }}" href="{{ route('admin.venue.setting', $venue->id) }}" >Settings</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.reviews') ? 'active' : '' }} {{ Route::is('admin.venue.reviews.filter') ? 'active' : '' }} {{ Route::is('admin.venue.review.edit') ? 'active' : '' }}" href="{{ route('admin.venue.reviews', $id) }}" >Reviews</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.products') ? 'active' : '' }} {{ Route::is('admin.venue.product.create') ? 'active' : '' }} {{ Route::is('admin.venue.product.edit') ? 'active' : '' }}" href="{{ route('admin.venue.products', $id) }}" >Products</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.ingredients') ? 'active' : '' }} {{ Route::is('admin.venue.ingredient.create') ? 'active' : '' }} {{ Route::is('admin.venue.ingredient.edit') ? 'active' : '' }} {{ Route::is('admin.venue.unit.create') ? 'active' : '' }} {{ Route::is('admin.venue.unit.edit') ? 'active' : '' }}" href="{{ route('admin.venue.ingredients', $id) }}" >Ingredients & Units</a>
		</li>
		
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.categories') ? 'active' : '' }} {{ Route::is('admin.venue.category.create') ? 'active' : '' }} {{ Route::is('admin.venue.category.edit') ? 'active' : '' }}" href="{{ route('admin.venue.categories', $id) }}" >Categories</a>
		</li>

		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.menus') ? 'active' : '' }} {{ Route::is('admin.venue.menu.create') ? 'active' : '' }} {{ Route::is('admin.venue.menu.edit') ? 'active' : '' }}" href="{{ route('admin.venue.menus', $id) }}" >Menus</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.tables') ? 'active' : '' }} {{ Route::is('admin.venue.table.create') ? 'active' : '' }} {{ Route::is('admin.venue.table.edit') ? 'active' : '' }}" href="{{ route('admin.venue.tables', $id) }}" >Tables</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.bars') ? 'active' : '' }} {{ Route::is('admin.venue.bar.create') ? 'active' : '' }} {{ Route::is('admin.venue.bar.edit') ? 'active' : '' }}" href="{{ route('admin.venue.bars', $id) }}" >Prep Area</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.venue.overview') ? 'active' : '' }}" href="{{ route('admin.venue.overview', $id) }}" >Overview</a>
		</li>

		<!-- <li class="nav-item">
			<a class="nav-link {{ Route::is('admin.table-booking-view') ? 'active' : '' }}" href="{{ route('admin.table-booking-view', $id) }}" >Table Booking View</a>
		</li> -->
		
	</ul>	
</div>