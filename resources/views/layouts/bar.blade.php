<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Strawberry pop | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link rel="icon" href="{{ asset('public/assets/logo.png') }}" type="image/png" sizes="16x16">
    <!-- PLUGINS STYLES-->
	@section('page_plugin_css')
	@show
	{{-- <link href="{{ asset('public/assets/vendors/morris.js/morris.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" /> --}}
    <!-- THEME STYLES-->
    <link href="{{ asset('public/assets/css/main.min2.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
	@section('page_css')
	@show
	<link href="{{ asset('public/assets/vendors/dataTables/datatables.min.css') }}" rel="stylesheet" />
	<style>
		table.dataTable{
			border-collapse: collapse !important;
		}
		
		table tr td{
			border: none !important;
			border-bottom: 1px solid #eee !important;
			border-right: 1px solid #eee !important;
		}
		
		.table_view_row .box{
			transition: 0.2s;
		}
		.table_view_row .box:hover{
			color: #000;
			transform: scale(1.05);
			
		}
		
		.header .admin-dropdown-menu .dropdown-arrow:after{
			background: #f75a5f;
		}
		
		.side-menu>li.active>a, .side-menu>li.active>a .arrow, .side-menu>li.active>a .sidebar-item-icon{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li.active>a:hover{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li:hover a{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li:hover ul li a{
			color: #b4bcc8;
			background: #34495f;;
		}
		
		.side-menu>li:hover ul li a:hover{
			color: #fff;
		}
		
		.header .admin-dropdown-menu .admin-features-item:hover{
			color: #f75a5f;
		}
		
		.content-wrapper{
			min-height: 850px;
		}
		
		.vene_name_box{
			position: absolute;
			top: 34px;
			right: 30px;
			font-size: 25px;
			font-weight: bold;
			text-transform: uppercase;
		}
		
		.header .admin-dropdown-menu .ssss{
			border-right: 0!important;
		}
		
		.header .admin-dropdown-menu .admin-features-item{
			width: 163px;
		}
		
		.sticky-wrapper{
			margin-bottom: 20px;
		}
		.sticky-wrapper.is-sticky #subnav{
			z-index: 20 !important;
		}

		/*table and pagination color*/

        .table-head-purple thead th {
            background-color: #707070 !important;
            border-color: #707070 !important;
            color: #fff;
        }

        .table-border-purple, .table-border-purple th {
            border-color: #707070 !important; 
        }

        .pagination .active>a, .pagination .active>a:focus, .pagination .active>a:hover, .pagination .active>span, .pagination .active>span:focus, .pagination .active>span:hover, .pagination .page-item.active .page-link {
            background-color: #707070 !important;
            border-color: #707070 !important;
            color: #fff;
        }

        /*table and pagination color ends*/
        
	</style>
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
           
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                    </li>
                   
                </ul>
				<div >
					<a href="{{ route('bar.home') }}"><img src="{{asset('public/assets/logo.png') }}" width='40'> <span style='font-size: 18px; margin-left: 4px; position: relative; top: 5px;' class='text-danger'>Strawberry Pop</span></a>
				</div>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar" >
                    @if(Auth::user()->photo == '')
					   <?php
							Auth::user()->photo = 'default.png';
						?>
				   @endif
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <span><b>{{ Auth::user()->name }}</b></span>
                            <img height='36' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                        </a>
                        <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu" >
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-header bg-danger" >
                                <div class="admin-avatar">
                                   <img height='88' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                                </div>
                                <div>
                                    <h5 class="font-strong text-white">{{ Auth::user()->name }}</h5>
									<p class='text-white'>Prep Admin</p>
									
                                    
                                </div>
                            </div>
                            <div class="admin-menu-features">
                              
                                <a class="admin-features-item" href="{{ route('bar.setting') }}"><i class="ti-settings"></i>
                                    <span>SETTINGS</span>
                                </a>
								
								<a class="admin-features-item ssss" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ti-shift-right"></i>
                                    <span>Logout</span>
                                </a>
								
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
                            </div>
							
                        </div>
                    </li>
                   
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
		
		<nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <ul class="side-menu">
                    <li class="{{ Route::is('bar.home') ? 'active' : '' }}">
                        <a href="{{ route('bar.home') }}"><i class="sidebar-item-icon ti-home"></i>
                            <span class="nav-label">Orders</span>
                        </a>
                    </li>
					
					<li class="{{ (request()->is('bar/cashup*')) ? 'active' : '' }}">
                        <a href="{{ route('bar.cashups') }}"><i class="sidebar-item-icon ti-location-pin"></i>
                            <span class="nav-label">Cashups</span>
                        </a>
						
						<!-- <div class="nav-2-level">
                            <ul>
								<li>
                                    <a href="{{ route('bar.orders') }}">Orders</a>
                                </li>
                                <li>
                                    <a href="{{ route('bar.cashups') }}">Cashups</a>
                                </li>
                                <li>
                                    <a href="{{ route('bar.tables') }}">Tables</a>
                                </li>
                            </ul>
                        </div> -->
                    </li>
					
					<li class="{{ Route::is('bar.setting') ? 'active' : '' }}">
                        <a href="{{ route('bar.setting') }}"><i class="sidebar-item-icon ti-settings"></i>
                            <span class="nav-label">Settings</span>
                        </a>
                    </li>
					
					<li>
                        <a onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();" href="{{ route('logout') }}"><i class="sidebar-item-icon ti-power-off"></i>
                            <span class="nav-label">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
			
			
			@section('page_content')
			@show
			
            <footer class="page-footer">
			<div class="font-13">2019 © All rights reserved by <b class='text-danger'>Strawberry Pop</b>.</div>
			<div>
				Made with <img style="width: 24px;" src="{{ asset('public/assets/heart.gif') }}"> by <a class='text-danger' target='_blank' href='https://www.arcticedgeapps.co.za/'><b>Arctic Edge Apps</b></a>.
			</div>
			<!--<div class="to-top"><i class="fa fa-angle-double-up"></i></div>-->
		</footer>
        </div>
    </div>
    
   
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    
    <!-- CORE PLUGINS-->
    <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-idletimer/dist/idle-timer.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- PAGE LEVEL PLUGINS-->
	@section('page_plugin_js')
	@show
	
	
    {{--<script src="{{ asset('public/assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/summernote/dist/summernote.min.js') }}"></script>
	--}}
    <!-- CORE SCRIPTS-->
	<script src="{{ asset('public/assets/vendors/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('public/assets/js/app.min.js') }}"></script>
    <!-- PAGE LEVEL SCRIPTS-->
	@section('page_js')
	@show
{{--	
<script>
	
	$(document).ready(function() {
	  $('.summernote').summernote({
		  height: "200px",
		  toolbar: [
			// [groupName, [list of button]]
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
		  ]
	  });
	});
    
</script>
	
--}}
<script>
$(function() {
	$('#datatable').DataTable({
		pageLength: 10,
		fixedHeader: true,
		responsive: true,
		"sDom": 'rtip',
		columnDefs: [{
			targets: 'no-sort',
			orderable: false
		}]
	});
	
	$('#datatable2').DataTable({
		pageLength: 10,
		fixedHeader: true,
		responsive: true,
		"sDom": 'rtip',
		columnDefs: [{
			targets: 'no-sort',
			orderable: false
		}]
	});

	var table = $('#datatable').DataTable();
	var table2 = $('#datatable2').DataTable();
	
	$('#key-search').on('keyup', function() {
		table.search(this.value).draw();
	});
	$('#type-filter').on('change', function() {
		table.column(3).search($(this).val()).draw();
	});
});
</script>


<script>
	
	@if(session('success'))
		Command: toastr['success']("{{ session('success') }}", 'Success');
	@endif
	
	@if(session('error'))
		Command: toastr['error']("{{ session('error') }}", 'Error');
	@endif
	
	toastr.options = {
	  'closeButton': true,
	  'debug': false,
	  'newestOnTop': false,
	  'progressBar': false,
	  'positionClass': 'toast-top-right',
	  'preventDuplicates': false,
	  'onclick': null,
	  'showDuration': '300',
	  'hideDuration': '1000',
	  'timeOut': '5000',
	  'extendedTimeOut': '1000',
	  'showEasing': 'swing',
	  'hideEasing': 'linear',
	  'showMethod': 'fadeIn',
	  'hideMethod': 'fadeOut'
	}
	
	
</script>

<script type="text/javascript" src="{{ asset('public/js/jquery.sticky.js') }}"></script>
<script>
$(window).on('load', function(){
  $("#subnav").sticky({
	  topSpacing: 66,
	  
	  });
  
});
</script>

</body>

</html>