<div id='subnav' class="ibox p-4">	
	<ul class="nav nav-tabs tabs-line tabs-line-pink">
		
		
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.staff') ? 'active' : '' }} {{ Route::is('venue.staff.create') ? 'active' : '' }} {{ Route::is('venue.staff.edit') ? 'active' : '' }}" href="{{ route('venue.staff') }}" >Staff</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.information') ? 'active' : '' }}" href="{{ route('venue.information') }}" >Information</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.settings') ? 'active' : '' }}" href="{{ route('venue.settings') }}" >Settings</a>
		</li>
		
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.products') ? 'active' : '' }} {{ Route::is('venue.product.create') ? 'active' : '' }} {{ Route::is('venue.product.edit') ? 'active' : '' }}" href="{{ route('venue.products') }}" >Products</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.ingredients') ? 'active' : '' }} {{ Route::is('venue.ingredient.create') ? 'active' : '' }} {{ Route::is('venue.ingredient.edit') ? 'active' : '' }} {{ Route::is('venue.unit.create') ? 'active' : '' }} {{ Route::is('venue.unit.edit') ? 'active' : '' }}" href="{{ route('venue.ingredients') }}" >Ingredients & Units</a>
		</li>

		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.categories') ? 'active' : '' }} {{ Route::is('venue.category.create') ? 'active' : '' }} {{ Route::is('venue.category.edit') ? 'active' : '' }}" href="{{ route('venue.categories') }}" >Categories</a>
		</li>

		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.menus') ? 'active' : '' }} {{ Route::is('venue.menu.create') ? 'active' : '' }} {{ Route::is('venue.menu.edit') ? 'active' : '' }}" href="{{ route('venue.menus') }}" >Menus</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.tables') ? 'active' : '' }} {{ Route::is('venue.table.create') ? 'active' : '' }} {{ Route::is('venue.table.edit') ? 'active' : '' }}" href="{{ route('venue.tables') }}" >Tables</a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link {{ Route::is('venue.bars') ? 'active' : '' }} {{ Route::is('venue.bar.create') ? 'active' : '' }} {{ Route::is('venue.bar.edit') ? 'active' : '' }}" href="{{ route('venue.bars') }}" >Prep Area</a>
		</li>
		
		
	</ul>	
</div>