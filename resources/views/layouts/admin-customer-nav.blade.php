@php
	$currency = DB::table('settings')->where('variable', 'currency')->first();
	if($currency){
		$currency = $currency->value;
	}else{
		$currency = '';
	}									
@endphp

<div class="ibox flex-1">
	<div class="ibox-body">
		<div class="flexbox">
			<div class="flexbox-b">
				<div class="ml-5 mr-5">
					@php
						if($customer->photo == ''){
							$customer->photo = 'default.png';
						}
						
						$order = DB::table('orders')->where('customer_id', $customer->id)->orderBy('id', 'desc')->first();
						$lifetime_spent = 0;
						$transactions = DB::table('transactions')->where('customer_id', $customer->id)->get();
						foreach($transactions as $transaction){
							$order_status = DB::table('orders')->where('id', $transaction->order_id)->where('order_status', 'Completed')->first();
							if($order_status){
								$lifetime_spent += $transaction->amount;
							}
						}
						
						$total_orders = DB::table('orders')->where('customer_id', $customer->id)->count('id');
						
						
					@endphp
					<img class="img-circle" src="{{ asset('public/uploads/users/' . $customer->photo) }}" alt="image" width="110" />
				</div>
				<div>
					<h4>{{ $customer->name }}</h4>
					<div class='text-muted'>
						Last Visited: @if(!$order) N/A @else {{ \Carbon\Carbon::parse($order->created_at)->format('d M Y') }} @endif
					</div>
					<div class='text-muted'>
						Member Since: {{ date('d M Y', strtotime($customer->created_at)) }}
					</div>
				</div>
			</div>
			<div class="d-inline-flex">
				<div class="px-4 text-center">
					<div class="text-muted font-13">Total Orders</div>
					<div class="h2 mt-2">{{ number_format($total_orders) }}</div>
				</div>
				<div class="px-4 text-center">
					<div class="text-muted font-13">Amount Spent</div>
					<div class="h2 mt-2">{{ $currency }} {{ number_format($lifetime_spent, 2) }}</div>
				</div>
			</div>
		</div>
	</div>
	
	<ul class="nav nav-tabs tabs-line m-0 pl-5 pr-3 tabs-line-danger">
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.customer.overview') ? 'active' : '' }}" href="{{ route('admin.customer.overview', $customer->id) }}">Overview</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.customer.orders') ? 'active' : '' }}" href="{{ route('admin.customer.orders', $customer->id) }}">Orders</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.customer.reviews') ? 'active' : '' }}" href="{{ route('admin.customer.reviews', $customer->id) }}">Ratings</a>
		</li>
		<li class="nav-item">
			<a class="nav-link {{ Route::is('admin.customer.edit') ? 'active' : '' }}" href="{{ route('admin.customer.edit', $customer->id) }}">Edit</a>
		</li>
	</ul>
	
</div>