<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <!-- csrf token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Strawberry pop | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
	<link rel="icon" href="{{ asset('public/assets/logo.png') }}" type="image/png" sizes="16x16">

    <link href="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />

    <!-- PLUGINS STYLES-->
	@section('page_plugin_css')
	@show
	{{-- <link href="{{ asset('public/assets/vendors/morris.js/morris.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/assets/vendors/summernote/dist/summernote.css') }}" rel="stylesheet" /> --}}
    <!-- THEME STYLES-->
    <link href="{{ asset('public/assets/css/main.min2.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
	@section('page_css')
	@show
	<link href="{{ asset('public/assets/vendors/dataTables/datatables.min.css') }}" rel="stylesheet" />
	<style>
		table.dataTable{
			border-collapse: collapse !important;
		}
		
		table tr td{
			border: none !important;
			border-bottom: 1px solid #eee !important;
			border-right: 1px solid #eee !important;
		}
		
		.table_view_row .box{
			transition: 0.2s;
		}
		.table_view_row .box:hover{
			color: #000;
			transform: scale(1.05);
			
		}
		
		.header .admin-dropdown-menu .dropdown-arrow:after{
			background: #f75a5f;
		}
		
		.side-menu>li.active>a, .side-menu>li.active>a .arrow, .side-menu>li.active>a .sidebar-item-icon{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li.active>a:hover{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li:hover a{
			color: #fff;
			background: #f75a5f;
		}
		
		.side-menu>li:hover ul li a{
			color: #b4bcc8;
			background: #34495f;;
		}
		
		.side-menu>li:hover ul li a:hover{
			color: #fff;
		}
		
		
		
		.header .admin-dropdown-menu .admin-features-item:hover{
			color: #f75a5f;
		}
		
		/*.content-wrapper{
			min-height: 1130px;
		}*/
		
		.vene_name_box{
			position: absolute;
			top: 34px;
			right: 30px;
			font-size: 25px;
			font-weight: bold;
			text-transform: uppercase;
		}
		
		.header .admin-dropdown-menu .ssss{
			border-right: 0!important;
		}
		
		.header .admin-dropdown-menu .admin-features-item{
			width: 163px;
		}
		
		
		.sticky-wrapper{
			margin-bottom: 20px;
		}
		.sticky-wrapper.is-sticky #subnav{
			z-index: 20 !important;
		}

        /*table and pagination color*/

        .table-head-purple thead th {
            background-color: #707070 !important;
            border-color: #707070 !important;
            color: #fff;
        }

        .table-border-purple, .table-border-purple th {
            border-color: #707070 !important; 
        }

        .pagination .active>a, .pagination .active>a:focus, .pagination .active>a:hover, .pagination .active>span, .pagination .active>span:focus, .pagination .active>span:hover, .pagination .page-item.active .page-link {
            background-color: #707070 !important;
            border-color: #707070 !important;
            color: #fff;
        }

        /*table and pagination color ends*/
       
	</style>
</head>

<body class="fixed-navbar">

    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
           
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler" href="javascript:;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                    </li>
					
                   
                </ul>
				<div>
					<a href="{{ route('admin.home') }}"><img src="{{ asset('public/assets/logo.png') }}" width='40'> <span style='font-size: 18px; margin-left: 4px; position: relative; top: 5px;' class='text-danger'>Strawberry Pop</span></a>
				</div>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar" >
                   @if(Auth::user()->photo == '')
					   <?php
							Auth::user()->photo = 'default.png';
						?>
				   @endif
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <span><b>{{ Auth::user()->name }}</b></span>
                            <img height='36' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                        </a>
                        <div class="dropdown-menu dropdown-arrow dropdown-menu-right admin-dropdown-menu" >
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-header bg-danger" >
                                <div class="admin-avatar">
                                    <img height='88' src="{{ asset('public/uploads/users/' . Auth::user()->photo) }}" alt="image" />
                                </div>
                                <div>
                                    <h5 class="font-strong text-white">{{ Auth::user()->name }}</h5>
									<p class='text-white'>Super Admin</p>
                                  
                                </div>
                            </div>
                            <div class="admin-menu-features">
                               
                                <a class="admin-features-item" href="{{ route('admin.setting') }}"><i class="ti-settings"></i>
                                    <span>SETTINGS</span>
                                </a>
								
								<a class="admin-features-item ssss" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="ti-shift-right"></i>
                                    <span>Logout</span>
                                </a>
								
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
								
                            </div>
							
                        </div>
                    </li>
                    
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
		
		<nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <ul class="side-menu">
                    <li class="{{ Route::is('admin.home') ? 'active' : '' }} {{ Route::is('admin.home.filter') ? 'active' : '' }}">
                        <a href="{{ route('admin.home') }}"><i class="sidebar-item-icon ti-home"></i>
                            <span class="nav-label">Dashboards</span>
                        </a>
                    </li>
					<li class="{{ Route::is('admin.transactions') ? 'active' : '' }} {{ Route::is('admin.transaction.filter') ? 'active' : '' }}">
                        <a href="{{ route('admin.transactions') }}"><i class="sidebar-item-icon ti-credit-card"></i>
                            <span class="nav-label">Transactions</span>
                        </a>
                    </li>
					<li class="{{ (request()->is('admin/venue*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.venues') }}"><i class="sidebar-item-icon ti-location-pin"></i>
                            <span class="nav-label">Venues</span>
                        </a>
                    </li>

                    @if (\Auth::user()->login_type == 'Mighty Super Admin')
                    <li class="{{ (request()->is('admin/public-holiday')) ? 'active' : '' }}">
                        <a href="{{ route('admin.holidays') }}"><i class="sidebar-item-icon ti-time"></i>
                            <span class="nav-label">Public Holidays</span>
                        </a>
                    </li>
                    @endif

					<li class="{{ (request()->is('admin/cms*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.cms') }}"><i class="sidebar-item-icon ti-layout-media-center-alt"></i>
                            <span class="nav-label">CMS</span>
                        </a>
                    </li>
					
					<li class="{{ (request()->is('admin/customer*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.customers') }}"><i class="sidebar-item-icon ti-user"></i>
                            <span class="nav-label">Customers</span>
                        </a>
                    </li>
					
					
					
					<li class="{{ (request()->is('admin/reservedfund*')) ? 'active' : '' }} {{ (request()->is('admin/reservedpayment*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.reservedfunds') }}"><i class="sidebar-item-icon ti-wallet"></i>
                            <span class="nav-label">Reserved Funds</span>
                        </a>
						<div class="nav-2-level">
                            <ul>
                                <li>
                                    <a href="{{ route('admin.reservedfunds') }}">Reserved Funds</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.reservedpayments') }}">Reserved Payments</a>
                                </li>
                            </ul>
                        </div>
						
                    </li>
					
					<li class="{{ (request()->is('admin/push-notification*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.push-notifications') }}"><i class="sidebar-item-icon ti-bell"></i>
                            <span class="nav-label">Push Notifications</span>
                        </a>
                    </li>
					
					<li class="{{ (request()->is('admin/report*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.reports') }}"><i class="sidebar-item-icon ti-bar-chart"></i>
                            <span class="nav-label">Reports</span>
                        </a>
                    </li>
					
					
					<li class="{{ (request()->is('admin/categor*')) ? 'active' : '' }} {{ (request()->is('admin/dietar*')) ? 'active' : '' }} {{ (request()->is('admin/facilit*')) ? 'active' : '' }} {{ (request()->is('admin/atmosphere*')) ? 'active' : '' }} {{ (request()->is('admin/cuisine*')) ? 'active' : '' }} {{ (request()->is('admin/ingredient*')) ? 'active' : '' }} {{ (request()->is('admin/unit*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.dietaries') }}"><i class="sidebar-item-icon ti-menu-alt"></i>
                            <span class="nav-label">Other</span>
                        </a>
						
						<div class="nav-2-level">
                            <ul>
                                <li>
                                    <a href="{{ route('admin.dietaries') }}">Dietaries</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.facilities') }}">Facilities</a>
                                </li>
								<li>
                                    <a href="{{ route('admin.atmospheres') }}">Atmosphere</a>
                                </li>
								<li>
                                    <a href="{{ route('admin.cuisines') }}">Cuisine</a>
                                </li>
								
                            </ul>
                        </div>
						
                    </li>
					
					
					
					<li class="{{ (request()->is('admin/support*')) ? 'active' : '' }}">
                        <a href="{{ route('admin.supports') }}"><i class="sidebar-item-icon ti-help"></i>
                            <span class="nav-label">Support</span>
                        </a>
                    </li>
					
					
					
					<li class="{{ Route::is('admin.refunds') ? 'active' : '' }} {{ Route::is('admin.refund.edit') ? 'active' : '' }}">
                        <a href="{{ route('admin.refunds') }}"><i class="sidebar-item-icon ti-loop"></i>
                            <span class="nav-label">Refunds</span>
                        </a>
                    </li>


                    <li class="{{ Route::is('admin.trail_logs') ? 'active' : '' }}">
                        <a href="{{ route('admin.trail_logs') }}"><i class="sidebar-item-icon ti-server"></i>
                            <span class="nav-label">Trail Logs</span>
                        </a>
                    </li>

                    @if(\Auth::user()->login_type == 'Mighty Super Admin')
                    
                    <li class="{{ Route::is('admin.featured') ? 'active' : '' }}">
                        <a href="{{ route('admin.featured') }}"><i class="sidebar-item-icon ti-star"></i>
                            <span class="nav-label">Featured</span>
                        </a>
                    </li>

                   
                    <li class="{{ Route::is('admin.manage.admins') ? 'active' : '' }}">
                        <a href="{{ route('admin.manage.admins') }}"><i class="sidebar-item-icon ti-user"></i>
                            <span class="nav-label">Manage Admins</span>
                        </a>
                    </li>
                    @endif
					
					<li class="{{ Route::is('admin.setting') ? 'active' : '' }}">
                        <a href="{{ route('admin.setting') }}"><i class="sidebar-item-icon ti-settings"></i>
                            <span class="nav-label">Settings</span>
                        </a>
                    </li>
					
					<li>
                        <a onclick="event.preventDefault();
										 document.getElementById('logout-form').submit();" href="{{ route('logout') }}"><i class="sidebar-item-icon ti-power-off"></i>
                            <span class="nav-label">Logout</span>
                        </a>
                    </li>
					
                </ul>
            </div>
        </nav>
        
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
			
			
			@section('page_content')
			@show
			
            <footer class="page-footer">
			<div class="font-13">2019 © All rights reserved by <b class='text-danger'>Strawberry Pop</b>.</div>
			<div>
				Made with <img style="width: 24px;" src="{{ asset('public/assets/heart.gif') }}"> by <a class='text-danger' target='_blank' href='https://www.arcticedgeapps.co.za/'><b>Arctic Edge Apps</b></a>.
			</div>
			<!--<div class="to-top"><i class="fa fa-angle-double-up"></i></div>-->
		</footer>
        </div>
    </div>

    <div class="modal fade update_user_password" id="update_user_password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">

        <form method="POST" action="{{route('admin.user-password-update')}}" id="user_password_update_form">
            {{csrf_field()}}
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span> -->
            </button>
          </div>
          <div class="modal-body">

                <div class="form-group mb-4 col-md-6" style="margin-left: 200px">
                    <img src="{{asset('public/uploads/users/update_password.jpg')}}" style="width: 400px;">
                </div>

                <p class="alert alert-danger confirm-password-message" style="display: none;"></p>

                <div class="form-group mb-4 col-md-12">
                    <label>Password</label>
                    <div class="input-group-icon input-group-icon-left">
                        <span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
                        <input class="form-control password" name='password' type="password" value="" required>
                    </div>
                </div>

                
                <div class="form-group mb-4 col-md-12">
                    <label>Confirm Password</label>
                    <div class="input-group-icon input-group-icon-left">
                        <span class="input-icon input-icon-left"><i class="ti-unlock"></i></span>
                        <input name='confirm_password' value="" class="form-control confirm_password" type="password" required>
                    </div>
                    
                </div>

          
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            <button type="submit" class="btn btn-primary">Change Password</button>
          </div>
        </div>
    </form>
      </div>
    </div>
    
   
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    
    <!-- CORE PLUGINS-->
    <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-idletimer/dist/idle-timer.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- PAGE LEVEL PLUGINS-->
	@section('page_plugin_js')
	@show
	
	
    {{--<script src="{{ asset('public/assets/vendors/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<script src="{{ asset('public/assets/vendors/summernote/dist/summernote.min.js') }}"></script>
	--}}
	<script src="{{ asset('public/assets/vendors/dataTables/datatables.min.js') }}"></script>
    <!-- CORE SCRIPTS-->
    <script src="{{ asset('public/assets/js/app.min.js') }}"></script>
    <!-- PAGE LEVEL SCRIPTS-->

    <script src="{{ asset('public/assets/vendors/smalot-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    
	@section('page_js')
	@show

      <!-- check for password days -->
    @php
    $date = \Carbon\Carbon::parse(\Auth::user()->last_password_update);
    $now = \Carbon\Carbon::now();

    $diff = $date->diffInDays($now);

    if ($diff > 90) {

        echo "<script>
            $('#update_user_password').modal('show');
        </script>";
    }
    @endphp

<script>

    $('#user_password_update_form').submit(function(e) {
        e.preventDefault();
            var _password = $('.password').val();
            var _password_confirm = $('.confirm_password').val();

                if(_password != '') {

                    if (_password.length < 8) {
                        $('.confirm-password-message').text('Password must be 8 chanracters long.');
                        $('.confirm-password-message').css('display', 'block');
                         return false;
                    } else {
                          $('.confirm-password-message').css('display', 'none');
                    }

                    if(_password != _password_confirm) {
                        $('.confirm-password-message').text('Password Mismatch.');
                        $('.confirm-password-message').css('display', 'block');
                        return false;
                    } else {
                          $('.confirm-password-message').css('display', 'none');
                    }
                    
                }

            // ajax call
            $.ajax({
                type: 'GET',
                data:{
                    user_password: _password
                },
                url: "{{ route('ajax.get-user-password') }}",
                success: function(res) {
                    console.log(res);
                    if (res['error'] != null) {
                        $('.confirm-password-message').text(res['error']);
                        $('.confirm-password-message').css('display', 'block');

                    } else {
                        e.currentTarget.submit();
                    }
                }
            });


    });

 // Table Availability Checker
$('.table-checker').click(function(){
    $('.table-check-modal').modal('show');
});

$(".table_datetime").datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    startDate:'+0d',
    autoclose: true,
    
});
 // Table Availability Checker Ends


$(function() {
	$('#datatable').DataTable({
		pageLength: 10,
		fixedHeader: true,
		responsive: true,
		"sDom": 'rtip',
		columnDefs: [{
			targets: 'no-sort',
			orderable: false
		}]
	});
	
	$('#datatable2').DataTable({
		pageLength: 10,
		fixedHeader: true,
		responsive: true,
		"sDom": 'rtip',
		columnDefs: [{
			targets: 'no-sort',
			orderable: false
		}]
	});

	var table = $('#datatable').DataTable();
	var table2 = $('#datatable2').DataTable();
	
	$('#key-search').on('keyup', function() {
		table.search(this.value).draw();
	});
	
	$('#key-search2').on('keyup', function() {
		table2.search(this.value).draw();
	});
	
	$('#type-filter').on('change', function() {
		table.column(3).search($(this).val()).draw();
	});
});
</script>

{{--    
<script>
    
    $(document).ready(function() {
      $('.summernote').summernote({
          height: "200px",
          toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
          ]
      });
    });
    
</script>
    --}}

<script>
	
	@if(session('success'))
		Command: toastr['success']("{{ session('success') }}", 'Success');
	@endif
	
	@if(session('error'))
		Command: toastr['error']("{{ session('error') }}", 'Error');
	@endif

	@if(session('errors'))
		Command: toastr['error']("{{ $errors->first() }}", 'Error');
	@endif
	
	toastr.options = {
	  'closeButton': true,
	  'debug': false,
	  'newestOnTop': false,
	  'progressBar': false,
	  'positionClass': 'toast-top-right',
	  'preventDuplicates': false,
	  'onclick': null,
	  'showDuration': '300',
	  'hideDuration': '1000',
	  'timeOut': '5000',
	  'extendedTimeOut': '1000',
	  'showEasing': 'swing',
	  'hideEasing': 'linear',
	  'showMethod': 'fadeIn',
	  'hideMethod': 'fadeOut'
	}
	
	
</script>
<script type="text/javascript" src="{{ asset('public/js/jquery.sticky.js') }}"></script>
<script>
$(window).on('load', function(){
  $("#subnav").sticky({
	  topSpacing: 66,
	  
	  });
});

</script>
</body>

</html>