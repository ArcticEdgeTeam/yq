<!-- venue toggle buttion -->
	<div class="ibox-tools pull-right">
		<span style='font-size: 11px;'><b>Venue Open</b></span>
		<br />
		<label class="ui-switch switch-icon switch-solid-danger switch-large">
			<input @if($venue->open == 1) checked @endif name='open' type="checkbox" class="venue-open-toggle">
			<span></span>
		</label>
	</div>
	<!-- Venue toggle buttion -->

	<div class="ibox-tools pull-right" style="padding-right: 15px; padding-top: 15px;">

    <button class="btn btn-danger btn-fix btn-animated from-left table-checker">
        <span class="visible-content">Add Booking</span>
        <span class="hidden-content">
          <span class="btn-icon"><i class="ti-check pr-0 pl-2"></i> Add</span>
        </span>
        </button>
	</div>

<!-- Modal -->
    <div class="modal fade table-check-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <form method="GET"
        @if (\Auth::user()->role == "admin")
          action="{{route('admin.venue.table-availability-checker', $venue->id)}}"
        @else
          action="{{route('venue.table-availability-checker')}}"
        @endif
         >
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Check Availability</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group mb-4 col-md-12">
                <label class="font-normal">Date & Time</label>
                <div class="input-group date table_datetime" >
                    <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                    <input autocomplete='off' required name='table_date_time' class="form-control">
                </div>
            </div>   
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Get Tables</button>
          </div>
        </div>
        </form>
      </div>
    </div>
    <!-- Table Checker Modal Ends -->