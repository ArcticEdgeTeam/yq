<form action="https://sandbox.payfast.co.za/eng/process" method="POST">
	<span style="font-size:15px;font-family:pt sans;">
	@php
	foreach ( $pfData as $key => $val )
	{
		if ( !empty( $val ) && $key != 'submit' && $key != 'passphrase' )
		{
			?>
			<input type="hidden" name="<?php echo $key?>" value="<?php echo $val?>"/>
			<?php
		}

	}
	@endphp
	
	<input type="hidden" name="signature" value="{{ $signature }}" />
	<input type='submit' name='action' value='Pay Now'>
	</span>
</form>