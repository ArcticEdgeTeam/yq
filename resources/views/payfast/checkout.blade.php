<!DOCTYPE html>
<html lang="en">
<head>
    <title>Purchase Ticket</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <div class="row" style="margin-top:10%">
        <div class="col-lg-12 text-center">
            <img src="app-assets/images/payfast.png">
        </div>
    </div>
    <br/>
    <div class="row">
        <h5>Payment Summary</h5>
        <br/>
        <table class="table table-bordered">

            <tbody>
            <tr>
                <td colspan="2">Amount</td>
                <td>R {{round($order_amount,2)}}</td>
            </tr>

            <tr>
                <td colspan="2">Admin Fee (R {{$amin_percent}})</td>
                <td>R {{round($admin_comission_amount,2)}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>Subtotal</b></td>
                <td>R {{round($amount_after_admin_comission,2)}}</td>
            </tr>
            <tr>
                <td colspan="2">Payfast fee</td>
                <td>R {{round($payfast_tax_amount,2)}}</td>
            </tr>
            <tr>
                <td colspan="2"><b>Total</b></td>
                <td>R {{round($final_amount_get_from_customer,2)}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- <form action="https://sandbox.payfast.co.za/eng/process" method="POST" style="margin-top:5%"> -->
    @php
        $testingMode = true;
        $pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
    @endphp
    <form action="https://{{$pfHost}}/eng/process" method="post" style="margin-top:5%">
  <span style="font-size:15px;font-family:pt sans;">

      {{--@foreach($data as $name=> $value)
           <input name="{{$value}}" type="hidden" value="{{$value}}" />
      @endforeach--}}
  <input type="hidden" name="merchant_id"  value="{{$data['merchant_id']}}">
  <input type="hidden" name="merchant_key" value="{{$data['merchant_key']}}">
  <input type="hidden" name="return_url"   value="{{$data['return_url']}}">
 <input type="hidden" name="cancel_url"    value="{{$data['cancel_url']}}">
 <input type="hidden" name="notify_url"    value="{{$data['notify_url']}}">
 <input type="hidden" name="name_first"    value="{{$data['name_first']}}">
 <input type="hidden" name="name_last"     value="{{$data['name_last']}}">
 <input type="hidden" name="email_address" value="{{$data['email_address']}}">
 <input type="hidden" name="m_payment_id"  value="{{$data['m_payment_id']}}">
 <input type="hidden" name="amount"        value="{{$data['amount']}}">
 <input type="hidden" name="item_name"     value="{{$data['item_name']}}">
 <input type="hidden" name="item_description" value="{{$data['item_description']}}">
 <input type="hidden" name="custom_int1"    value="{{$data['custom_int1']}}">
 <input type="hidden" name="custom_int2"    value="{{$data['custom_int2']}}">
 <input type="hidden" name="custom_int3"    value="{{$data['custom_int3']}}">
 <input type="hidden" name="custom_str4"    value="{{$data['custom_str4']}}">
 <input type="hidden" name="payment_method"  value="cc">
 <input type="hidden" name="signature"         value="{{$data['signature']}}">
 <input type="hidden" name="setup" value='{"split_payment":{"merchant_id":{{$data['merchant_id']}},"amount":{{round($admin_comission_amount,2)}}}}'>
  <!--  <input type="submit" class="btn" name="submit" value='Submit' /> -->
 <button type="submit" class="btn btn-primary btn-block">Go To Payfast</button>

        </span>
    </form>

</div>

</body>
</html>
