<!DOCTYPE html>
<html style="background: #f9f9f9;font-family: Arial;">
<head>
	<title>Forgot Password Notification</title>
</head>

<body style="background: #f9f9f9; font-family: Arial; padding: 40px 0;">
	
	
	<section>
		<div class='container' style="border: 1px solid #e5e5e5; width: 560px; background: #fff; margin: auto;padding: 30px;box-sizing: border-box;text-align: center;font-size: 14px;">
				
			<div class='row'>
				<div class='col'>
					<div class='logo_box'>
						<a href="{{ url(config('app.url')) }}">
						<img style="width: 80px;" src="{{ $message->embed(asset('public/assets/logo.png')) }}" alt='Strawberry Pop'/></a>
					</div>
				</div>
			</div>
			
			<div class='row'>
				<div class='col'>
					<div class='content_box' style="text-align: left; margin: 20px 0;line-height: 24px;">
						<h1 style="margin-bottom: 20px;margin-top: 0;text-align: center;">Hello!</h1>
						<p style="color: #666;margin-top: 10px;">
							You are receiving this email because we received a password reset request for your account.
						</p>
						
						<p style="color: #666;margin-top: 10px;">
							<a href="{{ url(config('app.url')).route('password.reset', $token, false) }}" style='font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1'>Reset Password</a>
						</p>
						
						<p style="color: #666;margin-top: 10px;">
							
							This password reset link will expire in 60 minutes.
							<br />
							If you did not request a password reset, no further action is required.
						</p>
						
						<div style='border-bottom: 1px solid #edeff2;'></div>
						
						<p style="color: #666;margin-top: 10px; overflow-wrap: break-word;">
						<small>
							If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: {{ url(config('app.url')) }}/password/reset/{{$token}}
						</small>
						</p>
						
						
					</div>
				</div>
			</div>
			
		</div>
		
		<footer style="margin-top: 10px;text-align: center;line-height: 20px;font-size: 12px;">
		{{-- <div class='row'>
				<div class='col'>
					<div class='social_icon_box'>
						<a href='https://www.facebook.com/jenny.toussaint.58910' target='_blank'><img style="width: 40px;" src="{{ $message->embed(public_path().'/img/facebook.png') }}" /></a>
						<a href='https://twitter.com/PASSNCLEXEXAM' target='_blank'><img style="width: 40px;" src="{{ $message->embed(public_path().'/img/twitter.png') }}" /></a>
						<a href='https://www.instagram.com/nclexnotesnow/' target='_blank'><img style="width: 40px;" src="{{ $message->embed(public_path().'/img/instagram.png') }}" /></a>
					</div>
				</div>
			</div> --}}
			
			<div class='row'>
				<div class='col'>
					<div class='copyright_box_data'>
						<p style="color: #666;margin-top: 10px; text-align: center;">
							&copy; 2020 Strawberry Pop.
						</p>
					</div>
				</div>
			</div>
		</footer>
		
	</section>

</body>

</html>