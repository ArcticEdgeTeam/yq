@php
    $total = 0;
    $currency = DB::table('settings')->where('variable', 'currency')->first();
    if($currency){
        $currency = $currency->value;
    }else{
        $currency = '';
    }
    
@endphp
<html lang="en">
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
  
            <table class="table table-bordered table-hover" id="datatable">
                <thead class="thead-default thead-lg">
                    <tr>
                    <td colspan="8">
                        <p style="text-align: center"> Cahups Report For {{$venue->name}} <br>
                            <span style="text-align: center">  @if ($start != '' && $end != '') From {{$start}} To {{ $end}} @else All Time Cashups Report @endif </span>
                        </p>

                    </td>  
                    </tr>
                    <tr>
                        <th></th>
                        <th>Invoice</th>
                        <th>Customer</th>
                        <th>Waiter</th>
                        <th>Date</th>
                        <th>Paid</th>
                        <th>Tip</th>
                        <th>Admin Comm</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>

                @if ($transactions->isEmpty())
                 <tr>
                    <td colspan="8"><p style="text-align: center"> No transactions found!</p></td>  
                </tr>
                @else
                @foreach($transactions as $transaction)
                    @php
                       
                        $order = DB::table('orders')->where('id', $transaction->order_id)->first();
                        $customer = DB::table('users')->where('id', $transaction->customer_id)->first();
                        $waiter = DB::table('users')->where('id', $transaction->waiter_id)->first();
                        
                        if($transaction->transaction_status != 'Refunded'){
                            $total += $transaction->amount + $transaction->waiter_tip;
                        }
                    @endphp
                    <tr>
                        <td></td>
                        <td>{{ $order->invoice_number }}</td>
                        <td> {{ $customer->name }}</td>
                        <td> {{ $waiter->name }}</td>
                        <td> {{ $transaction->created_at->format('d M Y') }}</td>
                        <td style='position: relative;'>{{ $currency }} {{ number_format($transaction->amount, 2) }} @if($transaction->transaction_status == 'Refunded') Refunded @endif</td>
                        <td>{{ $currency }} {{ number_format($transaction->waiter_tip, 2) }}</td>
                        <td>{{ $currency }} {{ number_format($transaction->admin_commission, 2) }}</td>
                        <td>{{ $currency }} {{ number_format($transaction->amount +  $transaction->waiter_tip, 2) }}</td>
                    </tr>
                @endforeach
                @endif
                    
                </tbody>
                
                <tfoot>
                    <tr>
                        <th colspan='8'>
                            <div class='text-right'>
                                <b style='margin-right: 50px;'>Total: {{ $currency }} {{ number_format($total, 2) }}</b>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>
            
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>