@php $counter++ @endphp
@if($child_category->categories)
    <option @if($category->category_id == $child_category->id) selected @endif value="{{ $child_category->id }}">
		@php for($i = 0; $i < $counter; $i++){ echo ' - '; } @endphp {{ $child_category->name }}
        @foreach ($child_category->categories as $childCategory)
			
			@php
				if($edited_id == $childCategory->id){
					continue;
				}
			@endphp
			
            @include('child_category', ['child_category' => $childCategory])
        @endforeach
    </option>
@endif