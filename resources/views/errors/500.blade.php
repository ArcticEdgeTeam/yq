<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>Strawberry pop | Dashboard</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('public/assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/themify-icons/css/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/animate.css/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/toastr/toastr.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('public/assets/vendors/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <!-- THEME STYLES-->
    <link href="{{ asset('public/assets/css/main.min.css') }}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
    <style>
        body {
            background-color: #fff;
            background-repeat: no-repeat;
            background-image: url({{ asset('public/assets/img/icons/server-error-2.svg') }});
            background-position: 80% 10px;
        }

        .error-content {
            max-width: 620px;
            margin: 200px auto 0;
        }

        .error-icon {
            height: 160px;
            width: 160px;
            background-image: url({{ asset('public/assets/img/icons/server-error.svg') }});
            background-size: cover;
            background-repeat: no-repeat;
            margin-right: 80px;
        }

        .error-code {
            font-size: 120px;
            color: #5c6bc0;
        }
    </style>
</head>

<body>
    <div class="error-content flexbox">
        <span class="error-icon"></span>
        <div class="flex-1">
            <h1 class="error-code text-danger">500</h1>
            <h3 class="font-strong">Internal Server Error</h3>
            <p class="mb-4">There is a problem with server.</p>
         
			
			@if(Auth::check())
			@if(Auth::user()->role == 'admin')
			<div>
                <a href="{{ route('admin.home')  }}" class="btn btn-gradient-peach btn-fix  text-white">
				<span class="btn-icon"><i class="la la-dashboard"></i>Go to Dashboard</span>
				</a>
            </div>
			@elseif(Auth::user()->role == 'venue')
			<div>
                <a href="{{ route('venue.home')  }}" class="btn btn-gradient-peach btn-fix  text-white">
				<span class="btn-icon"><i class="la la-dashboard"></i>Go to Dashboard</span>
				</a>
            </div>
			@elseif(Auth::user()->role == 'bar')
			<div>
                <a href="{{ route('bar.home')  }}" class="btn btn-gradient-peach btn-fix  text-white">
				<span class="btn-icon"><i class="la la-dashboard"></i>Go to Dashboard</span>
				</a>
            </div>
			@endif
			@else
			<div>
                <a href="{{ url('')  }}" class="btn btn-gradient-peach btn-fix  text-white">
				<span class="btn-icon"><i class="la la-dashboard"></i>Go Back</span>
				</a>
            </div>
			@endif
        </div>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- CORE PLUGINS-->
    <script src="{{ asset('public/assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-idletimer/dist/idle-timer.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('public/assets/vendors/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <!-- CORE SCRIPTS-->
    <script src="{{ asset('public/assets/js/app.min.js') }}"></script>
    <!-- PAGE LEVEL SCRIPTS-->
</body>

</html>