<!DOCTYPE html>
<html>
<head>
	<title>Error 404</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body style="background-image: url('{{ asset('public/svg/error-404.svg')}}'); background-repeat: no-repeat;background-size: cover;">
 
 		<header class="page header" >
         
	         <!-- Body Content -->
	         <div class="container" style="padding-top:270px;padding-bottom:45px;">
	               <div class="row">
	                  <div class="col-md-6">
	                    <!-- <h1 class="regular display-4 color-1 mb-4 banner-heading">404</h1>-->
	                      </div>
	               </div>
	         </div>
         </header>

	    <div class="content-404" >     
	      <div style="text-align: center;">
	        <h1 class="heading-404" style="color: #ff0000">Oooops!</h1>
	        <p style="font-size:24px;">The page you are looking for cannot be found.</p>
	       		

                @if (Auth::check() && Auth::user()->role == 'bar') 
					<a href="{{url('/bar')}}" class="btn btn-primary">Go to Dashboard</a>
				
				@elseif (Auth::check() && Auth::user()->role == 'venue') 
					<a href="{{url('/venue')}}" class="btn btn-primary">Go to Dashboard</a>

				@elseif (Auth::check() && Auth::user()->role == 'admin')
					<a href="{{url('/admin')}}" class="btn btn-primary">Go to Dashboard</a>

				@else
					<a href="{{url('/')}}" class="btn btn-primary">Go to Homepage</a>

				@endif
	        
	      </div>
	    </div>

</body>
</html>