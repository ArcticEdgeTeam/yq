<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_products', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->float('price');
			$table->integer('product_id')->unsigned();
			$table->integer('bar_id')->unsigned();
			$table->integer('venue_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_products');
    }
}
