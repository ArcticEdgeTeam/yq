<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->integer('venue_id')->unsigned();
			$table->integer('order_id')->unsigned();
			$table->integer('waiter_id')->unsigned();
			$table->float('food_rating')->unsigned();
			$table->float('service_rating')->unsigned();
			$table->float('value_rating')->unsigned();
			$table->float('venue_rating')->unsigned();
			$table->text('comment')->nullable();
			$table->float('average_rating')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
