<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('order_number');
			$table->integer('venue_id')->unsigned();
			$table->integer('customer_id')->unsigned();
			$table->integer('waiter_id')->unsigned();
			$table->integer('table_id')->unsigned();
			$table->float('total_amount');
			$table->float('admin_commission')->nullable();
			$table->float('waiter_tip')->nullable();
			$table->string('order_status')->nullable();
			$table->string('payment_type')->nullable();
			$table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
