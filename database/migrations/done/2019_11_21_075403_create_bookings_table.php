<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->integer('venue_id')->unsigned();
			$table->integer('table_id')->nullable();
			$table->timestamp('datetime')->nullable();
			$table->integer('seating')->nullable();
			$table->string('smooking')->nullable();
			$table->string('area')->nullable();
			$table->text('message')->nullable();
			$table->string('booking_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
