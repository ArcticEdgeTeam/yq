<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->increments('id');
			$table->string('company_name');
			$table->string('contact_person')->nullable();
			$table->string('vat')->nullable();
			$table->string('registration_number')->nullable();
			$table->string('banner')->nullable();
			$table->string('prefered_way')->nullable();
			$table->string('paypal_id')->nullable();
			$table->string('bank_name')->nullable();
			$table->string('bank_account_type')->nullable();
			$table->string('iban')->nullable();
			$table->integer('dietary_id')->unsigned;
			$table->boolean('is_closed_weekday')->nullable();
			$table->boolean('is_closed_saturday')->nullable();
			$table->boolean('is_closed_sunday')->nullable();
			$table->boolean('is_closed_public')->nullable();
			$table->timestamp('mon_fri_from')->nullable();
			$table->timestamp('mon_fri_to')->nullable();
			$table->timestamp('saturday_from')->nullable();
			$table->timestamp('saturday_to')->nullable();
			$table->timestamp('sunday_from')->nullable();
			$table->timestamp('sunday_to')->nullable();
			$table->timestamp('public_holiday_from')->nullable();
			$table->timestamp('public_holiday_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venues');
    }
}
