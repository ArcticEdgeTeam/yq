<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartProductVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_product_variables', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('variable_id');
			$table->string('type');
			$table->string('name');
			$table->integer('quantity');
			$table->integer('cart_product_id');
			$table->float('price');
			$table->integer('order_by');
			$table->string('order_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_product_variables');
    }
}
