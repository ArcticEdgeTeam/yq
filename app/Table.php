<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    //
    public function bookings()
    {
        return $this->belongsToMany('App\Booking', 'booking_tables', 'booking_id', 'table_id');
    }
}
