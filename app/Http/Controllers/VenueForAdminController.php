<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dietary;
use App\Facility;
use App\Venue;
use App\FacilityVenue;
use App\User;
use Storage;
use App\Cuisine;
use App\Order;
use App\Booking;
use App\View;
use App\Atmosphere;
use App\VenueDietary;
use App\TradingHour;
use Carbon\Carbon;
use App\OrderedProduct;
use App\BookingDay;
use Hash;
use DB;
use App\Review;
use App\Bar;
use App\Product;
use App\Table;
use App\Transaction;
use Image;
use Auth;
use App\TrailLog;
use App\Featured;

class VenueForAdminController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
        $this->middleware('admin');
    }

	public function index(Request $request){
		$counter = 1;

		$getVenues = Venue::query();
		$getVenues = $getVenues->select('venues.*', 'users.id as admin_id', 'users.name as admin_name')
							->leftjoin('users', 'users.id', '=', 'venues.created_by');

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getVenues = $getVenues->whereBetween('venues.created_at', [$start, $end]);

		}

		if($request->user != '') {
			$getVenues = $getVenues->where('venues.created_by', $request->user);
		}

		if($request->venue != '') {
			$getVenues = $getVenues->where('venues.id', $request->venue);
		}

		if($request->status != '') {
			$getVenues = $getVenues->where('venues.status', $request->status);
		}

		$getVenues = $getVenues->orderBy('venues.id', 'DESC')
							->paginate(15);

		// get featured
		$getFeatured = Featured::first();

		// get user
		$users = User::where('status', 1)
						->where('role', 'admin')
						->get();

		//get venues
		$venues = Venue::where('status', 1)->get();

		return view('admin.venues', compact('getVenues', 'counter', 'getFeatured', 'users', 'venues'));
	}

	public function create() {
		$cuisines = Cuisine::where('status', 1)->get();
		$atmospheres = Atmosphere::where('status', 1)->get();
		$dietaries = Dietary::where('status', 1)->get();
		$facilities = Facility::where('status', 1)->get();

		// get booking days
		$getDays = BookingDay::get();
		return view('admin.venue-create', compact('cuisines', 'atmospheres', 'dietaries', 'facilities', 'getDays'));
	}

	public function save(Request $request) {
		// get admin threshold  value
		$userThreshold = User::where('login_type', 'Mighty Super Admin')->first();

		$venue = new Venue;
		$venue->name = $request->name;
		$venue->company_id = $request->company_id;
		$venue->contact_email = $request->contact_email;
		$venue->contact_number = $request->contact_number;
		$venue->vat = $request->vat;
		$venue->vat_name = $request->vat_name;
		$venue->trading_name = $request->trading_name;
		$venue->about = $request->about;
		$venue->registration_number = $request->registration_number;
		$venue->address = $request->address;
		$venue->tagline = $request->tagline;
		$venue->cuisine_id = $request->cuisine_id;
		$venue->atmosphere_id = $request->atmosphere_id;
		$venue->avg_cost = str_replace(",", "", $request->avg_cost);

		//save lat and long for venue
		$venue->latitude = $request->latitude;
		$venue->longitude = $request->longitude;
		$venue->location_id = $request->location_id;

		// calculate commission
		$commissionThreshold = ($userThreshold->percentage / 100) * $userThreshold->commission;

		$venue->commission_threshold_limit	 = $userThreshold->commission - $commissionThreshold;

		$venue->admin_commission_percentage = $userThreshold->percentage;
		$venue->status = 1;
		$venue->notice = $request->notice;
		$venue->postal_address = $request->postal_address;

		if(isset($request->booking)) {
			$venue->booking = 1;
			$venue->average_booking = $request->average_booking;
		} else {
			$venue->booking = 0;
			$venue->average_booking = '';
		}

		if(isset($request->booking_auto_accept)) {
			$venue->booking_auto_accept = 1;
		} else {
			$venue->booking_auto_accept = 0;
		}

		if($request->file('banner')){
			SaveBannerAllSizes($request, 'banners/');
			//$request->file('banner')->store('banners/', ['disk' => 'public']);
			$venue->banner = $request->banner->hashName();
		}


		//social Links
		$venue->fb_link = $request->fb_link;
		$venue->insta_link = $request->insta_link;
		$venue->twitter_link = $request->twitter_link;
		$venue->web_link = $request->web_link;

		$venue->created_by = Auth::user()->id;


		$days = $request->days;
		$from = $request->from;
		$to = $request->to;
		$isclosed = $request->isclosed;


		if($venue->save()){

			foreach($days as $key => $day) {

				$TradingHour = new TradingHour;
				$TradingHour->booking_id = $key;
				$TradingHour->from = $from[$key];
				$TradingHour->to = $to[$key];
				$TradingHour->isclosed = $isclosed[$key];
				$TradingHour->venue_id = $venue->id;
				$TradingHour->save();
			}

			if(is_array($request->facilities)){
				foreach($request->facilities as $facility){
					$FacilityVenue = new FacilityVenue;
					$FacilityVenue->facility_id = $facility;
					$FacilityVenue->venue_id = $venue->id;
					$FacilityVenue->save();
				}
			}

			if(is_array($request->dietaries)){
				foreach($request->dietaries as $dietary){
					$VenueDietary = new VenueDietary;
					$VenueDietary->dietary_id = $dietary;
					$VenueDietary->venue_id = $venue->id;
					$VenueDietary->save();
				}
			}

			$user = new User;
			$user->name = $request->user_name;
			$user->phone = $request->phone;
			$user->email = $request->email;
			$user->role = 'venue';
			$user->login_type = 'Mighty Super Venue Admin';
			$user->venue_id = $venue->id;
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();
			$user->save();


			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = 'Venue Added';
			$venueLog->event_message = Auth::user()->name.' created new venue '.$venue->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return redirect(route('admin.venues'))->with('success', 'Record has been added.');
		} else{
			return redirect(route('admin.venues'))->with('error', 'Record has not been added.');
		}
	}



	public function update(Request $request, $id) {

		$venue = Venue::find($id);
		$venue->name = $request->name;
		$venue->contact_email = $request->contact_email;
		$venue->contact_number = $request->contact_number;
		$venue->vat = $request->vat;
		$venue->registration_number = $request->registration_number;
		$venue->address = $request->address;
		$venue->tagline = $request->tagline;
		$venue->vat_name = $request->vat_name;
		$venue->trading_name = $request->trading_name;
		$venue->about = $request->about;
		$venue->cuisine_id = $request->cuisine_id;
		$venue->atmosphere_id = $request->atmosphere_id;
		$venue->avg_cost = str_replace(",", "", $request->avg_cost);

		//save lat and long for venue
		$venue->latitude = $request->latitude;
		$venue->longitude = $request->longitude;
		$venue->location_id = $request->location_id;

		$venue->notice = $request->notice;
		$venue->postal_address = $request->postal_address;

		if(isset($request->open)){
			$venue->open = 1;
		}else{
			$venue->open = 0;
		}

		if($request->file('banner')){
			UpdateBannerAllSizes($request, 'banners/', $venue->banner);
			//Storage::disk('public')->delete('banners/' . $venue->banner);
			//$request->file('banner')->store('banners/', ['disk' => 'public']);
			$venue->banner = $request->banner->hashName();
		}

		$days = $request->days;
		$from = $request->from;
		$to = $request->to;
		$isclosed = $request->isclosed;

		if($venue->save()){

			// delete previous trading hours
			$deletetradingHours = TradingHour::where('venue_id',  $id)->delete();

			foreach($days as $key => $day) {

				$TradingHour = new TradingHour;
				$TradingHour->booking_id = $key;
				$TradingHour->from = $from[$key];
				$TradingHour->to = $to[$key];
				$TradingHour->isclosed = $isclosed[$key];
				$TradingHour->venue_id = $venue->id;
				$TradingHour->save();
			}

			$facilities = FacilityVenue::where('venue_id', $id)->delete();
			$dietaries = VenueDietary::where('venue_id', $id)->delete();

			if(is_array($request->facilities)){
				foreach($request->facilities as $facility){
					$FacilityVenue = new FacilityVenue;
					$FacilityVenue->facility_id = $facility;
					$FacilityVenue->venue_id = $venue->id;
					$FacilityVenue->save();
				}
			}

			if(is_array($request->dietaries)){
				foreach($request->dietaries as $dietary){
					$VenueDietary = new VenueDietary;
					$VenueDietary->dietary_id = $dietary;
					$VenueDietary->venue_id = $venue->id;
					$VenueDietary->save();
				}
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = 'Venue Information Updated';
			$venueLog->event_message = Auth::user()->name.' updated venue '.$venue->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return redirect(route('admin.venue.information', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.venue.information', $id))->with('error', 'Record has not been updated.');
		}
	}

	public function venueStatusUpdate(Request $request) {
		if ($request->ajax()) {
			$venueStatus = '';
			$venue = Venue::find($request->id);
			$venue->open = $request->status;
			$venue->save();

			if ($request->status == 0) {
				$venueType = 'Venue Closed';
				$venueStatus = 'close';
			} else {
				$venueType = 'Venue Opened';
				$venueStatus = 'open';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = $venueType;
			$venueLog->event_message = Auth::user()->name.' changed venue '.$venue->name.' status to '.$venueStatus.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return  'status update';
		}
	}

	public function trailLog(Request $request) {
		// get logs
		$getLogs = TrailLog::query();
		$getLogs = $getLogs->select('trail_logs.*', 'venues.name as venue_name')
							->leftjoin('venues', 'venues.id', '=', 'trail_logs.venue_id')
							->where('trail_logs.user_role', 'admin');

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getLogs = $getLogs->whereBetween('trail_logs.created_at', [$start, $end]);

		}

		if($request->user != '') {
			$getLogs = $getLogs->where('trail_logs.user_id', $request->user);
		}

		if($request->venue != '') {
			$getLogs = $getLogs->where('trail_logs.venue_id', $request->venue);
		}

		$getLogs = $getLogs->orderBy('trail_logs.id', 'DESC')
							->paginate(15);

		// get user
		$users = User::where('status', 1)
						->where('role', 'admin')
						->get();

		//get venues
		$venues = Venue::where('status', 1)->get();

		//dd($getLogs);
		return view('admin.trail_logs', compact('getLogs', 'users', 'venues'));
	}

	public function getProductID(Request $request) {
		if ($request->ajax()) {
			$message = [];

			// for edit
			if($request->type == 'edit') {
				$getProduct = Product::where('product_id', $request->product_id)
						->where('id', '!=',  $request->id)
						->where('venue_id', $request->venue_id)
						->first();
			} else {
				$getProduct = Product::where('product_id', $request->product_id)
						->where('venue_id', $request->id)
						->first();
			}

			if($getProduct != null) {
				$message['error'] = 'true';
			} else {
				$message['error'] = 'false';
			}

		}	return response($message);
	}

	public function publicHoliday(Request $request) {
		$holiday = BookingDay::find(8);
		return view('admin.public_holidays', compact ('holiday'));
	}

	public function savePublicHoliday(Request $request) {

		$holiday = BookingDay::find(8);
		$holiday->day = $request->holiday_title;
		$holiday->date = json_encode($request->date);
		$holiday->save();

		// insert log
		$venueLog = new TrailLog;
		$venueLog->event = 'Public Holidays';
		$venueLog->event_id = $holiday->id;
		$venueLog->event_type = 'Public Holidays Updated';
		$venueLog->event_message = Auth::user()->name.' updated public holidays.';
		$venueLog->user_id = Auth::user()->id;
		$venueLog->user_name = Auth::user()->name;
		$venueLog->user_role = Auth::user()->role;
		$venueLog->venue_id = 0;
		$venueLog->save();

		return redirect(route('admin.holidays'))->with('success', 'Record has been updated.');

	}



	public function overview($id) {
		$venue = Venue::find($id);
		$current_orders = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->count();
		$first_order = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'asc')->first();
		$pending_orders = Order::where('venue_id', $id)->where('order_status', 'Ordered')->count();
		$last_order = Order::where('venue_id', $id)->where('order_status', 'Ordered')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->first();

		$orders_today = Order::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();

		$sales_today = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->sales;
		if($sales_today == ''){
			$sales_today = 0;
		}

		$earnings_today = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as admin_commission'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->admin_commission;
		if($earnings_today == ''){
			$earnings_today = 0;
		}


		$customers_today = User::where('venue_id', $id)->where('role', 'customer')->whereDate('created_at', Carbon::today())->count();
		$bookings_today = Booking::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		$views_today = View::where('module', 'venue')->where('module_id', $id)->whereDate('created_at', Carbon::today())->count();

		$orders_lifetime = Order::where('venue_id', $id)->count();

		$sales_lifetime = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;

		if($sales_lifetime == ''){
			$sales_lifetime = 0;
		}

		$earnings_lifetime = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;

		if($earnings_lifetime == ''){
			$earnings_lifetime = 0;
		}


		$customers_lifetime = User::where('venue_id', $id)->where('role', 'customer')->count();
		$bookings_lifetime = Booking::where('venue_id', $id)->count();
		$views_lifetime = View::where('module', 'venue')->where('module_id', $id)->count();


		$bar_of_the_day = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereDate('created_at', Carbon::today())
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();

		Carbon::setWeekStartsAt(Carbon::SUNDAY);
		Carbon::setWeekEndsAt(Carbon::SATURDAY);

		$bar_of_the_week = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();

		$bar_of_the_month = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();

		$waiter_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();

		$waiter_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();

		$waiter_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();

		$product_of_the_day = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();

		$product_of_the_week = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();

		$product_of_the_month = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();

		$table_of_the_day = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();

		$table_of_the_week = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();

		$table_of_the_month = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();

		$rating_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->first();


		$rating_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->first();

		$rating_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->first();

		$total_bars = Bar::where('venue_id', $id)->count();
		$active_bars = Bar::where('venue_id', $id)->where('status', 1)->count();

		$total_products = Product::where('venue_id', $id)->count();
		$active_products = Product::where('venue_id', $id)->where('status', 1)->count();

		$total_tables = Table::where('venue_id', $id)->count();
		$active_tables = Table::where('venue_id', $id)->where('status', 1)->count();

		$total_satff = User::where('venue_id', $id)->count();
		$active_staff = User::where('venue_id', $id)->where('status', 1)->count();

		$current_year = date('Y');

		$jan_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 1)
							->first();
		if($jan_rating->average_rating){
			$jan_rating = $jan_rating->average_rating / $jan_rating->total_records;
		}else{
			$jan_rating = 0;
		}


		$feb_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 2)
							->first();
		if($feb_rating->average_rating){
			$feb_rating = $feb_rating->average_rating / $feb_rating->total_records;
		}else{
			$feb_rating = 0;
		}


		$mar_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 3)
							->first();

		if($mar_rating->average_rating){
			$mar_rating = $mar_rating->average_rating / $mar_rating->total_records;
		}else{
			$mar_rating = 0;
		}

		$apr_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 4)
							->first();

		if($apr_rating->average_rating){
			$apr_rating = $apr_rating->average_rating / $apr_rating->total_records;
		}else{
			$apr_rating = 0;
		}

		$may_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 5)
							->first();

		if($may_rating->average_rating){
			$may_rating = $may_rating->average_rating / $may_rating->total_records;
		}else{
			$may_rating = 0;
		}

		$jun_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 6)
							->first();

		if($jun_rating->average_rating){
			$jun_rating = $jun_rating->average_rating / $jun_rating->total_records;
		}else{
			$jun_rating = 0;
		}

		$jul_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 7)
							->first();

		if($jul_rating->average_rating){
			$jul_rating = $jul_rating->average_rating / $jul_rating->total_records;
		}else{
			$jul_rating = 0;
		}

		$aug_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 8)
							->first();

		if($aug_rating->average_rating){
			$aug_rating = $aug_rating->average_rating / $aug_rating->total_records;
		}else{
			$aug_rating = 0;
		}

		$sep_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 9)
							->first();

		if($sep_rating->average_rating){
			$sep_rating = $sep_rating->average_rating / $sep_rating->total_records;
		}else{
			$sep_rating = 0;
		}

		$oct_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 10)
							->first();

		if($oct_rating->average_rating){
			$oct_rating = $oct_rating->average_rating / $oct_rating->total_records;
		}else{
			$oct_rating = 0;
		}

		$nov_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 11)
							->first();

		if($nov_rating->average_rating){
			$nov_rating = $nov_rating->average_rating / $nov_rating->total_records;
		}else{
			$nov_rating = 0;
		}

		$dec_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 12)
							->first();

		if($dec_rating->average_rating){
			$dec_rating = $dec_rating->average_rating / $dec_rating->total_records;
		}else{
			$dec_rating = 0;
		}

		return view('admin.venue-overview', compact('id', 'venue', 'active_staff', 'total_satff', 'active_tables', 'total_tables', 'active_products', 'total_products', 'active_bars', 'total_bars', 'rating_of_the_month', 'rating_of_the_week', 'rating_of_the_day', 'table_of_the_month', 'table_of_the_week', 'table_of_the_day', 'product_of_the_month', 'product_of_the_week', 'product_of_the_day', 'waiter_of_the_month', 'waiter_of_the_week', 'waiter_of_the_day', 'bar_of_the_month', 'bar_of_the_week', 'bar_of_the_day', 'views_lifetime', 'bookings_lifetime', 'customers_lifetime', 'earnings_lifetime', 'sales_lifetime', 'orders_lifetime', 'views_today', 'bookings_today', 'customers_today', 'earnings_today', 'sales_today', 'orders_today', 'pending_orders', 'current_orders', 'jan_rating', 'feb_rating', 'mar_rating', 'apr_rating', 'may_rating', 'jun_rating', 'jul_rating', 'aug_rating', 'sep_rating', 'oct_rating', 'nov_rating', 'dec_rating', 'first_order', 'last_order'));
	}

	public function information($id) {

		$venue = Venue::find($id);
		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}
		$dietaries = Dietary::where('status', 1)->get();
		$facilities = Facility::where('status', 1)->get();
		$cuisines = Cuisine::where('status', 1)->get();
		$atmospheres = Atmosphere::where('status', 1)->get();
		//get booking days timing
		$getDayTiming = BookingDay::select('booking_days.id as book_id', 'booking_days.day', 'trading_hours.*')
								->leftjoin('trading_hours', 'trading_hours.booking_id', '=', 'booking_days.id')
								->where('trading_hours.venue_id', $id)
								->orderBy('booking_days.id', 'ASC')
								->get();

		// get booking days
		$getDays = BookingDay::get();

		return view('admin.venue-information', compact('cuisines', 'atmospheres', 'venue', 'id', 'dietaries', 'facilities', 'getDayTiming', 'getDays'));
	}

	public function tableBookingView(Request $request, $id){


		$venue = Venue::find($id);
		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}

		$tableData = [];
		//get tables for this venue
		$getTables = Table::where('status', 1)
						    ->where('venue_id', $id)
						    ->get();

		foreach($getTables as $table) {
			if($table->occupied == 0) {
				$occupied = 'Not Occupied';
				$color = 'red';
				$icon = 'fa-times-circle';
			} else {
				$occupied = 'Occupied';
				$color = 'green';
				$icon = 'fa-check-circle';
			}
			$tableData[] = array(
				'id' => $table->id,
				'title' => ' '.$table->name.' (S: '.$table->seats.')',
				'extendedProps'=> array(
					'occupied' => $occupied,
					'color' => $color,
					'icon' => $icon
					),
			);
		}

		$bookingData = [];
		//get table booking
		$tableBooking = Booking:: select('bookings.*', 'tables.id as table_id', 'tables.status as table_status', 'tables.seats as table_seating', 'booking_tables.*')
								->leftjoin('booking_tables', 'booking_tables.booking_id', '=', 'bookings.id')
								->leftjoin('tables', 'tables.id','=', 'booking_tables.table_id')
								->where('tables.status', 1)
								->where('bookings.venue_id', $id)
								->where('bookings.booking_status', '!=', 'cancelled')
								->get();


		foreach($tableBooking as $booking) {

			$startDate = date('Y-m-d',strtotime($booking->date_time));
			$startTime = date('H:i:s',strtotime($booking->date_time));

			if ($venue->average_booking == '') {
				$endTime = strtotime("+30 minutes", strtotime($startTime));
				$endTime = date('H:i:s', $endTime);
			} else {
				$endTime = strtotime("+".date('H',strtotime($venue->average_booking))." hour +".date('i',strtotime($venue->average_booking))." minutes +".date('s',strtotime($venue->average_booking))." seconds", strtotime($startTime));
				$endTime = date('H:i:s', $endTime);
			}

			if ($booking->booking_status == 'completed') {
				$status = 'Completed';
				$background = 'green';

			} else if ($booking->booking_status == 'booked') {
				$status = 'Booked';
				$background = 'blue';

			} else if($booking->booking_status == 'pending') {
					$status = 'Pending';
					$background = 'orange';
			} else {
					$status = 'Cancelled';
					$background = 'red';
			}

			$bookingData[] = array(
				'id' => $booking->booking_id,
				'resourceId' => $booking->table_id,
				'description' => $status,
				'extendedProps'=> array(
					'description' => $status.' (Booked Seats: '.$booking->seating.')'
					),
				'start' => $startDate.'T'.$startTime,
				'end' => $startDate.'T'.$endTime,
				'title' => $booking->name,
				'color' => $background,
				'textColor' => '#fff'

			);

		}

		$bookings = json_encode($bookingData);
		$tables = json_encode($tableData);

		// get table for booking
		$bookingTables = Table::where('venue_id', $venue->id)->where('status', 1)->get();

		return view('admin.booking_view', compact('venue', 'id', 'tables', 'bookings', 'bookingTables'));
	}

	public function featured (Request $request) {

		$getFeatured = Featured::first();

		return view('admin.featured', compact('getFeatured'));
	}

	public function featuredSave(Request $request) {

		$validatedData = $request->validate([
            'months' => 'required',
            'rating' => 'required',
            'reviews' => 'required',
            'charges' => 'required'
        ]);

        $featured = Featured::find(1);
        $featured->months = $request->months;
        $featured->rating = $request->rating;
        $featured->reviews = str_replace(",", "", $request->reviews);
        $featured->charges = str_replace(",", "", $request->charges);
        $featured->save();

        // update venue featured status
        $featuredstatus = Venue::where("created_at","<", Carbon::now()->subMonths($request->months))->update(['featured_status' => 0]);

		return redirect(route('admin.featured'))->with('success', 'Record has been updated.');
	}

	public function setting($id) {

		$venue = Venue::find($id);
		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}
		$user = User::where('venue_id', $id)->where('role', 'venue')->first();
		//$total_commission = Order::where('venue_id', $id)->sum('admin_commission');

		$total_commission = Transaction::where('venue_id', $venue->id)->where('transaction_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');

		return view('admin.venue-setting', compact('venue', 'user', 'id', 'total_commission'));
	}

	public function updateSetting(Request $request, $id){

		// get admin threshold  value
		$userThreshold = User::where('login_type', 'Mighty Super Admin')->pluck('commission')->toArray();
		$venue = Venue::find($id);
		if(Auth::user()->login_type == 'Mighty Super Admin') {
		$venue->payfast_merchant_id = $request->payfast_merchant_id;
		$venue->payfast_merchant_key = $request->payfast_merchant_key;
		}
		$venue->bank_name = $request->bank_name;
		$venue->bank_account_type = $request->bank_account_type;
		$venue->iban = $request->iban;
		$venue->prefered_way = $request->prefered_way;
		//$venue->admin_commission = $request->admin_commission;

		// calculate commission
		$commissionThreshold = ($request->admin_commission_percentage / 100) * $userThreshold[0];

		$venue->commission_threshold_limit = $userThreshold[0] - $commissionThreshold;
		$venue->admin_commission_percentage = $request->admin_commission_percentage;

		if(isset($request->status)){
			$venue->status = 1;
		}else{
			$venue->status = 0;
		}



		if(isset($request->booking)){
			$venue->booking = 1;
			$venue->average_booking = $request->average_booking;

		} else {
			$venue->booking = 0;
			$venue->average_booking = '';
		}

		if(isset($request->booking_auto_accept)){
			$venue->booking_auto_accept = 1;

		} else {
			$venue->booking_auto_accept = 0;
		}


		//social Links
		$venue->fb_link = $request->fb_link;
		$venue->insta_link = $request->insta_link;
		$venue->twitter_link = $request->twitter_link;
		$venue->web_link = $request->web_link;

		if($venue->save()) {
			$user_id = User::where('venue_id', $id)->where('login_type', 'Mighty Super Venue Admin')->pluck('id');
			$user = User::find($user_id[0]);
			$user->name = $request->name;
			$user->phone = $request->phone;

			if(isset($request->status)){
				$user->status = 1;
			}else{
				$user->status = 0;
			}

			if($request->password != ''){
				$user->password = Hash::make($request->password);
				$user->last_password_update = Carbon::now()->toDateTimeString();


				// trail log for password update
				$venueLog = new TrailLog;
				$venueLog->event = 'Admin';
				$venueLog->event_id = $user->id;
				$venueLog->event_type = 'venue Admin Password Updated';
				$venueLog->event_message = Auth::user()->name.' updated venue admin '.$request->name.' password.';
				$venueLog->user_id = Auth::user()->id;
				$venueLog->user_name = Auth::user()->name;
				$venueLog->user_role = Auth::user()->role;
				$venueLog->venue_id = $id;
				$venueLog->save();
			}

			$user->save();

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Setting';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = 'Venue Settings Updated';
			$venueLog->event_message = Auth::user()->name.' updated venue '.$venue->name.' settings information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return redirect(route('admin.venue.setting', $id))->with('success', 'Record has been updated.');
		} else {
			return redirect(route('admin.venue.setting', $id))->with('error', 'Record has not been updated.');
		}
	}



}
