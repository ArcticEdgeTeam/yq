<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductIngredient;
use App\ProductIngredientVariation;

class ProductIngredientController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function delete($venue_id, $ingredient_id){
		$ingredient = ProductIngredient::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		// Delete record from Product Ingredient Variation Table
		$deleteVariation = ProductIngredientVariation::where('ingredient_id', $ingredient_id)->delete();
		return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('success', 'Record has been deleted.');
	}
}
