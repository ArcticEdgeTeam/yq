<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Venue;
use App\Table;
use App\Booking;
use Hash;
use Storage;
use App\Order;
use Image;
use Carbon\Carbon;
use App\TrailLog;

class VenueController extends Controller
{
    //
	
	public function __construct()
    {
		$this->middleware('auth');
        $this->middleware('venue');
    }
	
	public function index(){
		$orders = Order::where('venue_id', Auth::user()->venue_id)->where('order_status', '!=', 'Completed')->where('order_status', '!=', 'Refunded')->orderBy('id', 'desc')->get();
		return view('venue.home', compact('orders'));
	}
	
	
	public function setting() {
		
		$user = User::find(Auth::user()->id);
		return view('venue.setting', compact('user'));
	}

	public function manageAdmins() {

		$getUsers = User::where('role', 'venue')
					->where('login_type', '!=', 'Mighty Super Venue Admin')
					->where('venue_id', Auth::user()->venue_id)
					->get();
		

		return view('venue.manage_admin', compact('getUsers'));
	}

	public function createAdmin() {

		return view('venue.create_admin');
	}

	public function saveAdmin(Request $request) {

		$request->validate([
      		'email' => 'required|email|unique:users,email',
  		]);

		$user = new User;;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->username = $request->username;
		$user->venue_id = Auth::user()->venue_id;
		$user->role = 'venue';

		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}
		
		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();
		}
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Venue Admin Added';
			$venueLog->event_message = Auth::user()->name.' added new venue admin '.$request->name.' to the system.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();

			return redirect(route('venue.manage.admins'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.manage.admins'))->with('error', 'Record has not been updated.');
		}
	}

	public function editAdmin(Request $request) {

		$user = User::find($request->id);
		return view('venue.edit_admin', compact('user'));
	}

	public function updateAdmin(Request $request, $id) {

		$user = User::find($id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		
		if(isset($request->status)) {
			$user->status = 1;
		} else{
			$user->status = 0;
		}

		
		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Veue Admin Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated user '.$request->name.' password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();
		}

		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Venue Admin Updated';
			$venueLog->event_message = Auth::user()->name.' updated admin '.$user->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();

			return redirect(route('venue.manage.admins', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.manage.admins', $id))->with('error', 'Record has not been updated.');
		}
	}
	
	
	public function update(Request $request, $id){
		
		$user = User::find($id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->username = $request->username;
		
		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Venue Admin Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated setting password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();
		}
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Venue Settings Updated';
			$venueLog->event_message = Auth::user()->name.' updated venue settings information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();

			return redirect(route('venue.setting'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.setting'))->with('error', 'Record has not been updated.');
		}
	}

	public function tableBookingView(Request $request){

		
		$venue = Venue::find(Auth::user()->venue_id);
		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}

		$tableData = [];
		//get tables for this venue
		$getTables = Table::where('status', 1)
						    ->where('venue_id',Auth::user()->venue_id)
						    ->get();


		foreach($getTables as $table) {
			if($table->occupied == 0) {
				$occupied = 'Not Occupied';
				$color = 'red';
				$icon = 'fa-times-circle';
			} else {
				$occupied = 'Occupied';
				$color = 'green';
				$icon = 'fa-check-circle';
			}
			$tableData[] = array(
				'id' => $table->id,
				'title' => ' '.$table->name.' (S: '.$table->seats.')',
				'extendedProps'=> array(
					'occupied' => $occupied,
					'color' => $color,
					'icon' => $icon
					),
			);
		}

		$bookingData = [];
		//get table booking
		$tableBooking = Booking:: select('bookings.*', 'tables.id as table_id', 'tables.status as table_status', 'tables.seats as table_seating', 'booking_tables.*')
								->leftjoin('booking_tables', 'booking_tables.booking_id', '=', 'bookings.id')
								->leftjoin('tables', 'tables.id','=', 'booking_tables.table_id')
								->where('tables.status', 1)
								->where('bookings.venue_id', Auth::user()->venue_id)
								->where('bookings.booking_status', '!=', 'cancelled')
								->get();

		foreach($tableBooking as $booking) {

			$startDate = date('Y-m-d',strtotime($booking->date_time));
			$startTime = date('H:i:s',strtotime($booking->date_time));

			if ($booking->average_booking == '') {
				$endTime = strtotime("+30 minutes", strtotime($startTime));
				$endTime = date('H:i:s', $endTime);
			} else {
				$endTime = strtotime("+".date('H',strtotime($booking->average_booking))." hour +".date('i',strtotime($booking->average_booking))." minutes +".date('s',strtotime($booking->average_booking))." seconds", strtotime($startTime));
				$endTime = date('H:i:s', $endTime);
			}

			if ($booking->booking_status == 'completed') {
				$status = 'Completed';
				$background = 'green';
			
			} elseif ($booking->booking_status == 'booked') {
				$status = 'Booked';
				$background = 'blue';
			
			} elseif($booking->booking_status == 'pending') {
					$status = 'Pending';
					$background = 'orange';
			} else {
					$status = 'Cancelled';
					$background = 'red';
			}

			$bookingData[] = array(
				'id' => $booking->booking_id,
				'resourceId' => $booking->table_id,
				'description' => $status,
				'extendedProps'=> array(
					'description' => $status.' (Booked Seats: '.$booking->seating.')'
					),
				'start' => $startDate.'T'.$startTime,
				'end' => $startDate.'T'.$endTime,
				'title' => $booking->name,
				'color' => $background,
				'textColor' => '#fff'
				
			);


		}

		$bookings = json_encode($bookingData);
		$tables = json_encode($tableData);

		// get table for booking
		$bookingTables = Table::where('venue_id', Auth::user()->venue_id)->where('status', 1)->get();
		
		return view('venue.booking_view', compact('venue','tables', 'bookings', 'bookingTables'));
	}


	public function venueStatusUpdate(Request $request) {
		if ($request->ajax()) {
			$venue = Venue::find($request->id);
			$venue->open = $request->status;
			$venue->save();

			if ($request->status == 0) {
				$venueType = 'Venue Closed';
				$venueStatus = 'close';
			} else {
				$venueType = 'Venue Opened';
				$venueStatus = 'open';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = $venueType;
			$venueLog->event_message = Auth::user()->name.' changed venue status to '.$venueStatus.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return  'status update';
		}	
	}
	
	public function checkEmail($email){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		  
		  $email_exist = User::where('email', $email)->count();
		  
		  if($email_exist > 0){
			return 'email_exist';
		  }
		  
		  return 'valid_email';
		  
		} else {
		  return 'invalid_email';
		}
	}
	
}
