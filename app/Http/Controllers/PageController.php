<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Auth;
use App\TrailLog;
class PageController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index() {

		$page1 = Page::find(1);
		$page2 = Page::find(2);
		$page3 = Page::find(3);
		$page4 = Page::find(4);
		return view('admin.cms', compact('page1', 'page2', 'page3', 'page4'));
	}
	
	public function update(Request $request, $id){
		
		$page = Page::find($id);
		
		if($id == 1) {
			$page->video = $request->video;
			$cms = 'about-us';

		} elseif ($id == 2) {
			$page->video = $request->video;
			$cms = 'help';
		} elseif ($id == 3)  {

			$cms = 'terms-&-conditions';
		} else {
			$cms = 'privacy-policy';
		}
		
		$page->text = $request->text;
		
		if($page->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'CMS';
			$venueLog->event_id = $id;
			$venueLog->event_type = 'CMS Updated';
			$venueLog->event_message = Auth::user()->name.' updated cms '.$cms.' information.';	
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.cms'))->with('success', 'Record has been updated.');

		} else {

			return redirect(route('admin.cms'))->with('error', 'Record has not been updated.');
		}
	}
}
