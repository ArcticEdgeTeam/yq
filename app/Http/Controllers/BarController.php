<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;
use Storage;
use App\Order;
use App\Bar;
use App\ManagerPrepArea;
use Image;
use Session;

class BarController extends Controller
{
    //
	
	public function __construct()
    {
		$this->middleware('auth');
        $this->middleware('bar');
    }

    public function dashboard() {

    	// if session is set hide this screen
    	if (Session::has('current_bar_id')) {
	    	return redirect(route('bar.home'));
	    }

    	//get user prep area
    	$prepStaff = Bar::select('bars.*', 'manager_prepareas.user_id as u_id', 'manager_prepareas.venue_id as v_id')
    							->leftjoin('manager_prepareas', 'manager_prepareas.bar_id', '=', 'bars.id')
								->where('manager_prepareas.venue_id', Auth::user()->venue_id)
								->where('manager_prepareas.user_id', Auth::user()->id)
								->where('bars.status', 1)
								->get();

		return view('bar.dashboard', compact('prepStaff'));
	}
	
	public function index(Request $request) {
		//
		if (!Session::has('current_bar_id')) {
			// set session bar id for further use
			Session::put('current_bar_id', decrypt($request->current_bar_id));
		}
		
		$orders = Order::where('venue_id', Auth::user()->venue_id)->where('order_status', '!=', 'Completed')->where('order_status', '!=', 'Refunded')->orderBy('id', 'desc')->get();
		
		$ordersListing = Order::where('venue_id', Auth::user()->venue_id)->where('order_status', '!=', 'Completed')->where('order_status', '!=', 'Refunded')->orderBy('id', 'desc')->get();

		return view('bar.home', compact('orders', 'ordersListing'));
	}
	
	public function setting(){
		$user = User::find(Auth::user()->id);
		return view('bar.setting', compact('user'));
	}
	
	public function update(Request $request, $id){
		
		$user = User::find($id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->username = $request->username;
		
		if($request->password != ''){
			$user->password = Hash::make($request->password);
		}
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){
			return redirect(route('bar.setting'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('bar.setting'))->with('error', 'Record has not been updated.');
		}
	}
}
