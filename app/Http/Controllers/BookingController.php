<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\Booking;
use App\BookingTable;
use App\User;
use App\Table;
use Carbon\Carbon;

class BookingController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
    }
	
	public function index(Request $request, $id) {
		$venue = Venue::find($id);
		
		// get current day booking only
		// $search_date = date('d-m-Y');
		// $search_date1 = Carbon::parse($search_date)->startOfDay();
		// $bookings = Booking::where('venue_id', $venue->id)->whereDate('date_time','=', $search_date1)->orderBy('id', 'desc')->get();
		$getBookings = Booking::query();
		$getBookings = $getBookings->where('venue_id', $venue->id);

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getBookings = $getBookings->whereBetween('created_at', [$start, $end]);
			
		}

		if($request->name != '') {
			$getBookings = $getBookings->where('name', 'like', '%' . $request->name . '%');
		}

		if($request->status != '') {
			$getBookings = $getBookings->where('booking_status', $request->status);
		}

		$getBookings = $getBookings->orderBy('id', 'DESC')
							->paginate(15);			
		
		
		foreach ($getBookings as $key => $value) {
			// get tables
			$tables = Table::select('tables.*', 'booking_tables.*')
							->leftjoin('booking_tables', 'booking_tables.table_id', '=', 'tables.id')
							->where('booking_tables.booking_id', $value->id)
							->pluck('name')->toArray();
			if($tables != null) {
				$value->booking_table = implode(',',$tables);
			} else {
				$value->booking_table = 'N/A';
			}
			
		}

		$counter = 1;
		return view('admin.venue-bookings', compact('counter', 'getBookings', 'venue', 'id'));
	}
	
	public function filter(Request $request, $id) {
		$venue = Venue::find($id);
		if(isset($request->search_date)) {
			$search_date = $request->search_date;
			$search_date = date('Y-m-d', strtotime($search_date));
			$search_date = Carbon::parse($search_date)->startOfDay();
			
			$bookings = Booking::where('venue_id', $venue->id)->whereDate('date_time', '=', $search_date)->orderBy('id', 'desc')->get();

			foreach ($bookings as $key => $value) {
				// get tables
				$tables = Table::select('tables.*', 'booking_tables.*')
								->leftjoin('booking_tables', 'booking_tables.table_id', '=', 'tables.id')
								->where('booking_tables.booking_id', $value->id)
								->pluck('name')->toArray();

				if($tables != null) {
				$value->booking_table = implode(',',$tables);
				} else {
					$value->booking_table = 'N/A';
				}
			}
			$counter = 1;
			
			$search_date = date('d-m-Y', strtotime($search_date));
			return view('admin.venue-bookings', compact('counter', 'bookings', 'venue', 'id', 'search_date'));
			
		}
		
		return abort(404);
	}
	
	public function edit($id, $booking_id){
		$booking = Booking::find($booking_id);
		$venue = Venue::find($id);

		$customer = User::find($booking->customer_id);
		if ($customer != null) {
			if($customer->photo == ''){
				$customer->photo = 'default.png';
			}
		}
		// get tables for booking
		$bookingTables = BookingTable::where('booking_id', $booking_id)->pluck('table_id')->toArray();

		$tables = Table::where('venue_id', $venue->id)->where('status', 1)->get();
		return view('admin.venue-booking-edit', compact('customer', 'booking', 'venue', 'tables', 'id', 'bookingTables'));
	}
	
	
	public function update(Request $request, $id, $booking_id){

		$booking = Booking::find($booking_id);
		// if($request->table_id != '') {
		// 	$booking->table_id = $request->table_id;
		// }else{
		// 	$booking->table_id = null;
		// }
		$booking->seating = $request->seating;
		// $booking->smooking = $request->smooking;
		// $booking->area = $request->area;
		$booking->message = $request->message;
		$booking->booking_status = $request->booking_status;
		$booking->date_time = date('Y-m-d H:i:s', strtotime($request->date_time));

		if($booking->save()) {

			// delete old booking tables
			$deleteTables = BookingTable::where('booking_id', $booking_id)->delete();
			// save table for this booking
			foreach($request->table_id as $table) {
				$bookingTable = new BookingTable;
				$bookingTable->booking_id = $booking_id;
				$bookingTable->table_id = $table;
				$bookingTable->save();
			}

			if($request->booking_page == 'booking_calendar') {
				return redirect(route('admin.table-booking-view', $booking->venue_id))->with('success', 'Record has been updated.');
			} else {
				return redirect(route('admin.venue.bookings', $booking->venue_id))->with('success', 'Record has been updated.');
			}
		}
	}
	
	public function cancel($id, $booking_id){
		$booking = Booking::find($booking_id);
		$booking->booking_status = 'cancelled';
		
		if($booking->save()){
			return redirect(route('admin.venue.bookings', $id))->with('success', 'Record has been updated.');;
		}
	}
	
	public function accept($id, $booking_id){
		$booking = Booking::find($booking_id);
		$booking->booking_status = 'booked';
		
		if($booking->save()){
			return redirect(route('admin.venue.bookings', $id))->with('success', 'Record has been updated.');;
		}
	}
	
	
	public function create($id){
		$venue = Venue::find($id);
		$customers = User::where('status', 1)->where('role', 'customer')->get();
		$tables = Table::where('venue_id', $venue->id)->where('status', 1)->get();
		return view('admin.venue-booking-create', compact('customers','venue', 'tables', 'id'));
	}
	
	
	public function save(Request $request, $id) {
		$booking_number = Booking::max('booking_number');
		
		if($booking_number < 1000){
			$booking_number = 1000;
		}
		
		$booking_number = $booking_number + 1;
		
		$booking = new Booking;
		$booking->booking_number = $booking_number;
		$booking->customer_id = '';
		//$booking->table_id = $request->table_id;
		$booking->seating = $request->seating;
		// $booking->smooking = $request->smooking;
		// $booking->area = $request->area;
		$booking->message = $request->message;
		$booking->date_time = date('Y-m-d H:i:s', strtotime($request->date_time));
		$booking->venue_id = $id;
		$booking->name = $request->name;
		$booking->phone = $request->phone;
		$booking->booking_status = 'booked';
		if($booking->save()) {
			$table_ids = [];
			if(isset($request->availability_check_page)) {
				$table_ids = json_decode($request->table_id);
			} else {
				$table_ids = $request->table_id;
			}

			// save table for this booking
			foreach($table_ids as $table) {
				$bookingTable = new BookingTable;
				$bookingTable->booking_id = $booking->id;
				$bookingTable->table_id = $table;
				$bookingTable->save();
			}
			

			if($request->booking_page == 'booking_calendar') {
				return redirect(route('admin.table-booking-view', $id))->with('success', 'Record has been updated.');
			} else {
				return redirect(route('admin.venue.bookings', $id))->with('success', 'Record has been updated.');
			}
		}
		
	}

	public function tableAvailabilityChecker(Request $request, $id) {
		$venue = Venue::find($id);
		
		// get date and time seperate
		$bookingDate = date('Y-m-d',strtotime($request->table_date_time));
		$bookingTime = date('H:i',strtotime($request->table_date_time));

		//get Table
		$getTables = Table::where('status', 1)
							->where('venue_id', $id)
							->get();
		foreach($getTables as $table) {
			//check booking date and time
			$getBookings = Booking::select('bookings.*', 'tables.id as table_id', 'booking_tables.*')
									->leftjoin('booking_tables', 'booking_tables.booking_id', '=', 'bookings.id')
									->leftjoin('tables', 'tables.id', '=', 'booking_tables.table_id')
									->where('bookings.venue_id', $id)
									->where('bookings.booking_status', 'booked')
									->where('booking_tables.table_id',  $table->id)
									->whereDate('bookings.date_time', $bookingDate)
									->get();
			
			$slotAvailable = '';

			if (!$getBookings->isEmpty()) {

				foreach($getBookings as $booking) {

					// booking start date
						$startTime = date('H:i',strtotime($booking->date_time));
						
						// check if avarege booking for venue exists or not
						if ($venue->average_booking == '') {
							$endTime = strtotime("+30 minutes", strtotime($startTime));
							$endTime = date('H:i', $endTime);
						} else {
							$endTime = strtotime("+".date('H',strtotime($venue->average_booking))." hour +".date('i',strtotime($venue->average_booking))." minutes +".date('s',strtotime($venue->average_booking))." seconds", strtotime($startTime));
							$endTime = date('H:i', $endTime);
						} // average booking

						// check if booking time is conflicting with database booking time
							if($bookingTime >= $startTime && $bookingTime <= $endTime) {
								$slotAvailable = 'no';
							}
				} // booking foreach ends

			} // booking empty check ends

			// booking slot status for this table
			$table->bookingSlot = $slotAvailable;

		} // table foreach ends

		//dd($getTables);

		// get table for booking
		$bookingTables = Table::where('venue_id', $venue->id)->where('status', 1)->get();

		return view('admin.venue_table_availability_checker', compact('venue', 'id', 'getTables', 'bookingTables'));
	}
	
}
