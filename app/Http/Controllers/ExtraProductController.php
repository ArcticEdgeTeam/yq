<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExtraProduct;

class ExtraProductController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function delete($venue_id, $ingredient_id){
		$ingredient = ExtraProduct::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('success', 'Record has been deleted.');
	}
	
	public function update(Request $request, $venue_id, $product_id) {
		// check if this product is not already taken
		$checkproduct = ExtraProduct::where('extra_id', $request->name)
										->where('product_id', $product_id)
										->where('id','!=', $request->id)
										->first();
		if ($checkproduct != null) {

			return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('error', 'Error! The product is already slected.');
		}

		$extra = ExtraProduct::find($request->id);
		$extra->extra_id = $request->name;
		$extra->discount = $request->price;
		if(isset($request->status)){
			$extra->status = 1;
		}else{
			$extra->status = 0;
		}
		$extra->save();
		return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('success', 'Record has been updated.');
	}
}
