<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Order;
use DB;
use App\Review;
use Storage;

class CustomerForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$id = Auth::user()->venue_id;
		$orders = Order::where('venue_id', $id)->groupBy('customer_id')->orderBy('id', 'desc')->get();
		return view('venue.customers', compact('orders', 'counter'));
	}
	
	public function overview($id){
		$counter = 1;
		$customer = User::find($id);
		$bars = DB::table('bars')->where('venue_id', Auth::user()->venue_id)->get();
		return view('venue.customer-overview', compact('counter', 'customer', 'bars'));
	}
	
	public function orders($id){
		$counter = 1;
		$customer = User::find($id);
		$orders = Order::where('customer_id', $customer->id)->where('venue_id', Auth::user()->venue_id)->get();
		return view('venue.customer-orders', compact('customer', 'orders', 'counter'));
	}
	
	public function reviews($id){
		$counter = 1;
		$customer = User::find($id);
		$reviews = Review::where('customer_id', $customer->id)->where('venue_id', Auth::user()->venue_id)->get();
		return view('venue.customer-reviews', compact('customer', 'reviews', 'counter'));
	}
	
	public function edit($id){
		$customer = User::find($id);
		if($customer->dob != ''){
			$customer->dob = date('d-m-Y', strtotime($customer->dob));
		}
		
		return view('venue.customer-edit', compact('customer'));
	}
	
	public function update(Request $request, $id){
		$customer = User::find($id);
		$customer->first_name = $request->first_name;
		$customer->last_name = $request->last_name;
		$customer->phone = $request->phone;
		$customer->gender = $request->gender;
		$customer->dob = date('Y-m-d', strtotime($request->dob));
		
		if(isset($request->status)){
			$customer->status = 1;
		}else{
			$customer->status = 0;
		}
		
		if($request->password != ''){
			$customer->password = Hash::make($request->password);
		}
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $customer->photo);
			//Storage::disk('public')->delete('users/' . $customer->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$customer->photo = $request->photo->hashName();
		}
		
		if($customer->save()){
			return redirect(route('venue.customer.edit', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.customer.edit', $id))->with('error', 'Record has not been updated.');
		}
	}
}
