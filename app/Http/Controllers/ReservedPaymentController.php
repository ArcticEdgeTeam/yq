<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Order;
use App\Venue;
use App\User;
use App\Table;
use App\OrderedProduct;
use App\ReservedFund;

class ReservedPaymentController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		$reservedpayments = Transaction::where('reserved_payment', '!=', 0)->where('transaction_status', 'completed')->get();
		$counter = 1;
		return view('admin.reservedpayments', compact('counter', 'reservedpayments'));
	}
	
	public function view($order_id){
		$total = 0;
		$order = Order::find($order_id);
		$counter = 1;
		$venue = Venue::find($order->venue_id);
		$waiter = User::where('id', $order->waiter_id)->first();
		$customer = User::where('id', $order->customer_id)->first();
		$table = Table::find($order->table_id);
		$ordered_products = OrderedProduct::where('order_id', $order->id)->orderBy('id', 'desc')->get();
		$total_amount = 0;
		$id = $venue->id;
		
		$customer_reserved_fund = ReservedFund::where('customer_id', $customer->id)->sum('amount');
		$customer_reserved_debit = Transaction::where('customer_id', $customer->id)->where('reserved_payment', '!=', 0)->where('order_id', '!=', $order->id)->sum('amount');
		
		$customer_reserved_fund = $customer_reserved_fund - $customer_reserved_debit;
		
		return view('admin.reservedpayment-view', compact('customer_reserved_fund', 'id', 'waiter', 'total_amount', 'ordered_products', 'counter', 'id', 'order', 'total', 'venue', 'customer', 'table'));
	}
	
	public function creditToVenue ($order_id){
		Transaction::where('order_id', $order_id)->update(['reserved_payment' => 2]);
		Order::where('id', $order_id)->update(['reserved_payment' => 2]);
		return redirect(route('admin.reservedpayments'))->with('success', 'Record has been updated.');
	}
}
