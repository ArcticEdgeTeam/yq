<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dietary;
use Storage;
use Auth;
use App\TrailLog;

class DietaryController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$dietaries = Dietary::all();
		
		return view('admin.dietaries', compact('dietaries', 'counter'));
	}
	
	public function create(){
		return view('admin.dietary-create');
	}
	
	public function save(Request $request){
		
		$dietary = new Dietary;
		$dietary->name = $request->name;
		
		if(isset($request->status)){
			$dietary->status = 1;
		}else{
			$dietary->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('dietaries/', ['disk' => 'public']);
			$dietary->image = $request->image->hashName();
		}
		
		if($dietary->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Dietary';
			$venueLog->event_id = $dietary->id;
			$venueLog->event_type = 'Dietary Added';
			$venueLog->event_message = Auth::user()->name.' added new dietary '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.dietaries'))->with('success', 'Record has been added.');
		} else {
			return redirect(route('admin.dietaries'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id){
		$dietary = Dietary::find($id);
		return view('admin.dietary-edit', compact('dietary'));
	}
	
	
	public function update(Request $request, $id){
		
		$dietary = Dietary::find($id);
		
		$dietary->name = $request->name;
		
		if(isset($request->status)){
			$dietary->status = 1;
		}else{
			$dietary->status = 0;
		}
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('dietaries/' . $dietary->image);
			$request->file('image')->store('dietaries/', ['disk' => 'public']);
			$dietary->image = $request->image->hashName();
		}
		
		if($dietary->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Dietary';
			$venueLog->event_id = $dietary->id;
			$venueLog->event_type = 'Dietary Updated';
			$venueLog->event_message = Auth::user()->name.' updated dietary '.$dietary->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.dietaries'))->with('success', 'Record has been updated.');
		} else {
			return redirect(route('admin.dietaries'))->with('error', 'Record has not been updated.');
		}
		
	}
}
