<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Venue;
use App\Category;
use Storage;
use App\Product;
use Session;
use App\TrailLog;

class CategoryForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index() {
		$counter = 1;
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$categories = Category::where('venue_id', $id)->orderBy('id', 'desc')->get();
		return view('venue.categories', compact('id', 'venue', 'categories', 'counter'));
	}
	
	public function create() {
		
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		
		$counter = 0;
		// $categories = Category::where('venue_id', $id)->where('status', 1)->orderBy('id', 'desc')->where('category_id', 0)
        //->with('childrenCategories')
        //->get();

		$categories = Category::where('venue_id', $id)
					->where('status', 1)
					->orderBy('name', 'ASC')
					->where('category_id', 0)
			        ->get();
	

		$edited_id = '';
		
		return view('venue.category-create', compact('counter', 'edited_id', 'id', 'venue', 'categories'));
	}
	
	public function save(Request $request) {

		$validatedData = $request->validate([
            'name' => 'required',
        ]);

		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

        // check for duplicate category name
        $categoryName = Category::where('name', $request->name)->where('venue_id', $id)->first();

        if($categoryName != null) {

        	return redirect(route('venue.categories'))->with('error', 'Category name already exits, try a new name.');
        }

		
		$category = new Category;
		$category->name = $request->name;
		$category->venue_id = $id;
		
		if($request->has_parent == 1) {
			$category->category_id = $request->category_id;
			// make all product of this parent category to error
			$makeError = Product::where('category_id', $request->category_id)->update(['product_error' => 1, 'status' => 0]);

			if ($makeError > 0) {
				Session::flash('error', 'Please go back to product section and assign valid categories!');
			}
		} else {
			$category->category_id = 0;
		}
		
		if(isset($request->status)){
			$category->status = 1;
		}else{
			$category->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('categories/', ['disk' => 'public']);
			$category->image = $request->image->hashName();
		}
		
		if($category->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Category';
			$venueLog->event_id = $category->id;
			$venueLog->event_type = 'Category Added';
			$venueLog->event_message = Auth::user()->name.' added new category '.$request->name. ' to venue.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();

			return redirect(route('venue.categories'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.categories'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($category_id){
		$category = Category::find($category_id);
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		
		$counter = 0;
		// $categories = Category::where('venue_id', $id)->where('status', 1)->orderBy('id', 'desc')->where('id', '!=', $category_id)->whereNull('category_id')
  //       ->with('childrenCategories')
  //       ->get();

		$categories = Category::where('venue_id', $id)
					->where('status', 1)
					->orderBy('name', 'ASC')
					->where('category_id', 0)
					->where('id', '!=', $category_id)
			        ->get();

		// get child categories categories
		$getChildCount = Category::where('category_id', $category_id)->count();

		$edited_id = $category_id;
		
		return view('venue.category-edit', compact('counter', 'edited_id', 'id', 'venue', 'category', 'categories', 'getChildCount'));
	}
	
	
	public function update(Request $request, $category_id) {

		$validatedData = $request->validate([
            'name' => 'required',
        ]);
		
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

		// check for duplicate category name
        $categoryName = Category::where('name', $request->name)
					        ->where('venue_id', $id)
					        ->where('id', '!=', $category_id)
					        ->first();

        if($categoryName != null) {

        	return redirect(route('venue.category.edit', $category_id))->with('error', 'Category name already exits, try a new name.');
        }


		$category = Category::find($category_id);
		// to check its parent category so that we can check after save that if it has still child remaining, then we will change status according
		$getParentId = $category->category_id;
		$category->name = $request->name;
		
		if(isset($request->status)) {
			$category->status = 1;
		}else{
			$category->status = 0;
		}
		
		if($request->has_parent == 1) {
			$category->category_id = $request->category_id;

			// check if category has child
			$checkChilds = Category::where('status', 1)
									->where('category_id', $category_id)
									->pluck('id')->toArray();
				if ($checkChilds != null) {
					// make all product of this parent category which is now going to be a child, to none error
					$removeProductError = Product::whereIn('category_id', $checkChilds)->update(['product_error' => 0, 'status' => 1]);
					$makeCurrentCategryError = Product::where('category_id', $category_id)->update(['product_error' => 0, 'status' => 1]);
				} else {
					// just make this parent category to error none
					$makeCurrentCategryError = Product::where('category_id', $category_id)->update(['product_error' => 0, 'status' => 1]);
				}


			// make all product of this parent category to error
			$makeParentCategoryError = Product::where('category_id', $request->category_id)->update(['product_error' => 1, 'status' => 0]);

			if ($makeParentCategoryError > 0) {
				Session::flash('error', 'Please go back to product section and assign valid categories!');
			}

		} else {
			$category->category_id = 0;
			// check if parent category has child
			$checkChilds = Category::where('status', 1)
									->where('category_id', $category_id)
									->get();
				if (!$checkChilds->isEmpty()) {
					// make all product of this parent category to error
					$makeError = Product::where('category_id', $category_id)->update(['product_error' => 1, 'status' => 0]);

					if ($makeError > 0) {
						Session::flash('error', 'Please go back to products section and assign valid categories!');
					}
				} else {
					$makeError = Product::where('category_id', $category_id)->update(['product_error' => 0, 'status' => 1]);
				}
		}
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('categories/' . $category->image);
			$request->file('image')->store('categories/', ['disk' => 'public']);
			$category->image = $request->image->hashName();
		}
		
		if($category->save()) {

			if($request->has_parent == 1) {
				// check if previous category has child
				$checkPreviousCategory = Category::where('status', 1)
									->where('category_id', $getParentId)
									->pluck('id')->toArray();
				if ($checkPreviousCategory == null) {
					$removePreviousParentError = Product::where('category_id', $getParentId)->update(['product_error' => 0, 'status' => 1]);
				}

			}


			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Category';
			$venueLog->event_id = $category->id;
			$venueLog->event_type = 'Category Updated';
			$venueLog->event_message = Auth::user()->name.' updated category '.$category->name. ' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();

			return redirect(route('venue.categories'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.categories'))->with('error', 'Record has not been updated.');
		}
		
	}
}
