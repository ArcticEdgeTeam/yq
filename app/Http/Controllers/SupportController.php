<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support;
use App\User;
use Auth;

class SupportController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$supports = Support::orderBy('id', 'desc')->get();
		return view('admin.supports', compact('supports', 'counter'));
	}
	
	public function edit($id){
		
		$support = Support::find($id);
		$support->status = 1;
		$support->update();
		
		$user = User::find($support->user_id);
		if($user->photo == ''){
			$user->photo = 'default.png';
		}
		return view('admin.support-edit', compact('support', 'user'));
	}
	
	public function indexForVenue(){
		$counter = 1;
		$supports = Support::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
		return view('venue.supports', compact('supports', 'counter'));
	}
	
	public function editForVenue($id){
		$support = Support::where('id', $id)->where('user_id', Auth::user()->id)->first();
		return view('venue.support-edit', compact('support'));
	}
	
	public function createForVenue(){
		return view('venue.support-create');
	}
	
	public function saveForVenue(Request $request){
		
		$support = new Support;
		$support->subject = $request->subject;
		$support->comment = $request->comment;
		$support->user_id = Auth::user()->id;
		$support->status = 0;
		
		if($support->save()){
			return redirect(route('venue.supports'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.supports'))->with('error', 'Record has not been added.');
		}
	}
	
}
