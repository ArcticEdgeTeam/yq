<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Venue;
use App\User;
use App\Table;
use App\OrderedProduct;
use Auth;
use App\OrderedProductVariable;
use Carbon\Carbon;

class OrderForVenueController extends Controller
{
    //
	
	 //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(Request $request) {

		$venue = Venue::find(Auth::user()->venue_id);
		// $orders = Order::where('venue_id', Auth::user()->venue_id)->where('order_status', '!=', 'Completed')->where('order_status', '!=', 'Refunded')->orderBy('id', 'desc')->get();

		$getOrders = Order::query();
		$getOrders = $getOrders->Select('orders.*', 'users.name as user_name', 'users.photo as user_photo')
		->leftjoin('users', 'users.id', '=', 'orders.customer_id')
		->where('orders.venue_id', $venue->id)
		->where('orders.order_status', '!=', 'Completed')
		->where('orders.order_status', '!=', 'Refunded');

		if($request->name != '') {
			$getOrders = $getOrders->where('users.name', 'like', '%' . $request->name . '%');
		}

		if($request->status != '') {
			$getOrders = $getOrders->where('orders.order_status', $request->status);
		}

		$getOrders = $getOrders->orderBy('orders.id', 'DESC')
							   ->paginate(15);	

		return view('venue.orders', compact('venue', 'getOrders'));
	}
	
	public function orderList(Request $request) {
		$venue = Venue::find(Auth::user()->venue_id);
		//$orders = Order::where('venue_id', Auth::user()->venue_id)->orderBy('id', 'desc')->get();

		$getOrders = Order::query();
		$getOrders = $getOrders->leftjoin('users as customer_table','customer_table.id', '=', 'orders.customer_id')
		->leftjoin('users as waiter_table','waiter_table.id', '=', 'orders.waiter_id')
		->leftjoin('tables', 'tables.id', '=', 'orders.table_id')
		->select('orders.*', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo', 'waiter_table.name as waiter_name', 'waiter_table.photo as waiter_photo', 'tables.id as table_id', 'tables.name as table_name')
		->where('orders.venue_id', $venue->id);
	

		if($request->name != '') {
			$getOrders = $getOrders->where('customer_table.name', 'like', '%' . $request->name . '%');
		}

		if($request->waiter != '') {
			$getOrders = $getOrders->where('orders.waiter_id', $request->waiter);
		}

		if($request->status != '') {
			$getOrders = $getOrders->where('orders.order_status', $request->status);
		}

		if($request->table != '') {
			$getOrders = $getOrders->where('orders.table_id', $request->table);
		}

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getOrders = $getOrders->whereBetween('orders.created_at', [$start, $end]);
			
		}

		$getOrders = $getOrders->orderBy('orders.id', 'desc')->paginate(15);

		//get waiters
		$getWaiters = User::where('status', 1)->where('role', 'waiter')->where('venue_id', $venue->id)->get();
		// get tables
		$getTables = Table::where('status', 1)->where('venue_id', $venue->id)->get();
		
		return view('venue.orderlist', compact('venue', 'getOrders', 'getWaiters', 'getTables'));
	}
	
	public function orderListFilter(Request $request){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$orders = Order::where('venue_id', Auth::user()->venue_id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			
			return view('venue.orderlist', compact('venue', 'orders', 'counter', 'start', 'end'));
		}
		
		return abort(404);
	}
	
	public function edit($id){
		$counter = 1;
		$venue = Venue::find(Auth::user()->venue_id);
		$order = Order::find($id);
		$waiter = User::where('id', $order->waiter_id)->first();
		$customer = User::where('id', $order->customer_id)->first();
		$table = Table::find($order->table_id);
		$ordered_products = OrderedProduct::where('order_id', $order->id)->orderBy('id', 'desc')->get();
		$total_amount = 0;
		return view('venue.order-edit', compact('counter', 'id', 'order', 'venue', 'waiter', 'customer', 'table', 'ordered_products', 'total_amount'));
	}
	
	public function quickview($order_id){
		$order = Order::find($order_id);
		$waiter = User::find($order->waiter_id);
		$customer = User::find($order->customer_id);
		$table = Table::find($order->table_id);
		$ordered_products = OrderedProduct::where('order_id', $order->id)->orderBy('id', 'desc')->get();
		$total_amount = 0;
		$counter = 1;
		return view('ajax.venue-order-quickview', compact('counter', 'order', 'customer', 'waiter', 'table', 'ordered_products', 'total_amount'));
	}
	
	public function popview($order_id){
		$order = Order::find($order_id);
		$waiter = User::find($order->waiter_id);
		$customer = User::find($order->customer_id);
		$table = Table::find($order->table_id);
		$ordered_products = OrderedProduct::where('order_id', $order->id)->orderBy('id', 'desc')->get();
		$total_amount = 0;
		$counter = 1;
		return view('ajax.venue-order-popview', compact('counter', 'order', 'customer', 'waiter', 'table', 'ordered_products', 'total_amount'));
	}
	
	public function updateOrderStatus(Request $request){
		$id = $request->id;
		$status = $request->status;
		$ordered_product = OrderedProduct::find($id);
		$ordered_product->order_status = $status;
		$ordered_product->save();
		OrderedProductVariable::where('ordered_product_id', $id)->update(['order_status' => $status]);
		
		if(!OrderedProduct::where('order_id', $ordered_product->order_id)->where('order_status', '!=', 3)->exists()){
			Order::where('id', $ordered_product->order_id)->where('order_status', '!=', 'Refunded')->where('order_status', '!=', 'Completed')->update(['order_status' => 'Completed']);
		}
	}
}
