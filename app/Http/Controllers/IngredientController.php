<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\Unit;
use App\Venue;

class IngredientController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index($id){
		$counter = 1;
		$counter2 = 1;
		$ingredients = Ingredient::where('venue_id', $id)->orderBy('id', 'desc')->get();
		$units = Unit::where('venue_id', $id)->orderBy('id', 'desc')->get();
		$venue = Venue::find($id);
		return view('admin.venue-ingredients', compact('id', 'venue', 'ingredients', 'units', 'counter', 'counter2'));
	}
	
	public function create($id){
		$venue = Venue::find($id);
		return view('admin.venue-ingredient-create', compact('id', 'venue'));
	}
	
	public function save(Request $request, $id){

		$validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);

        // check dupliacte Ingredient for venue
        $getIngredient = Ingredient::where(function($query) use ($request, $id){
								    $query->where('name', $request->name);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id){
        							$query->where('code', $request->code);
									$query->where('venue_id', $id);
        						})->first();

		if ($getIngredient != null) {

			return redirect(route('admin.venue.ingredient.create', $id))->with('error', 'Ingredient code or name already exists.');
		}
		
		$ingredient = new Ingredient;
		$ingredient->name = $request->name;
		$ingredient->code = $request->code;
		$ingredient->venue_id = $id;
		$venue = Venue::find($id);
		if(isset($request->status)){
			$ingredient->status = 1;
		}else{
			$ingredient->status = 0;
		}
		
		if($ingredient->save()){
			return redirect(route('admin.venue.ingredients', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.venue.ingredients', $id))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id, $ingredient_id){
		$ingredient = Ingredient::find($ingredient_id);
		$venue = Venue::find($id);
		return view('admin.venue-ingredient-edit', compact('id', 'venue', 'ingredient'));
	}
	
	
	public function update(Request $request, $id, $ingredient_id){

		$validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);

        // check dupliacte Ingredient for venue
        $getIngredient = Ingredient::where(function($query) use ($request, $id ,$ingredient_id){
								    $query->where('name', $request->name);
								    $query->where('id', '!=', $ingredient_id);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id ,$ingredient_id){
        							$query->where('code', $request->code);
								    $query->where('id', '!=', $ingredient_id);
									$query->where('venue_id', $id);
        						})->first();

		if ($getIngredient != null) {

			return redirect(route('admin.venue.ingredient.edit', [$id, $ingredient_id]))->with('error', 'Ingredient code or name already exists.');
		}
		
		$ingredient = Ingredient::find($ingredient_id);
		$venue = Venue::find($id);
		$ingredient->name = $request->name;
		$ingredient->code = $request->code;
		
		if(isset($request->status)){
			$ingredient->status = 1;
		}else{
			$ingredient->status = 0;
		}
		
		
		if($ingredient->save()){
			return redirect(route('admin.venue.ingredients', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.venue.ingredients', $id))->with('error', 'Record has not been updated.');
		}
		
	}
}
