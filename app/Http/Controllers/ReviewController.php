<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\Review;
use App\Order;
use App\User;
use App\Transaction;
use Carbon\Carbon;

class ReviewController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(Request $request, $id) {

		$venue = Venue::find($id);
		
		$getReviews = Review::query();
		$getReviews = $getReviews->select('reviews.*', 'orders.order_number', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo', 'waiter_table.name as waiter_name', 'waiter_table.photo as waiter_photo')
		->leftjoin('orders', 'orders.id', '=', 'reviews.order_id')
		->leftjoin('users as customer_table','customer_table.id', '=', 'orders.customer_id')
		->leftjoin('users as waiter_table','waiter_table.id', '=', 'orders.waiter_id')
		->where('reviews.venue_id', $id);


		if($request->name != '') {
			$getReviews = $getReviews->where('customer_table.name', 'like', '%' . $request->name . '%');
		}

		if($request->waiter != '') {
			$getReviews = $getReviews->where('orders.waiter_id', $request->waiter);
		}

		if($request->order_no != '') {
			$getReviews = $getReviews->where('orders.order_number', $request->order_no);
		}

		$getReviews = $getReviews->orderBy('reviews.id', 'desc')->paginate(15);

		//get waiters
		$getWaiters = User::where('status', 1)->where('role', 'waiter')->where('venue_id', $id)->get();

		return view('admin.venue-reviews', compact('getReviews', 'venue', 'id', 'getWaiters'));
	}
	
	public function filter(Request $request, $id){
		$venue = Venue::find($id);
		
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$reviews = Review::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			return view('admin.venue-reviews', compact('counter', 'reviews', 'venue', 'id', 'start', 'end'));
			
		}
		
		return abort(404);
	}
	
	public function edit($id, $review_id){
		$review = Review::find($review_id);
		$order = Order::where('id', $review->order_id)->first();
		$venue = Venue::find($id);
		$customer = User::find($review->customer_id);
		$lifetime_spent = Transaction::where('customer_id', $customer->id)->where('transaction_status', 'Completed')->sum('amount');
		
		if($customer->photo == ''){
			$customer->photo = 'default.png';
		}
		$waiter = User::find($review->waiter_id);
		if($waiter->photo == ''){
			$waiter->photo = 'default.png';
		}
		return view('admin.venue-review-edit', compact('review', 'venue', 'id', 'customer', 'waiter', 'order', 'lifetime_spent'));
	}
	
	public function update(Request $request, $id, $review_id){
		$review = Review::find($review_id);
		$review->comment = $request->comment;
		if(isset($request->status)){
			$review->status = 1;
		}else{
			$review->status = 0;
		}
		$review->save();
		return redirect(route('admin.venue.reviews', $id))->with('success', 'Record has been updated.');
	}
}
