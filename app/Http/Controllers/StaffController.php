<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Venue;
use App\Table;
use App\Order;
use App\WaiterPayment;
use App\TableWaiter;
use Storage;
use Hash;
use App\TrailLog;
use Auth;
use Carbon\Carbon;
use App\Bar;
use App\ManagerPrepArea;

class StaffController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(Request $request, $id) {
		$counter = 1;
		$venue = Venue::find($id);
		
		$getStaff = User::query();
		$getStaff = $getStaff->where(function ($query) use ($id){
				$query->where('venue_id', '=',  $id);
			})->where(function ($query) {
				$query->where('role', '=', 'bar')
				->orWhere('role', '=', 'waiter');
			});

		if($request->name != '') {
			$getStaff = $getStaff->where('name', 'like', '%' . $request->name . '%');
		}

		if($request->role != '') {
			$getStaff = $getStaff->where('role', $request->role);
		}

		if($request->status != '') {
			$getStaff = $getStaff->where('status', $request->status);
		}

		$getStaff = $getStaff->orderBy('id', 'desc')->paginate(15);

		
		return view('admin.venue-staff', compact('getStaff', 'id', 'venue', 'counter'));
	}
	
	public function create($id){
		$tables = Table::where('status', 1)->where('venue_id', $id)->get();
		$venue = Venue::find($id);
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		return view('admin.venue-staff-create', compact('id', 'venue', 'tables', 'bars'));
	}
	
	public function save(Request $request, $id) {

		// check user id
		$checkUser = User::where('user_id', $request->user_id)
							->where('venue_id', $id)
							->first();
		if($checkUser != null) {
			return redirect(route('admin.venue.staff', $id))->with('error', 'User ID already exists for this venue.');
		}
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->role = $request->role;
		// $user->address = $request->address;
		// $user->gender = $request->gender;
		$user->about = $request->about;
		$user->user_id = $request->user_id;
		$user->venue_id = $id;
		// $user->dob = date('Y-m-d', strtotime($request->dob));

		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();
		}
		
		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}
		
		if($request->file('photo')){
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()) {
			
			if(is_array($request->bar_id)) {
				foreach($request->bar_id as $bar) {
					$PrepUser = new ManagerPrepArea;
					$PrepUser->user_id = $user->id;
					$PrepUser->bar_id = $bar;
					$PrepUser->venue_id = $id;
					$PrepUser->save();
				}
			}

			if ($request->role == 'bar') {
				$role = 'manager';
			} else {
				$role = 'waiter';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Staff';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Staff Added';
			$venueLog->event_message = Auth::user()->name.' added '.$role.' staff '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();
			
			return redirect(route('admin.venue.staff', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.venue.staff', $id))->with('success', 'Record has not been added.');
		}
		
		
	}
	
	public function edit($id, $user_id){
		
		$tables = Table::where('status', 1)->where('venue_id', $id)->get();
		$venue = Venue::find($id);
		$user = User::find($user_id);
		if($user->dob != ''){
			$user->dob = date('d-m-Y', strtotime($user->dob));
		}
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		// get user bars
		$getUserBars = ManagerPrepArea::where('user_id', $user_id)->get();
		return view('admin.venue-staff-edit', compact('id', 'venue', 'tables', 'user', 'bars', 'getUserBars'));
	}
	
	public function update(Request $request, $id, $user_id) {
		// check user id
		$checkUser = User::where('user_id', $request->user_id)
							->where('venue_id', $id)
							->where('id', '!=', $user_id)
							->first();
		if($checkUser != null) {
			return redirect(route('admin.venue.staff', $id))->with('error', 'User ID already exists for this venue.');
		}

		$user = User::find($user_id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->gender = $request->gender;
		$user->about = $request->about;
		$user->user_id = $request->user_id;
		$user->dob = date('Y-m-d', strtotime($request->dob));

		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Venue Staff Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated user '.$request->name.' password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();
		}
		
		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}
		
		if($request->file('photo')){
			Storage::disk('public')->delete('users/' . $user->photo);
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()) {
			
			
			
			if(is_array($request->bar_id)) {

				ManagerPrepArea::where('user_id', $user_id)->delete();
				
				foreach($request->bar_id as $bar) {
					$PrepUser = new ManagerPrepArea;
					$PrepUser->user_id = $user->id;
					$PrepUser->bar_id = $bar;
					$PrepUser->venue_id = $id;
					$PrepUser->save();
				}
			}

			if ($user->role == 'bar') {
				$role = 'manager';
			} else {
				$role = 'waiter';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Staff';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Staff Updated';
			$venueLog->event_message = Auth::user()->name.' update '.$role.' staff '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();
			
			return redirect(route('admin.venue.staff', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.venue.staff', $id))->with('success', 'Record has not been updated.');
		}
		
	}
	
	public function wallet($id, $waiter_id){
		$venue = Venue::find($id);
		$user = User::find($waiter_id);
		if($user->photo == ''){
			$user->photo = 'default.png';
		}
		$orders = Order::where('waiter_id', $user->id)->get();
		$total_tip_amount = Order::where('waiter_id', $user->id)->sum('waiter_tip');
		$total_tip_paid = WaiterPayment::where('waiter_id', $user->id)->sum('amount');
		$waiter_payments = WaiterPayment::where('waiter_id', $user->id)->get();
		$transaction_counter = 1;
		$counter = 1;
		
		return view('admin.venue-staff-wallet', compact('id', 'venue', 'user', 'orders', 'total_tip_amount', 'total_tip_paid', 'waiter_payments', 'transaction_counter', 'counter'));
	}
	
	
	public function createPayment(Request $request, $id, $waiter_id){
		
		$payment = new WaiterPayment;
		$payment->amount = $request->amount;
		$payment->waiter_id = $waiter_id;
		$payment->save();
		
		return redirect(route('admin.venue.staff.wallet', [$id, $waiter_id]))->with('success', 'Record has been updated');
	}
}
