<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Venue;
use App\User;
use App\Table;
use App\Transaction;
use Carbon\Carbon;
use PDF;
use Excel;
use Mail;
use Session;
use App\TrailLog;
use Auth;

class CashupForAdminController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(Request $request, $id){

		$getTransactions = Transaction::query();
		$getTransactions = $getTransactions->select('transactions.*', 'orders.invoice_number', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo', 'waiter_table.name as waiter_name', 'waiter_table.photo as waiter_photo', 'tables.id as table_id', 'tables.name as table_name')
		->leftjoin('users as customer_table','customer_table.id', '=', 'transactions.customer_id')
		->leftjoin('users as waiter_table','waiter_table.id', '=', 'transactions.waiter_id')
		->leftjoin('orders', 'orders.id', '=', 'transactions.order_id')
		->leftjoin('tables', 'tables.id', '=', 'transactions.table_id')
		->where('transactions.venue_id', $id);

		if($request->name != '') {
			$getTransactions = $getTransactions->where('customer_table.name', 'like', '%' . $request->name . '%');
		}

		if($request->waiter != '') {
			$getTransactions = $getTransactions->where('transactions.waiter_id', $request->waiter);
		}

		if($request->status != '') {
			$getTransactions = $getTransactions->where('transactions.transaction_status', $request->status);
		}

		if($request->table != '') {
			$getTransactions = $getTransactions->where('transactions.table_id', $request->table);
		}

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getTransactions = $getTransactions->whereBetween('transactions.created_at', [$start, $end]);
			
		}

		$getTransactions = $getTransactions->orderBy('orders.id', 'desc')->paginate(15);

		//get waiters
		$getWaiters = User::where('status', 1)->where('role', 'waiter')->where('venue_id', $id)->get();
		// get tables
		$getTables = Table::where('status', 1)->where('venue_id', $id)->get();

		//hdd($getTransactions);
		$venue = Venue::find($id);
		$total = 0;
		
		return view('admin.venue-cashups', compact('getTransactions', 'getWaiters', 'getTables', 'venue', 'id', 'total'));
	}
	
	public function filter(Request $request, $id){
		$venue = Venue::find($id);
		
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$transactions = Transaction::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			return view('admin.venue-cashups', compact('counter', 'transactions', 'venue', 'id', 'total', 'start', 'end'));
			
		}
		
		return abort(404);
		
	}

	public function cashupsPDF(Request $request, $id){
		$venue = Venue::find($id);
		if($request->date_range != '') {
			
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// get format of date
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cashup Report';
			$venueLog->event_id = '';
			$venueLog->event_type = 'Cashup Report PDF';
			$venueLog->event_message = Auth::user()->name.' generated cashups report from '.$start.' to '.$end.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();
			
			// carbon format date
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			
			$transactions = Transaction::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));

			$pdf = PDF::loadView('pdf.cashups', ['counter' => $counter, 'transactions' => $transactions, 'venue' => $venue, 'id' => $id, 'total' => $total, 'start' => $start, 'end' => $end]);
			return $pdf->stream('Cashups Informations');
			
		} else {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cashup Report';
			$venueLog->event_id = '';
			$venueLog->event_type = 'Cashup Report PDF';
			$venueLog->event_message = Auth::user()->name.' generated all cashups report.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();
			
			// carbon format date
			$start = '';
			$end = '';
			
			$transactions = Transaction::where('venue_id', $id)->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;

			$pdf = PDF::loadView('pdf.cashups', ['counter' => $counter, 'transactions' => $transactions, 'venue' => $venue, 'id' => $id, 'total' => $total, 'start' => $start, 'end' => $end]);
			return $pdf->stream('Cashups Informations');

		}
		
		return abort(404);
		
	}

	public function cashupsExcel(Request $request, $id) {
		$venue = Venue::find($id);
		
		if($request->date_range != '') {
			
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cashup Report';
			$venueLog->event_id = '';
			$venueLog->event_type = 'Cashup Report Excel';
			$venueLog->event_message = Auth::user()->name.' generated cashups report from '.$start.' to '.$end.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();
			
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$transactions = Transaction::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));


			Excel::create(uniqid().'Cashups', function($excel) use($transactions,$start,$end,$venue) {
                $excel->sheet('New sheet', function($sheet) use($transactions,$start,$end,$venue) {
                    $sheet->loadView('excel.cashups', array('transactions' => $transactions, 'start'=> $start, 'end' => $end, 'venue' => $venue));
                });
            })->export('xlsx');
		} else {

			$start = '';
			$end = '';

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cashup Report';
			$venueLog->event_id = '';
			$venueLog->event_type = 'Cashup Report Excel';
			$venueLog->event_message = Auth::user()->name.' generated all cashups report.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();
			
			
			$transactions = Transaction::where('venue_id', $id)->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;


			Excel::create(uniqid().'Cashups', function($excel) use($transactions,$start,$end,$venue) {
                $excel->sheet('New sheet', function($sheet) use($transactions,$start,$end,$venue) {
                    $sheet->loadView('excel.cashups', array('transactions' => $transactions, 'start'=> $start, 'end' => $end, 'venue' => $venue));
                });
            })->export('xlsx');

		}
		
	}

	public function cashupsEmail(Request $request, $id){
		$venue = Venue::find($id);
		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$transactions = Transaction::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));

			$data["email"] = $request->recipient_email;
        	$data["subject"] = 'Cashup Report for '. $venue->name;
        	$data["bodyMessage"] = $venue->name.' from '.$start.' to '.$end ;

        	if ($request->format_type == 'pdf') {
        		$pdf = PDF::loadView('pdf.cashups', ['counter' => $counter, 'transactions' => $transactions, 'venue' => $venue, 'id' => $id, 'total' => $total, 'start' => $start, 'end' => $end])->setPaper('a4');

			

				//Feedback mail to client
			    Mail::send('emails.cashups_report', $data, function($message) use ($data,$pdf){
			            $message->from(config('mail.from.address'));
			            $message->to($data["email"]);
			            $message->subject($data["subject"]);
			            //Attach PDF doc
			            $message->attachData($pdf->output(),'cashups-report.pdf');
			        });

        	} else {


        		$file_name = uniqid().'Cashups';

        		$excel_file = Excel::create($file_name, function($excel) use($transactions,$start,$end,$venue) {
	                $excel->sheet('New sheet', function($sheet) use($transactions,$start,$end,$venue) {
	                    $sheet->loadView('excel.cashups', array('transactions' => $transactions, 'start'=> $start, 'end' => $end, 'venue' => $venue));
	                });
            	})->store('xlsx', public_path() . '\uploads\excel_file');


            	$base_path = $excel_file->storagePath.'/'. $file_name.'.xlsx';

            	//Feedback mail to client
			    Mail::send('emails.cashups_report', $data, function($message) use ($data,$excel_file,$base_path){
			            $message->from(config('mail.from.address'));
			            $message->to($data["email"]);
			            $message->subject($data["subject"]);
			            //Attach PDF doc
			            $message->attach($base_path);
			        });


       			//Feedback mail to client
			    // Mail::send('emails.cashups_report', $data, function($message) use ($data,$excel_file){
			    //         $message->from(config('mail.from.address'));
			    //         $message->to($data["email"]);
			    //         $message->subject($data["subject"]);
			    //         //Attach PDF doc
			    //         $message->attachData($excel_file->string("xlsx"), 'Cashups');
			    //     });


        	}

			Session::flash('success', 'Congratulation! Cashups report has been sent to your email.');

			return redirect()->back();	
		
		} else {

			$start = '';
			$end = '';

			$transactions = Transaction::where('venue_id', $id)->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;

			$data["email"] = $request->recipient_email;
        	$data["subject"] = 'Cashup Report for '. $venue->name;
        	$data["bodyMessage"] = $venue->name.' (Complate Cashups Report).';

        	if ($request->format_type == 'pdf') {
        		$pdf = PDF::loadView('pdf.cashups', ['counter' => $counter, 'transactions' => $transactions, 'venue' => $venue, 'id' => $id, 'total' => $total, 'start' => $start, 'end' => $end])->setPaper('a4');

			

				//Feedback mail to client
			    Mail::send('emails.cashups_report', $data, function($message) use ($data,$pdf){
			            $message->from(config('mail.from.address'));
			            $message->to($data["email"]);
			            $message->subject($data["subject"]);
			            //Attach PDF doc
			            $message->attachData($pdf->output(),'cashups-report.pdf');
			        });

        	} else {


        		$file_name = uniqid().'Cashups';

        		$excel_file = Excel::create($file_name, function($excel) use($transactions,$start,$end,$venue) {
	                $excel->sheet('New sheet', function($sheet) use($transactions,$start,$end,$venue) {
	                    $sheet->loadView('excel.cashups', array('transactions' => $transactions, 'start'=> $start, 'end' => $end, 'venue' => $venue));
	                });
            	})->store('xlsx', public_path() . '\uploads\excel_file');


            	$base_path = $excel_file->storagePath.'/'. $file_name.'.xlsx';

            	//Feedback mail to client
			    Mail::send('emails.cashups_report', $data, function($message) use ($data,$excel_file,$base_path){
			            $message->from(config('mail.from.address'));
			            $message->to($data["email"]);
			            $message->subject($data["subject"]);
			            //Attach PDF doc
			            $message->attach($base_path);
			        });


       			//Feedback mail to client
			    // Mail::send('emails.cashups_report', $data, function($message) use ($data,$excel_file){
			    //         $message->from(config('mail.from.address'));
			    //         $message->to($data["email"]);
			    //         $message->subject($data["subject"]);
			    //         //Attach PDF doc
			    //         $message->attachData($excel_file->string("xlsx"), 'Cashups');
			    //     });


        	}

        	Session::flash('success', 'Congratulation! Cashups report has been sent to your email.');

			return redirect()->back();	

		}
		
		return abort(404);
		
	}
}
