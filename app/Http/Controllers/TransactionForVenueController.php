<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Transaction;
use Carbon\Carbon;

class TransactionForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$transactions = Transaction::where('venue_id', Auth::user()->venue_id)->where('reserved_payment', '!=', 1)->orderBy('id', 'desc')->get();
		$counter = 1;
		$start = date('d-m-Y');
		$end = date('d-m-Y');
		return view('venue.transactions', compact('counter', 'transactions', 'start', 'end'));
	}
	
	public function filter(Request $request){
		
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$transactions = Transaction::where('venue_id', Auth::user()->venue_id)->where('reserved_payment', '!=', 1)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			
			return view('venue.transactions', compact('counter', 'transactions', 'start', 'end'));
			
		}
		
		return abort(404);
		
	}
}
