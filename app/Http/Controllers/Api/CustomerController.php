<?php

namespace App\Http\Controllers\Api;

use App\Models\PushNotificationToUser;
use App\PushNotification;
use App\PushNotificationUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Atmosphere;
use App\Booking;
use App\BookingDay;
use App\Category;
use App\Chat;
use App\ChatMessages;
use App\Cuisine;
use App\ExtraProduct;
use App\Facility;
use App\Menu;
use App\Order;
use App\OrderedProduct;
use App\OrderedProductVariable;
use App\Page;
use App\Product;
use App\ProductPreference;
use App\ProductSide;
use App\ProductVariant;
use App\Review;
use App\Table;
use App\TableWaiter;
use App\Transaction;
use App\User;
use App\UserConnect;
use App\UserLocation;
use App\Venue;
class CustomerController extends Controller
{
    public function getMenuProducts(Request $request) {

        //Validation Start here
        $validation = Validator::make(
            $request->all(),
            [
                'venue_id' => 'required'
            ]
        );

        if ($validation->fails()) {
            return response()->json([
                'error' => true,
                'message' =>$validation->getMessageBag()->all(),
                'data' => null,
            ], 400);
        }
        //Validation End here
        $sub_category = [];
        $getParentCategory = [];
        $product_data = [];
        $result = [];
        $Venue = Venue::select(['id','name','tagline'])->where('id', $request->venue_id)->where('status', $request->venue_id)->first();
        if ($Venue){
            //get Parent categories for this menu
            $query = Category::whereVenueId($request->venue_id)->whereStatus(1);
            $getParentCategory = $query->whereCategoryId(0)->get();
            //get product query
            $Product_query = Product::whereStatus(1)->whereVenueId($request->venue_id);
            if ($request->product_name){
                $Product_query = $Product_query->where('name', 'like', '%'.$request->product_name.'%');
                //$Product_query = $Product_query->whereLike('name',$request->product_name);
            }
            if ($request->sub_category_id){
                $check_sub_category = $query->whereCategoryId($request->sub_category_id)->first();
                $Product_query = $Product_query->whereCategoryId($request->sub_category_id);
            }
            if ($request->category_id && empty($request->sub_category_id)){
                $category = Category::find($request->category_id);
                $sub_query = $query->whereCategoryId($category->id);

                $Product_query = $Product_query->whereCategoryId($category->id);
                //get subcategories by category id
                $sub_category = $sub_query->get();
                //get sucategory ids for getting products
                $sub_category_ids = $sub_query->pluck('id');
                if (count($sub_category_ids) > 0){
                    $Product_query = $Product_query->whereInCategoryId($sub_category_ids);
                }
            }
            // get products results after filter out data
            $products = $Product_query->get();
            $result['venue'] = $Venue;
            $result['all_category'] = $getParentCategory;
            $result['sub_category'] = $sub_category;
            $result['products'] = $products;
            return [
                'error' => false,
                'message' => "record found",
                'data' => $result,
            ];

            //merge parent product and child products here

        } else {
            return [
                'error' => true,
                'message' => "Venue does not exist",
                'data' => $result,
            ];
        }

        //return response(array('parentCategory' => $parentCategory, 'childCategory' => $childCategory, 'childIndex' => $childIndex));
    }

    public function productDetails(Request $request){

        //Validation Start here
        $validation = Validator::make(
            $request->all(),
            [
                'product_id' => 'required'
            ]
        );

        if ($validation->fails()) {
            return response()->json([
                'error' => true,
                'message' =>$validation->getMessageBag()->all(),
                'data' => null,
            ], 400);
        }
        //Validation End here

        $product = Product::where('status', 1)->where('id', $request->product_id)->first();
        if ($product){
            if($product->image == ''){
                $product->image = asset('public/uploads/products/default.png');
            }else{
                $product->image = asset('public/uploads/products/' . $product->image);
            }

            $variants = ProductVariant::where('product_id', $request->product_id)->get();
            $extras = ExtraProduct::where('product_id', $request->product_id)->get();
            $prefrences= ProductPreference::where('product_id', $request->product_id)->get();
            $sides= ProductSide::where('product_id', $request->product_id)->get();

            return [

                'error' => false,
                'product' => $product,
                'variants' => $variants,
                'extras' => $extras,
                'prefrences' => $extras,
                'sides' => $sides,

            ];
        } else {

            return [

                'error' => true,
                'message' => 'product not available',

            ];
        }

    }

    public function getTables(Request $request) {

        //Validation Start here
        $validation = Validator::make(
            $request->all(),
            [
                'venue_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json([
                'error' => true,
                'message' =>$validation->getMessageBag()->all(),
                'data' => null,
            ], 400);
        }
        //Validation End here
        $venue = Venue::find($request->venue_id);
        $query = Table:: leftjoin('bookings', 'tables.id','=', 'bookings.table_id')
            ->leftjoin('table_waiters','table_waiters.table_id','=', 'tables.id')
            ->select('tables.id as table_id', 'tables.status as table_status','tables.occupied', 'tables.seats as table_seating','bookings.booking_status','bookings.date_time','table_waiters.user_id')
            ->where('tables.status', 1)
            ->where('tables.venue_id', $request->venue_id)
            ->orderBy('tables.id','desc')
            ->groupBy('tables.id');
        if ($request->occupied){
            $query = $query->where('tables.occupied', $request->occupied);
        }
        if ($request->user_id){
            $query = $query->where('table_waiters.user_id', $request->user_id);
        }
        $tables = $query->get();
        // read every item and assign colours of each table
        $tabledata = [];
        $colour = 'white';
        foreach($tables as $table) {
            if (isset($table->booking_status)){

                $colour = $this->getTableColours($table->booking_status);
            }
            $tabledata['table'][] = array(
                'table_id' => $table->table_id,
                'status' => $table->table_status,
                'is_occupied' => $table->occupied,
                'seats' => $table->table_seating,
                'booking_status' => $table->booking_status,
                'booking_date_time' => $table->date_time,
                'table_colour' => $colour,
            );
        }
        return [

            'error' => false,
            'data' => $tabledata,
            //'last_visit' => '15 min',

        ];
    }

    public function waiteReviews(Request $request) {

        //Validation Start here
        $validation = Validator::make(
            $request->all(),
            [
                'waiter_id' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json([
                'error' => true,
                'message' =>$validation->getMessageBag()->all(),
                'data' => null,
            ], 400);
        }
        //Validation End here
        $getReviews = Review::query();
        $getReviews = $getReviews->select('reviews.id','reviews.service_rating','reviews.comment','reviews.created_at', 'orders.order_number', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo')
            ->leftjoin('orders', 'orders.id', '=', 'reviews.order_id')
            ->leftjoin('users as customer_table','customer_table.id', '=', 'orders.customer_id')
            ->leftjoin('users as waiter_table','waiter_table.id', '=', 'orders.waiter_id')
            ->where('orders.waiter_id', $request->waiter_id)->get();

        return [

            'error' => false,
            'data' => $getReviews,
            //'last_visit' => '15 min',

        ];
    }

    public function waiterTips(Request $request) {
        $validator = Validator::make($request->all(), [
            'waiter_id' => 'required'
        ]);
        $query = Order::query();
        $query = $query->select( 'orders.order_number','orders.id','orders.invoice_number','orders.waiter_tip','orders.created_at',
            'customer_table.name as customer_name', 'customer_table.photo as customer_photo')
            ->leftjoin('users as customer_table','customer_table.id', '=', 'orders.customer_id')
            ->where('orders.waiter_id', $request->waiter_id);
        if ($request->date){
            $query = $query->where('orders.created_at',$request->date);
        }
        if ($request->tip_order){
            $query = $query->whereNotIn('orders.waiter_tip',[0]);
        }
        $data['waiter_tip'] = $query->get();
        $data['total_tip'] = $query->sum('orders.waiter_tip');
        return [

            'error' => false,
            'data' => $data,

        ];
    }

    public function allMesages(Request $request) {

        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'message_for' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $for = $request->message_for;
            $data = ChatMessages::whereMessageFor($for)->get();

            return [
                'error' => false,
                'data' => $data
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function allChatMesagesForWaiter(Request $request) {

        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $user_id = $request->user_id;
            $check = User::find($user_id);
            $message_data = [];
            if ($check) {
                $message = Chat::select('chats.id','chats.message','chats.created_at','chats.reciever_id as user_id','chats.sender_id','sender.name','sender.photo','sender.photo')
                    ->join('users','chats.reciever_id', '=', 'users.id')
                    ->join('users as sender','chats.sender_id', '=', 'sender.id')
                    ->where('chats.reciever_id',$user_id)->where('chats.status','publish')->get();
                if ($message){
                    foreach ($message as $data){
                        $message_data['message'][] =  array(
                            'id' => $data->id,
                            'message' => $data->message,
                            'user_id' => $data->user_id,
                            'photo' => $data->photo == null ? url('public/uploads/banners/default.png') : url('public/uploads/users/'.$data->photo),
                            'sender_id' => $data->sender_id,
                            'name' => $data->name,
                            'created_at' => $data->created_at->diffForHumans()
                        );
                    }
                }

                return [
                    'error' => false,
                    'data' => $message_data
                ];
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'User not found',
                    'data' => null,
                ], 404);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function ReplyMessages(Request $request) {

        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                    'message_id' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $user_id = $request->message_id;
            $check = Chat::find($request->message_id);
            $message_data = [];
            if ($check) {
                $message = Chat::whereId($request->message_id)->update(['status' => 'unPublish']);
                return [
                    'error' => false,
                    'data' => $message,
                    'message' => 'Status Updated Successfully',
                ];
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'User not found',
                    'data' => null,
                ], 404);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function allChatMesagesForCustomer(Request $request) {

        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $user_id = $request->user_id;
            $check = User::find($user_id);
            if ($check) {
                $data = Chat::join('users','chats.reciever_id', '=', 'users.id')
                    ->where('chats.reciever_id',$user_id)->where('chats.status','publish')->get();
                return [
                    'error' => false,
                    'data' => $data
                ];
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function saveMessages(Request $request) {

        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'sender_id' => 'required',
                    'reciever_id' => 'required',
                    'message' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            //save messages
            $userLocation = new Chat();
            $userLocation->sender_id = $request->sender_id;
            $userLocation->reciever_id = $request->reciever_id;
            $userLocation->message = $request->message;
            $userLocation->status = 1;
            $userLocation->save();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Notification Sent Succesfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'data' => null,
            ], 401);

        }

    }

    public function venueFilter(Request $request) {
        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                ]
            );
            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            $data = [];
            //Validation End here
            $atmosphere = Atmosphere::All();
            $cuisines = Cuisine::All();
            $facilities = Facility::All();

            $data['atmosphere'] = $atmosphere;
            $data['cuisines'] = $cuisines;
            $data['facilities'] = $facilities;
            return [
                'error' => false,
                'data' => $data,
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function SearchVenues(Request $request) {
        try {

            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                    'lat' => 'required',
                    'long' => 'required'
                ]
            );
            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            $data = [];
            $facilities = "";
            $diataries = "";
            if ($request->facilities) {
                $facilities_string = $request->facilities;
                $facilities = explode (",", $facilities_string);
            }
            if ($request->diataries) {
                $diataries_string = $request->diataries;
                $diataries = explode (",", $diataries_string);
            }
            $lat = $request->lat;
            $long = $request->long;
            $address = $request->address;
            $distance = $request->distance;
            $avg_person = $request->avg_person;
            $atmosphere = $request->atmosphere;
            $cuisines = $request->cuisines;
            $diataries = $request->diataries;
            $venue_rating= $request->venue_rating;
            //Validation End here
            $query = Venue::leftjoin('facility_venues', 'facility_venues.venue_id', '=', 'venues.id')
                ->leftjoin('user_venue_reviews', 'user_venue_reviews.venue_id', '=', 'venues.id')
                ->leftjoin('venue_dietaries', 'venue_dietaries.venue_id', '=', 'venues.id')
                ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude","venues.avg_cost", "venues.longitude", 'venues.status', 'venues.address'
                    , 'venues.cuisine_id', 'user_venue_reviews.average_rating', 'venue_dietaries.dietary_id', 'facility_venues.facility_id', 'venues.atmosphere_id'
                    ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $long . "))
		        + sin(radians(" .$lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
                ->orderBy('distance', 'ASC')
                ->where('venues.status', 1);

            if ($distance){
                $query = $query->having("distance", "<", $distance);
            }
            if ($address){
                $query = $query->where('venues.address', 'LIKE', "%{$address}%");
            }
            if ($avg_person){
                $query = $query->where("venues.avg_cost", "<=", $avg_person);
            }
            if ($atmosphere){
                $query = $query->where("venues.atmosphere_id", $atmosphere);
            }
            if ($cuisines){
                $query = $query->where("venues.cuisine_id",$cuisines);
            }
            if ($venue_rating){
                $query = $query->where("user_venue_reviews.average_rating", ">=",$venue_rating);
            }
            if ($facilities){
                $query = $query->where("facility_venues.facility_id",$facilities);
            }
            if ($diataries){
                $query = $query->where("venue_dietaries.dietary_id",$diataries);
            }
            $data_obj = $query->paginate(10);
            foreach ($data_obj as $item){
                $data['venues'][] = array(
                    'id' => $item->venue_id,
                    'name' => $item->name,
                    'banner' => $item->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$item->banner),
                    'latitude' => $item->latitude,
                    'longitude' => $item->longitude,
                    'avg_person_cost' => $item->avg_cost,
                    'status' => $item->avg_cost,
                    'address' => $item->address,
                    'cuisine_id' => $item->cuisine_id,
                    'average_rating' => $item->average_rating,
                    'dietary_id' => $item->dietary_id,
                    'facility_id' => $item->facility_id,
                    'atmosphere_id' => $item->atmosphere_id,
                    'distance' => $item->distance,
                );
            }
            return [
                'error' => false,
                'data' => $data,
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function WaiterProfile(Request $request) {
        try {

            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required'
                ]
            );
            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $user = User::find($request->user_id);
            $rating = Review::avg('service_rating');
            $userData['user'] =  array(
                'user_id' => $user->id,
                'name' => $user->name,
                'venue_id' => $user->venue_id,
                'image' => $user->photo == null ? url('public/uploads/banners/default.png') : url('public/uploads/users/'.$user->photo),
                'phone' => $user->phone,
                'rating' => $rating,
            );
            return [
                'error' => false,
                'data' => $userData,
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function waiterTable(Request $request) {
        try {
            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'venue_id' => 'required'
                ]
            );
            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            $tabledata= [];
            $assign = false;
            //Validation End here
            $tables = Table:: leftjoin('table_waiters','table_waiters.table_id','=', 'tables.id')
                ->select('tables.id as table_id', 'tables.status as table_status','tables.occupied', 'tables.seats as table_seating','table_waiters.user_id')
                ->where('tables.status', 1)
                ->where('tables.venue_id', $request->venue_id)
                ->orderBy('tables.id','desc')
                ->get();
            foreach($tables as $table) {
                if ($table->user_id == Auth::user()->id){
                    $assign = true;
                }
                $tabledata['table'][] = array(
                    'table_id' => $table->table_id,
                    'status' => $table->table_status,
                    'is_occupied' => $table->occupied,
                    'seats' => $table->table_seating,
                    'table_assign' => $assign,
                );
            }
            return [

                'error' => false,
                'data' => $tabledata,
                //'last_visit' => '15 min',

            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function getTableColours($status) {

        try {

            switch ($status) {
                case "pending":
                    $colour = 'red';
                    break;
                case "booked":
                    $colour = 'blue';
                    break;
                case "cancelled":
                    $colour = 'yellow';
                    break;
                case "completed":
                    $colour = 'green';
                    break;
                default:
                    // just set status as false
                    $colour = 'white';

            } // switch condition for page ends

            // return response
            return $colour;

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function customerVenuesHistory(Request $request) {
        try {
            $user = User::find($request->user_id);
            if ($user) {
                $edata= [];
                $User_history = UserConnect::leftjoin('users', 'user_connects.user_id', '=', 'users.id')
                    ->leftjoin('venues', 'venues.id', '=', 'user_connects.venue_id')
                    ->select("venues.id as venue_id", "venues.name", "venues.banner","user_connects.created_at")
                    // ->where('users.id', Auth::user()->id)
                    ->where('users.id', $request->user_id)
                    ->get();
                foreach($User_history as $history) {

                    $edata['venues'][] = array(
                        'venue_id' => $history->venue_id,
                        'venue_name' => $history->name,
                        'visit_date' => date("jS F Y ", strtotime($history->created_at)),
                        'venue_banner' => $history->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$history->banner),
                    );
                }
                return [
                    'error' => false,
                    'data' => $edata
                ];
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'user not found',
                    'data' => null,
                ], 401);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function customerTransactionHistory(Request $request) {
        try {
            $user = User::find($request->user_id);
            if ($user) {
                $edata= [];
                $transaction_history = Transaction::join('users', 'users.id', '=', 'transactions.customer_id')
                    ->leftjoin('orders', 'orders.id', '=', 'transactions.order_id')
                    ->leftjoin('venues', 'venues.id', '=', 'transactions.venue_id')
                    ->select("transactions.id as transactions_id", "transactions.waiter_tip","transactions.amount","transactions.total","transactions.transaction_status","venues.name", "venues.banner","transactions.created_at")
                    ->where('transactions.status', 1)
                    //->where('users.id', Auth::user()->id)
                    ->where('users.id', $request->user_id)
                    ->get();

                foreach($transaction_history as $history) {

                    $edata['transaction'][] = array(
                        'transaction_id' => $history->transactions_id,
                        'waiter_tip' => $history->waiter_tip,
                        'amount' => $history->amount,
                        'total' => $history->total,
                        'transaction_status' => $history->transaction_status,
                        'venue_name' => $history->name,
                        'transactions_date' => date("jS F Y ", strtotime($history->created_at)),
                        'venue_banner' => $history->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$history->banner),
                    );
                }
                return [
                    'error' => false,
                    'data' => $edata
                ];
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'user not found',
                    'data' => null,
                ], 400);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    /** @noinspection PhpUndefinedFieldInspection */
    public function orderSave(Request $request){

        try {

            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'customer_id' => 'required',
                    'product_id' => 'required',
                    'order_id' => 'required',
                    'waiter_tip' => 'required',
                    'total_amount' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $sides = "";
            $sides_count = "";
            $extras = "";
            $extras_count = "";
            if ($request->extras) {
                $extras_string = $request->extras;
                $extras = explode (",", $extras_string);
                $extras_count = count($extras);
            }
            if ($request->sides) {
                $sides_string = $request->sides;
                $sides = explode (",", $sides_string);
                $sides_count = count($sides);
            }

            $products = $request->product_id;
            $order = Order::find($request->order_id);
            $order->total_amount = $request->total_amount;
            $order->waiter_tip = $request->waiter_tip;
            $admin_percentage = User::where('role', 'admin')->first()->percentage;
            $venue = Venue::find($request->venue_id);
            $collected_commission = Transaction::where('venue_id', $venue->id)->where('transaction_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');
            $order_commission = round($admin_percentage * $request->total_amount / 100,2);
            $commission_in_venue = $venue->admin_commission;

            if($commission_in_venue > $collected_commission + $order_commission){
                $order->admin_commission = $order_commission;
            }else{
                $order->admin_commission = $commission_in_venue - $collected_commission;
            }

            if(isset($request->reserved_payment)){
                $order->reserved_payment = 1;
            }

            $order->save();
            $transaction_id = str_random();
            $transaction = new Transaction;
            $transaction->order_id = $order->id;
            $transaction->customer_id = $order->customer_id;
            $transaction->venue_id = $request->venue_id;
            $transaction->transaction_id = $transaction_id;
            $transaction->amount = $request->total_amount;
            $transaction->waiter_id = $order->waiter_id;
            $transaction->waiter_tip = $request->waiter_tip;
            $transaction->admin_commission = $order->admin_commission;
            $transaction->transaction_status = $order->order_status;
            $transaction->total = $request->total_amount + $request->waiter_tip;
            $transaction->save();

            $product_obj = Product::find($products);
            $ordered_product = new OrderedProduct;
            $ordered_product->product_id = $product_obj->id;
            $ordered_product->price = $product_obj->price;
            $ordered_product->order_status = $order->order_status;
            $ordered_product->quantity = $request->quantity;
            $ordered_product->bar_id = $product_obj->bar_id;
            $ordered_product->customer_id = $order->customer_id;
            $ordered_product->venue_id = $order->venue_id;
            $ordered_product->order_id = $order->id;
            $ordered_product->comment = $request->comment;
            $ordered_product->save();

            if ($request->variation){
                $variation_obj = ProductVariant::find($request->variation);
                $variation = new OrderedProductVariable;
                $variation->variable_id = $request->variation;
                $variation->type = 'variation';
                $variation->name = $variation_obj->name;
                $variation->quantity = 1;
                $variation->bar_id = $product_obj->bar_id;
                $variation->ordered_product_id = $ordered_product->id;
                $variation->price = $variation_obj->price;
                $variation->order_by = $order->customer_id;
                $variation->order_status = $order->order_status;
                $variation->save();
            }

            if ($sides){
                foreach ($sides as $side){
                    $side_obj = ProductSide::find($side);
                    $side_data = new OrderedProductVariable;
                    $side_data->variable_id = $side;
                    $side_data->type = 'sides';
                    $side_data->name = $side_obj->name;
                    $side_data->quantity = $sides_count;
                    $side_data->bar_id = $product_obj->bar_id;
                    $side_data->ordered_product_id = $ordered_product->id;
                    $side_data->order_by = $order->customer_id;
                    $side_data->order_status = $order->order_status;
                    $side_data->save();
                }

            }

            if ($extras){
                foreach ($extras as $extra){
                    $extra_obj = ExtraProduct::find($extra);
                    $extra_data = new OrderedProductVariable;
                    $extra_data->variable_id = $extra;
                    $extra_data->type = 'extras';
                    $extra_data->quantity = $sides_count;
                    $extra_data->bar_id = $product_obj->bar_id;
                    $extra_data->ordered_product_id = $ordered_product->id;
                    $extra_data->order_by = $order->customer_id;
                    $extra_data->order_status = $order->order_status;
                    $extra_data->save();
                }

            }

            if ($request->preferences){
                $preferences_obj = ProductPreference::find($request->preferences);
                $preferences = new OrderedProductVariable;
                $preferences->variable_id = $request->preferences;
                $preferences->type = 'preferences';
                $preferences->name = $preferences_obj->name;
                $preferences->quantity = 1;
                $preferences->bar_id = $product_obj->bar_id;
                $preferences->ordered_product_id = $ordered_product->id;
                $preferences->order_by = $order->customer_id;
                $preferences->order_status = $order->order_status;
                $preferences->save();
            }
            return [
                'error' => false,
                'message' => 'Record inserted successfully',
                'data' => ''
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function SplitPayment(Request $request, $amount= null){
        $order_amount = 300;
        $amin_percent = 20;
        $admin_comission_limit = 20000;
        $admin_comission_amount = ($amin_percent / 100) * ($order_amount); // 60
        $amount_after_admin_comission = $admin_comission_amount + $order_amount; // 360

        // static portion here
        $amount_after_first_step =(3.5/100)*($amount_after_admin_comission); // (3.5/100)*(360) = 12.6
        $amount_after_2nd_step = ($amount_after_first_step) + (2); // 12.6 + 2 = 14.6
        $amount_after_3rd_step = (15/100) * ($amount_after_2nd_step); // (15/100)*(14.6) = 2.19
        $payfast_tax_amount = ($amount_after_2nd_step) + ($amount_after_3rd_step); // 14.6 + 2.19 = 16.79
        $final_amount_get_from_customer = ($payfast_tax_amount) + ($amount_after_admin_comission); // 16.79 + 360 = 376.79
        $admin_transfered_amount = ($amount_after_admin_comission)-($admin_comission_amount); // 360 - 60 = 300

    }

    public function SplitPaymentstep(Request $request, $amount= null){
        $order_amount = 300;
        $amin_percent = 20;
        $admin_comission_limit = 20000;
        $admin_comission_amount = ($amin_percent / 100) * ($order_amount); // 60
        $amount_after_admin_comission = $admin_comission_amount + $order_amount; // 360

        // static portion here
        $amount_after_first_step =(3.5/100)*($amount_after_admin_comission); // (3.5/100)*(360) = 12.6
        $amount_after_2nd_step = ($amount_after_first_step) + (2); // 12.6 + 2 = 14.6
        $amount_after_3rd_step = (15/100) * ($amount_after_2nd_step); // (15/100)*(14.6) = 2.19
        $payfast_tax_amount = ($amount_after_2nd_step) + ($amount_after_3rd_step); // 14.6 + 2.19 = 16.79
        $final_amount_get_from_customer = ($payfast_tax_amount) + ($amount_after_admin_comission); // 16.79 + 360 = 376.79
        $admin_transfered_amount = ($amount_after_admin_comission)-($admin_comission_amount); // 360 - 60 = 300
    }

    public function orderedList(Request $request){

        try {

            //Validation Start here
            $validation = Validator::make(
                $request->all(),
                [
                    'customer_id' => 'required',
                    'order_id' => 'required',
                    'waiter_tip' => 'required',
                    'device_token' => 'required',
                    'Bereer_token' => 'required',
                ]
            );

            if ($validation->fails()) {
                return response()->json([
                    'error' => true,
                    'message' =>$validation->getMessageBag()->all(),
                    'data' => null,
                ], 400);
            }
            //Validation End here
            $order = Order::join('transactions','transactions.order_id', '=','orders.id')
                ->select('orders.id','orders.order_number','orders.total_amount','orders.total_amount','orders.created_at')
                ->where('orders.id',$request->order_id)
                ->first();
            // get ordered products here
            $order_products = OrderedProduct::join('ordered_product_variables','ordered_product_variables.ordered_product_id', '=','ordered_products.id')
                ->select('ordered_products.id','ordered_products.order_id','ordered_products.product_id','ordered_products.quantity',
                    'ordered_products.price','ordered_product_variables.type','ordered_product_variables.name')
                ->where('ordered_products.order_id',$request->order_id)
                ->get();

            return view('admin.venue-product-edit', compact('order',  'order_products'));
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    }

    public function notifyUser(Request $request){

        $users = User::whereRole('waiter')->pluck('id', 'device_token');
        $notification = PushNotification::wherePushNotificationStatus('Pending')->first();
        foreach ($users as $user){
            $notification_id = $notification->id;
            $title = $user->subject;
            $message = $user->body;
            $id = $user->id;
            $type = "basic";
            $res = sendNotificationFCM($notification_id, $title, $message, $id,$type);
            if($res == 1){
                $data = [
                    'user_id' => $id,
                    'push_notification_id' => $notification_id,
                    'status' => 1,
                ];
            }else{

                return response()->json([
                    'error' => true,
                    'message' => 'Device Key not mache',
                    'data' => null,
                ], 401);
            }
        }

    }

    public function listAllNotification(Request $request){
        try {
            $check = User::find($request->user_id);
            if ($check){
                $notification = PushNotificationUser::select('push_notifications.subject','push_notifications.body','push_notifications.created_at',)
                    ->leftjoin('push_notifications','push_notification_users.push_notification_id', '=', 'push_notifications.id')
                    ->leftjoin('users','push_notification_users.user_id', '=', 'users.id')
                    ->where('push_notifications.push_notification_status','Sent')
                    ->where('push_notification_users.user_id',$request->user_id)
                    ->get();
                //dd($notification);
                return response()->json([
                    'error' => false,
                    'message' => $notification,
                    'data' => null,
                ], 401);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'User not found',
                    'data' => null,
                ], 401);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }


    }
}
