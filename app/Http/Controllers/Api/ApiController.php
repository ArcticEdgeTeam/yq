<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use App\Page;
use App\Venue;
use App\UserLocation;
use App\UserFavoriteVenue;
use App\Review;
use App\UserVenueReview;
use App\Dietary;
use App\Facility;
use App\BookingDay;
use App\TradingHour;
use App\Table;
use App\Menu;
use App\UserConnect;
use App\Order;
use Hash;

class ApiController extends Controller
{

    public function signUp(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:20',
                'dob' => 'required',
                'gender' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required|min:6',
                'photo' => 'mimes:jpeg,jpg,png,gif|required|max:2048'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // check for email dupliction
            $checkEmail = User::where('email', $request->email)
                ->where('status', '!=', 2)
                ->first();

            if($checkEmail) {
                return response([
                    'error'=> true,
                    'message' => 'User email already exists.',
                    'data' => null
                ], 200);
            } // email duplication check ends

            // create user instance
            $user = new User;
            $user->status = 1;
            $user->role = 'customer';
            $user->email = $request->email;
            $user->name = $request->name;
            $user->dob = date('Y-m-d', strtotime($request->dob));
            $user->gender = $request->gender;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            // set default tip and notify budget here
            $user->default_tip = 15;
            $user->notify_budget = 15;

            if($request->file('photo')) {
                UpdatePhotoAllSizes($request, 'users/', $user->photo);
                $user->photo = $request->photo->hashName();
            }

            $user->device_token = $request->device_token;
            // save record
            $user->save();

            // get user and check connect status
            $getUser = User::find($user->id);
            // check image
            $user->photo = $getUser->photo == '' ? 'default.png' : $getUser->photo;

            // create token
            $token = $user->createToken('Personal Access Token')->accessToken;
            $userData['access_token'] =  $token;
            $userData['token_type'] =  'Bearer';

            // create user data
            $userData['user'] =  array(
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'dob' => $user->dob,
                'image' => url("public/uploads/users/".$user->photo),
                'roll' => $user->role,
                'gender' => $user->gender,
                'phone' => $user->phone,
                'address' => $getUser->address,
                'connect_status' => $getUser->connect,
                'connect_venue_id' => '',
                'connect_host_type' => '',
                'order_id' => '',
                'status' => $user->status,
                'device_token' => $user->device_token
            );

            // get near venues
            $userData['nearVenues'] = $this->getTitus($user->id, $request->lat, $request->long);
            // get featured venues
            $userData['featuredVenues'] = $this->getSeljanKhatoon($user->id, $request->lat, $request->long);
            // get favorite venues
            $userData['favoriteVenues'] = $this->getNoyan($user->id, $request->lat, $request->long);

            // get user locations
            $getUserLocations = UserLocation::select('user_locations.id as location_id', 'user_locations.latitude', 'user_locations.longitude', 'user_locations.address')
                ->where('user_id', $user->id)
                ->get();

            $userData['userLocations'] = $getUserLocations;

            // return response
            return response()->json([
                'error' => false,
                'message' => 'User sign up successfully.',
                'data' => $userData,
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // sugnUp function ends

    public function signIn(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'errors' => true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            //check crediantials
            if(!Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 'customer', 'status' => 1])) {

                return response()->json([
                    'error' => true,
                    'message' => 'User authentication failed.',
                    'data' => null
                ], 200);

            } // crediential check ends

            // update device token
            $updateDeviceToken = User::where('email', $request->email)->update(['device_token' => $request->device_token]);
            // get the authentictaed user
            $user = $request->user();
            // check image
            $user->photo = $user->photo == '' ? 'default.png' : $user->photo;

            // get connected venue id and host type
            $connectedVenueID = '';
            $connectHostType =  '';
            $connectHostOrder =  '';

            if($user->connect == 1) {

                $connectedVenue = UserConnect::where('user_id', $user->id)->first();
                $connectedVenueID = $connectedVenue->venue_id;
                $connectHostType =  $connectedVenue->type;
                $connectHostOrder =  $connectedVenue->order_id;
            }

            // create token
            $token = $user->createToken('Personal Access Token')->accessToken;
            $userData['access_token'] =  $token;
            $userData['token_type'] =  'Bearer';
            // create user data
            $userData['user'] =  array(
                'user_id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'dob' => $user->dob,
                'image' => url("public/uploads/users/".$user->photo),
                'roll' => $user->role,
                'gender' => $user->gender,
                'phone' => $user->phone,
                'address' => $user->address,
                'connect_status' => $user->connect,
                'connected_venue_id' => $connectedVenueID,
                'connect_host_type' => $connectHostType,
                'order_id' => $connectHostOrder,
                'status' => $user->status,
                'device_token' => $request->device_token
            );

            // get near venues
            $userData['nearVenues'] = $this->getTitus($user->id, $request->lat, $request->long);
            // get featured venues
            $userData['featuredVenues'] = $this->getSeljanKhatoon($user->id, $request->lat, $request->long);
            // get favorite venues
            $userData['favoriteVenues'] = $this->getNoyan($user->id, $request->lat, $request->long);

            // get user locations
            $getUserLocations = UserLocation::select('user_locations.id as location_id', 'user_locations.latitude', 'user_locations.longitude', 'user_locations.address')
                ->where('user_id', $user->id)
                ->get();

            $userData['userLocations'] = $getUserLocations;
            // return response
            return response()->json([
                'error' => false,
                'message' => 'User sign in successfully.',
                'data' => $userData,
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // signIn function endsl

    public function emailVerify(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'errors' => true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // verify email
            $checkEmail = User::where('email', $request->email)
                ->where('role', 'customer')
                ->where('status', 1)
                ->first();

            if($checkEmail == null) {
                return response()->json([
                    'error' => true,
                    'message' => 'Invalid email address.',
                    'data' => null,
                ], 200);
            } else {

                // generate 4 digit code
                $code = rand(1000 , 9999);
                // update code in database
                $updateCode = User::find($checkEmail->id);
                $updateCode->security_code = $code;
                $updateCode->save();

                // email perameters
                $data["email"] = $request->email;
                $data["subject"] = 'Verification Code';
                $data["bodyMessage"] = "Your security code is: ".$code;

                //send email to user
                Mail::send('emails.general', $data, function($message) use ($data) {
                    $message->from(config('mail.from.address'));
                    $message->to($data["email"]);
                    $message->subject($data["subject"]);
                });

                $userData = array(
                    'user_id' => $checkEmail->id,
                    'email' => $checkEmail->email,
                );

                // return response
                return response()->json([
                    'error' => false,
                    'message' => 'Security code sent successfully.',
                    'data' => $userData
                ], 200);

            }  // checkEmail check ends

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // emailVerify function ends

    public function codeVerify(Request $request) {
        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'code' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'errors' => true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // check code from database
            $userCode = User::where('security_code', $request->code)
                ->where('id', $request->user_id)
                ->first();

            // return response as per condition
            if ($userCode != null) {
                return response()->json([
                    'error' => false,
                    'message' => 'Security code is verified.',
                    'data' => null
                ], 200);

            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Invalid security code provided.',
                    'data' => null
                ], 200);

            } // code check ends

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // verify code function ends

    public function updatePassword(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6',
                'user_id' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'errors' => true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // update user password in database
            $user = User::find($request->user_id);
            if ($user) {
                $user->password = Hash::make($request->password);
                $user->device_token = $request->device_token;
                $user->save();

                // check image
                $user->photo = $user->photo == '' ? 'default.png' : $user->photo;

                // get connected venue id and host type
                $connectedVenueID = '';
                $connectHostType =  '';
                $connectHostOrder = '';

                if($user->connect == 1) {

                    $connectedVenue = UserConnect::where('user_id', $user->id)->first();
                    $connectedVenueID = $connectedVenue->venue_id;
                    $connectHostType =  $connectedVenue->type;
                    $connectHostOrder =  $connectedVenue->order_id;
                }

                // create token
                $token = $user->createToken('Personal Access Token')->accessToken;
                $userData['access_token'] =  $token;
                $userData['token_type'] =  'Bearer';
                // create user data
                $userData['user'] =  array(
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'dob' => $user->dob,
                    'image' => url("public/uploads/users/".$user->photo),
                    'roll' => $user->role,
                    'gender' => $user->gender,
                    'phone' => $user->phone,
                    'address' => $user->address,
                    'connect_status' => $user->connect,
                    'connected_venue_id' => $connectedVenueID,
                    'connect_host_type' => $connectHostType,
                    'order_id' => $connectHostOrder,
                    'status' => $user->status,
                    'device_token' => $user->device_token
                );

                // get near venues
                $userData['nearVenues'] = $this->getTitus($user->id, $request->lat, $request->long);
                // get featured venues
                $userData['featuredVenues'] = $this->getSeljanKhatoon($user->id, $request->lat, $request->long);
                // get favorite venues
                $userData['favoriteVenues'] = $this->getNoyan($user->id, $request->lat, $request->long);

                // get user locations
                $getUserLocations = UserLocation::select('user_locations.id as location_id', 'user_locations.latitude', 'user_locations.longitude', 'user_locations.address')
                    ->where('user_id', $user->id)
                    ->get();

                $userData['userLocations'] = $getUserLocations;

                // return response
                return response()->json([
                    'error' => false,
                    'message' => 'User password updated successfully.',
                    'data' => $userData,
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'User not found',
                    'data' => null,
                ], 404);
            }


        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // update password function ends

    public function page(Request $request) {

        try {

            // get page name
            $page = $request->page_name;
            $pageData = [];
            $message = '';
            $status = '';
            switch ($page) {
                case "about-us":
                    // find page
                    $getPage = Page::find(1);
                    // get about-us page data
                    $pageData['text'] =  strip_tags($getPage->text);
                    //$pageData['text'] =  $getPage->text;
                    $pageData['video'] = $getPage->video;
                    $message = 'About-Us page found.';
                    $status = false;
                    break;
                case "help":
                    // find page
                    $getPage = Page::find(2);
                    // get help page data
                    $pageData['text'] = strip_tags($getPage->text);
                    $pageData['video'] = $getPage->video;
                    $message = 'Help page found.';
                    $status = false;
                    break;
                case "privacy-terms":
                    // find page
                    $getPage = Page::find(3);
                    // get terms page data
                    $pageData['terms-text'] = strip_tags($getPage->text);
                    // find page
                    $getPage1 = Page::find(4);
                    // get privacy page data
                    $pageData['privacy-text'] = strip_tags($getPage1->text);
                    $message = 'Privacy and Terms pages found.';
                    $status = false;
                    break;
                default:
                    // just set status as false
                    $status = true;
                    $message = 'No page found for your request.';

            } // switch condition for page ends

            // return response
            return response()->json([
                'error' => $status,
                'message' => $message,
                'data' => $pageData,
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // pages function ends

    public function updateProfile(Request $request) {

        // validate the posted data
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:20',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return response([
                'errors' => true,
                'message' => $validator->errors()->all(),
                'data' => null
            ], 200);
        }
        try {

            // find user
            $user = User::find($request->user_id);
            if ($user) {
                if($request->file('photo')) {

                    // validate the posted data
                    $validator = Validator::make($request->all(), [
                        'photo' => 'mimes:jpeg,jpg,png,gif|required|max:2048'
                    ]);

                    // return errors
                    if ($validator->fails()) {
                        return response([
                            'error'=> true,
                            'message' => $validator->errors()->all(),
                            'data' => null
                        ], 200);
                    }

                    UpdatePhotoAllSizes($request, 'users/', $user->photo);
                    $path = $request->photo->hashName();
                }
                $user->name = !empty($request->name) ? $request->name : $user->name;
                $user->dob = !empty($request->dob) ? $request->dob : $user->dob;
                $user->gender = !empty($request->gender) ? $request->gender : $user->gender;
                $user->phone = !empty($request->phone) ? $request->phone : $user->phone;
                $user->password = !empty($request->password) ? bcrypt($request->password) : $user->password;
                $user->photo = !empty($request->photo) ? $path : $user->photo;
                $user->address = !empty($request->address) ? $request->address : $user->address;
                // save record
                $user->save();

                // check image
                $user->photo = $user->photo == '' ? 'default.png' : $user->photo;

                // create user data
                $userData['user'] =  array(
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'dob' => $user->dob,
                    'image' => url("public/uploads/users/".$user->photo),
                    'gender' => $user->gender,
                    'phone' => $user->phone,
                    'address' => $user->address,
                );

                // return response
                return response()->json([
                    'error' => false,
                    'message' => 'User profile updated successfully.',
                    'data' => $userData,
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'User not found.',
                    'data' => null,
                ], 400);
            }

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // updateProfile function ends

    public function contactUs(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'subject' => 'required|max:50',
                'message' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // email perameters
            $data["email"] = $request->email;
            $data["subject"] = $request->subject;
            $data["bodyMessage"] = $request->message;

            //send email to user
            Mail::send('emails.general', $data, function($message) use ($data) {
                $message->from($data["email"]);
                $message->to(config('mail.from.address'));
                $message->subject($data["subject"]);
            });

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Your message has been sent successfully.',
                'data' => null,
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // contactUs function ends here

    public function setting(Request $request) {

        try {

            // find user
            $user = User::find($request->user_id);
            if ($user){
                // create user data
                $userData = array(
                    'default_tip' => $user->default_tip,
                    'notify_budget' => $user->notify_budget,
                );
                // return response
                return response()->json([
                    'error' => false,
                    'message' => null,
                    'data' => $userData
                ], 200);
            } else {
                // return response
                return response()->json([
                    'error' => true,
                    'message' => 'User not found',
                    'data' => null
                ], 404);
            }

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // setting function ends

    public function updateTip(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'default_tip' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // find user
            $user = User::find($request->user_id);
            $user->default_tip = $request->default_tip;
            $user->save();

            // create user data
            $userData =  array(
                'default_tip' => $user->default_tip,
                'notify_budget' => $user->notify_budget,
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Dafault tip update successfully.',
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // updateTip function ends

    public function updateBudget(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'notify_budget' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // find user
            $user = User::find($request->user_id);
            $user->notify_budget = $request->notify_budget;
            $user->save();

            // create user data
            $userData =  array(
                'default_tip' => $user->default_tip,
                'notify_budget' => $user->notify_budget,
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Notify budget update successfully.',
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // updateBudget function ends

    public function getNotificationStatus(Request $request) {

        try {

            // find user
            $user = User::find($request->user_id);

            // create user data
            $userData =  array(

                'user_id' => $user->id,
                'notification_status' => $user->notification_status,
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // getNotificationStatus function ends

    public function updateNotificationStatus(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'notification_status' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            // find user
            $user = User::find($request->user_id);
            $user->notification_status = $request->notification_status;
            $user->save();

            // create user data
            $userData =  array(
                'user_id' => $user->id,
                'notification_status' => $user->notification_status,
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Notification status updated successfully.',
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => $e->getMessage(),
                'data' => null,
            ], 401);

        }

    } // updateNotificationStatus function ends

    public function deleteAccount(Request $request) {

        try {

            // find user
            $user = User::find($request->user_id);
            $user->status = 2;
            $user->save();

            // revoke token
            $token = $request->user()->token();
            $token->revoke();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Account deleted successfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // deleteAccount function ends

    public function saveLocation(Request $request) {

        try {

            // check for latitude and longitude
            $checkLatLong = UserLocation::where('latitude', $request->latitude)
                ->where('longitude', $request->longitude)
                ->where('user_id', '=', $request->user_id)
                ->first();
            // check if data exists
            if($checkLatLong == null) {

                //save lovations
                $userLocation = new UserLocation;
                $userLocation->user_id = $request->user_id;
                $userLocation->latitude = $request->latitude;
                $userLocation->longitude = $request->longitude;
                $userLocation->address = $request->address;
                $userLocation->save();

            }

            // return response
            return response()->json([
                'error' => false,
                'message' => 'User location saved successfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // saveLocation function ends

    public function getLocation(Request $request) {

        try {

            $getLocations = UserLocation::where('user_id', $request->user_id)->get();

            // define array for locations
            $userLocations = [];

            // get locations in array
            foreach($getLocations as $location) {
                $userLocations['userLocations'][] = array(
                    'location_id' => $location->id,
                    'address' => $location->address
                );
            }

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userLocations
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // getLocation function ends

    public function deleteLocation(Request $request) {

        try {

            // delete user records
            $deleteLocations = UserLocation::where('id', $request->location_id)->delete();
            $userLocations = [];
            $getLocations = UserLocation::where('user_id', $request->user_id)->get();
            if ($getLocations){
                foreach($getLocations as $location) {
                    $userLocations['userLocations'][] = array(
                        'location_id' => $location->id,
                        'address' => $location->address
                    );
                }
            }
            // define array for locations


            // get locations in array

            // return response
            return response()->json([
                'error' => false,
                'message' => 'User location deleted successfully.',
                'data' => $userLocations
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // deleteLocation function ends

    public function saveFavorite(Request $request) {

        try {

            // get data and add/remove to favorites
            $getfavorites = UserFavoriteVenue::where('user_id', $request->user_id)
                ->where('venue_id', $request->venue_id)
                ->first();

            // set message for response
            $message = $request->status == 1 ? 'Venue added to favorites.' : 'Venue removed from favorites.';

            // check for record
            if($getfavorites != null) {

                // update record
                $updatefavorites = UserFavoriteVenue::where('user_id', $request->user_id)
                    ->where('venue_id', $request->venue_id)
                    ->update(['status' => $request->status]);

            } else {

                // add venue to favorites
                $addToFavorites = new UserFavoriteVenue;
                $addToFavorites->user_id = $request->user_id;
                $addToFavorites->venue_id = $request->venue_id;
                $addToFavorites->status = $request->status;
                $addToFavorites->save();

            }

            // return response
            return response()->json([
                'error' => false,
                'message' => $message,
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // saveFavorite function ends

    public function UserFavoriteVenue(Request $request) {

        try {

            $getfavoriteVenues = DB::table("venues")
                ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude", "venues.longitude", "venues.about", "user_favorite_venues.status as favorite"
                    ,DB::raw("6371 * acos(cos(radians(" . $request->lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $request->long . "))
		        + sin(radians(" .$request->lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
                ->leftjoin('user_favorite_venues', 'user_favorite_venues.venue_id', '=', 'venues.id')
                ->where('user_favorite_venues.status', 1)
                ->where('user_favorite_venues.user_id', $request->user_id)
                ->where('venues.status', 1)
                ->orderBy('distance', 'ASC')
                ->take(10)
                ->get();

            foreach($getfavoriteVenues as $favoriteVenue) {

                // get banner url
                $favoriteVenue->banner = $favoriteVenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$favoriteVenue->banner);

                // get average ratings
                $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                    ->where('venue_id', $favoriteVenue->venue_id)
                    ->where('status', 1)
                    ->first();
                $favoriteVenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

            }

            $userData['userFavoriteVenues'] = $getfavoriteVenues;

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }
    } // UserFavoriteVenue function ends

    public function saveReview(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'food_rating' => 'required',
                'service_rating' => 'required',
                'value_rating' => 'required',
                'venue_rating' => 'required',
                'comment' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            //average rating
            $averageReview = ($request->food_rating + $request->service_rating + $request->value_rating + $request->venue_rating) / 4;

            $addReview = new Review;
            $addReview->food_rating = $request->food_rating;
            $addReview->service_rating = $request->service_rating;
            $addReview->value_rating = $request->value_rating;
            $addReview->venue_rating = $request->venue_rating;
            $addReview->comment = $request->comment;
            $addReview->average_rating = $averageReview;
            $addReview->customer_id = $request->user_id;
            $addReview->venue_id = $request->venue_id;
            $addReview->waiter_id = $request->waiter_id;
            $addReview->order_id = $request->order_id;
            $addReview->save();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Review added successfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // saveReview function ends

    public function editReview(Request $request) {

        try {

            // check review in database
            $getReview = Review::find($request->review_id);

            // user review data array
            $userData = [];

            // check data exists
            if ($getReview != null) {

                $userData = array(
                    'review_id' => $getReview->id,
                    'food_rating' => $getReview->food_rating,
                    'service_rating' => $getReview->service_rating,
                    'value_rating' => $getReview->value_rating,
                    'venue_rating' => $getReview->venue_rating,
                    'average_rating' => $getReview->average_rating,
                    'comment' => $getReview->comment,
                    'user_id' => $getReview->customer_id,
                    'venue_id' => $getReview->venue_id,
                );

            }

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // editReview function ends

    public function updateReview(Request $request) {

        try {

            // validate the posted data
            $validator = Validator::make($request->all(), [
                'food_rating' => 'required',
                'service_rating' => 'required',
                'value_rating' => 'required',
                'venue_rating' => 'required',
                'comment' => 'required'
            ]);

            // return errors
            if ($validator->fails()) {
                return response([
                    'error'=> true,
                    'message' => $validator->errors()->all(),
                    'data' => null
                ], 200);
            }

            //average rating
            $averageReview = ($request->food_rating + $request->service_rating + $request->value_rating + $request->venue_rating) / 4;

            $addReview = Review::find($request->review_id);
            $addReview->food_rating = $request->food_rating;
            $addReview->service_rating = $request->service_rating;
            $addReview->value_rating = $request->value_rating;
            $addReview->venue_rating = $request->venue_rating;
            $addReview->comment = $request->comment;
            $addReview->average_rating = $averageReview;
            $addReview->save();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Review updated successfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // updateReview function ends

    public function getUserReview(Request $request) {

        try {
            // check review in database
            $getUserReviews = Review::select('venues.banner', 'venues.name as venue_name','orders.order_number', 'customer_table.name as customer_name', 'waiter_table.name as waiter_name', 'reviews.id as review_id', 'reviews.average_rating', 'reviews.comment', 'reviews.created_at')
                ->leftjoin('venues', 'venues.id', '=', 'reviews.venue_id')
                ->leftjoin('orders', 'orders.id', '=', 'reviews.order_id')
                ->leftjoin('users as customer_table','customer_table.id', '=', 'reviews.customer_id')
                ->leftjoin('users as waiter_table','waiter_table.id', '=', 'reviews.waiter_id')
                ->where('reviews.customer_id', $request->user_id)
                ->where('reviews.status', 1)
                ->get();

            // reviews array
            $userData = [];

            //return $getReview;
            foreach( $getUserReviews as $userReview) {

                // get banner url
                $userReview->banner = $userReview->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$userReview->banner);

                // change date format
                $date = date_format($userReview->created_at,"d-M Y").' at '.date('h:i a ', strtotime($userReview->created_at));

                $userData['userReviews'][] = array(
                    'review_id' => $userReview->review_id,
                    'banner' => $userReview->banner,
                    'order_number' =>  $userReview->order_number,
                    'waiter_name' => $userReview->waiter_name,
                    'venue_name' => $userReview->venue_name,
                    'average_rating' => $userReview->average_rating,
                    'comment' => $userReview->comment,
                    'date' => $date
                );
            }

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // getReview function ends

    public function deleteReview(Request $request) {

        try {

            // delete user review
            $deleteReview = Review::where('id', $request->review_id)->delete();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'Review deleted successfully.',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // deleteReview function ends

    public function kurtoglu(Request $request) {

        try {

            $lat = $request->lat;
            $long = $request->long;

            // store all data in this array
            $getKurtoglu = [];

            // get near venues
            $getKurtoglu['nearVenues'] = $this->getTitus($request->user_id, $lat, $long);
            // get featured venues
            $getKurtoglu['featuredVenues'] = $this->getSeljanKhatoon($request->user_id, $lat, $long);
            // get favorite venues
            $getKurtoglu['favoriteVenues'] = $this->getNoyan($request->user_id, $lat, $long);

            // get user locations
            $getUserLocations = UserLocation::select('user_locations.id as location_id', 'user_locations.latitude', 'user_locations.longitude', 'user_locations.address')
                ->where('user_id', $request->user_id)
                ->get();

            $getKurtoglu['userLocations'] = $getUserLocations;

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $getKurtoglu
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // kurtoglu function ends

    public function getTitus($user_id, $lat, $long) {

        $getNearVenues = DB::table("venues")
            ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude", "venues.longitude", 'venues.status'
                ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $long . "))
		        + sin(radians(" .$lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
            ->orderBy('distance', 'ASC')
            ->where('venues.status', 1)
            ->take(10)
            ->get();

        foreach($getNearVenues as $nearVenue) {
            // get banner url
            $nearVenue->banner = $nearVenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$nearVenue->banner);

            // get average ratings
            $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                ->where('venue_id', $nearVenue->venue_id)
                ->where('status', 1)
                ->first();
            $nearVenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

            // check favourites
            $getfavorite = UserFavoriteVenue::where('user_id', $user_id)
                ->where('venue_id', $nearVenue->venue_id)
                ->first();
            $nearVenue->favorite = $getfavorite == null ? '0' : $getfavorite->status;
            $nearVenue->distance = round($nearVenue->distance,2);

        }
        return $getNearVenues;

    } // getTitus function ends

    public function getSeljanKhatoon($user_id, $lat, $long) {

        $getFeaturedVenues = DB::table("venues")
            ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude", "venues.longitude"
                ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $long . "))
		        + sin(radians(" .$lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
            ->where('featured_status', 1)
            ->where('status', 1)
            ->orderBy('distance', 'ASC')
            ->take(10)
            ->get();

        foreach($getFeaturedVenues as $featuredVenue) {

            // get banner url
            $featuredVenue->banner = $featuredVenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$featuredVenue->banner);

            // get average ratings
            $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                ->where('venue_id', $featuredVenue->venue_id)
                ->where('status', 1)
                ->first();
            $featuredVenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

            // check favourites
            $getfavorite = UserFavoriteVenue::where('user_id', $user_id)
                ->where('venue_id', $featuredVenue->venue_id)
                ->first();

            $featuredVenue->favorite = $getfavorite == null ? '0' : $getfavorite->status;

            $featuredVenue->distance = round($featuredVenue->distance,2);
        }

        return $getFeaturedVenues;

    } // getSeljanKhatoon function ends

    public function getNoyan($user_id, $lat, $long) {

        $getfavoriteVenues = DB::table("venues")
            ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude", "venues.longitude", "user_favorite_venues.status as favorite"
                ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $long . "))
		        + sin(radians(" .$lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
            ->leftjoin('user_favorite_venues', 'user_favorite_venues.venue_id', '=', 'venues.id')
            ->where('user_favorite_venues.status', 1)
            ->where('user_favorite_venues.user_id', $user_id)
            ->where('venues.status', 1)
            ->orderBy('distance', 'ASC')
            ->take(10)
            ->get();

        foreach($getfavoriteVenues as $favoriteVenue) {

            // get banner url
            $favoriteVenue->banner = $favoriteVenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$favoriteVenue->banner);

            // get average ratings
            $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                ->where('venue_id', $favoriteVenue->venue_id)
                ->where('status', 1)
                ->first();
            $favoriteVenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

            $favoriteVenue->distance = round($favoriteVenue->distance,2);

        }

        return $getfavoriteVenues;

    } // getNoyan function ends

    public function getAllVenue(Request $request) {

        try {

            $getAllVenues = DB::table("venues")
                ->select("venues.id as venue_id", "venues.name", "venues.banner", "venues.latitude", "venues.longitude", 'venues.about as venue_description'
                    ,DB::raw("6371 * acos(cos(radians(" . $request->lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $request->long . "))
		        + sin(radians(" .$request->lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
                ->where('status', 1)
                ->orderBy('distance', 'ASC')
                ->get();

            foreach($getAllVenues as $allVenue) {

                // get banner url
                $allVenue->banner = $allVenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$allVenue->banner);

                // get average ratings
                $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                    ->where('venue_id', $allVenue->venue_id)
                    ->where('status', 1)
                    ->first();
                $allVenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

                // check favourites
                $getfavorite = UserFavoriteVenue::where('user_id', $request->user_id)
                    ->where('venue_id', $allVenue->venue_id)
                    ->first();
                $allVenue->favorite = $getfavorite == null ? '0' : $getfavorite->status;
            }

            $venueListingData['allVenues'] = $getAllVenues;

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $venueListingData
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // getvenue function ends

    public function singlevenue(Request $request) {

        try {

            $getSinglevenue = DB::table("venues")
                ->select("venues.*", "venues.id as venue_id"
                    ,DB::raw("6371 * acos(cos(radians(" . $request->lat . "))
		        * cos(radians(venues.latitude))
		        * cos(radians(venues.longitude) - radians(" . $request->long . "))
		        + sin(radians(" .$request->lat. "))
		        * sin(radians(venues.latitude))) AS distance"))
                ->where('id', $request->venue_id)
                ->first();

            // get banner url
            if ($getSinglevenue){
                $getSinglevenue->banner = $getSinglevenue->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$getSinglevenue->banner);

                // get average ratings
                $getReview = UserVenueReview::select(DB::raw('avg(average_rating) as average_rating'))
                    ->where('venue_id', $getSinglevenue->id)
                    ->where('status', 1)
                    ->first();

                $getSinglevenue->average_rating = $getReview->average_rating == null ? '0.00' : $getReview->average_rating;

                // check favourites
                $getfavorite = UserFavoriteVenue::where('user_id', $request->user_id)
                    ->where('venue_id', $getSinglevenue->id)
                    ->first();
                $getSinglevenue->favorite = $getfavorite == null ? '0' : $getfavorite->status;

                // return response
                return response()->json([
                    'error' => false,
                    'message' => null,
                    'data' => $getSinglevenue
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => "Venue does not exist",
                    'data' => null
                ], 200);
            }


        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // singlevenue function ends

    public function getVenueReview(Request $request) {

        try {

            $getVenueReview = Venue::join('reviews', 'reviews.venue_id', '=', 'venues.id')
                ->select('venues.id as venue_id', 'venues.name', 'venues.banner', DB::raw('avg(reviews.food_rating) as food_rating'), DB::raw('avg(reviews.service_rating) as service_rating'),  DB::raw('avg(reviews.value_rating) as value_rating'),  DB::raw('avg(reviews.venue_rating) as venue_rating'))
                ->where('venues.id', $request->venue_id)
                ->where('reviews.status', 1)
                ->first();
            if ($getVenueReview) {
                // get banner url
                $getVenueReview->banner = $getVenueReview->banner == null ? url('public/uploads/banners/default.png') : url('public/uploads/banners/'.$getVenueReview->banner);

                // return response
                return response()->json([
                    'error' => false,
                    'message' => null,
                    'data' => $getVenueReview
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => 'Venue not exist',
                    'data' => null
                ], 200);
            }


        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // getVenueReview function ends

    public function getVenueInfo(Request $request) {

        try {

            $getVenueInfo = Venue::select('venues.id as venue_id', 'venues.name as venue_name', 'venues.about', 'venues.latitude', 'venues.longitude', 'venues.avg_cost', 'venues.notice as trading_comment', 'atmospheres.name as atmosphere_name', 'cuisines.name as cuisine_name', 'users.name as contact_name', 'users.email as conatct_email', 'users.phone as contact_phone')
                ->leftjoin('atmospheres', 'atmospheres.id', '=', 'venues.atmosphere_id')
                ->leftjoin('cuisines', 'cuisines.id', '=', 'venues.cuisine_id')
                ->leftjoin('users', 'users.venue_id', '=', 'venues.id')
                ->where('venues.id', $request->venue_id)
                ->where('atmospheres.status', 1)
                ->where('cuisines.status', 1)
                ->where('users.login_type', 'Mighty Super Venue Admin')
                ->first();
            if ($getVenueInfo){
                $venueInfo = [];

                // get dietaries
                $getDietaries = Dietary::leftjoin('venue_dietaries', 'venue_dietaries.dietary_id', '=', 'dietaries.id')
                    ->where('venue_dietaries.venue_id', $request->venue_id)
                    ->where('dietaries.status', 1)
                    ->pluck('name')->toArray();

                // get facilities
                $getFaccilties = Facility::leftjoin('facility_venues', 'facility_venues.facility_id', '=', 'facilities.id')
                    ->where('facility_venues.venue_id', $request->venue_id)
                    ->where('facilities.status', 1)
                    ->pluck('name')->toArray();

                // assign dietaries and facilities to venue array
                $getVenueInfo->dietaries_name = implode(',', $getDietaries);
                $getVenueInfo->facilities_name = implode(',', $getFaccilties);

                // array for storing trading hours
                $getTradingHours = [];

                // get venue timings
                $getBookingDays = BookingDay::All();
                foreach($getBookingDays as $day) {

                    //get trading hours for specific day
                    $tradingHours = TradingHour::where('booking_id', $day->id)
                        ->where('venue_id', $getVenueInfo->venue_id)
                        ->first();

                    $getTradingHours[$day->day] = array(
                        'from' => $tradingHours->from,
                        'to' => $tradingHours->to,
                        'is_closed' => $tradingHours->isclosed,
                    );
                }

                $venueInfo['venueInfo'] = $getVenueInfo;
                $venueInfo['tradingHours'] = $getTradingHours;


                // return response
                return response()->json([
                    'error' => false,
                    'message' => null,
                    'data' => $venueInfo
                ], 200);
            } else {
                // return response
                return response()->json([
                    'error' => true,
                    'message' => 'Venue Not found',
                    'data' => null
                ], 200);
            }


        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // getVenueInfo function ends

    public function connect(Request $request) {

        try {

            //get tables
            $getTables = Table::where('status', 1)
                ->where('occupied', 0)
                ->where('venue_id', $request->venue_id)
                ->get();

            //$venue Data
            $venueData = [];

            foreach($getTables as $table) {
                $venueData['tables'][] = array(
                    "table_id" => $table->id,
                    "table_name" => $table->name

                );
            }

            // get menus
            $getMenues = Menu::where('status', 1)
                ->where('venue_id', $request->venue_id)
                ->get();

            foreach($getMenues as $menu) {
                $venueData['menus'][] = array(
                    "menu_id" => $menu->id,
                    "menu_name" => $menu->name

                );
            }

            // get waiters
            $getWaiters = User::where('status', 1)
                ->where('role', 'waiter')
                ->where('venue_id', $request->venue_id)
                ->get();

            foreach($getWaiters as $waiter) {
                $venueData['waiters'][] = array(
                    "waiter_id" => $waiter->id,
                    "waiter_name" => $waiter->name

                );
            }

            // return only active host in the venue
            $getActiveHost = User::leftjoin('user_connects', 'user_connects.user_id', '=', 'users.id')
                ->where('users.connect', 1)
                ->where('user_connects.type', 'host')
                ->get();

            // active host array in case of null
            $venueData['activeHosts'] = [];
            foreach($getActiveHost as $activeUser) {
                $venueData['active_hosts'][] = array(
                    "host_id" => $activeUser->id,
                    "host__name" => $activeUser->name

                );
            }

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $venueData
            ], 200);


        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // connect function ends

    public function userConnect(Request $request) {

        try {

            $connect = 0;
            $hostOrderID = '';

            // store data to database
            $userConnect = new UserConnect;
            $userConnect->type = $request->type;
            $userConnect->host_pin = $request->host_pin;
            $userConnect->table_id = $request->table_id;
            $userConnect->waiter_id = $request->waiter_id;
            $userConnect->user_id = $request->user_id;
            $userConnect->venue_id = $request->venue_id;

            if ($request->type == 'host') {

                $userConnect->auto_accept = $request->auto_accept;
                $userConnect->budget = $request->budget;
                $userConnect->menu_id = $request->menu_id;
                $userConnect->status = 1;

                // get order number
                $order_number = Order::max('order_number') + 1;
                $order_number = $order_number < 1000 ? 1001 : $order_number;
                // create order thread for this host
                $order = new Order;
                $order->order_number = $order_number;
                $order->invoice_number = $order_number;
                $order->venue_id = $request->venue_id;
                $order->customer_id = $request->user_id;
                $order->waiter_id = $request->waiter_id;
                $order->table_id = $request->table_id;
                $order->order_status = 'In Progress';
                $order->connect_as = $request->type;
                $order->host_pin = $request->host_pin;
                $order->status = 1;
                // save order
                $order->save();

                // save this order ID in user connect table for relation
                $userConnect->order_id = $order->id;
                $userConnect->save();

                // update user connect status in users table
                $updateConnectStatus = User::where('id', $request->user_id)
                    ->update(['connect' => 1]);
                // connect status
                $connect = 1;
                $hostOrderID = $order->id;

            } else {

                // check if pin and user is valid
                $checkHost = UserConnect::where('host_pin', $request->host_pin)
                    ->where('user_id', $request->host_id)
                    ->where('venue_id', $request->venue_id)
                    ->where('type', 'host')
                    ->where('status', 1)
                    ->first();

                // if host exists
                if($checkHost) {

                    $hostOrderID = $checkHost->order_id;

                    // if auto accept is on
                    if($checkHost->auto_accept == 1) {

                        // store data to database
                        $userConnect->menu_id = $checkHost->menu_id;
                        $userConnect->order_id = $hostOrderID;
                        $userConnect->status = 1;
                        $userConnect->save();

                        // update user connect status in users table
                        $updateConnectStatus = User::where('id', $request->user_id)
                            ->update(['connect' => 1]);
                        // connect status
                        $connect = 1;

                    } else {

                        // store data to database
                        $userConnect->menu_id = $checkHost->menu_id;
                        $userConnect->order_id = $hostOrderID;
                        $userConnect->status = 0;
                        $userConnect->save();

                        // connect status
                        $connect = 0;


                        // $path_To_from='https://fcm.googleapis.com/fcm/send';
                        //       $server_key="";
                        //        $headers = array
                        //        (
                        //        	'Content-Type: application/json',
                        //        	'Authorization: key=' . $server_key

                        //        );

                        //        $fields = array
                        //        (
                        //        	'to' => $request->device_token,
                        //        	'notification' => array('title'=>'Confirmation', 'body' => 'Do you want to confirm user as guest?'),
                        //            'data' => array(
                        //                'user_id'=> $request->user_id,
                        //                'venue_id'=> $request->venue_id,
                        //                'pin'=> $request->host_pin,
                        //                )
                        //        );

                        //       $payload=json_encode($fields);

                        //       $ch = curl_init();
                        //       curl_setopt( $ch,CURLOPT_URL, $path_To_from );
                        //       curl_setopt( $ch,CURLOPT_POST, true );
                        //       curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                        //       curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                        //       curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                        //       //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
                        //       curl_setopt( $ch,CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                        //       curl_setopt( $ch,CURLOPT_POSTFIELDS, $payload );
                        //       $result = curl_exec($ch);
                        //       curl_close( $ch);

                    } // auto accept check ends

                } else {

                    // return response for host not found
                    return response()->json([
                        'error' => true,
                        'message' => 'Host not found.',
                        'data' => null
                    ], 200);

                } // host exists if ends

            } // host and guest check ends

            // response data
            $userData = array(
                "order_id" => $hostOrderID,
                "connect_status" => $connect,
                "connected_venue_id" => $request->venue_id,
                "connect_host_type" => $request->type
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // UserConnect function ends

    public function userConnectConfirmation(Request $request) {

        try {

            // confirm the user as guest
            $confirmUser = UserConnect::where('user_id', $request->user_id)
                ->where('venue_id', $request->venue_id)
                ->where('order_id', $request->order_id)
                ->update(['status' => 1]);

            // change status to connect of this user
            $user = User::find($request->user_id);
            $user->connect = 1;
            $user->save();

            // response data
            $userData = array(
                "order_id" => $request->order_id,
                "connect_status" => $user->connect,
                "connected_venue_id" => $request->venue_id,
                "connect_host_type" => 'guest'
            );

            // return response
            return response()->json([
                'error' => false,
                'message' => null,
                'data' => $userData
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);

        }

    } // userConnectConfirmation function ends

    public function logout(Request $request) {

        try {

            // revoke token
            $token = $request->user()->token();
            $token->revoke();

            // return response
            return response()->json([
                'error' => false,
                'message' => 'You have been succesfully logged out!',
                'data' => null
            ], 200);

        } catch(\Exception $e) {

            return response()->json([
                'error' => true,
                'message' => [$e->getMessage()],
                'data' => null,
            ], 401);
        }

    } // logout function is end

}
