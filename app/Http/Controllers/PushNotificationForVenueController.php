<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PushNotification;
use Auth;
use App\PushNotificationUser;
use App\User;
use App\Order;
use App\TrailLog;

class PushNotificationForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index() {
		$counter = 1;
		$push_notifications = PushNotification::where('created_by', Auth::user()->id)->orderBy('id', 'desc')->get();
		return view('venue.push-notifications', compact('push_notifications', 'counter'));
	}
	
	public function create(){
		$venue_id = Auth::user()->venue_id;
		$customers = Order::join('users', 'orders.customer_id', 'users.id')->select('users.name', 'users.id')->where('orders.venue_id', $venue_id)->groupBy('customer_id')->get();
		return view('venue.push-notification-create', compact('customers'));
	}
	
	public function save(Request $request){
		
		if($request->user_id == '') {
			return redirect(route('venue.push-notification.create'))->with('error', 'Please select users to send notification.');
		}
		
		$venue_id = Auth::user()->venue_id;
		
		$push_notification = new PushNotification;
		
		$push_notification->venue_id = $venue_id;
		
		// if(isset($request->sending_to)){
		// 	$push_notification->sending_to = $request->sending_to;
		// }
		
		
		//$request->notification_users
		$push_notification->subject = $request->subject;
		$push_notification->body = $request->body;
		$push_notification->push_notification_status = $request->push_notification_status;
		
		$push_notification->created_by = Auth::user()->id;
		
		if($request->push_notification_status == 'Sent') {
			$push_notification->status = 1;
			$push_notification->sending_date_time = date('Y-m-d H:i:s');
		} else {
			if($request->sending_date_time == ''){
				return redirect(route('venue.push-notification.create'))->with('error', 'Incorrect date format.');
			}
			$push_notification->sending_date_time = $request->sending_date_time;
		}
		
		if($push_notification->save()) {
			
			$users = json_decode($request->user_id);
			
			foreach($users as $user_id){
				$push_notification_user = new PushNotificationUser;
				$push_notification_user->push_notification_id = $push_notification->id;
				$push_notification_user->user_id = $user_id;
				$push_notification_user->save();
			}

		// insert log
		$venueLog = new TrailLog;
		$venueLog->event = 'Push Notification';
		$venueLog->event_id = $push_notification->id;
		$venueLog->event_type = 'Push Notification Added';
		$venueLog->event_message = Auth::user()->name.' added new push notification by subject '.$request->subject.'.';
		$venueLog->user_id = Auth::user()->id;
		$venueLog->user_name = Auth::user()->name;
		$venueLog->user_role = Auth::user()->role;
		$venueLog->venue_id = $venue_id;
		$venueLog->save();
			
			return redirect(route('venue.push-notifications'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.push-notifications'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit(Request $request, $id){
		$push_notification = PushNotification::find($id);
		if($push_notification->created_by != Auth::user()->id){
			return abort('404');
		}
		return view('venue.push-notification-edit', compact('push_notification'));
	}
	
	public function cancel($id) {
		$push_notification = PushNotification::find($id);
		$push_notification->push_notification_status = 'Cancelled';
		$push_notification->save();

		// insert log
		$venueLog = new TrailLog;
		$venueLog->event = 'Push Notification';
		$venueLog->event_id = $id;
		$venueLog->event_type = 'Push Notification Cancelled';
		$venueLog->event_message = Auth::user()->name.' cancelled push notification by subject '.$push_notification->subject.'.';
		$venueLog->user_id = Auth::user()->id;
		$venueLog->user_name = Auth::user()->name;
		$venueLog->user_role = Auth::user()->role;
		$venueLog->venue_id = Auth::user()->venue_id;
		$venueLog->save();

		if($push_notification->created_by != Auth::user()->id){
			return abort('404');
		}
		return redirect(route('venue.push-notifications'))->with('success', 'Push notification has been cancelled.');
	}
}
