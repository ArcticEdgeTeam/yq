<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Refund;
use App\User;
use App\Order;
use App\Transaction;

class RefundController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$refunds = Refund::orderBy('id', 'desc')->get();
		$counter = 1;
		return view('admin.refunds', compact('refunds', 'counter'));
	}
	
	public function edit($id){
		$refund = Refund::find($id);
		$customer = User::find($refund->customer_id);
		if($customer->photo == ''){
			$customer->photo = 'default.png';
		}
		$order = Order::find($refund->order_id);
		$waiter = User::find($order->waiter_id);
		return view('admin.refund-edit', compact('refund', 'customer', 'order', 'waiter'));
	}
	
	public function update(Request $request, $id){
		$refund = Refund::find($id);
		$refund->reply = $request->reply;
		$refund->reason = $request->reason;
		$refund->refund_status = $request->refund_status;
		$refund->save();
		return redirect(route('admin.refunds'))->with('success', 'Record has been updated.');
	}
	
	public function approve(Request $request, $id){
		$refund = Refund::find($id);
		$refund->reply = $request->reply;
		$refund->refund_status = 'Refunded';
		$refund->save();
		$order = Order::find($refund->order_id);
		$order->order_status = 'Refunded';
		$order->save();
		$transaction = Transaction::where('order_id', $order->id)->first();
		$transaction->transaction_status = 'Refunded';
		$transaction->save();
		return redirect(route('admin.refunds'))->with('success', 'Record has been updated.');
	}
	
	public function cancel(Request $request, $id){
		$refund = Refund::find($id);
		$refund->reply = $request->reply;
		$refund->refund_status = 'Cancelled';
		$refund->save();
		return redirect(route('admin.refunds'))->with('success', 'Record has been updated.');
	}
	
}
