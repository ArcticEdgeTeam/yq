<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use Carbon\Carbon;

class TransactionController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		$transactions = Transaction::orderBy('id', 'desc')->get();
		$counter = 1;
		$start = date('d-m-Y');
		$end = date('d-m-Y');
		return view('admin.transactions', compact('counter', 'transactions','start', 'end'));
	}
	
	public function filter(Request $request){
		
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			
			$transactions = Transaction::orderBy('id', 'desc')->whereBetween('created_at', [$start, $end])->get();
			$counter = 1;
			
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			return view('admin.transactions', compact('counter', 'transactions', 'start', 'end'));
			
		}
		
		return abort(404);
		
	}
}
