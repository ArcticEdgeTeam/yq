<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\User;
use App\Bar;
use Storage;
use Auth;

class BarForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();

		foreach($bars as $bar) {
			// get Prep staff 
			$prepStaff = User::select('users.*', 'manager_prepareas.*')
								->leftjoin('manager_prepareas', 'manager_prepareas.user_id', '=', 'users.id')
								->where('manager_prepareas.venue_id', $id)
								->where('manager_prepareas.bar_id', $bar->id)
								->where('users.status', 1)
								->pluck('name')->toArray();
			
			if($prepStaff != null) {
				$bar->prep_staff = implode(',',$prepStaff);
			} else {
				$bar->prep_staff = 'N/A';
			}
		}

		$counter = 1;
		return view('venue.bars', compact('venue', 'counter', 'bars'));
	}
	
	public function create(){
		$id = Auth::user()->venue_id;
		$managers = User::where('venue_id', $id)->where('role', 'bar')->where('status', 1)->get();
		$venue = Venue::find($id);
		return view('venue.bar-create', compact('venue', 'managers'));
	}
	
	public function save(Request $request){


		$validatedData = $request->validate([
            'name' => 'required',
            'bar_id' => 'required',
        ]);

        $id = Auth::user()->venue_id;

        // check for duplicate Prep name
        $getPrepId = Bar::where('bar_id', $request->bar_id)->where('venue_id', $id)->first();

        if($getPrepId != null) {

        	return redirect(route('venue.bar.create'))->with('error', 'This Prep ID already exists for this venue.');
        }

		$bar = new Bar;
		$bar->name = $request->name;
		$bar->bar_id = $request->bar_id;
		$bar->venue_id = $id;
		// $bar->user_id = $request->user_id;
		
		if(isset($request->status)){
			$bar->status = 1;
		}else{
			$bar->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('bars/', ['disk' => 'public']);
			$bar->image = $request->image->hashName();
		}
		
		if($bar->save()){
			return redirect(route('venue.bars', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.bars', $id))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($bar_id){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$bar = Bar::find($bar_id);
		if($bar->image == ''){
			$bar->image = 'default.png';
		}
		$managers = User::where('venue_id', $id)->where('role', 'bar')->where('status', 1)->get();
		return view('venue.bar-edit', compact('venue', 'bar', 'managers'));
	}
	
	public function update(Request $request, $bar_id){

		$validatedData = $request->validate([
            'name' => 'required',
            'bar_id' => 'required',
        ]);

        $id = Auth::user()->venue_id;

        $getPrepId = Bar::where('bar_id', $request->bar_id)
								->where('venue_id', $id)
								->where('id', '!=', $bar_id)
								->first();
			
			
		if ($getPrepId != null) {

			return redirect(route('venue.bar.edit', $bar_id))->with('error', 'This Prep ID already exists for this venue.');
			
		}

		$bar = Bar::find($bar_id);
		$bar->name = $request->name;
		$bar->bar_id = $request->bar_id;
		$bar->venue_id = $id;
		$bar->user_id = $request->user_id;
		
		if(isset($request->status)){
			$bar->status = 1;
		}else{
			$bar->status = 0;
		}
		
		if($request->file('image')){
			Storage::disk('public')->delete('bars/' . $bar->image);
			$request->file('image')->store('bars/', ['disk' => 'public']);
			$bar->image = $request->image->hashName();
		}
		
		if($bar->save()){
			return redirect(route('venue.bars'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.bars'))->with('error', 'Record has not been updated.');
		}
	}
}
