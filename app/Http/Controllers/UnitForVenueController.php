<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Unit;
use App\Venue;
use Auth;

class UnitForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		$counter = 1;
		$units = Unit::all();
		return view('admin.units', compact('units', 'counter'));
	}
	
	public function create(){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		return view('venue.unit-create', compact('id', 'venue'));
	}
	
	public function save(Request $request){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

			// check dupliacte Unit for venue
        $getUnit = Unit::where(function($query) use ($request, $id){
								    $query->where('name', $request->name);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id){
        							$query->where('symbol', $request->symbol);
									$query->where('venue_id', $id);
        						})->first();

        if ($getUnit != null) {

			return redirect(route('venue.unit.create'))->with('error', 'Unit name or symbol already exists.');
		}

		$unit = new Unit;
		$unit->name = $request->name;
		$unit->symbol = $request->symbol;
		$unit->venue_id = $id;
		
		if(isset($request->status)){
			$unit->status = 1;
		}else{
			$unit->status = 0;
		}
		
		if($unit->save()){
			return redirect(route('venue.ingredients'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.ingredients'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($unit_id){
		$unit = Unit::find($unit_id);
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		return view('venue.unit-edit', compact('id', 'unit', 'venue'));
	}
	
	
	public function update(Request $request, $unit_id){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

		// check dupliacte Unit for venue
        $getUnit = Unit::where(function($query) use ($request, $id, $unit_id){
								    $query->where('name', $request->name);
								    $query->where('id', '!=', $unit_id);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id, $unit_id){
        							$query->where('symbol', $request->symbol);
        							$query->where('id', '!=', $unit_id);
									$query->where('venue_id', $id);
        						})->first();

        if ($getUnit != null) {

			return redirect(route('venue.unit.edit', [$id, $unit_id]))->with('error', 'Unit name or symbol already exists.');
		}
		
		$unit = Unit::find($unit_id);
		$unit->name = $request->name;
		$unit->symbol = $request->symbol;
		
		if(isset($request->status)){
			$unit->status = 1;
		}else{
			$unit->status = 0;
		}
		
		if($unit->save()){
			return redirect(route('venue.ingredients', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.ingredients', $id))->with('error', 'Record has not been updated.');
		}
		
	}
}
