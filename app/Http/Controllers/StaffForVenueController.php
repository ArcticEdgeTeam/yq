<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Venue;
use App\Table;
use App\TableWaiter;
use Storage;
use Hash;
use Auth;
use App\TrailLog;
use App\Bar;
use App\ManagerPrepArea;
use Carbon\Carbon;

class StaffForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(Request $request) {
		$id = Auth::user()->venue_id;
		
		$venue = Venue::find($id);

		$getStaff = User::query();
		$getStaff = $getStaff->where(function ($query) use ($id){
				$query->where('venue_id', '=',  $id);
			})->where(function ($query) {
				$query->where('role', '=', 'bar')
				->orWhere('role', '=', 'waiter');
			});

		if($request->name != '') {
			$getStaff = $getStaff->where('name', 'like', '%' . $request->name . '%');
		}

		if($request->role != '') {
			$getStaff = $getStaff->where('role', $request->role);
		}

		if($request->status != '') {
			$getStaff = $getStaff->where('status', $request->status);
		}

		$getStaff = $getStaff->orderBy('id', 'desc')->paginate(15);
		
		return view('venue.staff', compact('staff', 'venue', 'getStaff'));
	}
	
	public function create(){
		$id = Auth::user()->venue_id;
		$tables = Table::where('status', 1)->get();
		$venue = Venue::find($id);
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		return view('venue.staff-create', compact('venue', 'tables', 'bars'));
	}
	
	public function save(Request $request) {

		// check user id
		$checkUser = User::where('user_id', $request->user_id)
							->where('venue_id', Auth::user()->venue_id)
							->first();
		if($checkUser != null) {
			return redirect(route('venue.staff'))->with('error', 'User ID already exists for this venue.');
		}

		$id = Auth::user()->venue_id;
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->role = $request->role;
		// $user->address = $request->address;
		// $user->gender = $request->gender;
		$user->about = $request->about;
		$user->user_id = $request->user_id;
		$user->venue_id = $id;
		// $user->dob = date('Y-m-d', strtotime($request->dob));

		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();
		}

		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}
		
		if($request->file('photo')){
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){
			
			if(is_array($request->bar_id)) {
				foreach($request->bar_id as $bar) {
					$PrepUser = new ManagerPrepArea;
					$PrepUser->user_id = $user->id;
					$PrepUser->bar_id = $bar;
					$PrepUser->venue_id = $id;
					$PrepUser->save();
				}
			}

			if ($request->role == 'bar') {
				$role = 'manager';
			} else {
				$role = 'waiter';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Staff';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Staff Added';
			$venueLog->event_message = Auth::user()->name.' added '.$role.' staff '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();
			
			return redirect(route('venue.staff'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.staff'))->with('success', 'Record has not been added.');
		}
		
		
	}
	
	public function edit($user_id){
		$id = Auth::user()->venue_id;
		$tables = Table::where('status', 1)->get();
		$venue = Venue::find($id);
		$user = User::find($user_id);
		if($user->dob != ''){
			$user->dob = date('d-m-Y', strtotime($user->dob));
		}

		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		// get user bars
		$getUserBars = ManagerPrepArea::where('user_id', $user_id)->get();

		return view('venue.staff-edit', compact('venue', 'tables', 'user', 'bars', 'getUserBars'));
	}
	
	public function update(Request $request, $user_id){

		// check user id
		$checkUser = User::where('user_id', $request->user_id)
							->where('venue_id', Auth::user()->venue_id)
							->where('id', '!=', $user_id)
							->first();
		if($checkUser != null) {
			return redirect(route('venue.staff'))->with('error', 'User ID already exists for this venue.');
		}
		
		$id = Auth::user()->venue_id;
		$user = User::find($user_id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->gender = $request->gender;
		$user->about = $request->about;
		$user->user_id = $request->user_id;
		$user->dob = date('Y-m-d', strtotime($request->dob));

		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Veue Staff Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated staff '.$request->name.' password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = Auth::user()->venue_id;
			$venueLog->save();
		}
		
		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}
		
		if($request->file('photo')){
			Storage::disk('public')->delete('users/' . $user->photo);
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){
			
			if(is_array($request->bar_id)) {

				ManagerPrepArea::where('user_id', $user_id)->delete();
				
				foreach($request->bar_id as $bar) {
					$PrepUser = new ManagerPrepArea;
					$PrepUser->user_id = $user->id;
					$PrepUser->bar_id = $bar;
					$PrepUser->venue_id = $id;
					$PrepUser->save();
				}
			}

			if ($user->role == 'bar') {
				$role = 'manager';
			} else {
				$role = 'waiter';
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Staff';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Staff Updated';
			$venueLog->event_message = Auth::user()->name.' update '.$role.' staff '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();
			
			return redirect(route('venue.staff'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.staff'))->with('success', 'Record has not been updated.');
		}
		
	}
}
