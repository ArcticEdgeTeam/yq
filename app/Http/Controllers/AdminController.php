<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Venue;
use Hash;
use Storage;
use App\Setting;
use App\Transaction;
use App\Order;
use App\Booking;
use App\View;
use Carbon\Carbon;
use App\OrderedProduct;
use DB;
use App\Review;
use App\Bar;
use App\Product;
use App\Table;
use Image;
use App\TrailLog;

class AdminController extends Controller
{
    //
	
	public function __construct()
    {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
	public function index(){
		
		if(Venue::exists()){
				
		$id = Venue::all()->first()->id;
		$venue_id = $id;
		$venues = Venue::all();
		
		$venue = Venue::find($id);
		$current_orders = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->count();
		$first_order = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'asc')->first();
		$pending_orders = Order::where('venue_id', $id)->where('order_status', 'Ordered')->count();
		$last_order = Order::where('venue_id', $id)->where('order_status', 'Ordered')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->first();
		
		$orders_today = Order::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$sales_today = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->sales;
		if($sales_today == ''){
			$sales_today = 0;
		}
		
		$earnings_today = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as admin_commission'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->admin_commission;
		if($earnings_today == ''){
			$earnings_today = 0;
		}
		
		
		$earnings_this_month = Order::where('venue_id', $id)->where('order_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');
		
		$customers_today = User::where('venue_id', $id)->where('role', 'customer')->whereDate('created_at', Carbon::today())->count();
		$bookings_today = Booking::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		$views_today = View::where('module', 'venue')->where('module_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$orders_lifetime = Order::where('venue_id', $id)->count();
		
		
		$sales_lifetime = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($sales_lifetime == ''){
			$sales_lifetime = 0;
		}
		
		$earnings_lifetime = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($earnings_lifetime == ''){
			$earnings_lifetime = 0;
		}
		
		
		$customers_lifetime = User::where('venue_id', $id)->where('role', 'customer')->count();
		$bookings_lifetime = Booking::where('venue_id', $id)->count();
		$views_lifetime = View::where('module', 'venue')->where('module_id', $id)->count();
		
		
		$bar_of_the_day = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereDate('created_at', Carbon::today())
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		Carbon::setWeekStartsAt(Carbon::SUNDAY);
		Carbon::setWeekEndsAt(Carbon::SATURDAY);
		
		$bar_of_the_week = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
		
		$bar_of_the_month = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		$waiter_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$product_of_the_day = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
		
		$product_of_the_week = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$product_of_the_month = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$table_of_the_day = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
			
		$table_of_the_week = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
		
		$table_of_the_month = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
						
		$rating_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->first();
		
							
		$rating_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->first();
		
		$rating_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->first();
						
		$total_bars = Bar::where('venue_id', $id)->count();
		$active_bars = Bar::where('venue_id', $id)->where('status', 1)->count();
		
		$total_products = Product::where('venue_id', $id)->count();
		$active_products = Product::where('venue_id', $id)->where('status', 1)->count();
		
		$total_tables = Table::where('venue_id', $id)->count();
		$active_tables = Table::where('venue_id', $id)->where('status', 1)->count();
		
		$total_satff = User::where('venue_id', $id)->count();
		$active_staff = User::where('venue_id', $id)->where('status', 1)->count();
		
		$current_year = date('Y');
		
		$jan_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 1)->sum('admin_commission');
		
		$feb_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 2)->sum('admin_commission');
		
		$mar_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 3)->sum('admin_commission');
		
		$apr_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 4)->sum('admin_commission');
							
		$may_earnings = Order::where('venue_id', $id)->where('order_status', 'Completed')->whereYear('created_at', $current_year)
							->whereMonth('created_at', 5)->sum('admin_commission');
							
		$jun_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 6)->sum('admin_commission');
		
		$jul_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 7)->sum('admin_commission');
		
		$aug_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 8)->sum('admin_commission');
							
		$sep_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 9)->sum('admin_commission');
							
		$oct_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 10)->sum('admin_commission');
							
		$nov_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 11)->sum('admin_commission');
							
		$dec_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 12)->sum('admin_commission');
		
		return view('admin.home', compact('venues', 'venue_id', 'id', 'venue', 'active_staff', 'total_satff', 'active_tables', 'total_tables', 'active_products', 'total_products', 'active_bars', 'total_bars', 'rating_of_the_month', 'rating_of_the_week', 'rating_of_the_day', 'table_of_the_month', 'table_of_the_week', 'table_of_the_day', 'product_of_the_month', 'product_of_the_week', 'product_of_the_day', 'waiter_of_the_month', 'waiter_of_the_week', 'waiter_of_the_day', 'bar_of_the_month', 'bar_of_the_week', 'bar_of_the_day', 'views_lifetime', 'bookings_lifetime', 'customers_lifetime', 'earnings_lifetime', 'sales_lifetime', 'orders_lifetime', 'views_today', 'bookings_today', 'customers_today', 'earnings_today', 'sales_today', 'orders_today', 'pending_orders', 'current_orders', 'first_order', 'last_order', 'earnings_this_month', 'jan_earnings', 'feb_earnings', 'mar_earnings', 'apr_earnings', 'may_earnings', 'jun_earnings', 'jul_earnings', 'aug_earnings', 'sep_earnings', 'oct_earnings', 'nov_earnings', 'dec_earnings'));
			
		}else{
			$venue_id = 0;
			$venues = Venue::all();
			return view('admin.home', compact('venues', 'venue_id'));
		}
		
		
	}
	
	
	public function setting(){
		$user = User::find(Auth::user()->id);
		return view('admin.setting', compact('user'));
	}

	public function manageAdmins() {

		$getUsers = User::where('role', 'Admin')
					->where('login_type', '!=', 'Mighty Super Admin')
					->get();
		

		return view('admin.manage_admins', compact('getUsers'));
	}

	public function createAdmin() {


		return view('admin.create_admin');
	}

	public function saveAdmin(Request $request) {

		$request->validate([
      		'email' => 'required|email|unique:users,email',
  		]);
		
		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->role = 'admin';

		if(isset($request->status)){
			$user->status = 1;
		}else{
			$user->status = 0;
		}

		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();
		}
		
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Added';
			$venueLog->event_message = Auth::user()->name.' added new admin '.$request->name.' to the system.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.manage.admins'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.manage.admins'))->with('error', 'Record has not been updated.');
		}
	}

	public function editAdmin(Request $request) {

		$user = User::find($request->id);
		return view('admin.edit_admin', compact('user'));
	}

	public function updateAdmin(Request $request, $id) {

		$user = User::find($id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		
		if(isset($request->status)) {
			$user->status = 1;
		} else{
			$user->status = 0;
		}

		
		if($request->password != ''){
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated user '.$request->name.' password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

		}

		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Updated';
			$venueLog->event_message = Auth::user()->name.' updated admin '.$user->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.edit.admin', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.edit.admin', $id))->with('error', 'Record has not been updated.');
		}
	}
	
	public function update(Request $request, $id){
		
		$user = User::find($id);
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->address = $request->address;
		// remove comma from commission
		$commission = str_replace(",", "", $request->commission);
		$user->commission = $commission;

		// update all venue commission threshold
		$getVenue = Venue::get();

		foreach($getVenue as $venue) {
			
			$venueID = Venue::find($venue->id);

			// calculate commission
			$commissionThreshold = ($venueID->admin_commission_percentage / 100) * $commission;

			$venueID->commission_threshold_limit = $commission - $commissionThreshold;

			$venueID->save();
		} 
		$user->percentage = $request->percentage;
		$user->username = $request->username;
		
		$settings = Setting::where('variable', 'currency')->first();
		if($settings){
			$settings->value = $request->currency;
		}else{
			$settings = new Setting;
			$settings->variable = 'currency';
			$settings->value = $request->currency;
		}
		
		$settings->save();
		
		if($request->password != '') {
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated settings password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

		}
		
		$user->payfast_merchant_id = $request->payfast_merchant_id;
		$user->payfast_merchant_key = $request->payfast_merchant_key;
		
		if($request->file('photo')){
			UpdatePhotoAllSizes($request, 'users/', $user->photo);
			//Storage::disk('public')->delete('users/' . $user->photo);
			//$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}
		
		if($user->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Settings Updated';
			$venueLog->event_message = Auth::user()->name.' updated settings information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.setting'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.setting'))->with('error', 'Record has not been updated.');
		}
	}


	public function checkEmail($email){
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
		  
		  $email_exist = User::where('email', $email)->count();
		  
		  if($email_exist > 0){
			return 'email_exist';
		  }
		  
		  return 'valid_email';
		  
		} else {
		  return 'invalid_email';
		}
	}
	
	public function checkCompanyID($company_id){
		
		$company_exist = Venue::where('company_id', $company_id)->count();
		
		  if($company_exist > 0){
			return 'company_exist';
		  }
	  
		return 'valid_id';
	}
	
	
	public function filter(Request $request){
		$venues = Venue::all();
		$id = $request->venue_id;
		$venue_id = $id;
		
		$venue = Venue::find($id);
		$current_orders = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->count();
		$first_order = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'asc')->first();
		$pending_orders = Order::where('venue_id', $id)->where('order_status', 'Ordered')->count();
		$last_order = Order::where('venue_id', $id)->where('order_status', 'Ordered')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->first();
		
		$orders_today = Order::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$sales_today = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->sales;
		if($sales_today == ''){
			$sales_today = 0;
		}
		
		$earnings_today = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as admin_commission'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->admin_commission;
		if($earnings_today == ''){
			$earnings_today = 0;
		}
		
		
		$earnings_this_month = Order::where('venue_id', $id)->where('order_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');
		
		$customers_today = User::where('venue_id', $id)->where('role', 'customer')->whereDate('created_at', Carbon::today())->count();
		$bookings_today = Booking::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		$views_today = View::where('module', 'venue')->where('module_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$orders_lifetime = Order::where('venue_id', $id)->count();
		
		
		$sales_lifetime = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($sales_lifetime == ''){
			$sales_lifetime = 0;
		}
		
		$earnings_lifetime = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($earnings_lifetime == ''){
			$earnings_lifetime = 0;
		}
		
		
		$customers_lifetime = User::where('venue_id', $id)->where('role', 'customer')->count();
		$bookings_lifetime = Booking::where('venue_id', $id)->count();
		$views_lifetime = View::where('module', 'venue')->where('module_id', $id)->count();
		
		
		$bar_of_the_day = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereDate('created_at', Carbon::today())
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		Carbon::setWeekStartsAt(Carbon::SUNDAY);
		Carbon::setWeekEndsAt(Carbon::SATURDAY);
		
		$bar_of_the_week = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
		
		$bar_of_the_month = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		$waiter_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$product_of_the_day = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
		
		$product_of_the_week = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$product_of_the_month = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$table_of_the_day = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
			
		$table_of_the_week = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
		
		$table_of_the_month = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
						
		$rating_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->first();
		
							
		$rating_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->first();
		
		$rating_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->first();
						
		$total_bars = Bar::where('venue_id', $id)->count();
		$active_bars = Bar::where('venue_id', $id)->where('status', 1)->count();
		
		$total_products = Product::where('venue_id', $id)->count();
		$active_products = Product::where('venue_id', $id)->where('status', 1)->count();
		
		$total_tables = Table::where('venue_id', $id)->count();
		$active_tables = Table::where('venue_id', $id)->where('status', 1)->count();
		
		$total_satff = User::where('venue_id', $id)->count();
		$active_staff = User::where('venue_id', $id)->where('status', 1)->count();
		
		$current_year = date('Y');
		
		$jan_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 1)->sum('admin_commission');
		
		$feb_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 2)->sum('admin_commission');
		
		$mar_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 3)->sum('admin_commission');
		
		$apr_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 4)->sum('admin_commission');
							
		$may_earnings = Order::where('venue_id', $id)->where('order_status', 'Completed')->whereYear('created_at', $current_year)
							->whereMonth('created_at', 5)->sum('admin_commission');
							
		$jun_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 6)->sum('admin_commission');
		
		$jul_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 7)->sum('admin_commission');
		
		$aug_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 8)->sum('admin_commission');
							
		$sep_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 9)->sum('admin_commission');
							
		$oct_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 10)->sum('admin_commission');
							
		$nov_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 11)->sum('admin_commission');
							
		$dec_earnings = Transaction::where('venue_id', $id)->where('transaction_status', 'Completed')->whereYear('created_at', $current_year)->whereMonth('created_at', 12)->sum('admin_commission');
		
		return view('admin.home', compact('venues', 'venue_id', 'id', 'venue', 'active_staff', 'total_satff', 'active_tables', 'total_tables', 'active_products', 'total_products', 'active_bars', 'total_bars', 'rating_of_the_month', 'rating_of_the_week', 'rating_of_the_day', 'table_of_the_month', 'table_of_the_week', 'table_of_the_day', 'product_of_the_month', 'product_of_the_week', 'product_of_the_day', 'waiter_of_the_month', 'waiter_of_the_week', 'waiter_of_the_day', 'bar_of_the_month', 'bar_of_the_week', 'bar_of_the_day', 'views_lifetime', 'bookings_lifetime', 'customers_lifetime', 'earnings_lifetime', 'sales_lifetime', 'orders_lifetime', 'views_today', 'bookings_today', 'customers_today', 'earnings_today', 'sales_today', 'orders_today', 'pending_orders', 'current_orders', 'first_order', 'last_order', 'earnings_this_month', 'jan_earnings', 'feb_earnings', 'mar_earnings', 'apr_earnings', 'may_earnings', 'jun_earnings', 'jul_earnings', 'aug_earnings', 'sep_earnings', 'oct_earnings', 'nov_earnings', 'dec_earnings'));
	}

	public function userPasswordUpdate (Request $request) {

		$user = User::find(Auth::user()->id);

		if($request->password != '') {
			$user->password = Hash::make($request->password);
			$user->last_password_update = Carbon::now()->toDateTimeString();

			// trail log for password update
			$venueLog = new TrailLog;
			$venueLog->event = 'Admin';
			$venueLog->event_id = $user->id;
			$venueLog->event_type = 'Admin Password Updated';
			$venueLog->event_message = Auth::user()->name.' updated password.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();
		}

		$user->save();

		return redirect(route('admin.home'))->with('success', 'User password updated successfully.');
	}
}
