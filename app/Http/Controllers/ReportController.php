<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use Carbon\Carbon;

class ReportController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		$venues = Venue::all();
		return view('admin.reports', compact('venues'));
	}
	
	public function export(Request $request){
		
		$venues = Venue::all();
		return view('admin.reports', compact('venues'));
		
		//$date_range = explode('-', $request->date_range);
		//return Carbon::today()->endOfMonth();
		//->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
	}
}
