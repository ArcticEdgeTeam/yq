<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Venue;
use App\Category;
use App\ProductVariant;
use App\ExtraProduct;
use App\CategoryProduct;
use Storage;
use App\TableWaiter;
use App\Table;
use App\User;

class TableForBarController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$tables = Table::where('venue_id', $venue->id)->orderBy('id', 'desc')->get();
		return view('bar.tables', compact('venue', 'tables', 'counter'));
	}
	
	public function create(){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$waiters = User::where('venue_id', $id)->where('role', 'waiter')->where('status', 1)->get();
		return view('bar.table-create', compact('venue', 'waiters'));
	}
	
	public function save(Request $request){
		$id = Auth::user()->venue_id;
		$table = new Table;
		$table->name = $request->name;
		$table->table_id = $request->table_id;
		$table->seats = $request->seats;
		$table->smoking = $request->smoking;
		$table->area = $request->area;
		$table->venue_id = $id;
		
		if(isset($request->status)){
			$table->status = 1;
		}else{
			$table->status = 0;
		}
		
		
		if($table->save()){
			
			if(is_array($request->waiters)){
				foreach($request->waiters as $waiter){
					$TableWaiter = new TableWaiter;
					$TableWaiter->user_id = $waiter;
					$TableWaiter->table_id = $table->id;
					$TableWaiter->venue_id = $id;
					$TableWaiter->save();
				}
			}
			return redirect(route('bar.tables'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('bar.tables'))->with('error', 'Record has not been added.');
		}
		
	}
	
	
	public function edit($table_id){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$table = Table::find($table_id);
		$waiters = User::where('venue_id', $id)->where('role', 'waiter')->where('status', 1)->get();
		return view('bar.table-edit', compact('table', 'venue', 'waiters'));
	}
	
	public function update(Request $request, $table_id){
		$id = Auth::user()->venue_id;
		$table = Table::find($table_id);
		$table->name = $request->name;
		$table->table_id = $request->table_id;
		$table->seats = $request->seats;
		$table->smoking = $request->smoking;
		$table->area = $request->area;
		$table->venue_id = $id;
		
		if(isset($request->status)){
			$table->status = 1;
		}else{
			$table->status = 0;
		}
		
		
		if($table->save()){
			
			$TableWaiter = TableWaiter::where('table_id', $table_id)->delete();
			if(is_array($request->waiters)){
				foreach($request->waiters as $waiter){
					$TableWaiter = new TableWaiter;
					$TableWaiter->user_id = $waiter;
					$TableWaiter->table_id = $table->id;
					$TableWaiter->venue_id = $id;
					$TableWaiter->save();
				}
			}
			return redirect(route('bar.tables'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('bar.tables'))->with('error', 'Record has not been updated.');
		}
		
		
	}
}
