<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Atmosphere;
use Auth;
use App\TrailLog;

class AtmosphereController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$atmospheres = Atmosphere::all();
		return view('admin.atmospheres', compact('atmospheres', 'counter'));
	}
	
	public function create(){
		return view('admin.atmosphere-create');
	}
	
	public function save(Request $request){
		
		$atmosphere = new Atmosphere;
		$atmosphere->name = $request->name;
		
		if(isset($request->status)){
			$atmosphere->status = 1;
		}else{
			$atmosphere->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('atmospheres/', ['disk' => 'public']);
			$atmosphere->image = $request->image->hashName();
		}
		
		if($atmosphere->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Atmosphere';
			$venueLog->event_id = $atmosphere->id;
			$venueLog->event_type = 'Atmosphere Added';
			$venueLog->event_message = Auth::user()->name.' added new atmosphere '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.atmospheres'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.atmospheres'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id){
		$atmosphere = Atmosphere::find($id);
		return view('admin.atmosphere-edit', compact('atmosphere'));
	}
	
	
	public function update(Request $request, $id){
		
		$atmosphere = Atmosphere::find($id);
		
		$atmosphere->name = $request->name;
		
		if(isset($request->status)){
			$atmosphere->status = 1;
		}else{
			$atmosphere->status = 0;
		}
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('atmospheres/' . $atmosphere->image);
			$request->file('image')->store('atmospheres/', ['disk' => 'public']);
			$atmosphere->image = $request->image->hashName();
		}
		
		if($atmosphere->save()){

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Atmosphere';
			$venueLog->event_id = $atmosphere->id;
			$venueLog->event_type = 'Atmosphere Updated';
			$venueLog->event_message = Auth::user()->name.' update atmosphere '.$atmosphere->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.atmospheres'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.atmospheres'))->with('error', 'Record has not been updated.');
		}
		
	}
}
