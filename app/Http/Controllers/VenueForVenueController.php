<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use Auth;
use App\Dietary;
use App\Facility;
use App\FacilityVenue;
use App\Product;
use Storage;
use App\User;
use App\Booking;
use App\BookingDay;
use App\Table;
use App\Cuisine;
use App\Atmosphere;
use App\VenueDietary;
use App\TradingHour;
use App\Order;
use App\View;
use Carbon\Carbon;
use App\OrderedProduct;
use Hash;
use DB;
use App\Review;
use App\Bar;
use App\Transaction;
use Image;
use App\BookingTable;
use App\TrailLog;
use App\Featured;

class VenueForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
        $this->middleware('venue');
    }
	
	public function information(){
		$venue = Venue::find(Auth::user()->venue_id);

		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}
		
		$dietaries = Dietary::where('status', 1)->get();
		$facilities = Facility::where('status', 1)->get();
		$cuisines = Cuisine::where('status', 1)->get();
		$atmospheres = Atmosphere::where('status', 1)->get();
		//$trading_hours = TradingHour::where('venue_id', $venue->id)->orderBy('id', 'asc')->get();
		//get booking days timing
		$getDayTiming = BookingDay::select('booking_days.id as book_id', 'booking_days.day', 'trading_hours.*')
								->leftjoin('trading_hours', 'trading_hours.booking_id', '=', 'booking_days.id')
								->where('trading_hours.venue_id', $venue->id)
								->orderBy('booking_days.id', 'ASC')
								->get();

		// get booking days
		$getDays = BookingDay::get();
		return view('venue.information', compact('cuisines', 'atmospheres', 'venue', 'dietaries', 'facilities','getDayTiming', 'getDays'));
	}
	
	public function informationUpdate(Request $request) {
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$venue->name = $request->name;
		$venue->contact_email = $request->contact_email;
		$venue->contact_number = $request->contact_number;
		$venue->vat = $request->vat;
		$venue->registration_number = $request->registration_number;
		$venue->address = $request->address;
		$venue->tagline = $request->tagline;
		$venue->vat_name = $request->vat_name;
		$venue->trading_name = $request->trading_name;
		$venue->about = $request->about;
		$venue->cuisine_id = $request->cuisine_id;
		$venue->atmosphere_id = $request->atmosphere_id;
		$venue->avg_cost = str_replace(",", "", $request->avg_cost);
		$venue->notice = $request->notice;
		$venue->postal_address = $request->postal_address;
		
		if(isset($request->open)){
			$venue->open = 1;
		}else{
			$venue->open = 0;
		}
	
		if($request->file('banner')){
			UpdateBannerAllSizes($request, 'banners/', $venue->banner);
			//Storage::disk('public')->delete('banners/' . $venue->banner);
			//$request->file('banner')->store('banners/', ['disk' => 'public']);
			$venue->banner = $request->banner->hashName();
		}
		
		$days = $request->days;
		$from = $request->from;
		$to = $request->to;
		$isclosed = $request->isclosed;
		
		if($venue->save()){
				// delete previous trading hours
			$deletetradingHours = TradingHour::where('venue_id',  $id)->delete();
			
			foreach($days as $key => $day) {
				
				$TradingHour = new TradingHour;
				$TradingHour->booking_id = $key;
				$TradingHour->from = $from[$key];
				$TradingHour->to = $to[$key];
				$TradingHour->isclosed = $isclosed[$key];
				$TradingHour->venue_id = $venue->id;
				$TradingHour->save();
			}
			
			$facilities = FacilityVenue::where('venue_id', $id)->delete();
			$dietaries = VenueDietary::where('venue_id', $id)->delete();
		
			if(is_array($request->facilities)){
				foreach($request->facilities as $facility){
					$FacilityVenue = new FacilityVenue;
					$FacilityVenue->facility_id = $facility;
					$FacilityVenue->venue_id = $venue->id;
					$FacilityVenue->save();
				}
			}
			
			if(is_array($request->dietaries)){
				foreach($request->dietaries as $dietary){
					$VenueDietary = new VenueDietary;
					$VenueDietary->dietary_id = $dietary;
					$VenueDietary->venue_id = $venue->id;
					$VenueDietary->save();
				}
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Venue';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = 'Venue Information Updated';
			$venueLog->event_message = Auth::user()->name.' updated venue information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();
			
			return redirect(route('venue.information'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.information'))->with('error', 'Record has not been updated.');
		}
	}
	
	public function settings(){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		if($venue->banner == ''){
			$venue->banner = 'default.png';
		}
		
		$user = User::where('venue_id', $id)->where('role', 'venue')->first();
		return view('venue.settings', compact('venue', 'user'));
	}
	
	
	public function updateSettings(Request $request){
		
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		if (Auth::user()->login_type == 'Mighty Super Venue Admin') {
			$venue->payfast_merchant_id = $request->payfast_merchant_id;
			$venue->payfast_merchant_key = $request->payfast_merchant_key;
		}
		$venue->bank_name = $request->bank_name;
		$venue->bank_account_type = $request->bank_account_type;
		$venue->iban = $request->iban;
		$venue->prefered_way = $request->prefered_way;
		$venue->average_booking = $request->average_booking;

		//social Links
		$venue->fb_link = $request->fb_link;
		$venue->insta_link = $request->insta_link;
		$venue->twitter_link = $request->twitter_link;
		$venue->web_link = $request->web_link;
		
		if(isset($request->status)){
			$venue->status = 1;
		}else{
			$venue->status = 0;
		}
		
		if(isset($request->booking_auto_accept)){
			$venue->booking_auto_accept = 1;
		}else{
			$venue->booking_auto_accept = 0;
		}
		
		if(isset($request->booking)){
			$venue->booking = 1;
		}else{
			$venue->booking = 0;
		}
		
		if($venue->save()){
			// $user = User::where('venue_id', $id)->first();
			// $user->name = $request->name;
			// $user->phone = $request->phone;
			
			// if($request->password != ''){
			// 	$user->password = Hash::make($request->password);
			// }
			
			// $user->save();

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Setting';
			$venueLog->event_id = $venue->id;
			$venueLog->event_type = 'Venue Settings Updated';
			$venueLog->event_message = Auth::user()->name.' updated venue settings information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $venue->id;
			$venueLog->save();

			return redirect(route('venue.settings'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.settings'))->with('error', 'Record has not been updated.');
		}
	}
	
	
	public function overview(){
		
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$current_orders = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->count();
		$first_order = Order::where('venue_id', $id)->where('order_status', '!=', 'Ordered')->where('order_status', '!=', 'Completed')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'asc')->first();
		$pending_orders = Order::where('venue_id', $id)->where('order_status', 'Ordered')->count();
		$last_order = Order::where('venue_id', $id)->where('order_status', 'Ordered')->whereDate('created_at', Carbon::today())->orderBy('created_at', 'desc')->first();
		
		$orders_today = Order::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$sales_today = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->sales;
		if($sales_today == ''){
			$sales_today = 0;
		}
		
		$earnings_today = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as admin_commission'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')
						->whereDate('created_at', Carbon::today())->first()->admin_commission;
		if($earnings_today == ''){
			$earnings_today = 0;
		}
		
	
		$customers_today = User::where('venue_id', $id)->where('role', 'customer')->whereDate('created_at', Carbon::today())->count();
		$bookings_today = Booking::where('venue_id', $id)->whereDate('created_at', Carbon::today())->count();
		$views_today = View::where('module', 'venue')->where('module_id', $id)->whereDate('created_at', Carbon::today())->count();
		
		$orders_lifetime = Order::where('venue_id', $id)->count();
		
		
		$sales_lifetime = DB::table('transactions')
						->select(DB::raw('sum(amount) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($sales_lifetime == ''){
			$sales_lifetime = 0;
		}
		
		$earnings_lifetime = DB::table('transactions')
						->select(DB::raw('sum(admin_commission) as sales'))
						->where('venue_id', $id)
						->where('transaction_status', 'Completed')->first()->sales;
		
		if($earnings_lifetime == ''){
			$earnings_lifetime = 0;
		}
		
		$customers_lifetime = User::where('venue_id', $id)->where('role', 'customer')->count();
		$bookings_lifetime = Booking::where('venue_id', $id)->count();
		$views_lifetime = View::where('module', 'venue')->where('module_id', $id)->count();
		
		
		$bar_of_the_day = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereDate('created_at', Carbon::today())
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		Carbon::setWeekStartsAt(Carbon::SUNDAY);
		Carbon::setWeekEndsAt(Carbon::SATURDAY);
		
		$bar_of_the_week = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
		
		$bar_of_the_month = DB::table('ordered_products')
						->select(DB::raw('sum(price) as sales, bar_id'))
						->where('venue_id', $id)
						->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
						->groupBy('bar_id')
						->orderBy('sales', 'desc')
						->first();
						
		$waiter_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$waiter_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, waiter_id, order_id, count(id) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('waiter_id')
							->orderBy('average_rating', 'desc')
							->first();
							
		$product_of_the_day = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
		
		$product_of_the_week = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$product_of_the_month = DB::table('ordered_products')
							->select(DB::raw('sum(quantity) as quantity, product_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('product_id')
							->orderBy('quantity', 'desc')
							->first();
						
		$table_of_the_day = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
			
		$table_of_the_week = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
		
		$table_of_the_month = DB::table('orders')
							->select(DB::raw('count(table_id) as total_orders, table_id'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->groupBy('table_id')
							->orderBy('total_orders', 'desc')
							->first();
						
		$rating_of_the_day = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereDate('created_at', Carbon::today())
							->first();
		
							
		$rating_of_the_week = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
							->first();
		
		$rating_of_the_month = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
							->first();
						
		$total_bars = Bar::where('venue_id', $id)->count();
		$active_bars = Bar::where('venue_id', $id)->where('status', 1)->count();
		
		$total_products = Product::where('venue_id', $id)->count();
		$active_products = Product::where('venue_id', $id)->where('status', 1)->count();
		
		$total_tables = Table::where('venue_id', $id)->count();
		$active_tables = Table::where('venue_id', $id)->where('status', 1)->count();
		
		$total_satff = User::where('venue_id', $id)->count();
		$active_staff = User::where('venue_id', $id)->where('status', 1)->count();
		
		$current_year = date('Y');
		
		$jan_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 1)
							->first();
		if($jan_rating->average_rating){
			$jan_rating = $jan_rating->average_rating / $jan_rating->total_records;
		}else{
			$jan_rating = 0;
		}
		
		
		$feb_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 2)
							->first();
		if($feb_rating->average_rating){
			$feb_rating = $feb_rating->average_rating / $feb_rating->total_records;
		}else{
			$feb_rating = 0;
		}					
		
		
		$mar_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 3)
							->first();
							
		if($mar_rating->average_rating){
			$mar_rating = $mar_rating->average_rating / $mar_rating->total_records;
		}else{
			$mar_rating = 0;
		}
		
		$apr_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 4)
							->first();
							
		if($apr_rating->average_rating){
			$apr_rating = $apr_rating->average_rating / $apr_rating->total_records;
		}else{
			$apr_rating = 0;
		}
		
		$may_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 5)
							->first();
							
		if($may_rating->average_rating){
			$may_rating = $may_rating->average_rating / $may_rating->total_records;
		}else{
			$may_rating = 0;
		}
		
		$jun_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 6)
							->first();
		
		if($jun_rating->average_rating){
			$jun_rating = $jun_rating->average_rating / $jun_rating->total_records;
		}else{
			$jun_rating = 0;
		}
		
		$jul_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 7)
							->first();
		
		if($jul_rating->average_rating){
			$jul_rating = $jul_rating->average_rating / $jul_rating->total_records;
		}else{
			$jul_rating = 0;
		}
		
		$aug_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 8)
							->first();
		
		if($aug_rating->average_rating){
			$aug_rating = $aug_rating->average_rating / $aug_rating->total_records;
		}else{
			$aug_rating = 0;
		}
		
		$sep_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 9)
							->first();
		
		if($sep_rating->average_rating){
			$sep_rating = $sep_rating->average_rating / $sep_rating->total_records;
		}else{
			$sep_rating = 0;
		}
		
		$oct_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 10)
							->first();
		
		if($oct_rating->average_rating){
			$oct_rating = $oct_rating->average_rating / $oct_rating->total_records;
		}else{
			$oct_rating = 0;
		}
		
		$nov_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 11)
							->first();
		
		if($nov_rating->average_rating){
			$nov_rating = $nov_rating->average_rating / $nov_rating->total_records;
		}else{
			$nov_rating = 0;
		}
		
		$dec_rating = DB::table('reviews')
							->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
							->where('venue_id', $id)
							->whereYear('created_at', $current_year)
							->whereMonth('created_at', 12)
							->first();
		
		if($dec_rating->average_rating){
			$dec_rating = $dec_rating->average_rating / $dec_rating->total_records;
		}else{
			$dec_rating = 0;
		}
		
		return view('venue.overview', compact('id', 'venue', 'active_staff', 'total_satff', 'active_tables', 'total_tables', 'active_products', 'total_products', 'active_bars', 'total_bars', 'rating_of_the_month', 'rating_of_the_week', 'rating_of_the_day', 'table_of_the_month', 'table_of_the_week', 'table_of_the_day', 'product_of_the_month', 'product_of_the_week', 'product_of_the_day', 'waiter_of_the_month', 'waiter_of_the_week', 'waiter_of_the_day', 'bar_of_the_month', 'bar_of_the_week', 'bar_of_the_day', 'views_lifetime', 'bookings_lifetime', 'customers_lifetime', 'earnings_lifetime', 'sales_lifetime', 'orders_lifetime', 'views_today', 'bookings_today', 'customers_today', 'earnings_today', 'sales_today', 'orders_today', 'pending_orders', 'current_orders', 'jan_rating', 'feb_rating', 'mar_rating', 'apr_rating', 'may_rating', 'jun_rating', 'jul_rating', 'aug_rating', 'sep_rating', 'oct_rating', 'nov_rating', 'dec_rating', 'first_order', 'last_order'));
	}
	
	
	public function bookings(Request $request){
		$venue = Venue::find(Auth::user()->venue_id);

		// get current day booking only
		// $search_date = date('d-m-Y');
		// $search_date1 = Carbon::parse($search_date)->startOfDay();
		// $bookings = Booking::where('venue_id', $venue->id)->whereDate('date_time','=', $search_date1)->orderBy('id', 'desc')->get();

		$getBookings = Booking::query();
		$getBookings = $getBookings->where('venue_id', $venue->id);

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getBookings = $getBookings->whereBetween('created_at', [$start, $end]);
			
		}

		if($request->name != '') {
			$getBookings = $getBookings->where('name', 'like', '%' . $request->name . '%');
		}

		if($request->status != '') {
			$getBookings = $getBookings->where('booking_status', $request->status);
		}

		$getBookings = $getBookings->orderBy('id', 'DESC')
							->paginate(15);			
		
		foreach ($getBookings as $key => $value) {
			// get tables
			$tables = Table::select('tables.*', 'booking_tables.*')
							->leftjoin('booking_tables', 'booking_tables.table_id', '=', 'tables.id')
							->where('booking_tables.booking_id', $value->id)
							->pluck('name')->toArray();
			if($tables != null) {
				$value->booking_table = implode(',',$tables);
			} else {
				$value->booking_table = 'N/A';
			}
			
		}

		return view('venue.bookings', compact('id', 'getBookings', 'venue'));
	}
	
	public function bookingsFilter(Request $request){
		
		$venue = Venue::find(Auth::user()->venue_id);

		if(isset($request->search_date)) {
			$search_date = $request->search_date;
			$search_date = date('Y-m-d', strtotime($search_date));
			$search_date = Carbon::parse($search_date)->startOfDay();
			
			$bookings = Booking::where('venue_id', $venue->id)->whereDate('date_time', '=', $search_date)->orderBy('id', 'desc')->get();

			foreach ($bookings as $key => $value) {
				// get tables
				$tables = Table::select('tables.*', 'booking_tables.*')
								->leftjoin('booking_tables', 'booking_tables.table_id', '=', 'tables.id')
								->where('booking_tables.booking_id', $value->id)
								->pluck('name')->toArray();

				if($tables != null) {
				$value->booking_table = implode(',',$tables);
				} else {
					$value->booking_table = 'N/A';
				}
			}
			$counter = 1;
			
			$search_date = date('d-m-Y', strtotime($search_date));
			return view('venue.bookings', compact('counter', 'bookings', 'venue', 'id', 'search_date'));
			
		}
		
		return abort(404);
		
	}
	
	public function editBooking($id) {
		$booking = Booking::find($id);
		$venue = Venue::find(Auth::user()->venue_id);
		
		$customer = User::find($booking->customer_id);
		if ($customer != null) {
			if($customer->photo == '') {
				$customer->photo = 'default.png';
			}
		}

		// get tables for booking
		$bookingTables = BookingTable::where('booking_id', $id)->pluck('table_id')->toArray();

		$tables = Table::where('venue_id', $venue->id)->where('status', 1)->get();
		return view('venue.booking-edit', compact('customer', 'booking', 'venue', 'tables', 'bookingTables'));
	}
	
	public function createBooking(){
		
		$venue = Venue::find(Auth::user()->venue_id);
		$customers = User::where('status', 1)->where('role', 'customer')->get();
		
		$tables = Table::where('venue_id', $venue->id)->where('status', 1)->get();
		$id = $venue->id;
		return view('venue.booking-create', compact('customers','venue', 'tables'));
	}
	
	
	public function updateBooking(Request $request, $id) {
	
		$booking = Booking::find($id);
		
		$booking->seating = $request->seating;
		$booking->message = $request->message;
		$booking->booking_status = $request->booking_status;
		$booking->date_time = date('Y-m-d H:i:s', strtotime($request->date_time));

		if($booking->save()) {

			// delete old booking tables
			$deleteTables = BookingTable::where('booking_id', $id)->delete();
			// save table for this booking
			foreach($request->table_id as $table) {
				$bookingTable = new BookingTable;
				$bookingTable->booking_id = $id;
				$bookingTable->table_id = $table;
				$bookingTable->save();
			}

			if($request->booking_page == 'booking_calendar') {
				return redirect(route('venue.table-booking-view'))->with('success', 'Record has been updated.');
			} else {
				return redirect(route('venue.bookings'))->with('success', 'Record has been updated.');
			}
		}
		
	}
	
	
	public function saveBooking(Request $request){
		$booking_number = Booking::max('booking_number');
		
		if($booking_number < 1000){
			$booking_number = 1000;
		}
		
		$booking_number = $booking_number + 1;
		
		$booking = new Booking;
		$booking->booking_number = $booking_number;
		$booking->customer_id = '';
		//$booking->table_id = $request->table_id;
		$booking->seating = $request->seating;
		// $booking->smooking = $request->smooking;
		// $booking->area = $request->area;
		$booking->message = $request->message;
		$booking->date_time = date('Y-m-d H:i:s', strtotime($request->date_time));
		$booking->venue_id = Auth::user()->venue_id;
		$booking->name = $request->name;
		$booking->phone = $request->phone;
		$booking->booking_status = 'booked';
		if($booking->save()) {
			$table_ids = [];
			if(isset($request->availability_check_page)) {
				$table_ids = json_decode($request->table_id);
			} else {
				$table_ids = $request->table_id;
			}

			// save table for this booking
			foreach($table_ids as $table) {
				$bookingTable = new BookingTable;
				$bookingTable->booking_id = $booking->id;
				$bookingTable->table_id = $table;
				$bookingTable->save();
			}
			

			if($request->booking_page == 'booking_calendar') {
				return redirect(route('venue.table-booking-view'))->with('success', 'Record has been updated.');
			} else {
				return redirect(route('venue.bookings'))->with('success', 'Record has been updated.');
			}
		}
		
	}
	
	
	public function cancelBooking($id){
		$booking = Booking::find($id);
		$booking->booking_status = 'Cancelled';
		if($booking->save()){
			return redirect(route('venue.bookings'))->with('success', 'Record has been updated.');;
		}
	}
	
	public function acceptBooking($id){
		$booking = Booking::find($id);
		$booking->booking_status = 'Scheduled';
		
		if($booking->save()){
			return redirect(route('venue.bookings'))->with('success', 'Record has been updated.');;
		}
	}
	
	
	
	public function reviews(Request $request) {
		$venue = Venue::find(Auth::user()->venue_id);
		// $reviews = Review::where('venue_id', $venue->id)->get();
		$getReviews = Review::query();
		$getReviews = $getReviews->select('reviews.*', 'orders.order_number', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo', 'waiter_table.name as waiter_name', 'waiter_table.photo as waiter_photo')
		->leftjoin('orders', 'orders.id', '=', 'reviews.order_id')
		->leftjoin('users as customer_table','customer_table.id', '=', 'orders.customer_id')
		->leftjoin('users as waiter_table','waiter_table.id', '=', 'orders.waiter_id')
		->where('reviews.venue_id', $venue->id);


		if($request->name != '') {
			$getReviews = $getReviews->where('customer_table.name', 'like', '%' . $request->name . '%');
		}

		if($request->waiter != '') {
			$getReviews = $getReviews->where('orders.waiter_id', $request->waiter);
		}

		if($request->order_no != '') {
			$getReviews = $getReviews->where('orders.order_number', $request->order_no);
		}

		$getReviews = $getReviews->orderBy('reviews.id', 'desc')->paginate(15);

		//get waiters
		$getWaiters = User::where('status', 1)->where('role', 'waiter')->where('venue_id', $venue->id)->get();
		
		return view('venue.reviews', compact('venue', 'getReviews', 'getWaiters'));
	}
	
	public function reviewsFilter(Request $request){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$reviews = Review::where('venue_id', $venue->id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			
			return view('venue.reviews', compact('venue', 'reviews', 'counter', 'start', 'end'));
		}
		
		return abort(404);
		
	}

	
	public function cashups(){
		$venue = Venue::find(Auth::user()->venue_id);
		return view('venue.cashups', compact('venue'));
	}

	public function tableAvailabilityChecker(Request $request) {

		$venue = Venue::find(Auth::user()->venue_id);
		
		// get date and time seperate
		$bookingDate = date('Y-m-d',strtotime($request->table_date_time));
		$bookingTime = date('H:i',strtotime($request->table_date_time));

		//get Table
		$getTables = Table::where('status', 1)
							->where('venue_id', Auth::user()->venue_id)
							->get();
		foreach($getTables as $table) {
			//check booking date and time
			$getBookings = Booking::select('bookings.*', 'tables.id as table_id', 'booking_tables.*')
									->leftjoin('booking_tables', 'booking_tables.booking_id', '=', 'bookings.id')
									->leftjoin('tables', 'tables.id', '=', 'booking_tables.table_id')
									->where('bookings.venue_id', Auth::user()->venue_id)
									->where('bookings.booking_status', 'booked')
									->where('booking_tables.table_id',  $table->id)
									->whereDate('bookings.date_time', $bookingDate)
									->get();
			
			$slotAvailable = '';

			if (!$getBookings->isEmpty()) {

				foreach($getBookings as $booking) {

					// booking start date
						$startTime = date('H:i',strtotime($booking->date_time));
						
						// check if avarege booking for venue exists or not
						if ($venue->average_booking == '') {
							$endTime = strtotime("+30 minutes", strtotime($startTime));
							$endTime = date('H:i', $endTime);
						} else {
							$endTime = strtotime("+".date('H',strtotime($venue->average_booking))." hour +".date('i',strtotime($venue->average_booking))." minutes +".date('s',strtotime($venue->average_booking))." seconds", strtotime($startTime));
							$endTime = date('H:i', $endTime);
						} // average booking

						// check if booking time is conflicting with database booking time
							if($bookingTime >= $startTime && $bookingTime <= $endTime) {
								$slotAvailable = 'no';
							}
				} // booking foreach ends

			} // booking empty check ends

			// booking slot status for this table
			$table->bookingSlot = $slotAvailable;

		} // table foreach ends

		//dd($getTables);

		// get table for booking
		$bookingTables = Table::where('venue_id', Auth::user()->venue_id)->where('status', 1)->get();

		return view('venue.venue_table_availability_checker', compact('venue', 'id', 'getTables', 'bookingTables'));
	}

	public function trailLog(Request $request)
	{
		// get logs
		$getLogs = TrailLog::query();
		$getLogs = $getLogs->select('trail_logs.*', 'venues.name as venue_name')
							->leftjoin('venues', 'venues.id', '=', 'trail_logs.venue_id')
							->where('user_role', 'venue')
							->where('venue_id', Auth::user()->venue_id);

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getLogs = $getLogs->whereBetween('trail_logs.created_at', [$start, $end]);
			
		}

		if($request->user != '') {
			$getLogs = $getLogs->where('trail_logs.user_id', $request->user);
		}


		$getLogs = $getLogs->orderBy('trail_logs.id', 'DESC')
							->paginate(15);

		// get user
		$users = User::where('status', 1)
						->where('role', 'venue')
						->where('venue_id', Auth::user()->venue_id)
						->get();

		//dd($getLogs);

		return view('venue.trail_logs', compact('getLogs', 'users'));

	}

	public function featured(Request $request)
	{
		$venue = Venue::find(Auth::user()->venue_id);
		$getFeatured = Featured::first();

		return view('venue.featured',compact('venue', 'getFeatured'));

	}
	
	
	
}
