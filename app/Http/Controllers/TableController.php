<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\Table;
use App\TableWaiter;
use App\User;

class TableController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index($id) {
		$counter = 1;
		$venue = Venue::find($id);
		$tables = Table::where('venue_id', $id)->orderBy('id', 'desc')->get();
		return view('admin.venue-tables', compact('venue', 'id', 'tables', 'counter'));
	}
	
	public function create($id){
		$waiters = User::where('venue_id', $id)->where('role', 'waiter')->where('status', 1)->get();
		$venue = Venue::find($id);
		return view('admin.venue-table-create', compact('venue', 'id', 'waiters'));
	}
	
	public function save(Request $request, $id) {

		$validatedData = $request->validate([
            'name' => 'required',
            'table_id' => 'required',
        ]);

        $getTableInfo = Table::where('table_id', $request->table_id)->where('venue_id', $id)->first();


		if ($getTableInfo != null) {
			
			return redirect(route('admin.venue.table.create', $id))->with('error', 'This Table ID already exists for this venue.');
		}
		
		$table = new Table;
		$table->name = $request->name;
		$table->table_id = $request->table_id;
		$table->seats = $request->seats;
		$table->smoking = $request->smoking;
		$table->area = $request->area;
		$table->venue_id = $id;
		
		if(isset($request->status)){
			$table->status = 1;
		}else{
			$table->status = 0;
		}
		
		
		if($table->save()){
			
			if(is_array($request->waiters)) {
				foreach($request->waiters as $waiter){
					$TableWaiter = new TableWaiter;
					$TableWaiter->user_id = $waiter;
					$TableWaiter->table_id = $table->id;
					$TableWaiter->venue_id = $id;
					$TableWaiter->save();
				}
			}
			
			return redirect(route('admin.venue.tables', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.venue.tables', $id))->with('error', 'Record has not been added.');
		}
		
	}
	
	
	public function edit($id, $table_id){
		$waiters = User::where('venue_id', $id)->where('role', 'waiter')->where('status', 1)->get();
		$table = Table::find($table_id);
		$venue = Venue::find($id);
		return view('admin.venue-table-edit', compact('id', 'table', 'venue', 'waiters'));
	}
	
	public function update(Request $request, $id, $table_id) {

		$validatedData = $request->validate([
            'name' => 'required',
            'table_id' => 'required',
        ]);

        $getTableInfo = Table::where('table_id', $request->table_id)
								->where('venue_id', $id)
								->where('id', '!=', $table_id)
								->first();

		if ($getTableInfo != null) {
			
			return redirect(route('admin.venue.table.edit', [$id, $table_id]))->with('error', 'This Table ID already exists for this venue.');
		}
		
		$table = Table::find($table_id);
		$table->name = $request->name;
		$table->table_id = $request->table_id;
		$table->seats = $request->seats;
		$table->smoking = $request->smoking;
		$table->area = $request->area;
		$table->occupied = $request->table_occupied;
		$table->venue_id = $id;
		
		if(isset($request->status)){
			$table->status = 1;
		}else{
			$table->status = 0;
		}
		
		
		if($table->save()){
			
			
			if(is_array($request->waiters)) {
				$TableWaiter = TableWaiter::where('table_id', $table_id)->delete();
				foreach($request->waiters as $waiter){
					$TableWaiter = new TableWaiter;
					$TableWaiter->user_id = $waiter;
					$TableWaiter->table_id = $table->id;
					$TableWaiter->venue_id = $id;
					$TableWaiter->save();
				}
			}
			return redirect(route('admin.venue.tables', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.venue.tables', $id))->with('error', 'Record has not been updated.');
		}
		
		
	}
	
}
