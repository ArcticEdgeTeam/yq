<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ingredient;
use App\Unit;
use App\Venue;
use Auth;

class IngredientForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index() {
		$counter = 1;
		$counter2 = 1;
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$ingredients = Ingredient::where('venue_id', $id)->orderBy('id', 'desc')->get();
		$units = Unit::where('venue_id', $id)->orderBy('id', 'desc')->get();
		return view('venue.ingredients', compact('id', 'venue', 'ingredients', 'units', 'counter', 'counter2'));
	}
	
	public function create() {
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		return view('venue.ingredient-create', compact('id', 'venue'));
	}
	
	public function save(Request $request) {

		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

		$validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required',
        ]);

        // check dupliacte Ingredient for venue
        $getIngredient = Ingredient::where(function($query) use ($request, $id){
								    $query->where('name', $request->name);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id){
        							$query->where('code', $request->code);
									$query->where('venue_id', $id);
        						})->first();

		if ($getIngredient != null) {

			return redirect(route('venue.ingredient.create'))->with('error', 'Ingredient code or name already exists.');
		}

		
		$ingredient = new Ingredient;
		$ingredient->name = $request->name;
		$ingredient->code = $request->code;
		$ingredient->venue_id = $id;
		if(isset($request->status)){
			$ingredient->status = 1;
		}else{
			$ingredient->status = 0;
		}
		
		if($ingredient->save()) {
			return redirect(route('venue.ingredients'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('venue.ingredients'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($ingredient_id) {
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		$ingredient = Ingredient::find($ingredient_id);
		return view('venue.ingredient-edit', compact('id', 'venue', 'ingredient'));
	}
	
	public function update(Request $request, $ingredient_id) {
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);

		$validatedData = $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);

        // check dupliacte Ingredient for venue
        $getIngredient = Ingredient::where(function($query) use ($request, $id ,$ingredient_id){
								    $query->where('name', $request->name);
								    $query->where('id', '!=', $ingredient_id);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id ,$ingredient_id){
        							$query->where('code', $request->code);
								    $query->where('id', '!=', $ingredient_id);
									$query->where('venue_id', $id);
        						})->first();

		if ($getIngredient != null) {

			return redirect(route('venue.ingredient.edit', $ingredient_id))->with('error', 'Ingredient code or name already exists.');
		}

		$ingredient = Ingredient::find($ingredient_id);
		$ingredient->name = $request->name;
		$ingredient->code = $request->code;
		
		if(isset($request->status)){
			$ingredient->status = 1;
		}else{
			$ingredient->status = 0;
		}
		
		if($ingredient->save()){
			return redirect(route('venue.ingredients', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('venue.ingredients', $id))->with('error', 'Record has not been updated.');
		}
		
	}
}
