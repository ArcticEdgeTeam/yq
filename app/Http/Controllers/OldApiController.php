<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Mail;
use Storage;
use App\Page;
use App\Support;
use App\Venue;
use App\Order;
use App\Table;
use App\TableWaiter;
use App\Transaction;
use Carbon\Carbon;
use App\OrderedProduct;
use App\Product;
use App\ReservedFund;
use App\CustomerFavoriteVenue;
use App\ExtraProduct;
use App\ProductVariant;
use DB;
use App\TradingHour;
use App\FacilityVenue;
use App\VenueDietary;
use App\Facility;
use App\Dietary;
use App\Guest;
use App\CartProduct;
use App\CartProductVariable;
use App\OrderedProductVariable;
use App\Booking;

class OldApiController extends Controller
{
    //
    public function customerRegister(Request $request){

        $check_email = User::where('email', $request->email)->first();
       if($check_email) {
           return "{'status':false,'message':'This email is already exist.'}";
       }
	    $user = new User;
	    $user->status = 1;
	    $user->role = 'customer';
	    $user->email = $request->email;
	    $user->name = $request->name;
	    $user->dob = date('Y-m-d', strtotime($request->dob));
	    $user->gender = $request->gender;
	    $user->phone = $request->phone;

	    if($request->password != '') {
			$user->password = Hash::make($request->password);
		}

		if($request->file('photo')) {
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}

		$user->save();
		return "{'status':true,'message':'Registration completed.'}";
	}



	public function customerLogin(Request $request) {

		$model = User::where('email', $request->email)->where('role', 'customer')->first();

		if($model){

	    	if($model->status == 0) {
    		    return '{"status":false,"message":"Your account is already suspended."}';
    		}

		 if(Hash::check($request->password, $model->password)) {
		     $model->status = true;
		     if($model->photo == ''){
		         $model->photo = 'default.png';
		     }
			return $model;
		  }else{
		      return '{"status":false,"message":"Invalid email or password."}';
		  }

	    }

	    return '{"status":false,"message":"Invalid email or password."}';
	}





		public function customerPasswordReset(Request $request){

	    $model = User::where('email', $request->email)->where('status', 1)->where('role', 'customer')->first();

		$password = str_random(8);
	    $hashed_random_password = Hash::make($password);
		$request->password = $password;

		if($model) {

		    Mail::send(['html'=>'emails.app.reset-password'], ['request' => $request], function($message) use ($request) {

    		$message->to($request->email, 'Customer')->subject('Password Reset');
    			$message->from('pop@directframing.com.au', 'POP');
    		});

    		$model->password = $hashed_random_password;
		    $model->save();

		    return "{'status':true,'message':'Email has been sent to you. Check your email $request->email and find the new
password.'}";


		}

		return '{"status":false,"message":"Email does not exist."}';
	}




	public function editCustomer($id) {
	    $model = User::where('id', $id)->where('status', 1)->where('role', 'customer')->first();

	    $model->photo = asset('public/uploads/users/' . $model->photo);

	    if($model->photo == ''){
	        $model->photo = asset('public/uploads/users/default.png');
	    }

	    $model->status = true;

	    if($model->address == null){
	        $model->address = '';
	    }

	    if($model->phone == null){
	        $model->phone = '';
	    }

	    if($model->name == null){
	        $model->name = '';
	    }

	    if($model->gender == null){
	        $model->gender = '';
	    }

	    if($model->default_tip == null){
	        $model->default_tip = 0;
	    }

	    return $model;
	}

	public function updateCustomer(Request $request, $id){


	    $user = User::where('id', $id)->where('status', 1)->where('role', 'customer')->first();
	    $user->name = $request->name;
	    $user->dob = date('Y-m-d', strtotime($request->dob));
	    $user->gender = $request->gender;
	    $user->address = $request->address;
	    $user->phone = $request->phone;
	    $user->default_tip = $request->default_tip;

	    if($request->password != ''){
			$user->password = Hash::make($request->password);
		}

		if($request->file('photo')){
			Storage::disk('public')->delete('users/' . $user->photo);
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}

		$user->save();

		return "{'status':true,'message':'Profile has been updated.'}";

	}

	public function suspendCustomer($id){
	    $user = User::where('id', $id)->where('status', 1)->where('role', 'customer')->first();
	    $user->status = 0;
	    $user->save();

	    return "{'status':true,'message':'Your account has been suspended.'}";
	}



	public function waiterLogin(Request $request) {

		$model = User::where('email', $request->email)->where('role', 'waiter')->first();


		if($model){

		    if($model->status == 0){
    		    return '{"status":false,"message":"Your account is already suspended."}';
    		}

		 if(Hash::check($request->password, $model->password)) {
		     $model->status = true;
		     if($model->photo == ''){
		         $model->photo = 'default.png';
		     }
			return $model;
		  }else{
		      return '{"status":false,"message":"Invalid email or password."}';
		  }

	    }

	    return '{"status":false,"message":"Invalid email or password."}';
	}




	public function waiterPasswordReset(Request $request){

	    $model = User::where('email', $request->email)->where('status', 1)->where('role', 'waiter')->first();

		$password = str_random(8);
	    $hashed_random_password = Hash::make($password);
		$request->password = $password;

		if($model){

		    Mail::send(['html'=>'emails.app.reset-password'], ['request' => $request], function($message) use ($request) {

    		$message->to($request->email, 'Customer')->subject('Password Reset');
    			$message->from('pop@directframing.com.au', 'POP');
    		});

    		$model->password = $hashed_random_password;
		    $model->save();

		     return "{'status':true,'message':'Email has been sent to you. Check your email $request->email and find the new
password.'}";


		}

		return '{"status":false,"message":"Email does not exist."}';
	}


	public function editWaiter($id){
	    $model = User::where('id', $id)->where('status', 1)->where('role', 'waiter')->first();

	    $model->photo = asset('public/uploads/users/' . $model->photo);

	    if($model->photo == ''){
	        $model->photo = asset('public/uploads/users/default.png');
	    }

	    $model->status = true;


	    if($model->address == null){
	        $model->address = '';
	    }

	    if($model->phone == null){
	        $model->phone = '';
	    }

	    if($model->name == null){
	        $model->name = '';
	    }

	    if($model->gender == null){
	        $model->gender = '';
	    }

	    if($model->default_tip == null){
	        $model->default_tip = 0;
	    }


	    return $model;
	}

	public function updateWaiter(Request $request, $id){


	    $user = User::where('id', $id)->where('status', 1)->where('role', 'waiter')->first();
	    $user->name = $request->name;
	    $user->dob = date('Y-m-d', strtotime($request->dob));
	    $user->gender = $request->gender;
	    $user->address = $request->address;
	    $user->phone = $request->phone;

	    if($request->password != ''){
			$user->password = Hash::make($request->password);
		}

		if($request->file('photo')){
			Storage::disk('public')->delete('users/' . $user->photo);
			$request->file('photo')->store('users/', ['disk' => 'public']);
			$user->photo = $request->photo->hashName();
		}

		$user->save();

		return "{'status':true,'message':'Profile has been updated.'}";

	}

	public function suspendWaiter($id){
	    $user = User::where('id', $id)->where('status', 1)->where('role', 'waiter')->first();
	    $user->status = 0;
	    $user->save();

	    return "{'status':true,'message':'Your account has been suspended.'}";
	}


	public function aboutUs(){
	    $page = Page::find(1);
	    if($page){
	        $page->status = true;
	        return $page;
	    }

	    return "{'status':false,'message':'No data found.'}";
	}

	public function signUpTerms(){
	    $page = Page::find(2);
	    if($page){
	        $page->status = true;
	        return $page;
	    }

	    return "{'status':false,'message':'No data found.'}";
	}

	public function termsAndConditions(){
	     $page = Page::find(3);
	    if($page){
	        $page->status = true;
	        return $page;
	    }

	    return "{'status':false,'message':'No data found.'}";
	}


	public function supportCreate(Request $request, $id){
	    $support = new Support;
	    $support->subject = $request->subject;
	    $support->comment = $request->comment;
	    $support->user_id = $id;
	    $support->status = 0;
	    $support->save();
	    return "{'status':true,'message':'Your message has been sent to our support team. We will contact you back through email.'}";
	}


	public function orderCreate(){
		$venue = Venue::orderBy('id', 'desc')->first();
		$order_number = Order::max('order_number') + 1;
		if($order_number < 1000){
			$order_number = 1001;
		}
		$customer = User::where('role', 'customer')->where('status', 1)->orderBy('id', 'desc')->first();
		$customer_reserved_fund = ReservedFund::where('customer_id', $customer->id)->sum('amount');
		$customer_reserved_debit = Transaction::where('customer_id', $customer->id)->where('reserved_payment', '!=', 0)->sum('amount');

		$customer_reserved_fund = $customer_reserved_fund - $customer_reserved_debit;

		$tables = Table::where('venue_id', $venue->id)->where('status', 1)->get();
		return view('app.order-create', compact('venue', 'order_number', 'customer', 'tables', 'customer_reserved_fund'));
	}

	public function orderSave(Request $request){

		$order_number = Order::max('order_number') + 1;
		if($order_number < 1000){
			$order_number = 1001;
		}

		$products = $request->products;

		$order = new Order;
		$order->order_number = $order_number;
		$order->invoice_number = $order_number;
		$order->payment_type = $request->payment_type;
		$order->venue_id = $request->venue_id;
		$order->customer_id = $request->customer_id;
		$order->table_id = $request->table_id;
		$waiter = TableWaiter::where('table_id', $request->table_id)->first();
		if(!$waiter){
			return 'No waiter assign to this table';
		}

		$order->waiter_id = $waiter->user_id;
		$order->total_amount = $request->total_amount;
		$order->waiter_tip = $request->waiter_tip;
		$order->order_status = $request->order_status;
		$admin_percentage = User::where('role', 'admin')->first()->percentage;
		$venue = Venue::find($request->venue_id);
		$collected_commission = Transaction::where('venue_id', $venue->id)->where('transaction_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');
		$order_commission = round($admin_percentage * $request->total_amount / 100,2);
		$commission_in_venue = $venue->admin_commission;

		if($commission_in_venue > $collected_commission + $order_commission){
			$order->admin_commission = $order_commission;
		}else{
			$order->admin_commission = $commission_in_venue - $collected_commission;
		}

		if(isset($request->reserved_payment)){
			$order->reserved_payment = 1;
		}

		$order->save();
		$transaction_id = str_random();
		$transaction = new Transaction;
		$transaction->order_id = $order->id;
		$transaction->customer_id = $request->customer_id;
		$transaction->venue_id = $request->venue_id;
		$transaction->transaction_id = $transaction_id;
		$transaction->amount = $request->total_amount;
		$transaction->waiter_id = $waiter->user_id;
		$transaction->waiter_tip = $request->waiter_tip;
		$transaction->admin_commission = $order->admin_commission;
		$transaction->transaction_status = $request->order_status;
		$transaction->total = $request->total_amount + $request->waiter_tip;

		if(isset($request->reserved_payment)){
			$transaction->reserved_payment = 1;
		}

		$transaction->save();

		$counter = 0;

		foreach($products as $product){
			$product_obj = Product::find($product);
			$ordered_product = new OrderedProduct;
			$ordered_product->product_id = $product_obj->id;
			$ordered_product->price = $product_obj->price;
			$ordered_product->order_status = 3;
			$ordered_product->quantity = 1;
			$ordered_product->bar_id = $product_obj->bar_id;
			$ordered_product->customer_id = $request->customer_id;
			$ordered_product->venue_id = $request->venue_id;
			$ordered_product->order_id = $order->id;
			$ordered_product->save();
			$counter++;
		}

		return 'Order has been created';
	}

	public function customerHome(Request $request){
	   $recently_visited_venues = [];
	   $favorite_venues = [];
	   $featured_venues = [];
	   $recommended_venues = [];

	   $customer_id = $request->customer_id;
	   $orders = Order::select('venue_id')->where('customer_id', $customer_id)->groupBy('venue_id')->orderBy('created_at', 'desc')->get();
	   $counter = 0;
	   foreach($orders as $order){
	       $venue = Venue::find($order->venue_id);
		   if($venue->status == 0){
			   continue;
		   }
		   $counter++;
	       if($venue->banner == ''){
	           $venue->banner = asset('public/uploads/banners/default.png');
	       }else{
	           $venue->banner = asset('public/uploads/banners/' . $venue->banner);
	       }

			$rating = DB::table('reviews')
						->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
						->where('venue_id', $venue->id)
						->first();
			if($rating->average_rating){
				$rating = $rating->average_rating / $rating->total_records;
			}else{
				$rating = 0;
			}

		   $venue->rating = $rating;
		   if(CustomerFavoriteVenue::where('venue_id', $venue->id)->where('customer_id', $customer_id)->exists()){
			   $venue->favorite = true;
		   }else{
			   $venue->favorite = false;
		   }
		   $recently_visited_venues[] = $venue;

		   if($counter == 7){
			   $counter = 0;
			   break;
		   }
	   }

		$featuredvenues = Venue::where('status', 1)->where('featured', 1)->orderBy('created_at', 'asc')->get();
		foreach($featuredvenues as $featuredvenue){

	       $venue = Venue::find($featuredvenue->id);
		   if($venue->status == 0){
			   continue;
		   }
		   $counter++;

	       if($venue->banner == ''){
	           $venue->banner = asset('public/uploads/banners/default.png');
	       }else{
	           $venue->banner = asset('public/uploads/banners/' . $venue->banner);
	       }

			$rating = DB::table('reviews')
						->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
						->where('venue_id', $venue->id)
						->first();
			if($rating->average_rating){
				$rating = $rating->average_rating / $rating->total_records;
			}else{
				$rating = 0;
			}

		   $venue->rating = $rating;
		   if(CustomerFavoriteVenue::where('venue_id', $venue->id)->where('customer_id', $customer_id)->exists()){
			   $venue->favorite = true;
		   }else{
			   $venue->favorite = false;
		   }
		   $featured_venues[] = $venue;

		   if($counter == 7){
			   $counter = 0;
			   break;
		   }
	   }



		$customerFavoriteVenues = CustomerFavoriteVenue::where('customer_id', $customer_id)->orderBy('created_at', 'desc')->get();

		foreach($customerFavoriteVenues as $customerFavoriteVenue){
	       $venue = Venue::find($customerFavoriteVenue->venue_id);
		   if($venue->status == 0){
			   continue;
		   }
		   $counter++;
	       if($venue->banner == ''){
	           $venue->banner = asset('public/uploads/banners/default.png');
	       }else{
	           $venue->banner = asset('public/uploads/banners/' . $venue->banner);
	       }


		   $rating = DB::table('reviews')
						->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
						->where('venue_id', $venue->id)
						->first();
			if($rating->average_rating){
				$rating = $rating->average_rating / $rating->total_records;
			}else{
				$rating = 0;
			}

		   $venue->rating = $rating;
		   if(CustomerFavoriteVenue::where('venue_id', $venue->id)->where('customer_id', $customer_id)->exists()){
			   $venue->favorite = true;
		   }else{
			   $venue->favorite = false;
		   }
	       $favorite_venues[] = $venue;

		   if($counter == 7){
			   $counter = 0;
			   break;
		   }
	   }

	   $recommendedvenues = DB::table('reviews')
				->select(DB::raw('venue_id, sum(average_rating) as average_rating, count(average_rating) as total_records'))
				->groupBy('venue_id')
				->orderBy('average_rating', 'desc')
				->get();


		foreach($recommendedvenues as $recommendedvenue){
	       $venue = Venue::find($recommendedvenue->venue_id);
		   if($venue->status == 0){
			   continue;
		   }
		   $counter++;
	       if($venue->banner == ''){
	           $venue->banner = asset('public/uploads/banners/default.png');
	       }else{
	           $venue->banner = asset('public/uploads/banners/' . $venue->banner);
	       }

		   $rating = $recommendedvenue->average_rating / $recommendedvenue->total_records;

		   $venue->rating = $rating;
		   if(CustomerFavoriteVenue::where('venue_id', $venue->id)->where('customer_id', $customer_id)->exists()){
			   $venue->favorite = true;
		   }else{
			   $venue->favorite = false;
		   }
	       $recommended_venues[] = $venue;

		   if($counter == 7){
			   $counter = 0;
			   break;
		   }
	   }

	   return [
				'recently_visited_venues' => $recently_visited_venues,
				'featured_venues' => $featured_venues,
				'favorite_venues' => $favorite_venues,
				'recommended_venues' => $recommended_venues,
				'status' => true,
			   ];

	}



	public function makeFavoriteVenue(Request $request){
		$customer_favorite_venue = CustomerFavoriteVenue::where('venue_id', $request->venue_id)->where('customer_id', $request->customer_id);
		if($customer_favorite_venue->exists()){
			$customer_favorite_venue->delete();
		}else{
			$customer_favorite_venue = new CustomerFavoriteVenue;
			$customer_favorite_venue->customer_id = $request->customer_id;
			$customer_favorite_venue->venue_id = $request->venue_id;
			$customer_favorite_venue->save();
		}

		return ['status' => 'true'];
	}

	public function venueProducts(Request $request){
		$product_list = [];
		$products = Product::where('status', 1)->where('venue_id', $request->venue_id)->get();
		foreach($products as $product){
			if($product->image == ''){
				$product->image = asset('public/uploads/products/default.png');
			}else{
				$product->image = asset('public/uploads/products/' . $product->image);
			}
			$product_list[] = $product;
		}

		return [
				'products' => $product_list,
				'status'=> true
				];
	}

	public function productDetails(Request $request){
		$product = Product::find($request->product_id);

		if($product->image == ''){
			$product->image = asset('public/uploads/products/default.png');
		}else{
			$product->image = asset('public/uploads/products/' . $product->image);
		}

		$variants = ProductVariant::where('product_id', $request->product_id)->get();
		$extras = ExtraProduct::where('product_id', $request->product_id)->get();

		return [
				'product' => $product,
				'variants' => $variants,
				'extras' => $extras,
				'status' => true
				];
	}

	public function venueDetails(Request $request){
		$venue = Venue::find($request->venue_id);

		$distance = 0;
		if(CustomerFavoriteVenue::where('venue_id', $request->venue_id)->where('customer_id', $request->customer_id)->exists()){
			$favorite = true;
		}else{
			$favorite = false;
		}

		$rating = DB::table('reviews')
						->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
						->where('venue_id', $request->venue_id)
						->first();
		if($rating->average_rating){
			$rating = $rating->average_rating / $rating->total_records;
		}else{
			$rating = 0;
		}

		return [
				'venue' => $venue,
				'status' => true,
				'favorite' => $favorite,
				'rating' => $rating,
				'distance' => 0,

				];
	}


	public function venueInfo(Request $request){
		$venue = Venue::find($request->venue_id);
		$venue_admin = User::where('role', 'venue')->where('venue_id', $venue->id)->first();
		$trading_hours = TradingHour::where('venue_id', $request->venue_id)->get();
		$facilities = FacilityVenue::where('venue_id', $venue->id)->get();
		$dietaries = VenueDietary::where('venue_id', $venue->id)->get();

		foreach($facilities as $facility){
			$_facility = Facility::find($facility->facility_id);
			$facility->name = $_facility->name;
		}

		foreach($dietaries as $dietary){
			$_dietary = Dietary::find($dietary->dietary_id);
			$dietary->name = $_dietary->name;
		}

		return [
				'venue' => $venue,
				'trading_hours' => $trading_hours,
				'venue_admin' => $venue_admin,
				'facilities' => $facilities,
				'dietaries' => $dietaries,
				'status' => true,
				];
	}

	public function venues(){
		$venue_list = [];
		$venues = Venue::where('status', 1)->get();
		foreach($venues as $venue){
			if($venue->banner == ''){
			   $venue->banner = asset('public/uploads/banners/default.png');
			}else{
			   $venue->banner = asset('public/uploads/banners/' . $venue->banner);
			}

			$rating = DB::table('reviews')
						->select(DB::raw('sum(average_rating) as average_rating, count(average_rating) as total_records'))
						->where('venue_id', $venue->id)
						->first();
			if($rating->average_rating){
				$rating = $rating->average_rating / $rating->total_records;
			}else{
				$rating = 0;
			}
			$venue->rating = $rating;
			$venue_list[] = $venue;
		}

		return [
				'venues' => $venue_list,
				'status' => true,
				];
	}


	public function makeOrder(Request $request){

		//check transaction against last order of current customer
		$last_order = Order::where('customer_id', $request->customer_id)->max('id');

		if($last_order){
			if(Transaction::where('order_id', $last_order)->doesntExist()){
				return [
						'status' => false,
						'message' => 'Please Complete your pending order.'
						];
			}
		}

		$order_number = Order::max('order_number') + 1;
		if($order_number < 1000){
			$order_number = 1001;
		}


		$waiter = TableWaiter::where('table_id', $request->table_id)->first();
		if(!$waiter){
			return [
					'status' => false,
					'message' => 'No waiter assign to this table.',
					];
		}

		$order = new Order;
		$order->order_number = $order_number;
		$order->invoice_number = $order_number;
		$order->venue_id = $request->venue_id;
		$order->customer_id = $request->customer_id;
		$order->table_id = $request->table_id;
		$order->connect_as = $request->connect_as;
		$order->host_pin = $request->host_pin;
		$order->pay_by = $request->customer_id;
		$order->waiter_id = $waiter->user_id;
		$order->order_status = 'Ordered';
		$order->status = 1;

		if(isset($request->reserve_payment) && $request->reserve_payment == 1){

			$customer_reserved_fund = ReservedFund::where('customer_id', $request->customer_id)->sum('amount');
			$customer_reserved_debit = Transaction::where('customer_id', $request->customer_id)->where('reserved_payment', '!=', 0)->sum('amount');
			$customer_reserved_fund = $customer_reserved_fund - $customer_reserved_debit;

			if($request->reserve_funds != '' && $request->reserve_funds > $customer_reserved_fund){
				return [
						'status' => false,
						'message' => "You don't have enough balance in your wallet for reseved funds.",
						];

			}

			$order->reserve_funds = $request->reserve_funds;
			$order->reserved_payment = 1;
		}

		$order->save();

		return [
				'status' => true,
				'message' => 'Order has been created.',
				'order_id' => $order->id,
				'venue_id' => $order->venue_id,
				];
	}

	public function connectAsGuest(Request $request){
		$order = Order::where('customer_id', $request->host_id)->where('host_pin', $request->host_pin)->first();
		if(!$order){
			return [
				'status' => false,
				'message' => 'Invalid host pin.',
				];
		}

		if($request->pay_by == 'myself'){
			$order->pay_by = $request->guest_id;
			$order->save();
		}

		$guest = Guest::where('guest_id', $request->guest_id)->where('order_id', $order->id)->first();
		if($guest){

			$guest->online = 1;
			$guest->save();

			return [
				'status' => true,
				'message' => 'You are now connected as a guest.',
				'order_id' => $order->id,
				'venue_id' => $order->venue_id
				];
		}

		$guest = new Guest;
		$guest->guest_id = $request->guest_id;
		$guest->order_id = $order->id;
		$guest->save();

		return [
				'status' => true,
				'message' => 'You are now connected as a guest.',
				'order_id' => $order->id,
				'venue_id' => $order->venue_id
				];

	}


	public function addToCart(Request $request){

		$order = Order::find($request->order_id);
		if(!$order){
			return [
					'status' => false,
					'message' => 'Order not found.'
					];
		}

		$product = Product::find($request->product_id);

		if(!$product){
			return [
					'status' => false,
					'message' => 'Product not found.'
					];
		}

		$_product = new CartProduct;
		$_product->order_id = $order->id;
		$_product->venue_id = $order->venue_id;
		$_product->customer_id = $order->customer_id;
		$_product->product_id = $product->id;
		$_product->price = $product->price;
		$_product->bar_id = $product->bar_id;
		$_product->order_by = $request->customer_id;
		$_product->order_status = 'Ordered';
		$_product->comment = $request->comment;

		if($request->quantity != ''){
			$_product->quantity = $request->quantity;
		}

		$_product->save();

		$variant_quantities = array_filter($request->variant_quantities);
		$extra_quantities = array_filter($request->extra_quantities);

		$variants = $request->variants;
		$extras = $request->extras;

		$counter = 0;
		foreach($variants as $variant){
			$variable = ProductVariant::find($variant);
			$_variable = new CartProductVariable;
			$_variable->type = 'variant';
			$_variable->variable_id = $variant;
			$_variable->name = $variable->name;
			$_variable->price = $variable->price;
			$_variable->order_by = $request->customer_id;
			$_variable->order_status = 'Ordered';
			$_variable->quantity = $variant_quantities[$counter];
			$_variable->cart_product_id = $_product->id;
			$_variable->save();
			$counter++;
		}

		$counter = 0;
		foreach($extras as $extra){
			$variable = ExtraProduct::find($extra);
			$_variable = new CartProductVariable;
			$_variable->type = 'extra';
			$_variable->variable_id = $extra;
			$_variable->name = $variable->name;
			$_variable->price = $variable->price;
			$_variable->order_by = $request->customer_id;
			$_variable->order_status = 'Ordered';
			$_variable->quantity = $extra_quantities[$counter];
			$_variable->cart_product_id = $_product->id;
			$_variable->save();
			$counter++;
		}

		return [
				'status' => true,
				'message' => 'Product has been added to cart.'
				];
	}

	public function customerCartProducts(Request $request){

		$products_list = [];
		$variant_list = [];
		$extra_list = [];

		$cart_products = CartProduct::where('order_by', $request->customer_id)->where('checkout', 0)->where('order_id', $request->order_id)->get();

		foreach($cart_products as $cart_product){
			$product = Product::find($cart_product->product_id);

			if($product->image == ''){
				$product->image = asset('public/uploads/products/default.png');
			}else{
				$product->image = asset('public/uploads/products/' . $product->image);
			}

			$variants = CartProductVariable::where('cart_product_id', $cart_product->id)->where('order_by', $request->customer_id)->where('type', 'variant')->get();

			$extras = CartProductVariable::where('cart_product_id', $cart_product->id)->where('order_by', $request->customer_id)->where('type', 'extra')->get();

			$cart_product->name = $product->name;
			$cart_product->image = $product->image;
			$cart_product->extras = $extras;
			$cart_product->variants = $variants;
			$products_list[] = $cart_product;
		}


		return [
				'products' => $products_list,
				'status' => true,
				];
	}

	public function allCartProducts(Request $request){

		$products_list = [];
		$variant_list = [];
		$extra_list = [];

		$cart_products = CartProduct::where('order_id', $request->order_id)->get();

		foreach($cart_products as $cart_product){

			$product = Product::find($cart_product->product_id);

			if($product->image == ''){
				$product->image = asset('public/uploads/products/default.png');
			}else{
				$product->image = asset('public/uploads/products/' . $product->image);
			}

			$variants = CartProductVariable::where('cart_product_id', $cart_product->id)->where('type', 'variant')->get();
			$extras = CartProductVariable::where('cart_product_id', $cart_product->id)->where('type', 'extra')->get();
			$customer = User::find($cart_product->order_by);

			$cart_product->name = $product->name;
			$cart_product->image = $product->image;
			$cart_product->order_by_name = $customer->name;
			$cart_product->extras = $extras;
			$cart_product->variants = $variants;
			$products_list[] = $cart_product;

		}


		return [
				'products' => $products_list,
				'status' => true,
				];

	}

	public function checkoutCustomerProducts(Request $request){
		CartProduct::where('order_by', $request->customer_id)->where('order_id', $request->order_id)->update(['checkout' => 1]);
		return [
				'status' => true,
				];
	}

	public function checkoutAllProducts(Request $request){
		$total_amount = 0;
		CartProduct::where('order_by', $request->customer_id)->where('order_id', $request->order_id)->update(['checkout' => 1]);

		if(CartProduct::where('order_by', $request->customer_id)->where('checkout', 0)->exists()){
			return [
				'status' => false,
				'message' => 'Your guest has not been checkedout.',
				];
		}

		$order = Order::find($request->order_id);
		if(!$order){
			return [
					'status' => false,
					'message' => 'Order not found.'
					];
		}

		$products = CartProduct::where('order_id', $request->order_id)->get();

		foreach($products as $product){
			$_product = new OrderedProduct;
			$_product->order_id = $order->id;
			$_product->venue_id = $order->venue_id;
			$_product->customer_id = $order->customer_id;
			$_product->product_id = $product->product_id;
			$_product->price = $product->price;
			$_product->bar_id = $product->bar_id;
			$_product->order_by = $product->order_by;
			$_product->order_status = 0;
			$_product->comment = $product->comment;
			$_product->quantity = $product->quantity;
			$_product->save();

			$variables = CartProductVariable::where('cart_product_id', $product->id)->get();
			foreach($variables as $variable){

				$_variable = new OrderedProductVariable;
				$_variable->type = $variable->type;
				$_variable->variable_id = $variable->variable_id;
				$_variable->name = $variable->name;
				$_variable->price = $variable->price;
				$_variable->order_by = $variable->order_by;
				$_variable->order_status = 0;
				$_variable->quantity = $variable->quantity;
				$_variable->ordered_product_id = $_product->id;
				$_variable->bar_id = $product->bar_id;
				$_variable->save();
			}

			$variant_total = OrderedProductVariable::select(DB::raw('sum(quantity * price) as total'))->where('type', 'variant')->where('ordered_product_id', $_product->id)->first();

			if($variant_total->total){
				$total_amount += $variant_total->total;
			}else{
				$total_amount += $_product->price;
			}

			$extra_total = OrderedProductVariable::select(DB::raw('sum(quantity * price) as total'))->where('type', 'extra')->where('ordered_product_id', $_product->id)->first();

			if($extra_total->total){
				$total_amount += $extra_total->total;
			}

			CartProductVariable::where('cart_product_id', $product->id)->delete();
		}

		CartProduct::where('order_id', $request->order_id)->delete();

		// handle admin commission
		$venue = Venue::find($order->venue_id);
		$admin_percentage = User::where('role', 'admin')->first()->percentage;
		$collected_commission = Transaction::where('venue_id', $venue->id)->where('transaction_status', 'Completed')->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])->sum('admin_commission');
		$order_commission = round($admin_percentage * $total_amount / 100,2);
		$commission_in_venue = $venue->admin_commission;

		if($commission_in_venue > $collected_commission + $order_commission){
			$order->admin_commission = $order_commission;
		}else{
			$order->admin_commission = $commission_in_venue - $collected_commission;
		}
		$order->total_amount = $total_amount;
		$order->save();

		return [
				'status' => true,
				];
	}


	public function deleteCartProduct(Request $request){

		CartProduct::where('id', $request->cart_product_id)->delete();
		CartProductVariable::where('cart_product_id', $request->cart_product_id)->delete();

		return [
				'status' => true,
				];
	}

	public function updateCartProduct(Request $request){

		CartProduct::where('id', $request->cart_product_id)->update(['quantity' => $request->quantity]);
		return [
				'status' => true,
				];
	}

	public function deleteCartProductVariable(Request $request){

		CartProductVariable::where('id', $request->cart_product_variable_id)->delete();

		return [
				'status' => true,
				];
	}


	public function updateCartProductVariable(Request $request){

		CartProductVariable::where('id', $request->cart_product_variable_id)->update(['quantity' => $request->quantity]);
		return [
				'status' => true,
				];
	}

	public function searchVenue(Request $request){
		if($request->search == ''){
			return [
					'status' => false,
					'message' => 'Search field is empty.'
					];
		}

		$venues = Venue::where('status', 1)->where('name','LIKE','%'.$request->search."%")->get();

		foreach($venues as $venue){
			if($venue->banner == ''){
				$venue->banner = asset('public/uploads/banners/default.png');
			}else{
				$venue->banner = asset('public/uploads/banners/' . $venue->banner);
			}
		}


		return [
					'status' => true,
					'venues' => $venues
					];
	}

	public function searchVenueProduct(Request $request){
		if($request->search == ''){
			return [
					'status' => false,
					'message' => 'Search field is empty.'
					];
		}

		$products = Product::where('venue_id', $request->venue_id)->where('status', 1)->where('name','LIKE','%'.$request->search."%")->get();

		foreach($products as $product){
			if($product->image == ''){
				$product->image = asset('public/uploads/products/default.png');
			}else{
				$product->image = asset('public/uploads/products/' . $product->image);
			}
		}

		return [
					'status' => true,
					'products' => $products
					];
	}


	public function connectToVenue(Request $request){
		$tables = Table::where('venue_id', $request->venue_id)->where('status', 1)->get();
		$hosts = Order::where('venue_id', $request->venue_id)->where('order_status', 'Ordered')->groupBy('customer_id')->get();
		foreach($hosts as $host){
			$_host = User::find($host->customer_id);
			$host->name = $_host->name;
		}
		return [
				'status' => true,
				'tables' => $tables,
				'hosts' => $hosts
				];
	}

	public function orderedProducts(Request $request){

		$ordered_products = [];

		$customers = OrderedProduct::distinct()->get(['order_by']);
		$final = [];

		foreach($customers as $customer){

			$_customer = User::find($customer->order_by);
			$products = OrderedProduct::where('order_id', $request->order_id)->where('order_by', $request->customer_id)->get();

			$ordered_products[]['customer'] = $_customer;

			foreach($products as $product){
				$_product = Product::find($product->product_id);
				$product->name = $_product->name;
				$product->image = $_product->image;

				if($product->image == ''){
					$product->image = asset('public/uploads/products/default.png');
				}else{
					$product->image = asset('public/uploads/products/' . $product->image);
				}

				$variants = orderedProductVariable::where('ordered_product_id', $product->id)->where('type', 'Variant')->get();
				$extras = orderedProductVariable::where('ordered_product_id', $product->id)->where('type', 'Extra')->get();


				$product->variants = $variants;
				$product->extras = $extras;
			}

			$ordered_products[]['products'] = $products;

		}

		return [
				'status' => true,
				'ordered_products' => $ordered_products
				];
	}


	public function payOrder(Request $request){
		if(Transaction::where('order_id', $request->order_id)->exists()){
			return [
						'status' => false,
						'message' => 'A customer has paid for this order.'
					];
		}

		$order = Order::where('id', $request->order_id)->first();
		if(!$order){
			return [
						'status' => false,
						'message' => 'Order not found.'
					];
		}
		$order->waiter_tip = $request->waiter_tip;
		$order->order_status = 'Completed';
		if($order->customer_id == $order->pay_by){

			//Check Reserved funds balance of customer
			$customer_reserved_fund = ReservedFund::where('customer_id', $order->customer_id)->sum('amount');
			$customer_reserved_debit = Transaction::where('customer_id', $order->customer_id)->where('reserved_payment', '!=', 0)->sum('amount');
			$customer_reserved_fund = $customer_reserved_fund - $customer_reserved_debit;

			if($order->total_amount + $order->waiter_tip > $customer_reserved_fund){
				return [
							'status' => false,
							'message' => "You don't have enough balance in your wallet to pay this reseved funds."
						];
			}

			$transaction = new Transaction;
			$transaction->transaction_id = str_random();
			$transaction->order_id = $order->id;
			$transaction->customer_id = $order->customer_id;
			$transaction->waiter_id = $order->waiter_id;
			$transaction->venue_id = $order->venue_id;
			$transaction->table_id = $order->table_id;
			$transaction->amount = $order->total_amount;
			$transaction->admin_commission = $order->admin_commission;
			$transaction->waiter_tip = $order->waiter_tip;
			$transaction->transaction_status = $order->order_status;
			$transaction->reserved_payment = $order->reserved_payment;
			$transaction->reserve_funds = $order->reserve_funds;
			$transaction->pay_by = $order->pay_by;

			$order->payment_type = 'Wallet';

			$transaction->payment_type = $order->payment_type;



		}else{

			$transaction = new Transaction;
			$transaction->transaction_id = str_random();
			$transaction->order_id = $order->id;
			$transaction->customer_id = $order->customer_id;
			$transaction->waiter_id = $order->waiter_id;
			$transaction->venue_id = $order->venue_id;
			$transaction->table_id = $order->table_id;
			$transaction->amount = $order->total_amount;
			$transaction->admin_commission = $order->admin_commission;
			$transaction->waiter_tip = $order->waiter_tip;
			$transaction->transaction_status = $order->order_status;
			$transaction->reserved_payment = 0;
			$transaction->reserve_funds = 0;
			$transaction->pay_by = $order->pay_by;

			$order->payment_type = 'Strip';

			$transaction->payment_type = $order->payment_type;

		}

		$order->save();
		$transaction->save();

		return [
					'status' => true,
					'message' => 'You have paid successfully for your order.'
				];
	}

	public function customerBills(Request $request){
		$transactions = Transaction::where('pay_by', $request->customer_id)->where('venue_id', $request->venue_id)->get();

		return [
				'status' => true,
				'bills' => $transactions
			];
	}

	public function addBooking(Request $request){
		$booking_number = Booking::max('booking_number');

		if($booking_number < 1000){
			$booking_number = 1000;
		}

		$booking_number = $booking_number + 1;

		$booking = new Booking;
		$booking->booking_number = $booking_number;
		$booking->customer_id = $request->customer_id;
		$booking->seating = $request->seating;
		$booking->smooking = $request->smoking;
		$booking->area = $request->area;
		$booking->message = $request->message;

		$request->booking_date = str_ireplace('/', '-', $request->booking_date);
		$request->booking_time = str_ireplace('/', '-', $request->booking_time);

		$booking->booking_date = date('Y-m-d', strtotime($request->booking_date));
		$booking->booking_time = date('H:i', strtotime($request->booking_time));


		if(Booking::where('customer_id', $request->customer_id)->where('booking_date', $booking->booking_date)->where('booking_time', $booking->booking_time)->exists()){
			return [
						'status' => false,
						'message' => 'You have already requested for this date & time.'
					];
		}

		$booking->date_time = date('Y-m-d H:i:s', strtotime($booking->booking_date . ' ' . $booking->booking_time));

		$booking->venue_id = $request->venue_id;
		$booking->booking_status = 'Pending';
		$booking->save();

		return [
					'status' => true,
					'message' => 'Your request for booking has been sent.'
				];
	}



}
