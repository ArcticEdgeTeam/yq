<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\User;
use App\Table;
use App\Transaction;
use Auth;
use Carbon\Carbon;

class CashupForBarController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(Request $request) {
		$id = Auth::user()->venue_id;
		// $transactions = Transaction::where('venue_id', $id)->orderBy('id', 'desc')->get();

		$getTransactions = Transaction::query();
		$getTransactions = $getTransactions->select('transactions.*', 'orders.invoice_number', 'customer_table.name as customer_name', 'customer_table.photo as customer_photo', 'waiter_table.name as waiter_name', 'waiter_table.photo as waiter_photo', 'tables.id as table_id', 'tables.name as table_name')
		->leftjoin('users as customer_table','customer_table.id', '=', 'transactions.customer_id')
		->leftjoin('users as waiter_table','waiter_table.id', '=', 'transactions.waiter_id')
		->leftjoin('orders', 'orders.id', '=', 'transactions.order_id')
		->leftjoin('tables', 'tables.id', '=', 'transactions.table_id')
		->where('transactions.reserved_payment', '!=', 1)
		->where('transactions.venue_id', $id);

		if($request->name != '') {
			$getTransactions = $getTransactions->where('customer_table.name', 'like', '%' . $request->name . '%');
		}

		if($request->waiter != '') {
			$getTransactions = $getTransactions->where('transactions.waiter_id', $request->waiter);
		}

		if($request->status != '') {
			$getTransactions = $getTransactions->where('transactions.transaction_status', $request->status);
		}

		if($request->table != '') {
			$getTransactions = $getTransactions->where('transactions.table_id', $request->table);
		}

		if($request->date_range != '') {
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];

			// change date format
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));

			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();

			$getTransactions = $getTransactions->whereBetween('transactions.created_at', [$start, $end]);
			
		}

		$getTransactions = $getTransactions->orderBy('orders.id', 'desc')->paginate(15);

		//get waiters
		$getWaiters = User::where('status', 1)->where('role', 'waiter')->where('venue_id', $id)->get();
		// get tables
		$getTables = Table::where('status', 1)->where('venue_id', $id)->get();

		$venue = Venue::find($id);
		$total = 0;
		return view('bar.cashups', compact('getTransactions', 'getWaiters', 'getTables', 'venue','total'));
	}
	
	public function filter(Request $request){
		$id = Auth::user()->venue_id;
		$venue = Venue::find($id);
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$transactions = Transaction::where('venue_id', $id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			$total = 0;
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			
			return view('bar.cashups', compact('counter', 'transactions', 'venue', 'id', 'total', 'start', 'end'));
		}
		
		return abort(404);
		
	}
}
