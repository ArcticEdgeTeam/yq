<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ReservedFund;
use DB;

class ReservedFundController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		$reservedfunds = DB::table('reserved_funds')
						->select(DB::raw('id, customer_id, sum(amount) as amount, created_at'))
						->groupBy('customer_id')
						->orderBy('created_at', 'desc')
						->get();
		$counter = 1;
		return view('admin.reservedfunds', compact('counter', 'reservedfunds'));
	}
	
	public function view($customer_id){
		$total = 0;
		$reservedfunds = ReservedFund::where('customer_id', $customer_id)->get();
		$counter = 1;
		return view('admin.reservedfund-view', compact('counter', 'reservedfunds', 'total'));
	}
}
