<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use Auth;
use App\Order;
use App\User;
use App\Table;
use App\OrderedProduct;
use DB;
use App\OrderedProductVariable;
use Carbon\Carbon;
use Session;

class OrderForBarController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index(){
		
		$counter = 1;
		$venue = Venue::find(Auth::user()->venue_id);
		$orders = Order::where('venue_id', Auth::user()->venue_id)->where('order_status', '!=', 'Completed')->where('order_status', '!=', 'Refunded')->orderBy('id', 'desc')->get();
		return view('bar.orders', compact('venue', 'orders', 'counter'));
	}
	
	public function orderList() {
		$venue = Venue::find(Auth::user()->venue_id);
		$orders = Order::where('venue_id', Auth::user()->venue_id)->orderBy('id', 'desc')->get();
		$counter = 1;
		$start = date('d-m-Y');
		$end = date('d-m-Y');
		return view('bar.orderlist', compact('venue', 'orders', 'counter', 'start', 'end'));
	}
	
	public function orderListFilter(Request $request){
		$venue = Venue::find(Auth::user()->venue_id);
		if(isset($request->date_range)){
			$date_range = explode('/', $request->date_range);
			$start = $date_range[0];
			$end = $date_range[1];
			
			$start = date('Y-m-d', strtotime($start));
			$end = date('Y-m-d', strtotime($end));
			
			$start = Carbon::parse($start)->startOfDay();
			$end = Carbon::parse($end)->endOfDay();
			$orders = Order::where('venue_id', Auth::user()->venue_id)->whereBetween('created_at', [$start, $end])->orderBy('id', 'desc')->get();
			$counter = 1;
			
			$start = date('d-m-Y', strtotime($start));
			$end = date('d-m-Y', strtotime($end));
			
			return view('bar.orderlist', compact('venue', 'orders', 'counter', 'start', 'end'));
			
		}
		
		return abort(404);
		
	}
	
	
	public function edit($id){
		$counter = 1;
		$venue = Venue::find(Auth::user()->venue_id);
		$order = Order::find($id);
		$waiter = User::where('id', $order->waiter_id)->first();
		$customer = User::where('id', $order->customer_id)->first();
		$table = Table::find($order->table_id);
		//$bar_id = DB::table('bars')->where('user_id', Auth::user()->id)->first()->id;
		$ordered_products = OrderedProduct::where('order_id', $order->id)->where('bar_id', Session::get('current_bar_id'))->orderBy('id', 'desc')->get();
		$total_amount = 0;
		
		return view('bar.order-edit', compact('counter', 'id', 'order', 'venue', 'waiter', 'customer', 'table', 'ordered_products', 'total_amount'));
	}
	
	
	public function quickview($order_id) {
		$order = Order::find($order_id);
		$waiter = User::find($order->waiter_id);
		$customer = User::find($order->customer_id);
		$table = Table::find($order->table_id);
		//$bar_id = DB::table('bars')->where('user_id', Auth::user()->id)->first()->id;
		$ordered_products = OrderedProduct::where('order_id', $order->id)->where('bar_id', Session::get('current_bar_id'))->orderBy('id', 'desc')->get();
		$total_amount = 0;
		$counter = 1;
		return view('ajax.bar-order-quickview', compact('counter', 'order', 'customer', 'waiter', 'table', 'ordered_products', 'total_amount'));
	}
	
	
	public function popview($order_id){
		$order = Order::find($order_id);
		$waiter = User::find($order->waiter_id);
		$customer = User::find($order->customer_id);
		$table = Table::find($order->table_id);
		//$bar_id = DB::table('bars')->where('user_id', Auth::user()->id)->first()->id;
		$ordered_products = OrderedProduct::where('order_id', $order->id)->where('bar_id', Session::get('current_bar_id'))->orderBy('id', 'desc')->get();
		$total_amount = 0;
		$counter = 1;
		return view('ajax.bar-order-popview', compact('counter', 'order', 'customer', 'waiter', 'table', 'ordered_products', 'total_amount'));
	}
	
	public function updateOrderStatus(Request $request){
		$id = $request->id;
		$status = $request->status;
		$ordered_product = OrderedProduct::find($id);
		$ordered_product->order_status = $status;
		$ordered_product->save();
		OrderedProductVariable::where('ordered_product_id', $id)->update(['order_status' => $status]);
		
		if(!OrderedProduct::where('order_id', $ordered_product->order_id)->where('order_status', '!=', 3)->exists()){
			Order::where('id', $ordered_product->order_id)->where('order_status', '!=', 'Refunded')->where('order_status', '!=', 'Completed')->update(['order_status' => 'Completed']);
		}
	}
}
