<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Unit;
use App\Venue;

class UnitController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$units = Unit::all();
		return view('admin.units', compact('units', 'counter'));
	}
	
	public function create($id){
		$venue = Venue::find($id);
		return view('admin.venue-unit-create', compact('id', 'venue'));
	}
	
	public function save(Request $request, $id) {

		// check dupliacte Unit for venue
        $getUnit = Unit::where(function($query) use ($request, $id){
								    $query->where('name', $request->name);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id){
        							$query->where('symbol', $request->symbol);
									$query->where('venue_id', $id);
        						})->first();

        if ($getUnit != null) {

			return redirect(route('admin.venue.unit.create', $id))->with('error', 'Unit name or symbol already exists.');
		}
		
		$unit = new Unit;
		$unit->name = $request->name;
		$unit->symbol = $request->symbol;
		$unit->venue_id = $id;
		$venue = Venue::find($id);
		if(isset($request->status)){
			$unit->status = 1;
		}else{
			$unit->status = 0;
		}
		
		if($unit->save()){
			return redirect(route('admin.venue.ingredients', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.venue.ingredients', $id))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id, $unit_id){
		$unit = Unit::find($unit_id);
		$venue = Venue::find($id);
		return view('admin.venue-unit-edit', compact('id', 'unit', 'venue'));
	}
	
	
	public function update(Request $request, $id, $unit_id) {

		// check dupliacte Unit for venue
        $getUnit = Unit::where(function($query) use ($request, $id, $unit_id){
								    $query->where('name', $request->name);
								    $query->where('id', '!=', $unit_id);
									$query->where('venue_id', $id);
								})
        						->orWhere(function($query) use ($request, $id, $unit_id){
        							$query->where('symbol', $request->symbol);
        							$query->where('id', '!=', $unit_id);
									$query->where('venue_id', $id);
        						})->first();

        if ($getUnit != null) {

			return redirect(route('admin.venue.unit.edit', [$id, $unit_id]))->with('error', 'Unit name or symbol already exists.');
		}
		
		$unit = Unit::find($unit_id);
		$unit->name = $request->name;
		$unit->symbol = $request->symbol;
		$venue = Venue::find($id);
		if(isset($request->status)){
			$unit->status = 1;
		}else{
			$unit->status = 0;
		}
		
		if($unit->save()){
			return redirect(route('admin.venue.ingredients', $id))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.venue.ingredients', $id))->with('error', 'Record has not been updated.');
		}
		
	}
}
