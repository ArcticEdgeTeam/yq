<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductVariant;
use App\ProductIngredientVariation;

class ProductVariantController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function delete($venue_id, $ingredient_id){
		$ingredient = ProductVariant::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		// Delete record from Product Ingredient Variation Table
		$deleteVariation = ProductIngredientVariation::where('variation_id', $ingredient_id)->delete();
		return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('success', 'Record has been deleted.');
	}
	
	public function update(Request $request, $venue_id, $product_id){
		$variant = ProductVariant::find($request->id);
		$variant->name = $request->name;
		$variant->price = $request->price;
		if(isset($request->status)){
			$variant->status = 1;
		}else{
			$variant->status = 0;
		}
		$variant->save();
		return redirect(route('admin.venue.product.edit', [$venue_id, $product_id]))->with('success', 'Record has been updated.');
	}
}
