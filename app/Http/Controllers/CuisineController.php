<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Cuisine;
use Auth;
use App\TrailLog;

class CuisineController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$cuisines = Cuisine::all();
		return view('admin.cuisines', compact('cuisines', 'counter'));
	}
	
	public function create(){
		return view('admin.cuisine-create');
	}
	
	public function save(Request $request){
		
		$cuisine = new Cuisine;
		$cuisine->name = $request->name;
		
		if(isset($request->status)){
			$cuisine->status = 1;
		}else{
			$cuisine->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('cuisines/', ['disk' => 'public']);
			$cuisine->image = $request->image->hashName();
		}
		
		if($cuisine->save()) {

				// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cuisine';
			$venueLog->event_id = $cuisine->id;
			$venueLog->event_type = 'Cuisine Added';
			$venueLog->event_message = Auth::user()->name.' added new cuisine '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.cuisines'))->with('success', 'Record has been added.');
		} else {
			return redirect(route('admin.cuisines'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id){
		$cuisine = Cuisine::find($id);
		return view('admin.cuisine-edit', compact('cuisine'));
	}
	
	
	public function update(Request $request, $id){
		
		$cuisine = Cuisine::find($id);
		
		$cuisine->name = $request->name;
		
		if(isset($request->status)){
			$cuisine->status = 1;
		}else{
			$cuisine->status = 0;
		}
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('cuisines/' . $cuisine->image);
			$request->file('image')->store('cuisines/', ['disk' => 'public']);
			$cuisine->image = $request->image->hashName();
		}
		
		if($cuisine->save()) {

				// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Cuisine';
			$venueLog->event_id = $cuisine->id;
			$venueLog->event_type = 'Cuisine Updated';
			$venueLog->event_message = Auth::user()->name.' update cuisine '.$cuisine->name.' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.cuisines'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.cuisines'))->with('error', 'Record has not been updated.');
		}
		
	}
}
