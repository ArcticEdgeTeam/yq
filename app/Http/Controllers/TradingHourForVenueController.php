<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TradingHour;
use Auth;

class TradingHourForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
		$this->middleware('venue');
	}
	public function delete($tradinghour_id){
		TradingHour::where('id', $tradinghour_id)->where('venue_id', Auth::user()->venue_id)->delete();
		return redirect(route('venue.information'))->with('success', 'Record has been deleted.');
	}
}
