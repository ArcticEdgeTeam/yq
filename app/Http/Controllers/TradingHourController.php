<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TradingHour;

class TradingHourController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function delete($venue_id, $tradinghour_id){
		TradingHour::find($tradinghour_id)->delete();
		return redirect(route('admin.venue.information', $venue_id))->with('success', 'Record has been deleted.');
	}
}
