<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Facility;
use Storage;
use Auth;
use App\TrailLog;

class FacilityController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(){
		$counter = 1;
		$facilities = Facility::all();
		return view('admin.facilities', compact('facilities', 'counter'));
	}
	
	public function create(){
		return view('admin.facility-create');
	}
	
	public function save(Request $request){
		
		$facility = new Facility;
		$facility->name = $request->name;
		
		if(isset($request->status)){
			$facility->status = 1;
		}else{
			$facility->status = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('facilities/', ['disk' => 'public']);
			$facility->image = $request->image->hashName();
		}
		
		if($facility->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Facility';
			$venueLog->event_id = $facility->id;
			$venueLog->event_type = 'Facility Added';
			$venueLog->event_message = Auth::user()->name.' added new facility '.$request->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();

			return redirect(route('admin.facilities'))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.facilities'))->with('error', 'Record has not been added.');
		}
		
	}
	
	public function edit($id){
		$facility = Facility::find($id);
		return view('admin.facility-edit', compact('facility'));
	}
	
	
	public function update(Request $request, $id){
		
		$facility = Facility::find($id);
		
		$facility->name = $request->name;
		
		if(isset($request->status)){
			$facility->status = 1;
		}else{
			$facility->status = 0;
		}
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('facilities/' . $facility->image);
			$request->file('image')->store('facilities/', ['disk' => 'public']);
			$facility->image = $request->image->hashName();
		}
		
		if($facility->save()) {

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Facility';
			$venueLog->event_id = $facility->id;
			$venueLog->event_type = 'Facility Updated';
			$venueLog->event_message = Auth::user()->name.' updated facility '.$facility->name.' informaton.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = 0;
			$venueLog->save();


			return redirect(route('admin.facilities'))->with('success', 'Record has been updated.');
		}else{
			return redirect(route('admin.facilities'))->with('error', 'Record has not been updated.');
		}
		
	}
}
