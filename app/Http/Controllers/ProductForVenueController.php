<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\Category;
use App\Menu;
use App\Product;
use App\ExtraProduct;
use App\ProductVariant;
use App\ProductPreference;
use App\CategoryProduct;
use App\MenuProduct;
use App\ProductSide;
use App\ProductSideChild;
use App\Bar;
use Storage;
use App\Ingredient;
use App\Unit;
use App\ProductIngredient;
use App\ProductIngredientVariation;
use Image;
use Auth;
use App\TrailLog;

class ProductForVenueController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index(Request $request) {
		
		$id = Auth::user()->venue_id;
		// $products = Product::where('venue_id', $id)->orderBy('id', 'desc')->get();
		$getProducts = Product::query();
		$getProducts = $getProducts->select('products.*', 'bars.name as bar_name', 'bars.image as bar_image', 'categories.name as category_name')
		->leftjoin('bars', 'bars.id', '=', 'products.bar_id')
		->leftjoin('categories', 'categories.id', '=', 'products.category_id')
		->where('products.venue_id', $id)
		->orderBy('products.id', 'desc');

		if($request->product_id != '') {
			$getProducts = $getProducts->where('products.product_id', $request->product_id);
		}

		if($request->name != '') {
			$getProducts = $getProducts->where('products.name', 'like', '%' . $request->name . '%');
		}

		if($request->prep_id != '') {
			$getProducts = $getProducts->where('products.bar_id', $request->prep_id);
		}

		if($request->category_id != '') {
			$getProducts = $getProducts->where('products.category_id', $request->category_id);
		}


		if($request->status != '') {
			$getProducts = $getProducts->where('products.status', $request->status);
		}

		$getProducts = $getProducts->orderBy('products.id', 'desc')->paginate(5);

		
		// get prep areas
		$getPrepAreas = Bar::where('status', 1)->where('venue_id', $id)->get();
		// get categories
		$getCategories = Category::where('status', 1)->where('venue_id', $id)->get();

		$venue = Venue::find($id);
		return view('venue.products', compact('venue','getProducts', 'getPrepAreas', 'getCategories'));
	}
	
	public function create(){
		
		$id = Auth::user()->venue_id;

		$categories = Category::where('status', 1)
					->where('venue_id', $id)
					->where('category_id', 0)
					->orderBy('name', 'ASC')
					->get();
		// check if category has childs
		foreach ($categories as $key => $value) {

				// get child categories
				$childCategories = Category::where('status', 1)
									->where('category_id', $value->id)
									->orderBy('name', 'ASC')
									->get();
				$childArray = [];
				// get childs for parent
				if (!$childCategories->isEmpty()) {
					$value->has_childs = 'yes';
					foreach ($childCategories as $child) {
						$childArray[] = $child->id.'/'.$child->name;
					}
					$value->child = $childArray;
				} else {
					$value->has_childs = 'no';
				}

		} // endforeach
		
		$venue = Venue::find($id);
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		$menus = Menu::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();
		$ingredients = Ingredient::where('status', 1)->where('venue_id', $venue->id)->orderBy('name', 'ASC')->get();
		$units = Unit::where('status', 1)->where('venue_id', $venue->id)->orderBy('name', 'ASC')->get();
		// get products
		$Products = Product::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();
		// get side products
		$sideProducts = Product::where('status', 1)
								->where('side_product', 1)
								->where('venue_id', $id)
								->orderBy('name', 'ASC')
								->get();
		
		return view('venue.product-create', compact('ingredients', 'units', 'venue', 'categories', 'menus',  'bars', 'sideProducts', 'Products'));
	}
	
	public function save(Request $request){

		$validatedData = $request->validate([
            'name' => 'required',
            'product_id' => 'required',
            'category' => 'required',
            'description' => 'required',
            'bar_id' => 'required'  
        ]);

        // check product Id
        $getProduct = Product::where('product_id', $request->product_id)
						->where('venue_id', Auth::user()->venue_id)
						->first();

		if ($getProduct != null) {

			return redirect(route('venue.products', Auth::user()->venue_id))->with('error', 'Product ID already exists.');
		}

		$id = Auth::user()->venue_id;

		$product = new Product;
		$product->name = $request->name;
		$product->product_id = $request->product_id;
		$product->bar_id = $request->bar_id;
		$product->video = $request->video;
		$product->description = $request->description;
		$product->type = $request->type;
		$product->price = str_replace(",", "", $request->price);
		$product->category_id = $request->category;
		$product->venue_id = $id;

		if($request->type == 'simple') {
			$product->variants = 0;
		} else {
			$product->variants = 1;
		}
		
		if(isset($request->status)){
			$product->status = 1;
		}else{
			$product->status = 0;
		}

		if(isset($request->side_product)){
			$product->side_product = 1;
		}else{
			
			$product->side_product = 0;
		}

		if (!isset($request->side_product) && $request->type != 'variant') {
			$validatedData = $request->validate([
            'price' => 'required',
        	]);
		}

		if(isset($request->child_side_product)) {
			$product->child_side_product = 1;
		}else{
			$product->child_side_product = 0;
		}
		
		if(isset($request->allergen)){
			$product->allergen = 1;
		}else{
			$product->allergen = 0;
		}
		
		if($request->file('image')){
			$validatedData = $request->validate([
	            'image' => 'mimes:jpeg,jpg,png,gif|max:10000', // max 10000kb
	        ]);
			SaveImageAllSizes($request, 'products/');
			//$request->file('image')->store('products/', ['disk' => 'public']);
			$product->image = $request->image->hashName();
		}
		
		if($product->save()) {
			
			if(is_array($request->menus)) {
				foreach($request->menus as $menu){
					$menuProdusts = new MenuProduct;
					$menuProdusts->menu_id = $menu;
					$menuProdusts->venue_id = $id;
					$menuProdusts->bar_id = $product->bar_id;
					$menuProdusts->product_id = $product->id;
					$menuProdusts->save();
				}
			}

			if(isset($request->child_side_product)) {
				if(is_array($request->side_name)){
				foreach($request->side_name as $key => $side) {
					if ($side != null && $request->no_allowed[$key] != null ) {
						$sideProduct = new ProductSide;
						$sideProduct->name = $side;
						$sideProduct->no_allowed = $request->no_allowed[$key];
						$sideProduct->product_id = $product->id;

						if ($sideProduct->save()) {
							
							if(is_array($request->child_side_name)) {

								foreach($request->child_side_name[$key] as $child_side) {
									if ($child_side != null ) {
										$childSideProduct = new ProductSideChild;
										$childSideProduct->side_child_id = $child_side;
										$childSideProduct->side_id = $sideProduct->id;
										$childSideProduct->product_id = $product->id;
										$childSideProduct->save();

									}
								}
							
							} // child side if ends

						} // product save if ends

					} // side and no if ends

				} // side foreach ends
			
			}	// in array if ends
			}// isset if ends

		if(isset($request->extra)) {

			$extras = array_filter(array_combine($request->extra_name, $request->extra_price));
			
			foreach($extras as $name => $price){
				if ($name != "" && $price != "") {
					$ExtraProduct = new ExtraProduct;
					$ExtraProduct->extra_id = $name;
					$ExtraProduct->discount = $price;
					$ExtraProduct->product_id = $product->id;
					$ExtraProduct->venue_id = $id;
					$ExtraProduct->bar_id = $product->bar_id;
					$ExtraProduct->save();
				}
			}
		}

		if($request->type == 'simple') {
			$ingredients = array_filter($request->ingredients);
			$units = $request->units;
			$quantities = $request->quantities;
			$ingredient_counter = 0;
			// save simple ingredients
				foreach($ingredients as $ingredient_id) {
				if($units[$ingredient_counter] == '' || $quantities[$ingredient_counter] == '') {
					$ingredient_counter++;
					continue;
				}
				$ingredient = new ProductIngredient;
				$ingredient->ingredient_id = $ingredient_id;
				$ingredient->unit_id = $units[$ingredient_counter];
				$ingredient->quantity = str_replace(",", "", $quantities[$ingredient_counter]);

				$ingredient->product_id = $product->id;
				$ingredient->save();
				$ingredient_counter++;
			}
		} else {
			
			$variants = array_filter(array_combine($request->variable_name, $request->variable_price));
			$variantName = [];

			foreach($variants as $name => $price){
				$ProductVariant = new ProductVariant;
				$ProductVariant->name = $name;
				$ProductVariant->price = str_replace(",", "", $price);
				$ProductVariant->product_id = $product->id;
				$ProductVariant->venue_id = $id;
				$ProductVariant->bar_id = $product->bar_id;
				$ProductVariant->save();
				// get id and variable name
				$variantName[$ProductVariant->id] = str_replace(' ', '_', $name);
			}

			// save the ingredient for differnet variation
			foreach($request->var_ingredients as $index => $ingredient_name) {
				if ($ingredient_name != '' && $request->var_units[$index] != '') {
					$productIngredient = new ProductIngredient;
					$productIngredient->ingredient_id = $ingredient_name;
					$productIngredient->unit_id = $request->var_units[$index];
					$productIngredient->product_id = $product->id;
					$productIngredient->save();

					// store variable against each ingredient
					foreach ($variantName as $varId => $varName) {
						$ingredientVariation = new ProductIngredientVariation;
						$ingredientVariation->ingredient_id = $productIngredient->id;
						$ingredientVariation->variation_id = $varId;
						$ingredientVariation->quantity = str_replace(",", "", $request->$varName[$index]);

						$ingredientVariation->save();
					} // variable foreach ends
				} //null if ends
			} // foeach loop
		}

		if(isset($request->preferences)) {

			foreach($request->preference_name as $pref_name){
				if ($pref_name != '') {
					$ProductVariant = new ProductPreference;
					$ProductVariant->name = $pref_name;
					$ProductVariant->product_id = $product->id;
					$ProductVariant->venue_id = $id;
					$ProductVariant->bar_id = $product->bar_id;
					$ProductVariant->save();
				}
			}
		}

		$venue = Venue::find($id);

		// insert log
		$venueLog = new TrailLog;
		$venueLog->event = 'Product';
		$venueLog->event_id = $product->id;
		$venueLog->event_type = 'Product Added';
		$venueLog->event_message = Auth::user()->name.' added new product '.$request->name. ' to venue.';
		$venueLog->user_id = Auth::user()->id;
		$venueLog->user_name = Auth::user()->name;
		$venueLog->user_role = Auth::user()->role;
		$venueLog->venue_id = $id;
		$venueLog->save();
				
			return redirect(route('venue.products', $id))->with('success', 'Record has been added.');

		} else {
			return redirect(route('venue.products', $id))->with('error', 'Record has not been added.');
		}
		
	}
	
	
	public function edit($product_id) {

		$id = Auth::user()->venue_id;

		$venue = Venue::find($id);
		$bars = Bar::where('venue_id', $id)->where('status', 1)->get();
		$productvariants = ProductVariant::where('product_id', $product_id)->get();
		// get extra product
		// get products ids
		$extras_ids = ExtraProduct::where('product_id', $product_id)->pluck('extra_id')->toArray();
		//dd($extras_ids);
		$extras = Product::select('products.name as extra_name', 'products.id as prod_id', 'extra_products.*')
								->leftjoin('extra_products','extra_products.extra_id', '=', 'products.id')
								->where('extra_products.product_id', $product_id)
								->whereIn('extra_products.extra_id', $extras_ids)
								->get();
		//dd($extras);
		$product = Product::find($product_id);
		//dd($product);
		$productIngredients = "";
		$totalVariables = 0;
		$variableIds = [];
		if ($product->variants == 1) {

			//get ingredients and units of this product
			$productIngredients = Ingredient::select('ingredients.*', 'units.id as unit_id', 'units.name as unit_name', 'units.symbol as unit_symbol', 'product_ingredients.*')
											->leftjoin('product_ingredients','product_ingredients.ingredient_id', '=', 'ingredients.id')
											->leftjoin('units', 'units.id', '=', 'product_ingredients.unit_id')
											->where('ingredients.status', 1)
											->where('units.status', 1)
											->where('product_ingredients.product_id', $product_id)
											->get();
			//dd($productIngredients);
			//get variation of this product ingredient
			$ProductIngredientVariation = ProductIngredientVariation::select('product_ingredient_variations.*', 'product_variants.name as variantion_name', 'product_variants.status as variant_staus', 'product_ingredients.product_id')
				->leftjoin('product_variants', 'product_variants.id', '=', 'product_ingredient_variations.variation_id')		
				->leftjoin('product_ingredients', 'product_ingredients.id', '=', 'product_ingredient_variations.ingredient_id')
				->where('product_variants.product_id', $product_id)
				->get();
			//dd($ProductIngredientVariation);
			
			$secondRow = 0;
			foreach ($productIngredients as $ingredient) {

				// array for ingredientVariation
				$variation = [];
				
				foreach ($ProductIngredientVariation  as $ingredientVariation) {
					if ($ingredientVariation->ingredient_id == $ingredient->id) {

						$variation[$ingredientVariation->variantion_name] = $ingredientVariation->quantity;
						
						if ($secondRow == 0) {
							$totalVariables++;
							$variableIds[$ingredientVariation->variation_id] = $ingredientVariation->variantion_name;
						}

						
					} //if ends
				
				} //foreach ends
				$secondRow++;
				$ingredient->variations = $variation;
			}

			//dd($productIngredients);
			//dd($variableIds);

		}
		
		if($product->image == ''){
			$product->image = 'default.png';
		}
		
		$ingredients = Ingredient::where('status', 1)->where('venue_id', $venue->id)->orderBy('name', 'ASC')->get();
		$units = Unit::where('status', 1)->where('venue_id', $venue->id)->orderBy('name', 'ASC')->get();
		$product_ingredients = ProductIngredient::where('product_id', $product_id)->get();
		
		$categories = Category::where('status', 1)
					->where('venue_id', $id)
					->where('category_id', 0)
					->orderBy('name', 'ASC')
					->get();
		// check if category has childs
		foreach ($categories as $key => $value) {

				// get child categories
				$childCategories = Category::where('status', 1)
									->where('category_id', $value->id)
									->orderBy('name', 'ASC')
									->get();
				$childArray = [];
				// get childs for parent
				if (!$childCategories->isEmpty()) {
					$value->has_childs = 'yes';
					foreach ($childCategories as $child) {
						$childArray[] = $child->id.'/'.$child->name;
					}
					$value->child = $childArray;
				} else {
					$value->has_childs = 'no';
				}

		} // endforeach

		$menus = Menu::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();

		// get side products
		$sideProducts = Product::where('status', 1)->where('venue_id', $id)->where('side_product', 1)->orderBy('name', 'ASC')->get();

		// get this product sides
		$getSides =  ProductSide::select('product_sides.*', 'products.id as product_id')
									->leftjoin('products', 'products.id', '=', 'product_sides.product_id')
									->where('products.id',  $product_id)
									->where('products.venue_id', $id)
									->get();
		// get child side product
		$getChildSides = ProductSideChild::select('product_side_childs.*', 'products.*')
									->leftjoin('products', 'products.id', '=', 'product_side_childs.product_id')
									->where('products.id',  $product_id)
									->get();

		// get products
		$Products = Product::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();

		// preferences
		$preferences = ProductPreference::select('products.id as product_id ', 'product_preferences.*')
								->leftjoin('products','products.id', '=', 'product_preferences.product_id')
								->where('product_preferences.product_id', $product_id)
								->get();
		//dd($product);
		
		return view('venue.product-edit', compact('product_ingredients', 'ingredients', 'units', 'id', 'product', 'venue', 'categories', 'menus', 'extras', 'productvariants', 'bars', 'sideProducts', 'getSides', 'getChildSides', 'Products', 'preferences', 'productIngredients', 'totalVariables', 'variableIds'));
	}
	
	
	
	public function update(Request $request, $product_id){

		$validatedData = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'description' => 'required',
            'bar_id' => 'required',
            'product_id' => 'required'
        ]);

        $getProduct = Product::where('product_id', $request->product_id)
						->where('id', '!=',  $product_id)
						->where('venue_id', Auth::user()->venue_id)
						->first();

		if ($getProduct != null) {

			return redirect(route('venue.product.edit', $product_id))->with('error', 'Product ID already exists.');
		}
        
		$id = Auth::user()->venue_id;


		$product = Product::find($product_id);
		$product->name = $request->name;
		$product->product_id = $request->product_id;
		$product->bar_id = $request->bar_id;
		$product->video = $request->video;
		$product->description = $request->description;
		$product->type = $request->type;
		$product->price = str_replace(",", "", $request->price);
		$product->category_id = $request->category;
		$product->product_error = 0;
		$product->venue_id = $id;
		
		if(isset($request->status)){
			$product->status = 1;
		}else{
			$product->status = 0;
		}
		
		if(isset($request->allergen)){
			$product->allergen = 1;
		}else{
			$product->allergen = 0;
		}

		if(isset($request->side_product)){
			$product->side_product = 1;
		}else{
			
			$product->side_product = 0;
		}

		if (!isset($request->side_product) && $request->type != 'variant') {
			$validatedData = $request->validate([
            'price' => 'required',
        	]);
		}

		if(isset($request->child_side_product)) {
			$product->child_side_product = 1;
		} else {
			$product->child_side_product = 0;
		}

		if($request->type == 'simple') {
			$product->variants = 0;
		} else {
			$product->variants = 1;
		}
		
		if($request->file('image')){

			$validatedData = $request->validate([
              'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000', // max 10000kb
        	]);
			UpdateImageAllSizes($request, 'products/', $product->image);
			//Storage::disk('public')->delete('products/' . $product->image);
			//$request->file('image')->store('products/', ['disk' => 'public']);
			$product->image = $request->image->hashName();
		}
	
		
		if($product->save()) {

			//get ingredients ids
			$ingredientIds = ProductIngredient::where('product_id', $product_id)->pluck('id')->toArray();
			
			if($request->type == 'simple') {

				// remove data from Product ingredient and Product Ingredient Variation table remove variation
				$deleteIngredientIds = ProductIngredient::where('product_id', $product_id)->delete();
				$deleteVariationIds = ProductVariant::where('product_id', $product_id)->delete();
				$deleteIngredientVariation = ProductIngredientVariation::whereIn('ingredient_id', $ingredientIds)->delete();

				$ingredients = array_filter($request->ingredients);
				$units = $request->units;
				$quantities = $request->quantities;
				$ingredient_counter = 0;
					

				foreach($ingredients as $ingredient_id) {
					if($units[$ingredient_counter] == '' || $quantities[$ingredient_counter] == '') {
						$ingredient_counter++;
						continue;
					}
					
					$ingredient = new ProductIngredient;
					$ingredient->ingredient_id = $ingredient_id;
					$ingredient->unit_id = $units[$ingredient_counter];
					$ingredient->quantity = str_replace(",", "", $quantities[$ingredient_counter]);

					$ingredient->product_id = $product->id;
					$ingredient->save();
					$ingredient_counter++;
				}
				
			} else {
	
				// remove data from Product ingredient and Product Ingredient Variation table
				$deleteIngredientIds = ProductIngredient::where('product_id', $product_id)->delete();
				$deleteIngredientVariation = ProductIngredientVariation::whereIn('ingredient_id', $ingredientIds)->delete();

				$variants = array_filter(array_combine($request->variable_name, $request->variable_price));
				// get variale id's
				$varIds = json_decode($request->variableIds, true);
				$variantName = $varIds;

				foreach($variants as $name => $price){
					if ($name != "" && $price != "") {
						$ProductVariant = new ProductVariant;
						$ProductVariant->name = $name;
						$ProductVariant->price = str_replace(",", "", $price);
						$ProductVariant->product_id = $product->id;
						$ProductVariant->venue_id = $id;
						$ProductVariant->bar_id = $product->bar_id;
						$ProductVariant->save();
						// get id and variable name
						$variantName[$ProductVariant->id] = str_replace(' ', '_', $name);
					}
				}

				//dd($variantName);

				// save the ingredient for differnet variation
				foreach($request->var_ingredients as $index => $ingredient_name) {
					if ($ingredient_name != '' && $request->var_units[$index] != '') {
						$productIngredient = new ProductIngredient;
						$productIngredient->ingredient_id = $ingredient_name;
						$productIngredient->unit_id = $request->var_units[$index];
						$productIngredient->product_id = $product->id;
						$productIngredient->save();

						// store variable against each ingredient
						foreach ($variantName as $varId => $varName) {
							$replaceName = str_replace(' ', '_', $varName);
							$ingredientVariation = new ProductIngredientVariation;
							$ingredientVariation->ingredient_id = $productIngredient->id;
							$ingredientVariation->variation_id = $varId;
							$ingredientVariation->quantity = str_replace(",", "", $request->$replaceName[$index]);
							
							$ingredientVariation->save();
						} // variable foreach ends
					} //null if ends
				} // foeach loop
			}

			if(isset($request->extra)) {
				$extras = array_filter(array_combine($request->extra_name, $request->extra_price));

				foreach($extras as $name => $price){
					$ExtraProduct = new ExtraProduct;
					$ExtraProduct->extra_id = $name;
					$ExtraProduct->discount = $price;
					$ExtraProduct->product_id = $product->id;
					$ExtraProduct->venue_id = $id;
					$ExtraProduct->bar_id = $product->bar_id;
					$ExtraProduct->save();
				}
			}

			$menuProduct = MenuProduct::where('product_id', $product_id)->delete();

			if(is_array($request->menus)) {
				foreach($request->menus as $menu){
					$menuProdusts = new MenuProduct;
					$menuProdusts->menu_id = $menu;
					$menuProdusts->venue_id = $id;
					$menuProdusts->bar_id = $product->bar_id;
					$menuProdusts->product_id = $product->id;
					$menuProdusts->save();
				}
			}

			if(isset($request->child_side_product)) {

				// delete sides
				$deleteSides = ProductSide::where('product_id', $product_id)->delete();
				// delete child sides
				$deleteSides = ProductSideChild::where('product_id', $product_id)->delete();

				if(is_array($request->side_name)){
				foreach($request->side_name as $key => $side) {
					if ($side != null && $request->no_allowed[$key] != null ) {
						$sideProduct = new ProductSide;
						$sideProduct->name = $side;
						$sideProduct->no_allowed = $request->no_allowed[$key];
						$sideProduct->product_id = $product->id;

						if ($sideProduct->save()) {
							
							if(is_array($request->child_side_name)) {

								foreach($request->child_side_name[$key] as $child_side) {
									if ($child_side != null ) {
										$childSideProduct = new ProductSideChild;
										$childSideProduct->side_child_id = $child_side;
										$childSideProduct->side_id = $sideProduct->id;
										$childSideProduct->product_id = $product->id;
										$childSideProduct->save();

									}
								}
							
							} // child side if ends

						} // product save if ends

					} // side and no if ends

				} // side foreach ends
			
			}	// in array if ends
			}// isset if ends

			if(isset($request->preferences)) {

			// delete preferences
			$deleteSides = ProductPreference::where('product_id', $product_id)->delete();

			foreach($request->preference_name as $pref_name){
				if ($pref_name != '') {
					$ProductVariant = new ProductPreference;
					$ProductVariant->name = $pref_name;
					$ProductVariant->product_id = $product->id;
					$ProductVariant->venue_id = $id;
					$ProductVariant->bar_id = $product->bar_id;
					$ProductVariant->save();
				}
			}
		}

		// insert log
		$venueLog = new TrailLog;
		$venueLog->event = 'Product';
		$venueLog->event_id = $product->id;
		$venueLog->event_type = 'Product Updated';
		$venueLog->event_message = Auth::user()->name.' updated product '.$product->name. ' information.';
		$venueLog->user_id = Auth::user()->id;
		$venueLog->user_name = Auth::user()->name;
		$venueLog->user_role = Auth::user()->role;
		$venueLog->venue_id = $id;
		$venueLog->save();
			
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been updated.');
		} else {
			
			return redirect(route('venue.product.edit', $product_id))->with('error', 'Record has not been updated.');
		}
		
		
	}
	
	public function deleteProductIngredient($ingredient_id){
		$ingredient = ProductIngredient::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been deleted.');
	}
	
	public function deleteProductExtra($ingredient_id){
		$ingredient = ExtraProduct::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been deleted.');
	}
	
	public function deleteProductVariant($ingredient_id){
		$ingredient = ProductVariant::find($ingredient_id);
		$product_id = $ingredient->product_id;
		$ingredient->delete();
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been deleted.');
	}
	
	public function updateVariant(Request $request, $product_id){
		$variant = ProductVariant::find($request->id);
		$variant->name = $request->name;
		$variant->price = $request->price;
		if(isset($request->status)){
			$variant->status = 1;
		}else{
			$variant->status = 0;
		}
		$variant->save();
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been updated.');
	}
	
	
	public function updateExtra(Request $request, $product_id){
		$extra = ExtraProduct::find($request->id);
		$extra->name = $request->name;
		$extra->price = $request->price;
		if(isset($request->status)){
			$extra->status = 1;
		}else{
			$extra->status = 0;
		}
		$extra->save();
		return redirect(route('venue.product.edit', $product_id))->with('success', 'Record has been updated.');
	}

	public function getProductID(Request $request) {
		if ($request->ajax()) {
			$message = [];

			// for edit 
			if($request->type == 'edit') {
				$getProduct = Product::where('product_id', $request->product_id)
						->where('id', '!=',  $request->id)
						->where('venue_id', Auth::user()->venue_id)
						->first();
			} else {
				$getProduct = Product::where('product_id', $request->product_id)
						->where('venue_id', Auth::user()->venue_id)
						->first();
			}
			
			if($getProduct != null) {
				$message['error'] = 'true';
			} else {
				$message['error'] = 'false';
			}
			
		}	return response($message);
	}
}
