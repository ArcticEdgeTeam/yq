<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Transaction;

class payfastController extends Controller
{
    //

	public function index(){
		return view('payfast.index');
	}

	public function confirmPayment(Request $request){
		$pfOutput = '';
		$pfData = $_POST;
		foreach( $pfData as $key => $val )
		{
		if(!empty($val))
		 {
			$pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
		 }
		}
		// Remove last ampersand
		$getString = substr( $pfOutput, 0, -1 );
		//Uncomment the next line and add a passphrase if there is one set on the account
		$passPhrase = 'Starshare246';
		if( isset( $passPhrase ) )
		{
			$getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
		}   //Uncomment the next line and add a passphrase if there is one set on the account


		$data['preSigString']=$getString;
		$data['pfData']=$pfData;
		$data['signature'] = md5( $getString );
		$preSigString = $data['preSigString'];
		$signature = $data['signature'];
		return view('payfast.paynow', compact('pfData', 'signature'));
	}

	public function success(){
		return 'success';
	}

	public function cancel(){
		return 'cancel';
	}

    public function notify(Request $request){

        // Notify PayFast that information has been received
        header( 'HTTP/1.0 200 OK' );
        flush();

        // Posted variables from ITN
        $pfData = $_POST;

        //update db
        switch( $pfData['payment_status'] )
        {
         case 'COMPLETE':

            $transaction = new Transaction;
			$transaction->transaction_id = str_random();
			$transaction->order_id = 1;
			$transaction->customer_id = 9;
			$transaction->waiter_id = 11;
			$transaction->venue_id = 1;
			$transaction->table_id = 1;
			$transaction->amount = 100;
			$transaction->admin_commission = 8;
			$transaction->waiter_tip = 5;
			$transaction->transaction_status = 'Completed';
			$transaction->reserved_payment = 0;
			$transaction->reserve_funds = 0;
			$transaction->pay_by = 9;
			$transaction->payment_type = 'Strip';
			$transaction->save();

            // If complete, update your application, email the buyer and process the transaction as paid
         break;
         case 'FAILED':
            // There was an error, update your application
         break;
         default:
            // If unknown status, do nothing (safest course of action)
         break;
        }

		$file_name = time() . rand(1, 10000) . '_trn' . '.log';
		Storage::disk('public')->put('transactions/' . $file_name, $request);


	}

    public function checkOutAmount(){
        //dynamic portion
        $order_amount = 300;
        $amin_percent = 20;
        $admin_comission_limit = 20000;
        $admin_comission_amount = ($amin_percent / 100) * ($order_amount); // 60
        $amount_after_admin_comission = $admin_comission_amount + $order_amount; // 360

        // static portion here
        $amount_after_first_step =(3.5/100)*($amount_after_admin_comission); // (3.5/100)*(360) = 12.6
        $amount_after_2nd_step = ($amount_after_first_step) + (2); // 12.6 + 2 = 14.6
        $amount_after_3rd_step = (15/100) * ($amount_after_2nd_step); // (15/100)*(14.6) = 2.19
        $payfast_tax_amount = ($amount_after_2nd_step) + ($amount_after_3rd_step); // 14.6 + 2.19 = 16.79
        $final_amount_get_from_customer = ($payfast_tax_amount) + ($amount_after_admin_comission); // 16.79 + 360 = 376.79
        $admin_transfered_amount = ($amount_after_admin_comission)-($admin_comission_amount); // 360 - 60 = 300
        // tensting values
        $cartTotal = 10.00;// This amount needs to be sourced from your application
        $data = array(
            // Merchant details
            'merchant_id' => '10000100',
            'merchant_key' => '46f0cd694581a',
            'return_url' => 'https://localhost/strawberrypop/return',
            'cancel_url' => 'https://localhost/strawberrypop/cancel',
            'notify_url' => 'https://localhost/strawberrypop/notify',
            // Buyer details
            'name_first' => 'First Name',
            'name_last'  => 'Last Name',
            'email_address'=> 'test@test.com',
            // Transaction details
            'm_payment_id' => '1234', //Unique payment ID to pass through to notify_url
            'amount' => number_format( sprintf( '%.2f', $final_amount_get_from_customer ), 2, '.', '' ),
            'item_name' => 'Order#123',
            'item_description' => 'Order#123',
            'custom_int1' => '1223',
            'custom_int3' => '123',
            'custom_str4' => '456',
            'custom_int2' => '789',
        );
        $signature = $this->generateSignature($data);
        $data['signature'] = $signature;

        // If in testing mode make use of either sandbox.payfast.co.za or www.payfast.co.za
        /*$testingMode = true;
        $pfHost = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
        $htmlForm = '<form action="https://'.$pfHost.'/eng/process" method="post">';
        foreach($data as $name=> $value)
        {
            $htmlForm .= '<input name="'.$name.'" type="hidden" value="'.$value.'" />';
        }
        $htmlForm .= '<input type="submit" value="Pay Now" /></form>';
        echo $htmlForm;*/
        return view('payfast.checkout',compact('order_amount','admin_comission_amount','amin_percent','amount_after_admin_comission','payfast_tax_amount','final_amount_get_from_customer','data'));
    }

    public function test(){
        $array1 = ['judas', 'john', 'michael'];
        $array2 = ['fernando', 'jsamine', 'sam', 'walter'];
        $array3 = ['judas', 'john', 'mike', 'steve'];

        $all_arrays = [$array1, $array2, $array3];
        $merged = [];
        for ($i = 0; $i < 3; $i++) {
            $arr = $all_arrays[$i];
            $x = count($arr);
            for ($j=0; $j < $x; $j++) {
                // Using the value as the key in the merged array ensures
                // that you will end up with distinct values.
                $merged[$arr[$j]] = 1;
            }
        }
// You could use array_keys() to do get the final result, but if you
// want to use just loops then it would work like this:
        $final = [];
        $x = count($merged);
        for ($i=0; $i < $x; $i++) {
            $final[] = key($merged);
            next($merged);
        }

        dd($final);
	}

    /**
     * @param array $data
     * @param null $passPhrase
     * @return string
     */
    function generateSignature($data, $passPhrase = null) {
        // Create parameter string
        $pfOutput = '';
        foreach( $data as $key => $val ) {
            if(!empty($val)) {
                $pfOutput .= $key .'='. urlencode( trim( $val ) ) .'&';
            }
        }
        // Remove last ampersand
        $getString = substr( $pfOutput, 0, -1 );
        if( $passPhrase !== null ) {
            $getString .= '&passphrase='. urlencode( trim( $passPhrase ) );
        }
        return md5( $getString );
    }
}
