<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\TrailLog;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	
	
	protected function redirectTo( ) {

         // insert log
        $venueLog = new TrailLog;
        $venueLog->event = 'User LogIn';
        $venueLog->event_id = Auth::user()->id;
        $venueLog->event_type = 'User LogIn';
        $venueLog->event_message = Auth::user()->name.' logged in to the system.';
        $venueLog->user_id = Auth::user()->id;
        $venueLog->user_name = Auth::user()->name;
        $venueLog->user_role = Auth::user()->role;
        $venueLog->venue_id =  Auth::user()->role == 'admin'? 0 : Auth::user()->venue_id;
        $venueLog->save();
        
		if (Auth::check() && Auth::user()->role == 'bar') {
			return('/bar');
		}
		elseif (Auth::check() && Auth::user()->role == 'venue') {
			return('/venue');
		}
		else {
			return('/admin');
		}
	}
	
	
}
