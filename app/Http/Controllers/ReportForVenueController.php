<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use Carbon\Carbon;
use Auth;


class ReportForVenueController extends Controller
{
    //
    public function __construct()
    {
		$this->middleware('auth');
	}
	
	public function index() {
		$venue = Venue::select('venues.*', 'users.name as user_name', 'users.id as user_id', 'users.venue_id')
		->leftjoin('users', 'users.venue_id', '=', 'venues.id')
		->where('users.venue_id', Auth::user()->venue_id)
		->first();

		return view('venue.reports', compact('venue'));
	}

	public function export(Request $request){
		
		$venue = Venue::select('venues.*', 'users.name as user_name', 'users.id as user_id', 'users.venue_id')
		->leftjoin('users', 'users.venue_id', '=', 'venues.id')
		->where('users.venue_id', Auth::user()->venue_id)
		->first();
		return view('venue.reports', compact('venue'));
		
		//$date_range = explode('-', $request->date_range);
		//return Carbon::today()->endOfMonth();
		//->whereBetween('created_at', [Carbon::today()->startOfMonth(), Carbon::today()->endOfMonth()])
	}
}
