<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Venue;
use App\Order;
use App\Booking;
use App\Table;
use App\Bar;
use App\BookingTable;
use App\Ingredient;
use App\Category;
use App\Menu;
use App\Unit;
use Auth;
use Hash;

class AjaxController extends Controller
{
    //
	public function loadAllCustomers(){
		$customers = User::where('role', 'customer')->get();
		return view('ajax.load-all-customers', compact('customers'));
	}
	
	public function loadAllVenues(){
		$venues = Venue::all();
		return view('ajax.load-all-venues', compact('venues'));
	}
	
	public function loadVenueCustomers(Request $request){
		
		$venue_id = $request->venue_id;
		
		if($request->venue_id == ''){
			return "<div id='venue_elemets2'></div>";
		}
		
		$customers = Order::join('users', 'orders.customer_id', 'users.id')->select('users.name', 'users.id')->where('orders.venue_id', $request->venue_id)->groupBy('customer_id')->get();
		
		return view('ajax.load-venue-customers', compact('customers', 'venue_id'));
	}
	
	
	public function loadVenueWaiters(Request $request){
		
		$venue_id = $request->venue_id;
		
		if($request->venue_id == '') {
			return '';
		}
		
		$customers = User::where('role', 'waiter')->where('venue_id', $venue_id)->get();
		return view('ajax.load-venue-waiters', compact('customers', 'venue_id'));
	}
	
	
	
	public function loadVenueCustomersForVenue(){
		
		$venue_id = Auth::user()->venue_id;
		
		$customers = Order::join('users', 'orders.customer_id', 'users.id')->select('users.name', 'users.id')->where('orders.venue_id', $venue_id)->groupBy('customer_id')->get();
		
		return view('ajax.load-venue-customers-forvenue', compact('customers', 'venue_id'));
	}
	
	
	public function loadVenueWaitersForVenue(){
		
		$venue_id = Auth::user()->venue_id;
		$customers = User::where('role', 'waiter')->where('venue_id', $venue_id)->get();
		return view('ajax.load-venue-waiters-forvenue', compact('customers', 'venue_id'));
	}

	public function getUsers(Request $request) {
		
		if ($request->ajax()) {
			$userData = User::query();
			// check name
			if($request->name != '') {
				$userData = $userData->where('name', 'like', '%'.$request->name.'%');
			}

			if ($request->gender == 'male' || $request->gender == 'female') {
				$userData = $userData->where('gender', $request->gender);
			}
			
			$users = $userData->where('role', 'customer')->where('status', 1)->orderBy('id','DESC')->get();
			$userId = [];
			if (!$users->isEmpty()) {
				foreach($users as $user) {
					$age = date_diff(date_create($user->dob), date_create('now'))->y;

					if ($age >= $request->min && $age <= $request->max) {
						$userId[] = $user->id;
					} // age

				} // foreach
			
			} // empty

			return response($userId);
		} // ajax
			
	}

	public function getBookings(Request $request) {
		
		if ($request->ajax()) {
			// get date and time seperate
			$bookingDate = date('Y-m-d',strtotime($request->booking));
			$bookingTime = date('H:i',strtotime($request->booking));

			if ($request->type == 'edit') {

				// get all bookings for the selected date
			$bookings = Booking::select('bookings.*', 'tables.id as table_id', 'tables.status as table_status', 'venues.average_booking')
								->leftjoin('tables', 'tables.id','=', 'bookings.table_id')
								->leftjoin('venues', 'venues.id', '=', 'bookings.venue_id')
								->where('bookings.venue_id', $request->venue_id)
								->where('bookings.table_id',  $request->table)
								->whereDate('bookings.date_time', $bookingDate)
								->where('bookings.id', '!=', $request->id)
								->get();

			} else {
			
			// get all bookings for the selected date
			$bookings = Booking::select('bookings.*', 'tables.id as table_id', 'tables.status as table_status', 'venues.average_booking')
								->leftjoin('tables', 'tables.id','=', 'bookings.table_id')
								->leftjoin('venues', 'venues.id', '=', 'bookings.venue_id')
								->where('bookings.venue_id', $request->id)
								->where('bookings.table_id',  $request->table)
								->whereDate('bookings.date_time', $bookingDate)
								->get();
			}
			
			// array for storing message			
			$message = [];

			if (!$bookings->isEmpty()) {
				foreach($bookings as $booking) {
					// booking start date
					$startTime = date('H:i',strtotime($booking->date_time));
					
					// check if avarege booking for venue exists or not
					if ($booking->average_booking == '') {
						$endTime = strtotime("+30 minutes", strtotime($startTime));
						$endTime = date('H:i', $endTime);
					} else {
						$endTime = strtotime("+".date('H',strtotime($booking->average_booking))." hour +".date('i',strtotime($booking->average_booking))." minutes +".date('s',strtotime($booking->average_booking))." seconds", strtotime($startTime));
						$endTime = date('H:i', $endTime);
					} // average booking

					// check if booking time is conflicting with database booking time
					if($bookingTime >= $startTime && $bookingTime <= $endTime) {
						$message['error'] = 'This table is already booked from '.$startTime.' To '.$endTime.'. Would you like to proceed the Booking?';
					}
				} //foreachends

			} // if ends

			return $message;
		} // ajax
			
	}


	public function getTableInfomations(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			//get table infomation
			$table = Table::where('id', $request->table)->where('venue_id', $request->id)->first();

			if ($request->type == 'seating') {
				if($table->seats < $request->value) {
					$message['error'] = 'This table has capacity for only '.$table->seats.' seats.';
				}

			} elseif ($request->type == 'area') {

				if($table->area != $request->value) {
					$message['error'] = 'This table is available '.$table->area.'.';
				}

			}
			//return $message['error'] = $request->type;
			return $message;
		} // ajax
			
	}

	public function getBookingInformation(Request $request) {
		
		if ($request->ajax()) {

			$booking = Booking::find($request->booking_id);
			// get tables for this booking
			$bookingTables = BookingTable::where('booking_id', $request->booking_id)->pluck('table_id')->toArray();
			return response(array("booking" =>$booking, 'bookingTables' => $bookingTables));
		} // ajax
			
	}

	public function getStaffInformation(Request $request) {
		
		if ($request->ajax()) {
			$message = []; 
			$checkUser = User::where('user_id', $request->user_id)
							->where('venue_id', $request->venue_id)
							->first();
			if($checkUser != null) {
				$message['error'] = 'User ID already exists for this venue.';
			}
			
			return response($message);
		} // ajax
			
	}

	public function getEditStaffInformation(Request $request) {
		
		if ($request->ajax()) {
			$message = []; 
			// check user id
			$checkUser = User::where('user_id', $request->user_id)
								->where('venue_id', $request->venue_id)
								->where('id', '!=', $request->id)
								->first();
			if($checkUser != null) {
				$message['error'] = 'User ID already exists for this venue.';
			}
			
			return response($message);
		} // ajax
			
	}

	public function getTableID(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
				//get table infomation
				$table = Table::where('table_id', $request->table_id)->where('venue_id', $request->id)->first();
			} else {
				$table = Table::where('table_id', $request->table_id)
								->where('venue_id', $request->venue_id)
								->where('id', '!=', $request->id)
								->first();
			}
			
			if ($table != null) {
				
				$message['error'] = 'This Table ID already exists for this venue.';
			}

			//return $message['error'] = $request->type;
			return $message;
		} // ajax
			
	}

	public function getPrepID(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
				//get table infomation
				$getPrepId = Bar::where('bar_id', $request->bar_id)->where('venue_id', $request->id)->first();
			} else {
				$getPrepId = Bar::where('bar_id', $request->bar_id)
								->where('venue_id', $request->venue_id)
								->where('id', '!=', $request->id)
								->first();
			}
			
			if ($getPrepId != null) {
				
				$message['error'] = 'This Prep ID already exists for this venue.';
			}

			//return $message['error'] = $request->type;
			return $message;
		} // ajax
			
	}

	public function getUserPassword(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			$getPassword = User::find(Auth::user()->id)->password;

			 if (Hash::check($request->user_password, $getPassword)) {
			 	$message['error'] = 'This password matches your old password, please try a new one.';
			 }
			 return response($message);
		} // ajax
			
	}

	public function getIngredientInfo(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
			    $getIngredientName = Ingredient::where('name', $request->name)
												->where('venue_id', $request->id)
												->first();
				if($getIngredientName != null) {
					$message['error_name'] = 'Ingredient name already exists for this venue.';
				}

				$getIngredientCode = Ingredient::where('code', $request->code)
												->where('venue_id', $request->id)
												->first();
				if($getIngredientCode != null) {
					$message['error_code'] = 'Ingredient code already exists for this venue.';
				}
			} else {

				$getIngredientName = Ingredient::where('name', $request->name)
												->where('id', '!=', $request->ingredient_id)
												->where('venue_id', $request->id)
												->first();
				if($getIngredientName != null) {
					$message['error_name'] = 'Ingredient name already exists for this venue.';
				}

				$getIngredientCode = Ingredient::where('code', $request->code)
												->where('id', '!=', $request->ingredient_id)
												->where('venue_id', $request->id)
												->first();
				if($getIngredientCode != null) {
					$message['error_code'] = 'Ingredient code already exists for this venue.';
				}

			}
			return response($message);
								
		} // ajax
			
	}

	public function getUnitInfo(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
			    $getUnitName = Unit::where('name', $request->name)
									->where('venue_id', $request->id)
									->first();
				if($getUnitName != null) {
					$message['error_name'] = 'Unit name already exists for this venue.';
				}

				$getUnitSymbol = Unit::where('symbol', $request->symbol)
										->where('venue_id', $request->id)
										->first();
				if($getUnitSymbol != null) {
					$message['error_symbol'] = 'Unit symbol already exists for this venue.';
				}
			} else {

				 $getUnitName = Unit::where('name', $request->name)
				 					->where('id', '!=', $request->unit_id)
									->where('venue_id', $request->id)
									->first();
				if($getUnitName != null) {
					$message['error_name'] = 'Unit name already exists for this venue.';
				}

				$getUnitSymbol = Unit::where('symbol', $request->symbol)
										->where('id', '!=', $request->unit_id)
										->where('venue_id', $request->id)
										->first();
				if($getUnitSymbol != null) {
					$message['error_symbol'] = 'Unit symbol already exists for this venue.';
				}

			}
			return response($message);
								
		} // ajax
			
	}

	public function getCategoryInfo(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
				$getCategoryName = Category::where('name', $request->name)->where('venue_id', $request->id)->first();

	        	if($getCategoryName != null) {
	        		$message['error'] = 'Category name already exits, try a new name.';

	        	}
	        } else {

	        	 // check for duplicate category name
		        $getCategoryName = Category::where('name', $request->name)
							        ->where('venue_id', $request->id)
							        ->where('id','!=', $request->category_id)
							        ->first();

		        if($getCategoryName != null) {
		        	$message['error'] = 'Category name already exits, try a new name.';
		        }
	        }
			return response($message);
								
		} // ajax
			
	}

	public function getMenuInfo(Request $request) {
		
		if ($request->ajax()) {

			$message = [];
			if ($request->type == 'add') {
				$getMenuName = Menu::where('name', $request->name)->where('venue_id', $request->id)->first();

        		if($getMenuName != null) {
	        		$message['error'] = 'Menu name already exits, try a new name.';

	        	}
	        } else {

	        	 // check for duplicate category name
		        $getMenuName = Menu::where('name', $request->name)
							        ->where('venue_id', $request->id)
							        ->where('id', '!=', $request->menu_id)
							        ->first();

		        if($getMenuName != null) {
		        	$message['error'] = 'Menu name already exits, try a new name.';
		        }
	        }
			return response($message);
								
		} // ajax
			
	}

}
