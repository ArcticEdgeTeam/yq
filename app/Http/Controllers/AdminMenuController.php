<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Venue;
use App\Product;
use App\MenuProduct;
use App\Category;
use App\CategoryMenu;
use App\Menu;
use Storage;
use DB;
use Response;
use App\TrailLog;

class AdminMenuController extends Controller
{
    //
	public function __construct()
    {
		$this->middleware('auth');
	}
	public function index($id) {

		//
		$counter = 1;
		$menus = Menu::where('venue_id', $id)->orderBy('id', 'desc')->get();
		$venue = Venue::find($id);
		return view('admin.menu_view', compact('id', 'venue', 'menus', 'counter'));
	}

	public function create($id){
		
		$counter = 0;
		
		$edited_id = '';
		$products = Product::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();
		$venue = Venue::find($id);

		$categories = Category::where('status', 1)
					->where('venue_id', $id)
					->where('category_id', 0)
					->orderBy('name', 'ASC')
					->get();

			// check if category has childs
		foreach ($categories as $key => $value) {

				// get child categories
				$childCategories = Category::where('status', 1)
									->where('category_id', $value->id)
									->orderBy('name', 'ASC')
									->get();
				$childArray = [];
				// get childs for parent
				if (!$childCategories->isEmpty()) {
					$value->has_childs = 'yes';
					foreach ($childCategories as $child) {
						$childArray[] = $child->id.'/'.$child->name;
					}
					$value->child = $childArray;
				} else {
					$value->has_childs = 'no';
				}

		} // endforeach

		return view('admin.menu_create', compact('id', 'venue', 'counter', 'products', 'edited_id', 'categories'));
	}

	public function save(Request $request, $id) {

		$validatedData = $request->validate([
            'name' => 'required',
        ]);

        // check for duplicate menu name
        $menuName = Menu::where('name', $request->name)->where('venue_id', $id)->first();

        if($menuName != null) {

        	return redirect(route('admin.venue.menus', $id))->with('error', 'Menu name already exits, try a new name.');
        }

		$menu = new Menu;
		$menu->name = $request->name;
		$menu->venue_id = $id;
		
		
		if(isset($request->status)){
			$menu->status = 1;
		}else{
			$menu->status = 0;
		}

		if(isset($request->default)){
			// remove default from other records
			$removeDefault =  DB::table('menus')->update(['default' => 0]);
			$menu->default = 1;
		}else{
			$menu->default = 0;
		}
		
		if($request->file('image')){
			$request->file('image')->store('menus/', ['disk' => 'public']);
			$menu->image = $request->image->hashName();
		}

		
		if($menu->save()) {

			if(is_array($request->products)) {
				foreach($request->products as $product) {
					// get bar id
					$getBar = Product::where('id', $product)->pluck('bar_id')->toArray();
					$menuProdusts = new MenuProduct;
					$menuProdusts->menu_id = $menu->id;
					$menuProdusts->venue_id = $id;
					$menuProdusts->bar_id = $getBar[0];
					$menuProdusts->product_id = $product;
					$menuProdusts->save();
				}
			}

			if(is_array($request->parent_cat_order)) {

				foreach($request->parent_cat_order as $key => $value) {
					$catMenu = new CategoryMenu;
					$catMenu->category_id = $key;
					$catMenu->menu_id = $menu->id;
					$catMenu->category_order = $value;
					$catMenu->save();

					if (isset($request->child_cat_order[$key])) {
							foreach($request->child_cat_order[$key] as $index => $order_value) {
							$childCatMenu = new CategoryMenu;
							$childCatMenu->category_id = $index;
							$childCatMenu->menu_id = $menu->id;
							$childCatMenu->category_order = $order_value;
							$childCatMenu->save();
						}
					}
				}
			}

			$venue = Venue::find($id);
			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Menu';
			$venueLog->event_id = $menu->id;
			$venueLog->event_type = 'Menu Added';
			$venueLog->event_message = Auth::user()->name.' added new menu '.$request->name. ' to venue '.$venue->name.'.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();

			return redirect(route('admin.venue.menus', $id))->with('success', 'Record has been added.');
		}else{
			return redirect(route('admin.venue.menus', $id))->with('error', 'Record has not been added.');
		}
		
	}

	public function menuDefault($id, $menu_id, $default) {

		// remove default from other records
		$removeDefault =  DB::table('menus')->update(['default' => 0]);
		// find menu
		$menu = Menu::find($menu_id);

		if ($default == 1) {
			$value = 0;
		} else {
			$value = 1;
		}

		$menu->default = $value;
		$menu->save();

		return redirect(route('admin.venue.menus', $id))->with('success', 'Menu status has been updated.');
	}

	public function menustatus($id, $menu_id, $status) {

		// find menu
		$menu = Menu::find($menu_id);

		if ($status == 1) {
			$value = 0;
		} else {
			$value = 1;
		}

		$menu->status = $value;
		$menu->save();

		return redirect(route('admin.venue.menus', $id))->with('success', 'Menu status has been updated.');
	}

	public function getMenuProducts(Request $request) {

		if($request->ajax()) {

			//get Parent categories for this menu
			$getParentCategory = Category::select('categories.*', 'categories_menu.category_id as cat_id', 'categories_menu.menu_id', 'categories_menu.category_order')
											->leftjoin('categories_menu', 'categories_menu.category_id', '=', 'categories.id')
											->where('categories.status', 1)
											->where('categories.category_id', 0)
											->where('categories_menu.menu_id', $request->menu_id)
											->orderBy('categories_menu.category_order', 'ASC')
											->get();
			//parentCategory Array
			$parentCategory = [];
			//child category Array
			$childCategory = [];
			// child indexes
			$childIndex = [];

			// loop through parent categories and check for there childs and fetch products according
			foreach($getParentCategory as $key => $value) {
				//check for its child
				$getChildCategory = Category::select('categories.*', 'categories_menu.category_id as cat_id', 'categories_menu.menu_id', 'categories_menu.category_order')
											->leftjoin('categories_menu', 'categories_menu.category_id', '=', 'categories.id')
											->where('categories.status', 1)
											->where('categories.category_id', $value->id)
											->where('categories_menu.menu_id', $request->menu_id)
											->orderBy('categories_menu.category_order', 'ASC')
											->get();
				// check if child exits then fetch products for it
					if(!$getChildCategory->isEmpty()) {
						
						foreach($getChildCategory as $child) {
						//get products for this child category

						$getChildProducts = Product::select('products.*', 'menus.id', 'menu_products.*')
											->leftjoin('menu_products', 'menu_products.product_id', '=', 'products.id')
											->leftjoin('menus', 'menus.id', '=', 'menu_products.menu_id')
											->where('products.status', 1)
											->where('products.category_id', $child->id)
											->where('menus.id', $request->menu_id)
											->orderBy('products.name', 'ASC')
											->get();
							// check if child category has products
							if (!$getChildProducts->isEmpty()) {
								
								//make array of child category products
								foreach($getChildProducts as $childProduct) {
										$childIndex[$value->name][] = $child->name;
									$childCategory[$value->name][$child->name][] = $childProduct->name;
								
								} // product foreach ends

							// give parent category a child clue
							$parentCategory[$value->name]['child'] = 'yes';

							} // child product check ends
						} // child category foreach ends

					} else {
						// get product for Parent category					
						$getParentProducts = Product::select('products.*', 'menus.id', 'menu_products.*')
											->leftjoin('menu_products', 'menu_products.product_id', '=', 'products.id')
											->leftjoin('menus', 'menus.id', '=', 'menu_products.menu_id')
											->where('products.status', 1)
											->where('products.category_id', $value->id)
											->where('menus.id', $request->menu_id)
											->orderBy('products.name', 'ASC')
											->get();
							// if parent product exits
							if (!$getParentProducts->isEmpty()) {
								$parentCategory[$value->name]['child'] = 'no';
								// loop through parent product
								foreach ($getParentProducts as $parentProduct)
								$parentCategory[$value->name]['products'][] = $parentProduct->name;

							} // parent product foreach ends
					} //child check ends

			}// foreach ends

			return response(array('parentCategory' => $parentCategory, 'childCategory' => $childCategory, 'childIndex' => $childIndex));
		} // ajax ends
	}

	public function edit($id, $menu_id){

		$menu = Menu::find($menu_id);
		$venue = Venue::find($id);
		$counter = 0;
		$products = Product::where('status', 1)->where('venue_id', $id)->orderBy('name', 'ASC')->get();
		$edited_id = $menu_id;

		$categories = Category::where('status', 1)
					->where('venue_id', $id)
					->where('category_id', 0)
					->orderBy('name', 'ASC')
					->get();

			// check if category has childs
		foreach ($categories as $key => $value) {

				// get its order
				$getCategoryOrder = CategoryMenu::where('menu_id', $menu_id)
									->where('category_id', $value->id)
									->first();
				if ($getCategoryOrder != null) {
					$value->order = $getCategoryOrder->category_order;
				} else {
					$value->order  = "";
				}					
				// get child categories
				$childCategories = Category::where('status', 1)
									->where('category_id', $value->id)
									->orderBy('name', 'ASC')
									->get();
				$childArray = [];
				// get childs for parent
				if (!$childCategories->isEmpty()) {
					$value->has_childs = 'yes';
					foreach ($childCategories as $child) {
							// get its order
							$getChildCategoryOrder = CategoryMenu::where('menu_id', $menu_id)
													->where('category_id', $child->id)
													->first();
							if ($getChildCategoryOrder != null) {
								$order = $getChildCategoryOrder->category_order;
							} else {
								$order  = "";
							}
						$childArray[] = $child->id.'/'.$child->name.'/'.$order;
					}
					$value->child = $childArray;
				} else {
					$value->has_childs = 'no';
				}

		} // endforeach
		
		return view('admin.menu_edit', compact('counter', 'id', 'venue', 'products', 'menu', 'edited_id', 'categories'));
	}

	public function update(Request $request, $id, $menu_id){

		$validatedData = $request->validate([
            'name' => 'required',
        ]);

        // check for duplicate menu name
        $menuName = Menu::where('name', $request->name)
				        ->where('venue_id', $id)
				        ->where('id', '!=', $menu_id)
				        ->first();

        if($menuName != null) {

        	return redirect(route('admin.venue.menu.edit', [$id, $menu_id]))->with('error', 'Menu name already exits, try a new name.');
        }
        
		$menu = Menu::find($menu_id);
		$menu->name = $request->name;
		
		if(isset($request->status)){
			$menu->status = 1;
		}else{
			$menu->status = 0;
		}
		
		if(isset($request->default)) {
			// remove default from other records
			$removeDefault =  DB::table('menus')->where('id' ,'!=', $menu_id)->update(['default' => 0]);
			$menu->default = 1;
		}else{
			$menu->default = 0;
		}
		
		
		if($request->file('image')){
			
			Storage::disk('public')->delete('menus/' . $menu->image);
			$request->file('image')->store('menus/', ['disk' => 'public']);
			$menu->image = $request->image->hashName();
		}
		
		if($menu->save()) {

			if(is_array($request->products)) {
				// delete this menu products
				$menuProduct = MenuProduct::where('menu_id', $menu_id)->delete();
				foreach($request->products as $product) {
					// get bar id
					$getBar = Product::where('id', $product)->pluck('bar_id')->toArray();
					$menuProdusts = new MenuProduct;
					$menuProdusts->menu_id = $menu_id;
					$menuProdusts->venue_id = $id;
					$menuProdusts->bar_id = $getBar[0];
					$menuProdusts->product_id = $product;
					$menuProdusts->save();
				}
			}

			// delete previous category order
			$deleteOrder = CategoryMenu::where('menu_id', $menu_id)->delete();

			if(is_array($request->parent_cat_order)) {
				foreach($request->parent_cat_order as $key => $value) {
					$catMenu = new CategoryMenu;
					$catMenu->category_id = $key;
					$catMenu->menu_id = $menu->id;
					$catMenu->category_order = $value;
					$catMenu->save();

					if (isset($request->child_cat_order[$key])) {
							foreach($request->child_cat_order[$key] as $index => $order_value) {
							$childCatMenu = new CategoryMenu;
							$childCatMenu->category_id = $index;
							$childCatMenu->menu_id = $menu->id;
							$childCatMenu->category_order = $order_value;
							$childCatMenu->save();
						}
					}
				}
			}

			// insert log
			$venueLog = new TrailLog;
			$venueLog->event = 'Menu';
			$venueLog->event_id = $menu->id;
			$venueLog->event_type = 'Menu Updated';
			$venueLog->event_message = Auth::user()->name.' updated menu '.$menu->name. ' information.';
			$venueLog->user_id = Auth::user()->id;
			$venueLog->user_name = Auth::user()->name;
			$venueLog->user_role = Auth::user()->role;
			$venueLog->venue_id = $id;
			$venueLog->save();

			return redirect(route('admin.venue.menus', $id))->with('success', 'Record has been updated.');
		} else {
			return redirect(route('admin.venue.menus', $id))->with('error', 'Record has not been updated.');
		}
		
	}
}
