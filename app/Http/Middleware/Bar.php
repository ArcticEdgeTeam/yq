<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;

class Bar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   function handle($request, Closure $next)
	{
		if (Auth::check() && Auth::user()->role == 'bar') {
			if(Auth::user()->status == 0){
				Auth::logout();
				return redirect(route('login'))->with('error', 'Account suspended.');
			}
			
			if(DB::table('manager_prepareas')->where('user_id', Auth::user()->id)->where('venue_id', Auth::user()->venue_id)->doesntExist()) {
				Auth::logout();
				return redirect(route('login'))->with('error', 'No bar assigned.');
			}
			
			return $next($request);
		}
		elseif (Auth::check() && Auth::user()->role == 'venue') {
			return redirect('/venue');
		}
		else {
			return redirect('/admin');
		}
	}
}
