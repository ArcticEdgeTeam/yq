<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;

class CurrentBarMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('current_bar_id') == null || !Session::has('current_bar_id')) {
            return redirect(route('bar.dashboard'));
        }
        return $next($request);
    }
}
