<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Venue
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   function handle($request, Closure $next)
	{
		if (Auth::check() && Auth::user()->role == 'venue') {
			if(Auth::user()->status == 0){
				Auth::logout();
				return redirect(route('login'))->with('error', 'Account suspended.');
			}
			return $next($request);
		}
		elseif (Auth::check() && Auth::user()->role == 'bar') {
			return redirect('/bar');
		}
		else {
			return redirect('/admin');
		}
	}
}
