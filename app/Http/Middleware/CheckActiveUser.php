<?php

namespace App\Http\Middleware;

use Closure;

class CheckActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (\Auth::user()->device_token != $request->device_token) {
            // destroy token
            $token = $request->user()->token();
            $token->revoke();
            
             return response()->json([
                'error' => true,
                'message' => 'Access denied, Please login again!',
                'data' => null,
            ], 200);
       }

       return $next($request);
    }
}
