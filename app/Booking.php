<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
 	public function tables()
    {
        return $this->belongsToMany('App\Table', 'booking_tables', 'booking_id', 'table_id');
    }
    
}
