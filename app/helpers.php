<?php
//This function required intervention image library
function SaveImageAllSizes($request, $dir){

	$lg_image = Image::make($request->file('image'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('image'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('image'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('image'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->image->hashName();

	$request->file('image')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}


function UpdateImageAllSizes($request, $dir, $old_image){
	//Delete old images
	Storage::disk('public')->delete($dir . $old_image);
	Storage::disk('public')->delete($dir . 'lg/' . $old_image);
	Storage::disk('public')->delete($dir . 'md/' . $old_image);
	Storage::disk('public')->delete($dir . 'sm/' . $old_image);
	Storage::disk('public')->delete($dir . 'xs/' . $old_image);

	$lg_image = Image::make($request->file('image'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('image'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('image'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('image'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->image->hashName();

	$request->file('image')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}

function SaveBannerAllSizes($request, $dir){

	$lg_image = Image::make($request->file('banner'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('banner'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('banner'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('banner'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->banner->hashName();

	$request->file('banner')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}

function UpdateBannerAllSizes($request, $dir, $old_image){
	//Delete old images
	Storage::disk('public')->delete($dir . $old_image);
	Storage::disk('public')->delete($dir . 'lg/' . $old_image);
	Storage::disk('public')->delete($dir . 'md/' . $old_image);
	Storage::disk('public')->delete($dir . 'sm/' . $old_image);
	Storage::disk('public')->delete($dir . 'xs/' . $old_image);

	$lg_image = Image::make($request->file('banner'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('banner'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('banner'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('banner'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->banner->hashName();

	$request->file('banner')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}


function SavePhotoAllSizes($request, $dir){

	$lg_image = Image::make($request->file('photo'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('photo'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('photo'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('photo'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->photo->hashName();

	$request->file('photo')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}


function UpdatePhotoAllSizes($request, $dir, $old_image){
	//Delete old images
	Storage::disk('public')->delete($dir . $old_image);
	Storage::disk('public')->delete($dir . 'lg/' . $old_image);
	Storage::disk('public')->delete($dir . 'md/' . $old_image);
	Storage::disk('public')->delete($dir . 'sm/' . $old_image);
	Storage::disk('public')->delete($dir . 'xs/'. $old_image);

	$lg_image = Image::make($request->file('photo'))->fit(400);
	$lg_image->response('png');

	$md_image = Image::make($request->file('photo'))->fit(200);
	$md_image->response('png');

	$sm_image = Image::make($request->file('photo'))->fit(100);
	$sm_image->response('png');

	$xs_image = Image::make($request->file('photo'))->fit(50);
	$xs_image->response('png');

	$image_name = $request->photo->hashName();

	$request->file('photo')->store($dir, ['disk' => 'public']);
	Storage::disk('public')->put($dir . 'lg/' . $image_name, $lg_image);
	Storage::disk('public')->put($dir . 'md/' . $image_name, $md_image);
	Storage::disk('public')->put($dir . 'sm/' . $image_name, $sm_image);
	Storage::disk('public')->put($dir . 'xs/' . $image_name, $xs_image);
}

function sendNotificationFCM($notification_id, $title, $message, $id,$type) {

    $accesstoken = env('FCM_KEY');
    $URL = 'https://fcm.googleapis.com/fcm/send';
    $post_data = '{
            "to" : "' . $notification_id . '",
            "data" : {
              "body" : "",
              "title" : "' . $title . '",
              "type" : "' . $type . '",
              "id" : "' . $id . '",
              "message" : "' . $message . '",
            },
            "notification" : {
                 "body" : "' . $message . '",
                 "title" : "' . $title . '",
                  "type" : "' . $type . '",
                 "id" : "' . $id . '",
                 "message" : "' . $message . '",
                "icon" : "new",
                "sound" : "default"
                },

          }';

    $crl = curl_init();
    $headr = array();
    $headr[] = 'Content-type: application/json';
    $headr[] = 'Authorization: ' . $accesstoken;
    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($crl, CURLOPT_URL, $URL);
    curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
    curl_setopt($crl, CURLOPT_POST, true);
    curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
    $rest = curl_exec($crl);
    if ($rest === false) {
        // throw new Exception('Curl error: ' . curl_error($crl));
        //print_r('Curl error: ' . curl_error($crl));
        $result_noti = 0;
    } else {
        $result_noti = 1;
    }
    return $result_noti;
}
