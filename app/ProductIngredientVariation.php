<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductIngredientVariation extends Model
{
    //
    protected $table ='product_ingredient_variations';
}
