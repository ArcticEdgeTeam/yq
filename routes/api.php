<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => 'cors'], function() {
	Route::post('signup', 'Api\ApiController@signUp');
	Route::post('signin', 'Api\ApiController@signIn');
	Route::post('email_verify', 'Api\ApiController@emailVerify');
	Route::post('code_verify', 'Api\ApiController@codeVerify');
  	Route::post('code_resend', 'Api\ApiController@emailVerify');
  	Route::post('update_password', 'Api\ApiController@updatePassword');

	//Route::middleware(['auth:api', 'checkActiveUser'])->group(function () {
	Route::group(['middleware' => 'cors'], function () {
		// Customer Routes
	    Route::get('page/{page_name}', 'Api\ApiController@page');
	    Route::post('update_profile', 'Api\ApiController@updateProfile');
	    Route::post('contact_us', 'Api\ApiController@contactUs');
	    Route::get('setting', 'Api\ApiController@setting');
	    Route::post('setting/update_tip', 'Api\ApiController@updateTip');
	    Route::post('setting/update_budget', 'Api\ApiController@updateBudget');
	    Route::get('setting/get_notification_status', 'Api\ApiController@getNotificationStatus');
	    Route::post('setting/update_notification_status', 'Api\ApiController@updateNotificationStatus');
	    Route::post('delete_account', 'Api\ApiController@deleteAccount');
	    Route::post('save_location', 'Api\ApiController@saveLocation');
	    Route::get('get_location', 'Api\ApiController@getLocation');
	    Route::get('delete_location', 'Api\ApiController@deleteLocation');
	    Route::post('save_favorite', 'Api\ApiController@saveFavorite');
	    Route::post('save_review', 'Api\ApiController@saveReview');
	   	Route::post('delete_review', 'Api\ApiController@deleteReview');
	    Route::get('edit_review', 'Api\ApiController@editReview');
	    Route::post('update_review', 'Api\ApiController@updateReview');
	    Route::get('get_user_review', 'Api\ApiController@getUserReview');
	    // api for home screen
	    Route::get('kurtoglu', 'Api\ApiController@kurtoglu');
	    Route::get('get_all_venue', 'Api\ApiController@getAllVenue');
	    Route::get('single_venue', 'Api\ApiController@singleVenue');
	    Route::get('get_venue_review', 'Api\ApiController@getVenueReview');
	    Route::get('get_venue_info', 'Api\ApiController@getVenueInfo');
	    Route::get('user_favorite_venue', 'Api\ApiController@userFavoriteVenue');
	    Route::get('connect', 'Api\ApiController@connect');
	    Route::post('user_connect', 'Api\ApiController@userConnect');
	    Route::post('user_connect_confirmation', 'Api\ApiController@userConnectConfirmation');
	    Route::post('logout', 'Api\ApiController@logout');
        //get customer apis
        Route::get('getAllMenueProducts', 'Api\CustomerController@getMenuProducts');
        //product details here
        Route::get('productdetails', 'Api\CustomerController@productDetails');
        Route::get('table', 'Api\CustomerController@getTables');
        Route::get('waiter/reviews', 'Api\CustomerController@waiteReviews');
        Route::get('waiter/tips', 'Api\CustomerController@waiterTips');
        Route::get('allmesages', 'Api\CustomerController@allMesages');
        Route::get('allchatmesages', 'Api\CustomerController@allChatMesagesForWaiter');
        Route::post('replymessage', 'Api\CustomerController@ReplyMessages');
        Route::get('allchatmesages/customer', 'Api\CustomerController@allChatMesagesForCustomer');
        Route::post('messages/save', 'Api\CustomerController@saveMessages');
        Route::get('venue/filter', 'Api\CustomerController@venueFilter');
        Route::post('search/venues', 'Api\CustomerController@SearchVenues');
        Route::get('waiter/profile', 'Api\CustomerController@WaiterProfile');
        Route::get('waiter/table', 'Api\CustomerController@waiterTable');
        Route::get('customer/venues/history', 'Api\CustomerController@customerVenuesHistory');
        Route::get('customer/transaction/history', 'Api\CustomerController@customerTransactionHistory');
        Route::post('customer/order', 'Api\CustomerController@orderSave');
        Route::get('send/notification', 'Api\CustomerController@notifyWaiter');
        Route::get('list/notification', 'Api\CustomerController@listAllNotification');
	}); // auth middleware ends
	// for productImages
    Route::get('user_image/{filename}', function ($filename) {

    	// original path for storage blog images
        $path 	  = storage_path() . '/users/' .$filename;
        // get the file path
        $file 	  = File::get($path);
        // get the type or extension
        $type 	  = File::mimeType($path);
        // make image of that path using response
        $response = Response::make($file, 200);
        // give response its type
        $response->header("Content-Type", $type);
        return $response;
    });

}); //cors middleware end
