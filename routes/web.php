<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
   Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');
   Artisan::call('view:clear');
   return "Cache Cleared!";

});


Route::get('/test', function () {
  return testing();
});

Route::get('/', function () {
    return view('auth.login');
});

Route::get('invalid/token', function () {
    return redirect(route('login'))->with('error', 'Invalid token');
});

Route::get('app/login', 'ApiController@appLogin');

Route::post('app/customer/login', 'ApiController@customerLogin');
Route::post('app/customer/registration', 'ApiController@customerRegister');
Route::post('app/customer/password/reset', 'ApiController@customerPasswordReset');
Route::get('app/customer/{id}/edit', 'ApiController@editCustomer');
Route::post('app/customer/{id}/update', 'ApiController@updateCustomer');
Route::get('app/customer/{id}/suspend', 'ApiController@suspendCustomer');
Route::get('app/customer/signupterms', 'ApiController@signUpTerms');
Route::get('app/customer/termsandconditions', 'ApiController@termsAndConditions');
Route::get('app/customer/aboutus', 'ApiController@aboutUs');
Route::post('app/customer/support/{id}/create', 'ApiController@supportCreate');

Route::post('app/customer/home', 'ApiController@customerHome');
Route::get('app/customer/makefavoritevenue', 'ApiController@makeFavoriteVenue');
Route::get('app/customer/venueproducts', 'ApiController@venueProducts');
Route::get('app/customer/productdetails', 'ApiController@productDetails');
Route::get('app/customer/venuedetails', 'ApiController@venueDetails');
Route::get('app/customer/venueinfo', 'ApiController@venueInfo');
Route::get('app/customer/venues', 'ApiController@venues');
Route::post('app/customer/makeorder', 'ApiController@makeOrder');
Route::post('app/customer/connectasguest', 'ApiController@connectAsGuest');
Route::post('app/customer/addtocart', 'ApiController@addToCart');
Route::get('app/customer/customercartproducts', 'ApiController@customerCartProducts');
Route::get('app/customer/allcartproducts', 'ApiController@allCartProducts');
Route::post('app/customer/checkoutcustomerproducts', 'ApiController@checkoutCustomerProducts');
Route::post('app/customer/checkoutallproducts', 'ApiController@checkoutAllProducts');
Route::get('app/customer/deletecartproduct', 'ApiController@deleteCartProduct');
Route::get('app/customer/updatecartproduct', 'ApiController@updateCartProduct');
Route::get('app/customer/deletecartproductvariable', 'ApiController@deleteCartProductVariable');
Route::get('app/customer/updatecartproductvariable', 'ApiController@updateCartProductVariable');
Route::post('app/customer/searchvenue', 'ApiController@searchVenue');
Route::post('app/customer/searchvenueproduct', 'ApiController@searchVenueProduct');
Route::get('app/customer/connecttovenue', 'ApiController@connectToVenue');
Route::get('app/customer/orderedproducts', 'ApiController@orderedProducts');
Route::post('app/customer/payorder', 'ApiController@payOrder');
Route::get('app/customer/customerbills', 'ApiController@customerBills');
Route::post('app/customer/addbooking', 'ApiController@addBooking');

Route::post('app/waiter/login', 'ApiController@waiterLogin');
Route::post('app/waiter/password/reset', 'ApiController@waiterPasswordReset');
Route::get('app/waiter/{id}/edit', 'ApiController@editWaiter');
Route::post('app/waiter/{id}/update', 'ApiController@updateWaiter');
Route::get('app/waiter/{id}/suspend', 'ApiController@suspendWaiter');
Route::get('app/waiter/aboutus', 'ApiController@aboutUs');
Route::post('app/waiter/support/{id}/create', 'ApiController@supportCreate');

Route::get('app/order/create', 'ApiController@orderCreate')->name('order.create');
Route::post('app/order/create', 'ApiController@orderSave')->name('order.save');

//routes for payfast
Route::get('app/payfast', 'payfastController@index')->name('payfast');
Route::post('app/payfast/confirm', 'payfastController@confirmPayment')->name('payfast.confirm');
Route::get('app/payfast/return', 'payfastController@success')->name('payfast.return');
Route::get('app/payfast/cancel', 'payfastController@cancel')->name('payfast.cancel');
Route::post('app/payfast/notify', 'payfastController@notify')->name('payfast.notify');

Auth::routes(['register' => false]);
//Route::get('/bar', 'HomeController@index')->name('home');


//Ajax routes
Route::get('ajax/loadallcustomers', 'AjaxController@loadAllCustomers')->name('ajax.loadallcustomers');
Route::get('ajax/loadallvenues', 'AjaxController@loadAllVenues')->name('ajax.loadallvenues');
Route::get('ajax/loadvenuecustomers', 'AjaxController@loadVenueCustomers')->name('ajax.loadvenuecustomers');
Route::get('ajax/loadvenuewaiters', 'AjaxController@loadVenueWaiters')->name('ajax.loadvenuewaiters');

//get user for push notification
Route::get('ajax/get-users', 'AjaxController@getUsers')->name('ajax.push-notification.getuser');
// get booking dates
Route::get('ajax/get-bookings', 'AjaxController@getBookings')->name('ajax.get-bookings');
// get table informations for booking
Route::get('ajax/get-table-information', 'AjaxController@getTableInfomations')->name('ajax.get-table-information');

Route::get('ajax/get-booking-information', 'AjaxController@getBookingInformation')->name('ajax.get-booking-information');
// check staf user for venue
Route::get('ajax/get-staff-information', 'AjaxController@getStaffInformation')->name('ajax.get-staff-information');
Route::get('ajax/get-edit-staff-information', 'AjaxController@getEditStaffInformation')->name('ajax.get-edit-staff-information');


Route::get('ajax/loadvenuecustomersforvenue', 'AjaxController@loadVenueCustomersForVenue')->name('ajax.loadvenuecustomersforvenue');
Route::get('ajax/loadvenuewaitersforvenue', 'AjaxController@loadVenueWaitersForVenue')->name('ajax.loadvenuewaitersforvenue');

// check table ID
Route::get('ajax/get-table-id', 'AjaxController@getTableID')->name('ajax.get-table-id');
// check prep ID
Route::get('ajax/get-prep-id', 'AjaxController@getPrepID')->name('ajax.get-prep-id');

// check old password
Route::get('ajax/get-user-password', 'AjaxController@getUserPassword')->name('ajax.get-user-password');
// check ingredient information
Route::get('ajax/get-ingredient-info', 'AjaxController@getIngredientInfo')->name('ajax.get-ingredient-info');
Route::get('ajax/get-unit-info', 'AjaxController@getUnitInfo')->name('ajax.get-unit-info');

// check category Information
Route::get('ajax/get-category-info', 'AjaxController@getCategoryInfo')->name('ajax.get-category-info');
// check menu Information
Route::get('ajax/get-menu-info', 'AjaxController@getMenuInfo')->name('ajax.get-menu-info');

Route::prefix('admin')->group(function() {

	Route::get('/check/email/{email?}', 'AdminController@checkEmail')->name('check.email');
	Route::get('/check/company/id/{company_id?}', 'AdminController@checkCompanyID')->name('check.company.id');

	Route::get('/', 'AdminController@index')->name('admin.home');
	Route::get('/home', 'AdminController@index')->name('admin.home');
	Route::post('/home', 'AdminController@filter')->name('admin.home.filter');

	// Manage Admins
	Route::get('/manage-admin', 'AdminController@manageAdmins')->name('admin.manage.admins');
	Route::get('/create-admin', 'AdminController@createAdmin')->name('admin.create.new-admin');
	Route::post('/create-admin', 'AdminController@saveAdmin')->name('admin.create.new-admin');
	Route::get('/edit-admin/{id}', 'AdminController@editAdmin')->name('admin.edit.admin');
	Route::post('/edit-admin/{id}', 'AdminController@updateAdmin')->name('admin.update.admin');
	// update user password
	Route::post('user-password-update', 'AdminController@userPasswordUpdate')->name('admin.user-password-update');

	Route::get('/setting', 'AdminController@setting')->name('admin.setting');
	Route::post('/setting/{id}/update', 'AdminController@update')->name('admin.setting.update');

	Route::get('/cms', 'PageController@index')->name('admin.cms');
	Route::post('/cms/{id}/update', 'PageController@update')->name('admin.cms.update');

	Route::get('/dietaries', 'DietaryController@index')->name('admin.dietaries');
	Route::get('/dietary/create', 'DietaryController@create')->name('admin.dietary.create');
	Route::post('/dietary/create', 'DietaryController@save')->name('admin.dietary.save');
	Route::get('/dietary/{id}/edit', 'DietaryController@edit')->name('admin.dietary.edit');
	Route::post('/dietary/{id}/update', 'DietaryController@update')->name('admin.dietary.update');


	Route::get('/cuisines', 'CuisineController@index')->name('admin.cuisines');
	Route::get('/cuisine/create', 'CuisineController@create')->name('admin.cuisine.create');
	Route::post('/cuisine/create', 'CuisineController@save')->name('admin.cuisine.save');
	Route::get('/cuisine/{id}/edit', 'CuisineController@edit')->name('admin.cuisine.edit');
	Route::post('/cuisine/{id}/update', 'CuisineController@update')->name('admin.cuisine.update');

	Route::get('/venue/{id}/ingredients', 'IngredientController@index')->name('admin.venue.ingredients');
	Route::get('/venue/{id}/ingredient/create', 'IngredientController@create')->name('admin.venue.ingredient.create');
	Route::post('/venue/{id}/ingredient/create', 'IngredientController@save')->name('admin.venue.ingredient.save');
	Route::get('/venue/{id}/ingredient/{ingredient_id}/edit', 'IngredientController@edit')->name('admin.venue.ingredient.edit');
	Route::post('/venue/{id}/ingredient/{ingredient_id}/update', 'IngredientController@update')->name('admin.venue.ingredient.update');

	Route::get('/venue/{id}/units', 'UnitController@index')->name('admin.venue.units');
	Route::get('/venue/{id}/unit/create', 'UnitController@create')->name('admin.venue.unit.create');
	Route::post('/venue/{id}/unit/create', 'UnitController@save')->name('admin.venue.unit.save');
	Route::get('/venue/{id}/unit/{ingredient_id}/edit', 'UnitController@edit')->name('admin.venue.unit.edit');
	Route::post('/venue/{id}/unit/{ingredient_id}/update', 'UnitController@update')->name('admin.venue.unit.update');

	Route::get('/atmospheres', 'AtmosphereController@index')->name('admin.atmospheres');
	Route::get('/atmosphere/create', 'AtmosphereController@create')->name('admin.atmosphere.create');
	Route::post('/atmosphere/create', 'AtmosphereController@save')->name('admin.atmosphere.save');
	Route::get('/atmosphere/{id}/edit', 'AtmosphereController@edit')->name('admin.atmosphere.edit');
	Route::post('/atmosphere/{id}/update', 'AtmosphereController@update')->name('admin.atmosphere.update');

	Route::get('/customers', 'CustomerController@index')->name('admin.customers');
	Route::get('/customer/{id}/overview', 'CustomerController@overview')->name('admin.customer.overview');
	Route::get('/customer/{id}/orders', 'CustomerController@orders')->name('admin.customer.orders');
	Route::get('/customer/{id}/reviews', 'CustomerController@reviews')->name('admin.customer.reviews');
	Route::get('/customer/{id}/edit', 'CustomerController@edit')->name('admin.customer.edit');
	Route::post('/customer/{id}/update', 'CustomerController@update')->name('admin.customer.update');

	Route::get('/venues', 'VenueForAdminController@index')->name('admin.venues');
	Route::get('/venue/create', 'VenueForAdminController@create')->name('admin.venue.create');
	Route::post('/venue/create', 'VenueForAdminController@save')->name('admin.venue.save');
	Route::get('/venue/{id}/edit', 'VenueForAdminController@edit')->name('admin.venue.edit');
	Route::post('/venue/{id}/update', 'VenueForAdminController@update')->name('admin.venue.update');
	// ajax call for storing venue status
	Route::get('/venue/status-update', 'VenueForAdminController@venueStatusUpdate')->name('admin.venue-status.update');
	// get product ID
	Route::get('/venue/get-product', 'VenueForAdminController@getProductID')->name('admin.get-product');
	// Public holidays
	Route::get('public-holiday', 'VenueForAdminController@publicHoliday')->name('admin.holidays');
	Route::post('public-holiday', 'VenueForAdminController@savePublicHoliday')->name('admin.holidays');

	Route::get('/venue/{id}/overview', 'VenueForAdminController@overview')->name('admin.venue.overview');
	Route::get('/venue/{id}/information', 'VenueForAdminController@information')->name('admin.venue.information');
	Route::get('/venue/{id}/tradinghour/{tradinghour_id}/delete', 'TradingHourController@delete')->name('admin.venue.tradinghour.delete');
	Route::get('/venue/{id}/setting', 'VenueForAdminController@setting')->name('admin.venue.setting');
	Route::post('/venue/{id}/setting', 'VenueForAdminController@updateSetting')->name('admin.venue.settings');

	Route::get('/venue/{id}/table-booking-overview', 'VenueForAdminController@tableBookingView')->name('admin.table-booking-view');


	Route::get('/venue/{id}/products', 'ProductController@index')->name('admin.venue.products');
	Route::get('/venue/{id}/product/create', 'ProductController@create')->name('admin.venue.product.create');
	Route::post('/venue/{id}/product/create', 'ProductController@save')->name('admin.venue.product.save');
	Route::get('/venue/{id}/product/{product_id}/edit', 'ProductController@edit')->name('admin.venue.product.edit');
	Route::post('/venue/{id}/product/{product_id}/update', 'ProductController@update')->name('admin.venue.product.update');
	Route::get('/venue/{id}/productingredient/{ingredietn_id}/delete', 'ProductIngredientController@delete')->name('admin.venue.productingredient.delete');
	Route::get('/venue/{id}/productvariant/{variant_id}/delete', 'ProductVariantController@delete')->name('admin.venue.productvariant.delete');
	Route::get('/venue/{id}/productextra/{extra_id}/delete', 'ExtraProductController@delete')->name('admin.venue.productextra.delete');
	Route::post('/venue/{id}/extra/{product_id}/update', 'ExtraProductController@update')->name('admin.venue.extra.update');
	Route::post('/venue/{id}/variant/{product_id}/update', 'ProductVariantController@update')->name('admin.venue.variant.update');

	Route::get('/venue/{id}/categories', 'CategoryController@index')->name('admin.venue.categories');
	Route::get('/venue/{id}/category/create', 'CategoryController@create')->name('admin.venue.category.create');
	Route::post('/venue/{id}/category/create', 'CategoryController@save')->name('admin.venue.category.save');
	Route::get('/venue/{id}/category/{category_id}/edit', 'CategoryController@edit')->name('admin.venue.category.edit');
	Route::post('/venue/{id}/category/{category_id}/update', 'CategoryController@update')->name('admin.venue.category.update');

	// Menues Route
	Route::get('/venue/{id}/menus', 'AdminMenuController@index')->name('admin.venue.menus');
	Route::get('/venue/{id}/menu/create', 'AdminMenuController@create')->name('admin.venue.menu.create');
	Route::post('/venue/{id}/menu/create', 'AdminMenuController@save')->name('admin.venue.menu.save');
	Route::get('/venue/{id}/menu/{menu_id}/edit', 'AdminMenuController@edit')->name('admin.venue.menu.edit');
	Route::post('/venue/{id}/menu/{menu_id}/update', 'AdminMenuController@update')->name('admin.venue.menu.update');
	// change menu default status
	Route::get('/venue/{id}/menu/{menu_id}/menu-default/{default}', 'AdminMenuController@menuDefault')->name('admin.venue.menus-default');
	Route::get('/venue/{id}/menu/{menu_id}/menu-status/{status}', 'AdminMenuController@menuStatus')->name('admin.venue.menus-status');
	Route::post('/venue/menu/products', 'AdminMenuController@getMenuProducts')->name('admin.menu.products');

	Route::get('/venue/{id}/tables', 'TableController@index')->name('admin.venue.tables');
	Route::get('/venue/{id}/table/create', 'TableController@create')->name('admin.venue.table.create');
	Route::post('/venue/{id}/table/create', 'TableController@save')->name('admin.venue.table.save');
	Route::get('/venue/{id}/table/{table_id}/edit', 'TableController@edit')->name('admin.venue.table.edit');
	Route::post('/venue/{id}/table/{table_id}/update', 'TableController@update')->name('admin.venue.table.update');


	Route::get('/venue/{id}/bookings', 'BookingController@index')->name('admin.venue.bookings');
	Route::get('/venue/{id}/bookings/filter', 'BookingController@filter')->name('admin.venue.bookings.filter');
	Route::get('/venue/{id}/booking/create', 'BookingController@create')->name('admin.venue.booking.create');
	Route::post('/venue/{id}/booking/create', 'BookingController@save')->name('admin.venue.booking.save');
	Route::get('/venue/{id}/booking/{booking_id}/edit', 'BookingController@edit')->name('admin.venue.booking.edit');
	Route::post('/venue/{id}/booking/{booking_id}/update', 'BookingController@update')->name('admin.venue.booking.update');
	Route::get('/venue/{id}/booking/{booking_id}/cancel', 'BookingController@cancel')->name('admin.venue.booking.cancel');
	Route::get('/venue/{id}/booking/{booking_id}/accept', 'BookingController@accept')->name('admin.venue.booking.accept');

	// table vailablity checker
	Route::get('/venue/{id}/table-availability-checker', 'BookingController@tableAvailabilityChecker')->name('admin.venue.table-availability-checker');

	Route::get('/venue/{id}/staff', 'StaffController@index')->name('admin.venue.staff');
	Route::get('/venue/{id}/staff/create', 'StaffController@create')->name('admin.venue.staff.create');
	Route::post('/venue/{id}/staff/create', 'StaffController@save')->name('admin.venue.staff.save');
	Route::get('/venue/{id}/staff/{user_id}/edit', 'StaffController@edit')->name('admin.venue.staff.edit');
	Route::post('/venue/{id}/staff/{user_id}/update', 'StaffController@update')->name('admin.venue.staff.update');
	Route::get('/venue/{id}/staff/{user_id}/wallet', 'StaffController@wallet')->name('admin.venue.staff.wallet');
	Route::post('/venue/{id}/staff/{user_id}/payament/create', 'StaffController@createPayment')->name('admin.venue.staff.payment.create');

	Route::get('/venue/{id}/bars', 'BarForAdminController@index')->name('admin.venue.bars');
	Route::get('/venue/{id}/bar/create', 'BarForAdminController@create')->name('admin.venue.bar.create');
	Route::post('/venue/{id}/bar/create', 'BarForAdminController@save')->name('admin.venue.bar.save');
	Route::get('/venue/{id}/bar/{user_id}/edit', 'BarForAdminController@edit')->name('admin.venue.bar.edit');
	Route::post('/venue/{id}/bar/{user_id}/update', 'BarForAdminController@update')->name('admin.venue.bar.update');

	Route::get('/venue/{id}/reviews', 'ReviewController@index')->name('admin.venue.reviews');
	Route::get('/venue/{id}/reviews/filter', 'ReviewController@filter')->name('admin.venue.reviews.filter');
	Route::get('/venue/{id}/review/{review_id}/edit', 'ReviewController@edit')->name('admin.venue.review.edit');
	Route::post('/venue/{id}/review/{review_id}/edit', 'ReviewController@update')->name('admin.venue.review.update');

	Route::get('/venue/{id}/orders', 'OrderController@index')->name('admin.venue.orders');
	Route::get('/venue/{id}/order-list', 'OrderController@orderList')->name('admin.venue.order.list');
	Route::get('/venue/{id}/order-list/filter', 'OrderController@orderListFilter')->name('admin.venue.order.list.filter');
	Route::get('/venue/{id}/order/{order_id}/edit', 'OrderController@edit')->name('admin.venue.order.edit');
	Route::get('/venue/{id}/order/{order_id}/quickview', 'OrderController@quickview')->name('admin.venue.order.quickview');
	Route::get('/order/status/update', 'OrderController@updateOrderStatus')->name('admin.order.status.update');

	Route::get('/venue/{id}/cashups', 'CashupForAdminController@index')->name('admin.venue.cashups');
	Route::get('/venue/{id}/cashups/filter', 'CashupForAdminController@filter')->name('admin.venue.cashups.filter');
	// cashups pdf
	Route::get('/venue/{id}/cashups/pdf', 'CashupForAdminController@cashupsPDF')->name('admin.venue.cashups.pdf');
	// cashups Email
	Route::post('/venue/{id}/cashups/email', 'CashupForAdminController@cashupsEmail')->name('admin.venue.cashups.email');
	// cashups Excel
	Route::get('/venue/{id}/cashups/excel', 'CashupForAdminController@cashupsExcel')->name('admin.venue.cashups.excel');

	Route::get('/facilities', 'FacilityController@index')->name('admin.facilities');
	Route::get('/facility/create', 'FacilityController@create')->name('admin.facility.create');
	Route::post('/facility/create', 'FacilityController@save')->name('admin.facility.save');
	Route::get('/facility/{id}/edit', 'FacilityController@edit')->name('admin.facility.edit');
	Route::post('/facility/{id}/update', 'FacilityController@update')->name('admin.facility.update');


	Route::get('/push-notifications', 'PushNotificationController@index')->name('admin.push-notifications');
	Route::get('/push-notification/create', 'PushNotificationController@create')->name('admin.push-notification.create');
	Route::post('/push-notification/create', 'PushNotificationController@save')->name('admin.push-notification.save');
	Route::get('/push-notification/{id}/edit', 'PushNotificationController@edit')->name('admin.push-notification.edit');
	Route::get('/push-notification/{id}/cancel', 'PushNotificationController@cancel')->name('admin.push-notification.cancel');

	Route::get('/supports', 'SupportController@index')->name('admin.supports');
	Route::get('/supports/{id}/edit', 'SupportController@edit')->name('admin.support.edit');

	Route::get('/refunds', 'RefundController@index')->name('admin.refunds');
	Route::get('/refund/{id}/edit', 'RefundController@edit')->name('admin.refund.edit');
	Route::post('/refund/{id}/approve', 'RefundController@approve')->name('admin.refund.approve');
	Route::post('/refund/{id}/cancel', 'RefundController@cancel')->name('admin.refund.cancel');

	Route::get('/reports', 'ReportController@index')->name('admin.reports');
	Route::post('/reports/export', 'ReportController@export')->name('admin.report.export');

	Route::get('/transactions', 'TransactionController@index')->name('admin.transactions');
	Route::get('/transactions/filter', 'TransactionController@filter')->name('admin.transaction.filter');

	Route::get('/reservedfunds', 'ReservedFundController@index')->name('admin.reservedfunds');
	Route::get('/reservedfund/{id}/view', 'ReservedFundController@view')->name('admin.reservedfund.view');

	Route::get('/reservedpayments', 'ReservedPaymentController@index')->name('admin.reservedpayments');
	Route::get('/reservedpayments/{id}/view', 'ReservedPaymentController@view')->name('admin.reservedpayment.view');
	Route::get('/reservedpayments/{id}/credittovenue', 'ReservedPaymentController@creditToVenue')->name('admin.reservedpayment.credittovenue');

	// trail Logs
	Route::get('/trail_logs', 'VenueForAdminController@trailLog')->name('admin.trail_logs');
	// Featured
	Route::get('/featured', 'VenueForAdminController@featured')->name('admin.featured');
	Route::post('/featured', 'VenueForAdminController@featuredSave')->name('admin.featured-save');

});


Route::prefix('venue')->group(function() {
	Route::get('/', 'VenueController@index')->name('venue.home');
	Route::get('/home', 'VenueController@index')->name('venue.home');
	Route::get('/check/email/{email?}', 'VenueController@checkEmail')->name('venue.check.email');

	// manage admin

	// Manage Admins

	Route::get('/manage-admin', 'VenueController@manageAdmins')->name('venue.manage.admins');
	Route::get('/create-admin', 'VenueController@createAdmin')->name('venue.create.new-admin');
	Route::post('/create-admin', 'VenueController@saveAdmin')->name('venue.create.new-admin');
	Route::get('/edit-admin/{id}', 'VenueController@editAdmin')->name('venue.edit.admin');
	Route::post('/edit-admin/{id}', 'VenueController@updateAdmin')->name('venue.update.admin');

	Route::get('/setting', 'VenueController@setting')->name('venue.setting');
	Route::post('/setting/{id}/update', 'VenueController@update')->name('venue.setting.update');

	Route::get('/categories', 'CategoryForVenueController@index')->name('venue.categories');
	Route::get('/category/create', 'CategoryForVenueController@create')->name('venue.category.create');
	Route::post('/category/create', 'CategoryForVenueController@save')->name('category.save');
	Route::get('/category/{category_id}/edit', 'CategoryForVenueController@edit')->name('venue.category.edit');
	Route::post('/category/{category_id}/update', 'CategoryForVenueController@update')->name('venue.category.update');

	// Menues Route
	Route::get('/menus', 'VenueMenuController@index')->name('venue.menus');
	Route::get('menu/create', 'VenueMenuController@create')->name('venue.menu.create');
	Route::post('menu/create', 'VenueMenuController@save')->name('venue.menu.save');
	Route::get('menu/{menu_id}/edit', 'VenueMenuController@edit')->name('venue.menu.edit');
	Route::post('menu/{menu_id}/update', 'VenueMenuController@update')->name('venue.menu.update');
	// change menu default status
	Route::get('menu/{menu_id}/menu-default/{default}', 'VenueMenuController@menuDefault')->name('venue.menus-default');
	Route::get('menu/{menu_id}/menu-status/{status}', 'VenueMenuController@menuStatus')->name('venue.menus-status');
	Route::post('menu/products', 'VenueMenuController@getMenuProducts')->name('venue.menu.products');

	// ajax call for storing venue status
	Route::get('status-update', 'VenueController@venueStatusUpdate')->name('venue.venue-status.update');


	Route::get('/ingredients', 'IngredientForVenueController@index')->name('venue.ingredients');
	Route::get('/ingredient/create', 'IngredientForVenueController@create')->name('venue.ingredient.create');
	Route::post('/ingredient/create', 'IngredientForVenueController@save')->name('venue.ingredient.save');
	Route::get('/ingredient/{ingredient_id}/edit', 'IngredientForVenueController@edit')->name('venue.ingredient.edit');
	Route::post('/ingredient/{ingredient_id}/update', 'IngredientForVenueController@update')->name('venue.ingredient.update');

	Route::get('/units', 'UnitForVenueController@index')->name('venue.units');
	Route::get('/unit/create', 'UnitForVenueController@create')->name('venue.unit.create');
	Route::post('/unit/create', 'UnitForVenueController@save')->name('venue.unit.save');
	Route::get('/unit/{unit_id}/edit', 'UnitForVenueController@edit')->name('venue.unit.edit');
	Route::post('/unit/{unit_id}/update', 'UnitForVenueController@update')->name('venue.unit.update');

	Route::get('/supports', 'SupportController@indexForVenue')->name('venue.supports');
	Route::get('/supports/{id}/edit', 'SupportController@editForVenue')->name('venue.support.edit');
	Route::get('/supports/create', 'SupportController@createForVenue')->name('venue.support.create');
	Route::post('/supports/create', 'SupportController@saveForVenue')->name('venue.support.save');

	Route::get('/information', 'VenueForVenueController@information')->name('venue.information');
	Route::post('/information/update', 'VenueForVenueController@informationUpdate')->name('venue.information.update');

	Route::get('tradinghour/{tradinghour_id}/delete', 'TradingHourForVenueController@delete')->name('venue.tradinghour.delete');

	Route::get('/products', 'ProductForVenueController@index')->name('venue.products');
	Route::get('/product/create', 'ProductForVenueController@create')->name('venue.product.create');
	Route::post('/product/create', 'ProductForVenueController@save')->name('venue.product.save');
	Route::get('/product/{id}/edit', 'ProductForVenueController@edit')->name('venue.product.edit');
	Route::post('/product/{id}/update', 'ProductForVenueController@update')->name('venue.product.update');
	Route::get('/product/ingredient/{ingredient_id}/delete', 'ProductForVenueController@deleteProductIngredient')->name('venue.productingredient.delete');
	Route::get('/product/productvariant/{variant_id}/delete', 'ProductForVenueController@deleteProductVariant')->name('venue.productvariant.delete');
	Route::get('/product/productextra/{extra_id}/delete', 'ProductForVenueController@deleteProductExtra')->name('venue.productextra.delete');
	Route::post('/product/extra/{product_id}/update', 'ProductForVenueController@updateExtra')->name('venue.extra.update');
	Route::post('/product/variant/{product_id}/update', 'ProductForVenueController@updateVariant')->name('venue.variant.update');

	// get product ID
	Route::get('/get-product', 'ProductForVenueController@getProductID')->name('venue.get-product');

	Route::get('/tables', 'TableForVenueController@index')->name('venue.tables');
	Route::get('/table/create', 'TableForVenueController@create')->name('venue.table.create');
	Route::post('/table/create', 'TableForVenueController@save')->name('venue.table.save');
	Route::get('/table/{id}/edit', 'TableForVenueController@edit')->name('venue.table.edit');
	Route::post('/table/{id}/update', 'TableForVenueController@update')->name('venue.table.update');


	Route::get('/overview', 'VenueForVenueController@overview')->name('venue.overview');
	Route::get('/bookings', 'VenueForVenueController@bookings')->name('venue.bookings');
	Route::get('/bookings/filter', 'VenueForVenueController@bookingsFilter')->name('venue.bookings.filter');
	Route::get('/booking/create', 'VenueForVenueController@createBooking')->name('venue.booking.create');
	Route::post('/booking/create', 'VenueForVenueController@saveBooking')->name('venue.booking.save');
	Route::get('/booking/{id}/edit', 'VenueForVenueController@editBooking')->name('venue.booking.edit');
	Route::post('/booking/{id}/update', 'VenueForVenueController@updateBooking')->name('venue.booking.update');
	Route::get('/booking/{id}/cancel', 'VenueForVenueController@cancelBooking')->name('venue.booking.cancel');
	Route::get('/booking/{id}/accept', 'VenueForVenueController@acceptBooking')->name('venue.booking.accept');

	// calender View
	Route::get('table-booking-overview', 'VenueController@tableBookingView')->name('venue.table-booking-view');

	// table vailablity checker
	Route::get('/table-availability-checker', 'VenueForVenueController@tableAvailabilityChecker')->name('venue.table-availability-checker');

	Route::get('/cashups', 'CashupForVenueController@index')->name('venue.cashups');
	Route::get('/cashups/filter', 'CashupForVenueController@filter')->name('venue.cashups.filter');
	// cashups pdf
	Route::get('/cashups/pdf', 'CashupForVenueController@cashupsPDF')->name('venue.cashups.pdf');
	// cashups Email
	Route::post('/cashups/email', 'CashupForVenueController@cashupsEmail')->name('venue.cashups.email');
	// cashups Excel
	Route::get('/cashups/excel', 'CashupForVenueController@cashupsExcel')->name('venue.cashups.excel');


	Route::get('/reviews', 'VenueForVenueController@reviews')->name('venue.reviews');
	Route::get('/reviews/filter', 'VenueForVenueController@reviewsFilter')->name('venue.reviews.filter');

	Route::get('/orders', 'OrderForVenueController@index')->name('venue.orders');
	Route::get('/orderlist', 'OrderForVenueController@orderList')->name('venue.order.list');
	Route::get('/orderlist/filter', 'OrderForVenueController@orderListFilter')->name('venue.order.list.filter');
	Route::get('/order/{id}/edit', 'OrderForVenueController@edit')->name('venue.order.edit');
	Route::get('/order/{order_id}/quickview', 'OrderForVenueController@quickview')->name('venue.order.quickview');
	Route::get('/order/status/update', 'OrderForVenueController@updateOrderStatus')->name('venue.order.status.update');
	Route::get('/order/{order_id}/popview', 'OrderForVenueController@popview')->name('venue.order.popview');

	Route::get('/staff', 'StaffForVenueController@index')->name('venue.staff');
	Route::get('/staff/create', 'StaffForVenueController@create')->name('venue.staff.create');
	Route::post('/staff/create', 'StaffForVenueController@save')->name('venue.staff.save');
	Route::get('/staff/{id}/edit', 'StaffForVenueController@edit')->name('venue.staff.edit');
	Route::post('/staff/{id}/update', 'StaffForVenueController@update')->name('venue.staff.update');

	Route::get('/bars', 'BarForVenueController@index')->name('venue.bars');
	Route::get('/bar/create', 'BarForVenueController@create')->name('venue.bar.create');
	Route::post('/bar/create', 'BarForVenueController@save')->name('venue.bar.save');
	Route::get('/bar/{id}/edit', 'BarForVenueController@edit')->name('venue.bar.edit');
	Route::post('/bar/{id}/update', 'BarForVenueController@update')->name('venue.bar.update');

	Route::get('/customers', 'CustomerForVenueController@index')->name('venue.customers');
	Route::get('/customer/{id}/overview', 'CustomerForVenueController@overview')->name('venue.customer.overview');
	Route::get('/customer/{id}/orders', 'CustomerForVenueController@orders')->name('venue.customer.orders');
	Route::get('/customer/{id}/reviews', 'CustomerForVenueController@reviews')->name('venue.customer.reviews');
	Route::get('/customer/{id}/edit', 'CustomerForVenueController@edit')->name('venue.customer.edit');
	Route::post('/customer/{id}/update', 'CustomerForVenueController@update')->name('venue.customer.update');

	Route::get('/settings', 'VenueForVenueController@settings')->name('venue.settings');
	Route::post('/settings/update', 'VenueForVenueController@updateSettings')->name('venue.settings.update');

	Route::get('/reports', 'ReportForVenueController@index')->name('venue.reports');
	Route::post('/reports/export', 'ReportForVenueController@export')->name('venue.report.export');

	Route::get('/transactions', 'TransactionForVenueController@index')->name('venue.transactions');
	Route::get('/transactions.filter', 'TransactionForVenueController@filter')->name('venue.transactions.filter');

	Route::get('/push-notifications', 'PushNotificationForVenueController@index')->name('venue.push-notifications');
	Route::get('/push-notification/create', 'PushNotificationForVenueController@create')->name('venue.push-notification.create');
	Route::post('/push-notification/create', 'PushNotificationForVenueController@save')->name('venue.push-notification.save');
	Route::get('/push-notification/{id}/edit', 'PushNotificationForVenueController@edit')->name('venue.push-notification.edit');
	Route::get('/push-notification/{id}/cancel', 'PushNotificationForVenueController@cancel')->name('venue.push-notification.cancel');

	// trail Logs
	Route::get('/trail_logs', 'VenueForVenueController@trailLog')->name('venue.trail_logs');
	Route::get('/featured', 'VenueForVenueController@featured')->name('venue.featured');

});

Route::prefix('bar')->group(function() {
	Route::get('/', 'BarController@dashboard')->name('bar.dashboard');
	Route::get('/home', 'BarController@index')->name('bar.home');

	Route::group(['middleware'  =>  'currentBar'], function() {

		Route::get('/orders', 'OrderForBarController@index')->name('bar.orders');
		Route::get('/order/{id}/edit', 'OrderForBarController@edit')->name('bar.order.edit');
		Route::get('/orderlist', 'OrderForBarController@orderList')->name('bar.order.list');
		Route::get('/orderlist/filter', 'OrderForBarController@orderListFilter')->name('bar.order.list.filter');
		Route::get('/order/{order_id}/quickview', 'OrderForBarController@quickview')->name('bar.order.quickview');
		Route::get('/order/status/update', 'OrderForBarController@updateOrderStatus')->name('bar.order.status.update');
		Route::get('/order/{order_id}/popview', 'OrderForBarController@popview')->name('bar.order.popview');

		Route::get('/cashups', 'CashupForBarController@index')->name('bar.cashups');
		Route::get('/cashups/filter', 'CashupForBarController@filter')->name('bar.cashups.filter');

		Route::get('/tables', 'TableForBarController@index')->name('bar.tables');
		Route::get('/table/create', 'TableForBarController@create')->name('bar.table.create');
		Route::post('/table/create', 'TableForBarController@save')->name('bar.table.save');
		Route::get('/table/{id}/edt', 'TableForBarController@edit')->name('bar.table.edit');
		Route::post('/table/{id}/update', 'TableForBarController@update')->name('bar.table.update');

		Route::get('/setting', 'BarController@setting')->name('bar.setting');
		Route::post('/setting/{id}/update', 'BarController@update')->name('bar.setting.update');
	});

});

// checkout page render to android
Route::get('checkout/amount', 'payfastController@checkOutAmount')->name('checkout');
Route::get('checkout', 'payfastController@test')->name('checkout');


